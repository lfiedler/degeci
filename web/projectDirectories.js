const downloadPath = __dirname + "/public/downloads";
const examplePath = __dirname + "/public/examples";
const javaDir = __dirname + '/../DeGeCI'
const javaDirTax = __dirname + '/../DeGeCITax'
module.exports = {
    examplePath: examplePath,
    downloadPath : downloadPath,
    annotationPath : downloadPath + "/annotations",
    // javaDir: javaDir,
    javaDir : javaDirTax,
    pythonScript: "Draw_Annotation.py",
    externalDomain: "degeci2.informatik.uni-leipzig.de",
    domain: "http://localhost:3000"
}