var express = require('express');
let bodyParser = require('body-parser');
let multer = require('multer');
let upload = multer({dest:'./public/files/fasta'});
let result_controller = require('../controller/resultController');
const { body,validationResult , ValidationChain, oneOf, check} = require("express-validator");
const fsSync = require('fs');
let fs = require('promise-fs');
const JSZIP = require('jszip');
const AdmZip = require('adm-zip')
let util = require('util');
const pathes = require('../projectDirectories')
let zip = require('express-zip')
const { v4: uuidv4 } = require('uuid');
var router = express.Router();
let annotation_form_controller = require('../controller/annotationFormController');
let clientCount = -1;
const exampleSequence = fsSync.readFileSync(pathes.examplePath+"/NC_022828.fna",'utf8');
let nucleotideRegex = /^[ACGTNKMBVSWDYRH]+$/;
let fastaHeaderRegex = /^>.*/;
let clients = [];
let handledClients = [];
const oneDayMillisec = 86400000;
const oneMinute = 60000;
setInterval(() => {clearQueue()},oneDayMillisec);
// const url = require('url'); 

const global = {
  downloadPath : pathes.downloadPath
}


function clearQueue() {
  let annotationIds = fsSync.readdirSync(pathes.annotationPath);
  // let storedIds = handledClients.map(a => a.id);

  console.log("Ids in downloads: " + annotationIds.length)
  console.log("Ids in memory: " + handledClients.length)

  handledClients = handledClients.filter(v => annotationIds.includes(v.id))
  console.log("Ids in memory after remove: " + handledClients.length)

}

router.get('/', function(req, res, next) {
  res.render('index', { title: 'DeGeCI'});
});

router.get('/scenario',(req,res,next) => {
  //console.log(exampleSequence)
  res.render('annotation_form', {wVal:1, gVal:2, alphaVal:0.7, jobID: 'NC_022828',
    o1C:true, o2C:false, 
    circcheck:true, seq: exampleSequence,
    ref1:true, ref2:false,
    sel0:false, sel1:true, sel2:false, sel4:false, sel5:false, sel9:false, sel13:false, sel14:false, 
    taxh:"Cnidaria ",tax:"Cnidaria (6073)\t",
    seqPlaceholder: "ACTGAC..."})
//   res.render('annotation_form_example', {seq: exampleSequence, jobID: 'NC_022828'
// })
})

// router.get('/imprint',(req,res,next) => {
//   //console.log(exampleSequence)
//   res.render('imprint')
// })

// router.get('/annotationForm',(req,res,next) => {
//   res.render('annotation_form', {title: 'Enter Query Sequence', seqPlaceholder: "ACTGAC..."})
// })

router.get('/annotationForm',(req,res,next) => {
  res.render('annotation_form', {wVal:1, gVal:2, alphaVal:0.7, jobID: "",
    o1C:true, o2C:false, 
    circcheck:true, 
    ref1:true, ref2:false,
    sel0:false, sel1:true, sel2:false, sel4:false, sel5:false, sel9:false, sel13:false, sel14:false, 
    seqPlaceholder: "ACTGAC..."})
})


// router.get('/test',(req,res,next) => {
//   res.render('result',{linenumber: 4});
// })

router.get('/example',(req,res,next) => {
  res.render('example');
})


router.get('/results/:id',(req,res,next) => {
  console.log('render results')
  res.render('result');
})

router.get('/download/:id',(req,res,next) => {
  let linkVal = "/downloads/annotations/"+ req.params.id + "/degeciAnnotations.zip"
  res.render('download',{ link: linkVal});
})

router.post('/download/:id', (req,res,next) => {
  const directoryPath = pathes.annotationPath +"/" +req.params.id;


  res.download(directoryPath+"/degeciAnnotations.zip", 
    (err) => {
    if (err) {
      console.log(err);
      console.log("should render errors")
      res.render('errorDeletedFile')
    }
  });
})

function getHandledClient(clientId) {
  return handledClients.findIndex(element => element.id === clientId)
}

function getPosInQueue(clientId) {
  return clients.findIndex(element => element.id === clientId)
}

function notifyClientPosInQueue(res,clientId) {

  res.write('event: client\n');
  let pos = getPosInQueue(clientId)//annotation_form_controller.getPos() //+ " " +annotation_form_controller.getInfo();
  // console.log(clientId+': Notifying about POSITION in queue ' + pos)
  res.write('data:' + pos + '\n\n');
  return pos;
}

function notifyClientStatus(res,posInterval,clientId) {
  // console.log("trying to notify about status")
  // let pos = getPosInQueue(clientId);
  let status = annotation_form_controller.getInfo(clientId);


    
  if(status != "X") {
    // console.log(clientId+ ': Notifying about STATUS in queue ' + status)
    if(posInterval  !="") {
      // console.log("clearing posinterval in notifyClientstatus")
      clearInterval(posInterval)
    }
    
    // console.log(clientId + " "+ posInterval)
    res.write('event: status\n');
    res.write('data:' + status + '\n\n');

  }

  

  return status;
}

router.get('/results/:id/subscribe',  (req,res,next) => {

  const headers = {
    'Content-Type': 'text/event-stream',
    'Connection': 'keep-alive',
    'Cache-Control': 'no-cache',
    'X-Accel-Buffering' : 'no',
    'Transfer-encoding' : 'chunked',
  };
  const clientId = req.params.id;
 

   //client has already been served
  if(getHandledClient(clientId) != -1) {

    console.log("Client already been served")
    res.writeHead(200, headers);
    if(handledClients[getHandledClient(clientId)].status == "error") {
      console.log("to new tab error")
      res.write('event: err\n');
      res.write('data: error\n\n');
    }
    else {
      console.log("to new tab resulst")
      res.write('event: result\n');
      res.write('data: '+handledClients[getHandledClient(clientId)].status + '\n\n');
    }
    return;
  }



  let msgInterval;

  
   // client is currently served 
  if(getPosInQueue(clientId) != -1) { 
//todo
    let servedClient = clients[getPosInQueue(clientId)]
    
    console.log("Client already currently served")
    console.log("cleared intervals")
    //  delete existing connection
    if("pInterval" in servedClient){
      clearInterval(servedClient.pInterval);
      console.log(servedClient.pInterval)
    }
    if("mInterval" in servedClient) {
      clearInterval(servedClient.mInterval);
      console.log(servedClient.mInterval)
    }
    

    // do NOT delete this clientid  from queue, otherweise newclient wont receive the results -> he will remove the id from the queue
    servedClient.deleteFromQueue = false;
    servedClient.cl.write('event: newTab\n');
    servedClient.cl.write('data:' + "new tab" + '\n\n');


    // set new client
    servedClient.cl = res;


    res.writeHead(200, headers);
    let posInterval = ""
    // computation has not started yet
    if(annotation_form_controller.getInfo(clientId) == ""){
      conslole.log("computation not yet started")
      notifyClientPosInQueue(res,clientId); 
      posInterval = setInterval(() => {notifyClientPosInQueue(res,clientId)},15000);
      
    }
    else {
      notifyClientStatus(res,"",clientId)
    }
    msgInterval= setInterval(() => {notifyClientStatus(res,posInterval,clientId)},3000);
    servedClient.mInterval = msgInterval;
    servedClient.pInterval = posInterval;

  }
  else{

    console.log("Client not yet served")
    res.writeHead(200, headers);
    let aliveInterval = setInterval(() => {
      res.write(':keep-alive\n\n');
      // res.flush();
    }
    ,15000);

    let newClient = {
      id: clientId,
      cl: res,
      deleteFromQueue: true
    };
    clients.push(newClient);



    console.log(`${clientId} - Connection opened`);


    notifyClientPosInQueue(res,clientId);
    let posInterval = setInterval(() => {notifyClientPosInQueue(res,clientId)},15000);
    msgInterval= setInterval(() => {notifyClientStatus(res,posInterval,clientId)},3000);


    
    newClient.pInterval = posInterval;
    newClient.mInterval = msgInterval;

    // listen for client 'close' requests

    // console.log(clients)


    // console.log(posInterval)

    // console.log(clients[getPosInQueue[clientId]].pInterval)


  }

  req.on('close', () => {

    if(res === clients.find(element => element.id === clientId).cl) {
      console.log("removing corret client")
      // clearInterval(posInterval)
      console.log("closing connection")
      console.log(msgInterval)
      clearInterval(msgInterval);
      // clearInterval(aliveInterval);
      // console.log(clientId + " " +posInterval)
      console.log(`${clientId} - Connection closed`);
      clients = clients.filter(client => client.id !== clientId);
      console.log("Clients " + clients.length)
    }

})




});






const validateAnnotation = validations => {
  return async (req, res, next) => {
    // console.log(req);
    let valid = true;
    for(let i = 0; i < 4; i++) {
      let validation = validations[i];
      
      const result = await validation.run(req);
      if (result.errors.length) {
        console.log('validation failed')
        console.log(result.errors)
        valid = false;
        break;
      }
    }
    if(valid) {
      let validation1 = validations[4];
      const result1 = await validation1.run(req);
    }

    return next();
  };
};




function genUniqId(){
  return Date.now() + '-' + Math.floor(Math.random() * 1000000000);
}

router.post('/annotationForm',
  upload.single('file'),   
  validateAnnotation([
    check('id').custom( (value) => { 
      const validJobID = new RegExp('^[a-zA-Z0-9-_\.]{1,100}$');
      let test = validJobID.test(value);

      if(!test) {
        console.log("invalid jobid")
          return false;
      }

      return true;
  
    }).withMessage("Job title must consist only of letters, numbers, '-', '_', and '.'."),
    check('w').isInt({ min: 1}).withMessage("Edge weight must be an integer >0"),
    check('g0').isInt({ min: 1}).withMessage("Prediction count must be an integer >0"),
    check('alpha').isDecimal({ min: 0, max: 1}).withMessage("Alpha value must be between 0 and 1"),
    oneOf([
      check('file').custom( (value,{req}) => { 
        if(req.file == undefined) {
          console.log("no file")
            return false;
        }
        const data  = fsSync.readFileSync(req.file.path,'utf8');

        console.log(data)
        if(data.length < 50) {
          console.log('sequence too short')
          return false;
        }

        return true;
    
      }).withMessage('invalid File supplied'),
      check('sequence').custom(value => {
        value = value.trim();
        console.log(value.length)
        if(value.length < 50) {
          console.log('sequence too short')
          return false;
        }

        console.log("valid seq");
        return true;
      }).withMessage('invalid sequence supplied')
    ])
  ]),
  async (req,res,next) => {

    const errors = validationResult(req);

    
    
    if(!errors.isEmpty()) {
      console.log(errors.msg)
      console.log("error " +req.body.sequence)
      let refArray = [false,false]
      if(req.body.refseq == "RefSeq89"){
        refArray[0] = true
      }
      else{
        refArray[1] = true
      }

      let inclArray = [false,false]
      if(req.body.optradio == "o1") {
        inclArray[0] = true
      }
      else {
        inclArray[1] = true
      }

      let gencodeArray = [false,false,false,false,false,false,false,false]


      console.log(req.body.gencode)

      if(req.body.gencode == -1) {
        gencodeArray[0] = true;
      }
      else if(req.body.gencode == 1) {
        gencodeArray[1] = true; 
      }
      else if(req.body.gencode == 2) {
        gencodeArray[2] = true; 
      }
      else if(req.body.gencode == 4) {
        gencodeArray[3] = true; 
      }
      else if(req.body.gencode == 5) {
        gencodeArray[4] = true; 
      }
      else if(req.body.gencode == 9) {
        gencodeArray[5] = true; 
      }
      else if(req.body.gencode == 13) {
        gencodeArray[6] = true; 
      }
      else if(req.body.gencode == 14) {
        gencodeArray[7] = true; 
      }

      console.log("gencodes" + gencodeArray)

      res.render('annotation_form', { wVal:req.body.w, gVal:req.body.g0, alphaVal:req.body.alpha, 
        o1C:inclArray[0], o2C:inclArray[0], 
        circcheck:req.body.composition, 
        ref1:refArray[0], ref2:refArray[1],
        taxh:req.body.taxList,tax:req.body.selection,
        sel0:gencodeArray[0], sel1:gencodeArray[1], sel2:gencodeArray[2], sel4:gencodeArray[3], sel5:gencodeArray[4], sel9:gencodeArray[5], sel13:gencodeArray[6], sel14:gencodeArray[7], 
        jobID: req.body.id, seq: req.body.sequence , errors: errors.array()});

      return false;
  }

    let link = null;
    try {
      
      let uid = uuidv4();

      res.redirect('/catalog/results/'+uid);


      res.locals.uid = uid;
      clientCount=1+clientCount;
      link = await annotation_form_controller.enter_sequence_string_post(req,res,next, uid);

    } catch (error) {
      // console.log("error caugth")
      console.log('results could not be annotated catalog')
      console.log(error);
      clientCount=clientCount-1;
    }
    res.locals.link = link;
    console.log('Link: ' + link)

    return next()
  },async (req,res,next) => {
    console.log("refresh")
    sendRefresh(res.locals.uid, res.locals.link);
  }
);


function sendRefresh(uid, data) {

  let client = null; 
  let found = false;
  for(let i = 0; i < clients.length; i++ ) {
    if(clients[i].id == uid){
      client = clients[i];
      found = true;
    }
  }
  // clearInterval(client.pos);

  if(found) {
    console.log('notifying '+ client.id)
    // console.log(client.cl)

    if(data == null) {
  
      client.cl.write('event: err\n');
      console.log('notifying about error')
      client.cl.write('data: error\n\n');

      let handledClient = {id : client.id, status: "error"}
      handledClients.push(handledClient)
    }
    else {
      client.cl.write('event: result\n');
      console.log('notifying result data')
      client.cl.write('data: '+data+ '\n\n');
      let handledClient = {id : client.id, status:data}
      handledClients.push(handledClient)
    }
    // client.cl.flush();
    console.log("after flush");
  }
  else {
    console.log("client not found")
  }

}

// ACTAAAaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
module.exports = router;