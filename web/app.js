var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
const bodyParser = require("body-parser");
var indexRouter = require('./routes/index');
var catalogRouter = require('./routes/catalog');  
require('console-stamp')(console);

// safety precautions
const compression = require("compression");
const helmet = require("helmet");
const RateLimit = require("express-rate-limit");


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// app.use(compression())

// app.use(
//   helmet.contentSecurityPolicy({
//     directives: {
//       defaultSrc: ["'self'","auto","autoExample","bootstrapJS",  "https://cdn.jsdelivr.net/npm/bootstrap@5/dist/js/bootstrap.bundle.min.js","https://cdn.jsdelivr.net/gh/lekoala/bootstrap5-autocomplete@master/autocomplete.js"],
//       scriptSrc: ["'self'", "auto","autoExample","bootstrapJS", "https://cdn.jsdelivr.net/npm/bootstrap@5/dist/js/bootstrap.bundle.min.js","https://cdn.jsdelivr.net/gh/lekoala/bootstrap5-autocomplete@master/autocomplete.js"],
//       styleSrc: ["'self'", "bootstrapCSS","https://cdn.jsdelivr.net/npm/bootstrap-dark-5@1/dist/css/bootstrap-dark.min.css"],
//     },
//   }),
// );
app.use(
  helmet({
    contentSecurityPolicy: false,
  })
);
// const limiter = RateLimit({
//   windowMs: 1 * 60 * 1000, // 1 minute
//   max: 20,
// });
// //Apply rate limiter to all requests
// app.use(limiter);

app.use('/', indexRouter);
app.use('/catalog', catalogRouter); 


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
