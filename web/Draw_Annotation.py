#!/usr/bin/env python
# coding: utf-8

import os
import sys

from Bio.Graphics import GenomeDiagram
from Bio.SeqFeature import (
    FeatureLocation,
    SeqFeature,
)
from reportlab.lib.units import (
    cm,
    inch,
)
from dataclasses import dataclass


# tRNAs
# - metazoa default set
metazoa_trna = ["trnaA", "trnaR", "trnaN", "trnaD", "trnaC", "trnaQ", "trnaE",
                "trnaG", "trnaH", "trnaI", "trnaL1", "trnaL2", "trnaK",
                "trnaM", "trnaF", "trnaP", "trnaS1", "trnaS2", "trnaT",
                "trnaW", "trnaY", "trnaV"]
# some other tRNA names
other_trna = ["trnaL", "trnaS", "trnaX", "trnaZ"]
trna = metazoa_trna + other_trna

# list of rRNAs
metazoa_rrna = ["rrnS", "rrnL"]
rrna = metazoa_rrna

# list of protein coding genes
metazoa_prot = ['cox1', 'cox2', 'cox3', 'cob', 'atp6', 'atp8', 'nad1', 'nad2',
                'nad3', 'nad4', 'nad4l', 'nad5', 'nad6']
metazoa_other_prot = ['atp9', 'msh1', 'mttb', 'dpo', 'rpo', 'lagli']
fungi_prot = ['rps3', 'rps5', 'giy', 'lagli']
prot = metazoa_prot + metazoa_other_prot + fungi_prot

# construct a type mapping for the genes
types = {}
for t in trna:
    types[t] = "tRNA"
for t in rrna:
    types[t] = "rRNA"
for t in prot:
    types[t] = "gene"

def type_from_name(name):
    if name in types:
        return types[name]
    return None


@dataclass
class feature:
    name: str
    type: str
    start: int
    stop: int
    strand: int


def draw(features, size, filename):
    """
    Function to draw a genom in png format
    It returns the pna so that it can write on a web page.
    @param features a feature list in feature format
    @param size the size of the whole genom
    @param filename a file to write to (if None file content is returned)
    """
    gdd = GenomeDiagram.Diagram('Test Diagram')
    gdt_features = gdd.new_track(1, height=0.5, scale_smalltick_interval=1e3)
    gd_feature_set = gdt_features.new_set()

    for feat in features:
        # color the features
        if feat.type == "gene":
            color = "red"
        elif feat.type == "tRNA":
            color = "blue"
        elif feat.type == "rRNA":
            color = "green"
        else:
            color = "grey"
        # add it as a ARROW
        if feat.start > feat.stop:
            feature = SeqFeature(FeatureLocation(feat.start, size, strand=feat.strand))
            gd_feature_set.add_feature(feature, sigil="BOX", color=color, arrowshaft_height=1, label=False, label_size=6, label_angle=90, name="  " + feat.name, label_position="middle")  # ,
            feature = SeqFeature(FeatureLocation(0, feat.stop, strand=feat.strand))
            gd_feature_set.add_feature(feature, sigil="ARROW", color=color, arrowshaft_height=1, label=True, label_size=6, label_angle=90, name="  " + feat.name, label_position="middle")  # ,
        else:
            feature = SeqFeature(FeatureLocation(feat.start, feat.stop, strand=feat.strand))
            gd_feature_set.add_feature(feature, sigil="ARROW", color=color, arrowshaft_height=1, label=True, label_size=6, label_angle=90, name="  " + feat.name, label_position="middle")  # ,
    # draw it linear and in format 20/3
    gdd.draw(format='linear', pagesize=(8 * inch, 1.5 * inch), fragments=1, start=0, end=size, tracklines=0)
    if filename:
        gdd.write(filename=filename, output='PNG', dpi=300)
    else:
        return gdd.write_to_string("PNG", dpi=300)


def draw_final(inputfile: str, size: int):
    features = []
    with open(inputfile) as file:
        for line in file:
            genome, start, stop, gene, _, strand = line.split()
            features.append(feature(name=gene, type=type_from_name(gene), start=int(start), stop=int(stop), strand=1 if strand=="+" else -1))
    outputfile = os.path.splitext(inputfile)[0] + "_geneplot.png"
    print(outputfile)
    draw(features, 15574, outputfile)


#draw_final("NC_036405.bed", 15574)


if __name__ == '__main__':
    _, input_file, length,  = sys.argv
    draw_final(input_file, length)
