const { body,validationResult , oneOf, check} = require("express-validator");
const { ExpectationFailed } = require("http-errors");
let result_controller = require('../controller/resultController');
let fs = require('promise-fs')
const AdmZip = require('adm-zip')
const { spawn,exec, spawnSync } = require('child_process');
const { v4: uuidv4 } = require('uuid');
const { resolve } = require("path");
const fsSync = require('fs-extra');
const totalComputeNodes = 1;
let freeComputeNodes = totalComputeNodes;
const pathes = require('../projectDirectories')
const downloadPath = pathes.downloadPath;
const javaDir = pathes.javaDir;
const javaDirTax = pathes.javaDirTax;
const pythonScript = pathes.pythonScript;
const statusArray = ["Creating phylogenetic subtree of supplied taxonomic groups. Processing Step 1/4 ...","Creating the induced subgraph and bridging components. Processing Step 2/4 ...",
"Annotating the sequence. Processing Step 3/4 ...","Generating output files and plots. Processing Step 4/4 ...","No kmer matches could be found."];
let status = "X";
let currentID = "";

exports.enter_sequence_string_get = function(req,res,next) {
    res.render('annotation_form', {title: 'Enter Query Sequence'});
};

let nucleotideRegex = /^[ACGTNKMBVSWDYRH]+$/;
let fastaHeaderRegex = /^>.*/;

exports.getPos =function() {
    console.log("returning " + freeComputeNodes)
    return freeComputeNodes;
}

exports.getInfo =function(id) {
    if(id == currentID) {
        return status;
    }
    // console.log("ids do not match " + id + " " + currentID)
    return "X"
}

function getFreeComputeNode (req,res) {

    // all graphs are busy
    return new Promise((resolve, reject) => {

        console.log("Free compute nodes: " + freeComputeNodes)
        if(freeComputeNodes > 0) {
            freeComputeNodes = freeComputeNodes-1;
            resolve(freeComputeNodes);
        }
        else {
            let done = setInterval(() => {
                if(freeComputeNodes > 0) {
                    freeComputeNodes = freeComputeNodes-1;
                    clearInterval(done);
                    resolve(freeComputeNodes);
                }
                // console.log("still waiting");
            },10000);
        }

    });
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function clearAnnotationDirectory(dir, t){
    return new Promise( (resolve, reject) => {
      resolve(setTimeout(
        () =>{
            console.log("Clearing download: " + dir )
          fsSync.remove(dir)
        }
        ,
        t)) 
    })
}

function moveAnnotations(moveFrom, moveTo) {

    const zip = new AdmZip();
    const id = moveFrom.substring(moveFrom.lastIndexOf("/")+1);
    zip.addLocalFolder(moveFrom);
    zip.writeZip(moveFrom+"/degeciAnnotations.zip")

    fsSync.moveSync(moveFrom,moveTo ,(err) => {
        if (err) throw err;});
    
}

function launchAnnotation(req,res,paramArray,mail, id,resultDir,length,genomeName, usefilter){
    // data available for 48h -> delete thereafter
    // const dataAvailabilityTime = 172800000
    paramArray = [javaDir].concat(paramArray);

    // console.log("Using filter:" + usefilter)
    // if(usefilter) {
    //     paramArray = [javaDirTax].concat(paramArray);
    // } else{
    //     paramArray = [javaDir].concat(paramArray);
    // }
    
    console.log("Free compute nodes after: " + freeComputeNodes);
    // const link = pathes.externalDomain + "/catalog/download/" + id;
    const link =  "/catalog/download/" + id;
    console.time("annotation"+id);
    currentID = id;
    let workerProcess = spawn('bash',paramArray );
    return new Promise((resolve, reject) =>{
        workerProcess.stdout.on('data', function (data) {  
            console.log('stdout: ' + data);  
            if(data.includes("Launching Taxonomic Filtering")){
                status = statusArray[0];
            }
            else if(data.includes("Creating kmer mappings") || data.includes("Bridging CCs") ){
                status = statusArray[1];
            }
            else if(data.includes("Annotating")){
                status = statusArray[2];
            }
            console.log("Status: " + status);
         });  
        workerProcess.stderr.on('data', function (data) {  
            console.log('stderr: ' + data);  
            status = statusArray[4];
         });  
        workerProcess.on('close', function (code) {  
            console.log('child process exited with code ' + code);  
            status = "";
            // console.log("annotation time " + id);
            console.timeEnd("annotation"+id)
            if(code == 0){
                status = statusArray[3];
                // run python script for generating gene plot
                let pyprocess = spawn('.venv/bin/python3',[pythonScript, 
                resultDir+"/"+genomeName+".bed",
                length] ); 

                // Handle normal output
                pyprocess.stdout.on('data', (data) => {
                    console.log('stdout: ' + data);
                 });
                 
                 // Handle error output
                 pyprocess.stderr.on('data', (data) => {
                    console.log('stderr: ' + data);  
                 });
                 
                 pyprocess.on('close', (code) => {
                     console.log("Python quit with code : " + code);
                        if(code == 0){
                        // move the results to direcotry where they will be available for dataAvailibilityTime ms
                        moveAnnotations(resultDir,pathes.annotationPath + "/"+id)

                        console.log("updating " + freeComputeNodes)
                        status = "X";
                        currentID = "";
                        freeComputeNodes= freeComputeNodes+1;
                        
                        resolve(link);
                     }
                     else {
                        console.log("error!")
                        console.log("updating " + freeComputeNodes)
                        status = "X";
                        currentID = "";
                        freeComputeNodes= freeComputeNodes+1;
                        
                        reject(null);
                     }
                 });



            }
            else {
                
                console.log("error!")
                console.log("updating " + freeComputeNodes)
                status = "X";
                currentID = "";
                freeComputeNodes= freeComputeNodes+1;
               
                reject(null);
            }
         });  
    });
}



function setUpResultDirectories(uid) {
    let dir = downloadPath+ "/" + uid ;
    fsSync.mkdir(dir, { recursive: true }, (err) => {
        if (err) throw err;
      });
    return dir;
}

function parseSequence(data) {
    let sequence = "";
    let nucleotideRegex = /[^ACGTNKMBVSWDYRH]/ig;
    lines = data.split("\n");
    for(let lineCounter = 0; lineCounter < lines.length; lineCounter++) {
        if(lineCounter > 0 && lines[lineCounter].includes(">")) {
            break
        }
      if(!(lines[lineCounter].startsWith(">"))) {
        let seq = lines[lineCounter].replace("\n").trim();
        seq = seq.replace(nucleotideRegex,"").toUpperCase()
  
        //console.log(seq)
        sequence += seq;
      }

    }
  
    console.log("")
    console.log(sequence)
    return sequence
  
  }

function extractSequenceFromFasta(fastaFile) {
    const data = fsSync.readFileSync(fastaFile,'utf8');
    return(parseSequence(data))

}

// function extractSequenceFromFasta(fastaFile) {
//     const data = fsSync.readFileSync(fastaFile,'utf8');
    
//     lines = data.split("\n");
//     let sequence = "";

//     for(let lineCounter = 1; lineCounter < lines.length; lineCounter++) {
//         sequence += lines[lineCounter].replace("\n","");
//     }
//     return sequence;
// }



exports.enter_sequence_string_post = async function(req,res,next, uid) {
 

    try {
        await getFreeComputeNode(req,res);

    } catch (error) {
        console.log(error);
    }

    console.log("Free compute nodes after: " + freeComputeNodes)
    console.log("got free compute node");

    console.log(req.body.composition)
    console.log(req.body.refseq)

    let jobid = req.body.id;
    let dir = setUpResultDirectories(uid);
    if(jobid == "") {
    
        jobid = dir.substring(dir.lastIndexOf("/")+1);
    }
    console.log(dir)

    let sequence = "";
    // // sequence was supplied as text
    // if(req.body.sequence != undefined) {
    //     console.log("Sequence from text");
    //     sequence = req.body.sequence.trim();}
    // else {
    //     console.log("Sequence from fasta file");
    //     sequence = extractSequenceFromFasta(req.file.path).trim();
    //     console.log("Sequence size: "+ sequence.length);}

    // sequence was supplied as text
    if(req.body.sequence != undefined && req.body.sequence.length > 0) {
        console.log("Sequence from text");
        sequence = parseSequence(req.body.sequence)
    } 
    else {
        console.log("Sequence from fasta file");
        sequence = extractSequenceFromFasta(req.file.path).trim();
        console.log("Sequence size: "+ sequence.length);
    }

    let taxids;
    console.log(req.body.taxList)
    if(req.body.taxList == "") {
        console.log("empty")
        taxids = [];
    }
    else {
        taxids = req.body.taxList.trim().split(" ");
    }
        

    let paramArray = [
    "--name",jobid,
    "--result-directory",dir,
    "--sequence", sequence ,
    "--alpha", req.body.alpha]

    if(!req.body.composition) {
        console.log("linear")
        paramArray.push("--treat-linear")
        paramArray.push("true")
    }
    if(req.body.gencode != -1){
        paramArray.push("--trans-table");
        paramArray.push(req.body.gencode);
    }
    paramArray.push("--refseq");
    if(req.body.refseq == "RefSeq89"){
        paramArray.push(89);
    }
    else{
        paramArray.push(204);
    }
    if(taxids.length > 0) {

        paramArray.push("--exclude-taxids");
        const val = req.body.optradio == "o1" ? "false" : "true";
        paramArray.push(val);
        paramArray.push("--taxonomic-groups");
        paramArray = paramArray.concat(taxids);
    }

    console.log(paramArray);

    
    let link =null;
    try {
        link = await launchAnnotation(req,res,paramArray,req.body.mail,dir.substring(dir.lastIndexOf("/")+1),dir,sequence.length,jobid,taxids.length > 0);
        console.log("after launch")

    } catch (error) {
        console.log('results could not be annotated enter_sequence_string_post')
        console.log(error);
    }


    return new Promise((resolve, reject) => {
        if(link != null){
            resolve(link)
        }
        else{
            reject(link);
        }
    })
}

