const { body,validationResult } = require("express-validator");

exports.show_results = function(req,res,next) {
    res.render('result', {result: req.body.sequence});
};