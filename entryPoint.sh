#!/bin/bash
 
DB=/var/lib/postgresql
DB_NAME=mdbg



set -e


psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE DATABASE ${DB_NAME} WITH ENCODING = 'UTF8' TABLESPACE = pg_default LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8' CONNECTION LIMIT = -1 TEMPLATE template0;
EOSQL


cd ${DB}




    
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$DB_NAME" -c "CREATE SCHEMA complete"


ls -al
FILE=/var/lib/postgresql/refseq204Dump
if [ -f "$FILE" ]; then
    echo "populating with refseq 204"
    psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$DB_NAME" -f /var/lib/postgresql/refseq204Dump
    echo "done with 204"
fi

FILE=/var/lib/postgresql/refseq89Dump
if [ -f "$FILE" ]; then
    echo "populating with refseq 89"
    psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$DB_NAME" -f /var/lib/postgresql/refseq89Dump
    echo "done with 89"
fi









