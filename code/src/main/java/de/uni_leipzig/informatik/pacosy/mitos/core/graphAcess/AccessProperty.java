package de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess;

import net.sourceforge.argparse4j.inf.Argument;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.ArgumentType;

import java.time.Duration;
import java.util.Map;

public abstract class AccessProperty implements ArgumentType<Object> {

    public interface ALLOWED_KEYS_INTERFACE {
        String getIdentifier();
//        List<String> getAllIdentifier();
    }

    protected interface MUTABILITY_INTERFACE {

    }

    protected static class PropertyAttribute {
        String description;
        MUTABILITY_INTERFACE mutability;
        Class classType;
        Object defaultValue;

        protected PropertyAttribute(Class classType, Object defaultValue, MUTABILITY_INTERFACE mutability, String description) {
            this.description = description;
            this.classType = classType;
            this.defaultValue = defaultValue;
            this.mutability = mutability;
        }
    }

    private ALLOWED_KEYS_INTERFACE propertyKey;

    private Object value;
    public static String SPLIT_VALUE = ":";


    public AccessProperty() {

    }

    public AccessProperty(ALLOWED_KEYS_INTERFACE key, Object value) {
        this.propertyKey = key;
        PropertyAttribute attributes = getProperties().get(key);
        assert (value.getClass().equals(attributes.classType));
        this.value = value;
    }

    public AccessProperty(String str) {
        convertStringToAccessProperty(str);
    }

    @Override
    public Object convert(ArgumentParser parser, Argument arg, String value) throws ArgumentParserException {

        try {
            convertStringToAccessProperty(value);
        } catch (Exception e) {
            throw new ArgumentParserException(e, parser);
        }
        return this;
    }


    private void convertStringToAccessProperty(String stringProperty) {
        String [] parts = stringProperty.split(SPLIT_VALUE);
        for(ALLOWED_KEYS_INTERFACE key: getProperties().keySet()) {
            if(key.getIdentifier().equals(parts[0])) {
                this.propertyKey = key;
                PropertyAttribute attributes = getProperties().get(key);
                if(attributes.classType.equals(Boolean.class)) {
                    this.value = Boolean.valueOf(parts[1]);
                    return;
                }
                else if(attributes.classType.equals(Long.class)) {
                    this.value = Long.valueOf(parts[1]);
                    return;
                }
                else if(attributes.classType.equals(Integer.class)) {
                    this.value = Integer.valueOf(parts[1]);
                    return;
                }
                else if(attributes.classType.equals(Double.class)) {
                    this.value = Double.valueOf(parts[1]);
                    return;
                }
                else if(attributes.classType.equals(String.class)) {
                    this.value = parts[1];
                    return;
                }
                else if(attributes.classType.equals(Duration.class)) {
                    this.value = Duration.ofMillis(Long.valueOf(parts[0]));
                    return;
                }

            }
        }
    }

    public ALLOWED_KEYS_INTERFACE getPropertyKey() {
        return propertyKey;
    }

    public void setPropertyKey(ALLOWED_KEYS_INTERFACE propertyKey) {
        this.propertyKey = propertyKey;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public abstract <K extends ALLOWED_KEYS_INTERFACE> Map<K,PropertyAttribute> getProperties();

}
