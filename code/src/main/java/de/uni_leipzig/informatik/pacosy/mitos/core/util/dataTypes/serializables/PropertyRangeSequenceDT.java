package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class PropertyRangeSequenceDT extends PropertyRangeDT {

    private int rowNumber;
    private String Sequence;
    private int recordId;
    private int gene;
    public static final Encoder<PropertyRangeSequenceDT> ENCODER = Encoders.bean(PropertyRangeSequenceDT.class);

    public PropertyRangeSequenceDT(String category, String property, int start, int end, int length, int rowNumber, int gene, String sequence, int recordId) {
        super(category, property, start, end, length);
        this.rowNumber = rowNumber;
        Sequence = sequence;
        this.recordId = recordId;
        this.gene = gene;
    }

    public PropertyRangeSequenceDT() {
    }

    public int getGene() {
        return gene;
    }

    public void setGene(int gene) {
        this.gene = gene;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public String getSequence() {
        return Sequence;
    }

    public void setSequence(String sequence) {
        Sequence = sequence;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }
}
