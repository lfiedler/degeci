package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class RecordSequenceDT implements Serializable {
    public static final Encoder<RecordSequenceDT> ENCODER = Encoders.bean(RecordSequenceDT.class);
    protected int recordId;
    protected String sequence;

    public RecordSequenceDT() {
    }

    public RecordSequenceDT(int recordId, String sequence) {
        this.recordId = recordId;
        this.sequence = sequence;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }
}
