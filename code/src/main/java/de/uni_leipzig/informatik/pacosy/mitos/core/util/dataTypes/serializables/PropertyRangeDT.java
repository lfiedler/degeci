package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.apache.spark.sql.functions.*;

public class PropertyRangeDT implements Serializable, Comparable<PropertyRangeDT> {
    protected String category;
    protected String property;
    protected int start;
    protected int end;
    protected  int length;
    protected static final Logger LOGGER = LoggerFactory.getLogger(PropertyRangeDT.class);
    public static final int THRESHOLD = 50;
    public static final Encoder<PropertyRangeDT> ENCODER = Encoders.bean(PropertyRangeDT.class);
    public static final Map<String,Double> meanLengths;
    static {
        List<Row> r = SparkComputer.readORC(ProjectDirectoryManager.getGRAPH_DATA_DIR()+"/GENE_LENGTHS")
//                .filter(col(ColumnIdentifier.CATEGORY).isInCollection(Arrays.asList("protein","rrna","trna")))
                .withColumn("rate",col(ColumnIdentifier.MEAN).multiply(0.15).
                        cast(DataTypes.DoubleType)).select(ColumnIdentifier.PROPERTY,"rate").collectAsList();
        meanLengths = new HashMap<>();
        for(Row row : r) {
            meanLengths.put(row.getString(0),row.getDouble(1));
        }
    }





    public PropertyRangeDT(String category, String property, int start, int end, int length) {
        this.category = category;
        this.property = property;
        this.start = start;
        this.end = end;
        this.length = length;
    }

    public PropertyRangeDT(PropertyRangeDT propertyRange) {
        this.property = propertyRange.property;
        this.category = propertyRange.category;
        this.start = propertyRange.start;
        this.end = propertyRange.end;
        this.length = propertyRange.length;
    }



    @Override
    public String toString() {
        return new RangeDT(start,end,length).toString() + ": " +category + ", " + property;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PropertyRangeDT)) return false;
        PropertyRangeDT that = (PropertyRangeDT) o;
        return getStart() == that.getStart() &&
                getEnd() == that.getEnd() &&
                getLength() == that.getLength() &&
                Objects.equals(getCategory(), that.getCategory()) &&
                Objects.equals(getProperty(), that.getProperty());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCategory(), getProperty(), getStart(), getEnd(), getLength());
    }

    //    @Override
//    public int compareTo(PropertyRangeDT propertyRangeDT) {
//        int v = Integer.valueOf(this.start).compareTo(propertyRangeDT.start);
//        if(v == 0) {
//            return Integer.valueOf(propertyRangeDT.end).compareTo(this.end);
//        }
//        return v;
//    }

    @Override
    public int compareTo(PropertyRangeDT propertyRangeDT) {
        int v = Integer.valueOf(this.start).compareTo(propertyRangeDT.start);
        if(v == 0) {
            int e = Integer.valueOf(this.end).compareTo(propertyRangeDT.end);
            if(e == 0) {
                return this.property.compareTo(propertyRangeDT.property);
            }
            return e;
        }
        return v;
    }

    public PropertyRangeDT() {
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    protected static <T extends PropertyRangeDT> void removeContainedRanges(List<T> propertyRanges) {
        Collections.sort(propertyRanges);
//        Auxiliary.printSeq(propertyRanges);
//        System.out.println();
        Iterator<T> it = propertyRanges.iterator();
        T prevRange = it.next();
        T nextRange;


        while (it.hasNext()) {
            nextRange = it.next();
            if(nextRange.start >= prevRange.start && nextRange.end <= prevRange.end && prevRange.property.equals(nextRange.property)) {
                it.remove();
            }
            else {
                prevRange = nextRange;
            }
        }
//        System.out.println("after removal");
//        Auxiliary.printSeq(propertyRanges);


    }

//    public static  <T extends PropertyRangeDT> List<T> collapseRanges(List<T> propertyRanges) {
//
//        List<String> properties = propertyRanges.stream().map(p -> p.property).distinct().collect(Collectors.toList());
//        List<T> collapsedPropertyRanges = new ArrayList<>();
//        for(String property: properties) {
//            System.out.println(property);
//            List<T> propertyRange = propertyRanges.stream().filter(p -> p.property.equals(property)).collect(Collectors.toList());
//            removeContainedRanges(propertyRange);
//            collapsedPropertyRanges.addAll(merge(propertyRange));
//            Auxiliary.printSeq(propertyRange);
//        }
////        System.out.println("before return");
////        Auxiliary.printSeq(collapsedPropertyRanges);
//        return collapsedPropertyRanges;
//    }

    public static  <T extends PropertyRangeDT> List<PropertyRangeProbDT> collapseRanges(List<T> propertyRanges, int length) {

        List<PropertyDT> properties = propertyRanges.stream().map(p -> PropertyDT.convert((PropertyRangeProbDT) p)).
                flatMap(l -> l.stream()).collect(Collectors.toList());
        return PropertyDT.convert(properties,length);
    }

    protected static <T extends PropertyRangeDT> List<T> merge(List<T> propertyRanges) {
        Collections.sort(propertyRanges);
        List<T> mergedPropertyRanges = new ArrayList<>();
        mergedPropertyRanges.add(propertyRanges.get(0));
        T prevRange;
        T nextRange;

        for(int i = 1; i < propertyRanges.size(); i++) {
                prevRange = mergedPropertyRanges.get(mergedPropertyRanges.size()-1);
                nextRange = propertyRanges.get(i);
                if(prevRange.getEnd() == nextRange.getStart()-1 &&
                        prevRange.property.equals(nextRange.property)) {
                    prevRange.end++;
                    prevRange.length++;
//                    if(prevRange instanceof PropertyRangeProbDT) {
//                        PropertyRangeProbDT
//                    }
                }
                else {
                    mergedPropertyRanges.add(nextRange);
                }
        }
//        System.out.println("after merge");
//        Auxiliary.printSeq(mergedPropertyRanges);
        return mergedPropertyRanges;

    }

    protected static <T extends PropertyRangeDT> List<String> extractDistinctProperties(List<T> propertyRanges) {
//        return propertyRanges.stream().map(p -> p.property).distinct().collect(Collectors.toList());
        return propertyRanges.stream().map(p -> PropertyDT.rawProperty(p.property)).distinct().collect(Collectors.toList());
    }

    protected static <T extends PropertyRangeDT> List<T> extractRangeOfProperty(String property, List<T> propertyRanges) {
//        return propertyRanges.stream().filter(p -> p.property.equals(property)).collect(Collectors.toList());
        return propertyRanges.stream().filter(p -> PropertyDT.rawProperty(p.property).equals(property)).collect(Collectors.toList());
    }

    protected static <T extends PropertyRangeDT> List<Integer> createPositions(List<T> propertyRanges) {
        return propertyRanges.stream().map(p -> IntStream.rangeClosed(p.start, p.end).boxed()).
                reduce(Stream::concat).orElseGet(Stream::empty).distinct().collect(Collectors.toList());
    }

//    public static <T extends PropertyRangeDT> List<AlternativeJaccardStatisticDT> findAlternativeAgreement(List<T> p1, Map<String, List<T>> p2, boolean p1First) {
//
//        List<Integer> p1Positions = createPositions(p1);
//
//
//        Map<String,Double> candidates =  p2.entrySet().stream().filter(v -> !v.getKey().equals(p1.get(0).property)).
//                collect(Collectors.toMap(v -> v.getKey(),v -> JaccardStatisticDT.computeJaccardIndex(createPositions(v.getValue()),p1Positions))).
//                 entrySet().stream().filter(v -> v.getValue() >= JaccardStatisticDT.THRESHOLD).
//                 collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue()));
//
//        if(candidates.size() == 0) {
//            return Collections.emptyList();
//        }
//        List<AlternativeJaccardStatisticDT> alternativeJaccardStatistics = new ArrayList<>();
//        for(Map.Entry<String,Double> candidate : candidates.entrySet()) {
//            List<T> entry = p2.get(candidate.getKey());
//            List<Integer> p2Positions = createPositions(entry);
//            List<Integer> p2PositionsSingle = Auxiliary.deepCopy(p2Positions);
//            p2PositionsSingle.removeAll(p1Positions);
//            List<Integer> p1PositionsSingle = Auxiliary.deepCopy(p1Positions);
//            p1PositionsSingle.removeAll(p2Positions);
//
//            if(p1First) {
//                alternativeJaccardStatistics.add(new AlternativeJaccardStatisticDT(p1.get(0).property,p1.get(0).category,candidate.getValue(),
//                        p1Positions,p2Positions,entry.get(0).property,entry.get(0).category));
//            }
////            else {
////                alternativeJaccardStatistics.add(new AlternativeJaccardStatisticDT(p1.get(0).property,p1.get(0).category,candidate.getValue(),
////                        p1Positions.size(),p2Positions.size(),p1PositionsSingle.size(),p2PositionsSingle.size(),entry.get(0).property,entry.get(1).category));
////            }
//
//        }
//        return alternativeJaccardStatistics;
//
//    }
//
//
//    public static <T extends PropertyRangeDT> List<AlternativeJaccardStatisticDT> compareAgreemenAlternativeJaccard(List<T> p1, List<T> p2) {
//        Map<String, List<PropertyRangeDT>> p1PlusMap = p1.stream().collect(Collectors.groupingBy(PropertyRangeDT::getProperty));
//        Map<String, List<PropertyRangeDT>> p2PlusMap = p2.stream().collect(Collectors.groupingBy(PropertyRangeDT::getProperty));
//        Auxiliary.printMap(p1PlusMap);
//        System.out.println("p2");
//        Auxiliary.printMap(p2PlusMap);
//        System.out.println();
//        List<AlternativeJaccardStatisticDT> jaccardStatisticDTS = new ArrayList<>();
//
//
//        for(String property: p1PlusMap.keySet()) {
//
//            if(property.equals("S2")) {
//                System.out.println();
//            }
//            List<AlternativeJaccardStatisticDT> jaccardStatisticDT = findAlternativeAgreement(p1PlusMap.get(property),p2PlusMap,true);
//            if(jaccardStatisticDT.size() > 0) {
//                System.out.println(property);
//                Auxiliary.printSeq(jaccardStatisticDT);
//                System.out.println();
//            }
//
//                jaccardStatisticDTS.addAll(jaccardStatisticDT);
//
//        }
//        return jaccardStatisticDTS;
////        p2PlusMap.keySet().removeIf(p -> p.equals(jaccardStatisticDTS.stream().map(j -> j.property2).collect(Collectors.toList())));
////        for(String property: p2PlusMap.keySet()) {
////            jaccardStatisticDTS.addAll(findAlternativeAgreement(p2PlusMap.get(property),p1PlusMap,))
////        }
//
////        for(boolean annotatedStrand : Arrays.asList(true,false)) {
////
////            for (boolean refSeqStrand : Arrays.asList(true, false)) {
////
////            }
////        }
//    }

    public static <T extends PropertyRangeDT> void groupProperties(List<T> propertyRanges,int length) {
        Map<String, List<T>> propertyMap = propertyRanges.stream().collect(Collectors.groupingBy(PropertyRangeDT::getProperty));
        for(Map.Entry<String,List<T>> entry: propertyMap.entrySet()) {
            List<T> rangeList = entry.getValue();
            Collections.sort(rangeList);
            int counter = 1;

            T prevRange = rangeList.get(0);
            prevRange.setProperty(rangeList.get(0).getProperty() + "_" + counter);
            for(int i = 1; i < rangeList.size(); i++) {
                T nextRange = rangeList.get(i);
                if(nextRange.getStart()-prevRange.getEnd() <= THRESHOLD) {
                    nextRange.setProperty(prevRange.getProperty());
                }
                else {
                    counter++;
                    nextRange.setProperty(nextRange.getProperty()+ "_" + counter);
                }
                prevRange = nextRange;
            }
            if(Auxiliary.mod(rangeList.get(0).start-rangeList.get(rangeList.size()-1).end,length) <= THRESHOLD) {
                rangeList.get(rangeList.size()-1).setProperty(rangeList.get(0).property);
            }
        }
    }


    public static<T extends PropertyRangeProbStrandDT>  Map<String, List<Integer>> createPositionMap(List<T> pList) {
//        return pList.stream().filter(p -> !PropertyDT.rawProperty(p.property).equals(PropertyDT.VOID_IDENTIFIER)).collect(Collectors.groupingBy(p -> p.property + "_" + p.strand)).
//                entrySet().stream().collect(Collectors.toMap(e -> e.getKey(),e -> createPositions(e.getValue())));
        return pList.stream().filter(p -> !PropertyDT.rawProperty(p.property).equals(PropertyDT.VOID_IDENTIFIER)).
                collect(Collectors.groupingBy(p -> PropertyDT.rawProperty(p.property) + "_" + p.strand)).
                entrySet().stream().collect(Collectors.toMap(e -> e.getKey(),e -> createPositions(e.getValue())));
    }

    public static<T extends PropertyRangeProbStrandDT>  Map<String, List<T>> createPropertyMap(List<T> pList) {
        return pList.stream().filter(p -> !PropertyDT.rawProperty(p.property).equals(PropertyDT.VOID_IDENTIFIER)).collect(Collectors.groupingBy(p -> p.property + "_" + p.strand));
    }

    public static<T extends PropertyRangeProbStrandDT>  Map<String, Boolean> createStrandMap(List<T> pList) {
        return pList.stream().filter(p -> !PropertyDT.rawProperty(p.property).equals(PropertyDT.VOID_IDENTIFIER)).collect(Collectors.groupingBy(p -> p.property)).
                entrySet().stream().collect(Collectors.toMap(e -> e.getKey(),e -> e.getValue().get(0).strand));
    }

    public static<T extends PropertyRangeProbStrandDT>  Map<String, String> createCategoryMap(List<T> pList) {
//        return pList.stream().filter(p -> !PropertyDT.rawProperty(p.property).equals(PropertyDT.VOID_IDENTIFIER)).collect(Collectors.groupingBy(p -> p.property + "_" + p.strand)).
//                entrySet().stream().collect(Collectors.toMap(e -> e.getKey(),e -> e.getValue().get(0).category));
        return pList.stream().filter(p -> !PropertyDT.rawProperty(p.property).equals(PropertyDT.VOID_IDENTIFIER)).
                collect(Collectors.groupingBy(p -> PropertyDT.rawProperty(p.property) + "_" + p.strand)).
                entrySet().stream().collect(Collectors.toMap(e -> e.getKey(),e -> e.getValue().get(0).category));
    }

    public static double computeJaccardIndex(List<Integer> p1Positions, List<Integer> p2Positions) {
        List<Integer> intersection = new ArrayList<>(Auxiliary.deepCopy(p1Positions));
        intersection.retainAll(p2Positions);
        LOGGER.debug("Intersection: " + intersection);
        if(intersection.size() == 0) {
            return 0;
        }


        List<Integer> union = new ArrayList<>(Auxiliary.deepCopy(p1Positions));
        union.addAll(p2Positions);
        union = union.stream().distinct().collect(Collectors.toList());
        LOGGER.debug("Union: " + union);

        double jaccardIndex = ((double)intersection.size())/union.size();
        LOGGER.debug("JaccardIndex: " + jaccardIndex);
        return jaccardIndex;
    }

    public static boolean extracStrand(String property) {
        return Boolean.valueOf(property.substring(property.lastIndexOf("_")+1));
    }

    public static List<PropertyRangeProbStrandDT> filterSmallAnnotations(List<PropertyRangeProbStrandDT> annotations) {
        Set<String> genes = filterSmallAnnotations(createPositionMap(annotations).
                entrySet().stream().collect(Collectors.toMap(e -> e.getKey(),e -> e.getValue()))).keySet();
//        System.out.println(genes);
//        System.out.println(annotations.stream().map(p -> PropertyDT.rawProperty(p.property) + "_" + p.strand).collect(Collectors.toList()));
        return  annotations.stream().filter(p ->  genes.contains(PropertyDT.rawProperty(p.property) + "_" + p.strand)).collect(Collectors.toList());
    }

    public static Map<String, List<Integer>> filterSmallAnnotations( Map<String, List<Integer>> propertymap) {


//        System.out.println(propertymap.entrySet().stream().map(m -> PropertyDT.rawProperty(m.getKey())).collect(Collectors.joining(" ")));
        return propertymap.entrySet().stream().filter(m -> m.getValue().size() >=
                meanLengths.get(PropertyDT.rawProperty(m.getKey()))).collect(Collectors.toMap(e -> e.getKey(),e -> e.getValue()));
    }

    public static <T extends PropertyRangeProbStrandDT> List<AlternativeJaccardStatisticDT> compareAgreementJaccard(List<T> p1, List<T> p2) {

        List<AlternativeJaccardStatisticDT> jaccardStatistics = new ArrayList<>();
        final int minSize = 20;
//
//
//        List<String> properties1 = extractDistinctProperties(p1);
//        properties1.remove(voidIdentifier);
//        List<String> properties2 = extractDistinctProperties(p2);
//        properties2.remove(voidIdentifier);


//        List<String> matches = new ArrayList<>(Auxiliary.deepCopy(properties1));
//        matches.retainAll(properties2);
        Map<String, List<Integer>> propertymapP1 = filterSmallAnnotations(createPositionMap(p1).entrySet().stream().collect(Collectors.toMap(e -> e.getKey(),e -> e.getValue())));
//        Map<String, List<Integer>> propertymapP1 = createPositionMap(p1).entrySet().stream().filter(m -> m.getValue().size() >= minSize).
//                collect(Collectors.toMap(e -> e.getKey(),e -> e.getValue()));
        Map<String, List<Integer>>  propertymapP2 = createPositionMap(p2);
//        Map<String,Boolean> strandMapP1 = createStrandMap(p1);
//        Map<String,Boolean> strandMapP2 = createStrandMap(p2);
        Map<String,String> categoryMapP1 = createCategoryMap(p1);
        Map<String,String> categoryMapP2 = createCategoryMap(p2);
        List<String> usedProperties2 = new ArrayList<>();
        double bestJaccardIndex, jaccardIndex;


        for(Map.Entry<String, List<Integer>>   propertyEntry : propertymapP1.entrySet()) {
            String property1 = PropertyDT.rawProperty(propertyEntry.getKey());
            List<Integer> p1Positions = propertyEntry.getValue();
            String category1 = categoryMapP1.get(propertyEntry.getKey());
            boolean strand1 = extracStrand(propertyEntry.getKey());
            String usedProperty = null;
//            System.out.println(property1 + " " + category1 + " " + strand1 + " " + p1Positions.size());
            LOGGER.debug("Property: " + property1);
            bestJaccardIndex = 0;
            AlternativeJaccardStatisticDT jaccardStatistic = null;
            for(Map.Entry<String, List<Integer>>  propertyEntry2 : propertymapP2.entrySet()) {
                String property2 = PropertyDT.rawProperty(propertyEntry2.getKey());
                String category2 = categoryMapP2.get(propertyEntry2.getKey());
                boolean strand2 = extracStrand(propertyEntry2.getKey());
//                System.out.println(property2 + " " + category2 + " " + strand2);
                List<Integer> p2Positions = propertyEntry2.getValue();
                jaccardIndex = computeJaccardIndex(p1Positions,propertyEntry2.getValue());
                if(jaccardIndex > bestJaccardIndex) {
                    int matchFlag = 0;
                    boolean stop = false;
                    if(strand1 != strand2) {
                        if(!property1.equals( property2)) {
                            matchFlag  = -3;
                            if(jaccardIndex <= JaccardStatisticDT.THRESHOLD) {
                                stop = true;
                            }
//                            else {
//                                matchFlag  = -3;
//                            }

                        }
                        else {
                            matchFlag  = -2;
                            if(jaccardIndex <= JaccardStatisticDT.THRESHOLD) {
                                stop = true;
                            }
//                            else {
//                                matchFlag  = -2;
//                            }
                        }
                    }
                    else if(!property1.equals( property2)) {
                        matchFlag  = -1;
                        if(jaccardIndex <= JaccardStatisticDT.THRESHOLD) {
                            stop = true;
                        }
//                        else {
//                            matchFlag  = -1;
//                        }

                    }
                    if(!stop) {

                        jaccardStatistic = new AlternativeJaccardStatisticDT(property1,category1,jaccardIndex,p1Positions,p2Positions,strand1,matchFlag,property2,category2);
//                        System.out.println(jaccardStatistic);
                        bestJaccardIndex = jaccardIndex;
                        usedProperty = propertyEntry2.getKey();
                    }
                    else {
                        jaccardStatistic = new AlternativeJaccardStatisticDT(property1,category1,-1,p1Positions,new ArrayList<>(),strand1,matchFlag,null,null);
//                        System.out.println(jaccardStatistic);
                        bestJaccardIndex = jaccardIndex;
                        usedProperty = propertyEntry2.getKey();
                    }
                }
            }

            if(bestJaccardIndex == 0){
//                System.out.println("Zero fit: ");
                jaccardStatistic = new AlternativeJaccardStatisticDT(property1,category1,-1,p1Positions,new ArrayList<>(),strand1,0,null,null);
                jaccardStatistics.add(jaccardStatistic);
//                System.out.println(jaccardStatistic);
            }
            else {
//                System.out.println("Best fit: ");
//                System.out.println(jaccardStatistic);
                jaccardStatistics.add(jaccardStatistic);
                usedProperties2.add(usedProperty);
            }

        }
        propertymapP2.keySet().removeAll(usedProperties2);
        for(Map.Entry<String, List<Integer>>  propertyEntry : propertymapP2.entrySet()) {
            String property1 = PropertyDT.rawProperty(propertyEntry.getKey());
            List<Integer> p1Positions = propertyEntry.getValue();
            String category1 = categoryMapP2.get(propertyEntry.getKey());
            boolean strand1 = extracStrand(propertyEntry.getKey());
            jaccardStatistics.add(new AlternativeJaccardStatisticDT(property1,category1,-2,new ArrayList<>(),p1Positions,strand1,0,property1,category1));
        }

//        Auxiliary.printSeq(jaccardStatistics);


        return jaccardStatistics;

    }

    public static <T extends PropertyRangeProbStrandDT> List<AlternativeJaccardStatisticDT> compareAgreement_MITOS_Refseq_Jaccard(List<T> p1, List<T> p2) {

        List<AlternativeJaccardStatisticDT> jaccardStatistics = new ArrayList<>();

        Map<String, List<Integer>> propertymapP1 = createPositionMap(p1);

        Map<String, List<Integer>>  propertymapP2 = createPositionMap(p2);

        Map<String,String> categoryMapP1 = createCategoryMap(p1);
        Map<String,String> categoryMapP2 = createCategoryMap(p2);
        List<String> usedProperties2 = new ArrayList<>();
        double bestJaccardIndex, jaccardIndex;


        for(Map.Entry<String, List<Integer>>   propertyEntry : propertymapP1.entrySet()) {
            String property1 = PropertyDT.rawProperty(propertyEntry.getKey());
            List<Integer> p1Positions = propertyEntry.getValue();
            String category1 = categoryMapP1.get(propertyEntry.getKey());
            boolean strand1 = extracStrand(propertyEntry.getKey());
            String usedProperty = null;
//            System.out.println(property1 + " " + category1 + " " + strand1 + " " + p1Positions.size());
            LOGGER.debug("Property: " + property1);
            bestJaccardIndex = 0;
            AlternativeJaccardStatisticDT jaccardStatistic = null;
            for(Map.Entry<String, List<Integer>>  propertyEntry2 : propertymapP2.entrySet()) {
                String property2 = PropertyDT.rawProperty(propertyEntry2.getKey());
                String category2 = categoryMapP2.get(propertyEntry2.getKey());
                boolean strand2 = extracStrand(propertyEntry2.getKey());
//                System.out.println(property2 + " " + category2 + " " + strand2);
                List<Integer> p2Positions = propertyEntry2.getValue();
                jaccardIndex = computeJaccardIndex(p1Positions,propertyEntry2.getValue());
                if(jaccardIndex > bestJaccardIndex) {
                    int matchFlag = 0;
                    boolean stop = false;
                    if(strand1 != strand2) {
                        if(!property1.equals( property2)) {
                            matchFlag  = -3;
                            if(jaccardIndex <= JaccardStatisticDT.THRESHOLD) {
                                stop = true;
                            }
//                            else {
//                                matchFlag  = -3;
//                            }

                        }
                        else {
                            matchFlag  = -2;
                            if(jaccardIndex <= JaccardStatisticDT.THRESHOLD) {
                                stop = true;
                            }
//                            else {
//                                matchFlag  = -2;
//                            }
                        }
                    }
                    else if(!property1.equals( property2)) {
                        matchFlag  = -1;
                        if(jaccardIndex <= JaccardStatisticDT.THRESHOLD) {
                            stop = true;
                        }
//                        else {
//                            matchFlag  = -1;
//                        }

                    }
                    if(!stop) {

                        jaccardStatistic = new AlternativeJaccardStatisticDT(property1,category1,jaccardIndex,p1Positions,p2Positions,strand1,matchFlag,property2,category2);
//                        System.out.println(jaccardStatistic);
                        bestJaccardIndex = jaccardIndex;
                        usedProperty = propertyEntry2.getKey();
                    }
                    else {
                        jaccardStatistic = new AlternativeJaccardStatisticDT(property1,category1,-1,p1Positions,new ArrayList<>(),strand1,matchFlag,null,null);
//                        System.out.println(jaccardStatistic);
                        bestJaccardIndex = jaccardIndex;
                        usedProperty = propertyEntry2.getKey();
                    }
                }
            }

            if(bestJaccardIndex == 0){
//                System.out.println("Zero fit: ");
                jaccardStatistic = new AlternativeJaccardStatisticDT(property1,category1,-1,p1Positions,new ArrayList<>(),strand1,0,null,null);
                jaccardStatistics.add(jaccardStatistic);
//                System.out.println(jaccardStatistic);
            }
            else {
//                System.out.println("Best fit: ");
//                System.out.println(jaccardStatistic);
                jaccardStatistics.add(jaccardStatistic);
                usedProperties2.add(usedProperty);
            }

        }
        propertymapP2.keySet().removeAll(usedProperties2);
        for(Map.Entry<String, List<Integer>>  propertyEntry : propertymapP2.entrySet()) {
            String property1 = PropertyDT.rawProperty(propertyEntry.getKey());
            List<Integer> p1Positions = propertyEntry.getValue();
            String category1 = categoryMapP2.get(propertyEntry.getKey());
            boolean strand1 = extracStrand(propertyEntry.getKey());
            jaccardStatistics.add(new AlternativeJaccardStatisticDT(property1,category1,-2,new ArrayList<>(),p1Positions,strand1,0,property1,category1));
        }

//        Auxiliary.printSeq(jaccardStatistics);


        return jaccardStatistics;

    }

    public static Dataset<PropertyRangeDT> convertToPropertyRange(Dataset<PropertyRangeStrandDT> dataset) {
       return dataset.withColumn(ColumnIdentifier.PROPERTY2,when(col(ColumnIdentifier.STRAND).equalTo(true),concat_ws("",col(ColumnIdentifier.PROPERTY),lit("+"))).otherwise(concat_ws("",col(ColumnIdentifier.PROPERTY),lit("-")))).
               drop(ColumnIdentifier.PROPERTY,ColumnIdentifier.STRAND).withColumnRenamed(ColumnIdentifier.PROPERTY2,ColumnIdentifier.PROPERTY).as(ENCODER);
    }

}
