package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;


import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class RangeStatisticsDT extends RangeDT {

    private double meanCount;
    private int maxCount;
    private int minCount;
    private double kurtosis;
    private double skewness;
    private double stdDeviation;
    private double variance;
    public static final Encoder<RangeStatisticsDT> ENCODER = Encoders.bean(RangeStatisticsDT.class);

    public enum COLUMN_NAME implements DBColumnName {
        MEANCOUNT("meanCount"),
        MAXCOUNT("maxCount"),
        MINCOUNT("minCount"),
        // Univariate normal distribution: 3|
        // Platykurtic i.e. distribution produces fewer and less extreme outliers than normal distribution: < 3|
        // Leptokurtic i.e. distribution produces more outliers than normal distribution: > 3
        KURTOSIS("kurtosis"),
        // Measure for asymmetry. Tail is on the left of distribution: < 0|
        // tail is on the right of distribution: > 0|
        // No simple rule if tail is long on the one and fat on the other side
        SKEWNESS("skewness"),
        // Measure of the dispersion of a dataset relative to its mean,
        // i.e. the more spread out the data, the higher the standard deviation
        STD_DEVIATION("stdDeviation"),
        VARIANCE("variance"),
        START("start"),
        END("end"),
        LENGTH("length");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public RangeStatisticsDT() {

    }

    public double getMeanCount() {
        return meanCount;
    }

    public void setMeanCount(double meanCount) {
        this.meanCount = meanCount;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public int getMinCount() {
        return minCount;
    }

    public void setMinCount(int minCount) {
        this.minCount = minCount;
    }

    public double getKurtosis() {
        return kurtosis;
    }

    public void setKurtosis(double kurtosis) {
        this.kurtosis = kurtosis;
    }

    public double getSkewness() {
        return skewness;
    }

    public void setSkewness(double skewness) {
        this.skewness = skewness;
    }

    public double getStdDeviation() {
        return stdDeviation;
    }

    public void setStdDeviation(double stdDeviation) {
        this.stdDeviation = stdDeviation;
    }

    public double getVariance() {
        return variance;
    }

    public void setVariance(double variance) {
        this.variance = variance;
    }

}