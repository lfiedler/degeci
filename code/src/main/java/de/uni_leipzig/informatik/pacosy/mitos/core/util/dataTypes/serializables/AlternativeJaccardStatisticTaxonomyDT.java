package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class AlternativeJaccardStatisticTaxonomyDT extends AlternativeJaccardStatisticIdDT{
    protected String name;
    protected String taxonomicGroupName;
    protected int taxId;
    public static final Encoder<AlternativeJaccardStatisticTaxonomyDT> ENCODER = Encoders.bean(AlternativeJaccardStatisticTaxonomyDT.class);

    public AlternativeJaccardStatisticTaxonomyDT() {
    }

    public AlternativeJaccardStatisticTaxonomyDT(String name, String taxonomicGroupName, int taxId) {
        this.name = name;
        this.taxonomicGroupName = taxonomicGroupName;
        this.taxId = taxId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaxonomicGroupName() {
        return taxonomicGroupName;
    }

    public void setTaxonomicGroupName(String taxonomicGroupName) {
        this.taxonomicGroupName = taxonomicGroupName;
    }

    public int getTaxId() {
        return taxId;
    }

    public void setTaxId(int taxId) {
        this.taxId = taxId;
    }
}
