package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.*;

public class JaccardStatisticDT implements Serializable {

    public static final double THRESHOLD = 0.7;
    protected String property;
    protected String category;
    protected static final String CATEGORY_QUANTILES_PATH = ProjectDirectoryManager.getGRAPH_DATA_DIR() + "/CATEGORY_QUANTILES";
    protected static final String PROPERTY_QUANTILES_PATH = ProjectDirectoryManager.getGRAPH_DATA_DIR() + "/PROPERTY_QUANTILES";
    /**
     *|union|/|difference|
     * -1: only included in p1
     * -2: only included in p2
     * -3 strand switch
     */
    protected double jaccardIndex;
    protected int p1Size;
    protected int p2Size;
    /***
     * 0 same properties
     * -1 alternative properties
     * -2 strandswitch
     * -3 alternative + strandswitch
     */
    protected int matchFlag;

    protected int p1NoP2Size;
    protected int p2NoP1Size;
    protected List<Integer> positions1;
    protected List<Integer> positions2;
    protected boolean strand;
    public static final Encoder<JaccardStatisticDT> ENCODER = Encoders.bean(JaccardStatisticDT.class);


    public JaccardStatisticDT() {
    }

//    public JaccardStatisticDT(String property, String category, double jaccardIndex, int p1Size, int p2Size, int p1NoP2Size, int p2NoP1Size) {
//        this.property = property;
//        this.category = category;
//        this.jaccardIndex = jaccardIndex;
//        this.p1Size = p1Size;
//        this.p2Size = p2Size;
//        this.p1NoP2Size = p1NoP2Size;
//        this.p2NoP1Size = p2NoP1Size;
//    }

    public JaccardStatisticDT(String property, String category,
                              double jaccardIndex, List<Integer> positions1, List<Integer> positions2, boolean strand,int matchFlag) {
        this.property = property;
        this.category = category;
        this.jaccardIndex = jaccardIndex;
        this.positions1 = positions1;
        this.positions2 = positions2;
        this.p1Size = positions1.size();
        this.p2Size = positions2.size();
        this.strand = strand;
        this.matchFlag = matchFlag;

        List<Integer> differenceP1 = new ArrayList<>(Auxiliary.deepCopy(positions1));
        differenceP1.removeAll(positions2);
        p1NoP2Size = differenceP1.size();
        List<Integer> differenceP2 = new ArrayList<>(Auxiliary.deepCopy(positions2));
        differenceP2.removeAll(positions1);
        p2NoP1Size = differenceP2.size();
    }

    public int getMatchFlag() {
        return matchFlag;
    }

    public void setMatchFlag(int matchFlag) {
        this.matchFlag = matchFlag;
    }

    public boolean isStrand() {
        return strand;
    }

    public void setStrand(boolean strand) {
        this.strand = strand;
    }

    public List<Integer> getPositions1() {
        return positions1;
    }

    public void setPositions1(List<Integer> positions1) {
        this.positions1 = positions1;
    }

    public List<Integer> getPositions2() {
        return positions2;
    }

    public void setPositions2(List<Integer> positions2) {
        this.positions2 = positions2;
    }

    public int getP1NoP2Size() {
        return p1NoP2Size;
    }

    public void setP1NoP2Size(int p1NoP2Size) {
        this.p1NoP2Size = p1NoP2Size;
    }

    public int getP2NoP1Size() {
        return p2NoP1Size;
    }

    public void setP2NoP1Size(int p2NoP1Size) {
        this.p2NoP1Size = p2NoP1Size;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getJaccardIndex() {
        return jaccardIndex;
    }

    public void setJaccardIndex(double jaccardIndex) {
        this.jaccardIndex = jaccardIndex;
    }

    public int getP1Size() {
        return p1Size;
    }

    public void setP1Size(int p1Size) {
        this.p1Size = p1Size;
    }

    public int getP2Size() {
        return p2Size;
    }

    public void setP2Size(int p2Size) {
        this.p2Size = p2Size;
    }

    @Override
    public String toString() {
        return property + " " + category + " JI: " + jaccardIndex + " p1Size: " + p1Size + " p2Size: " + p2Size + " p1NoP2Size: " + p1NoP2Size + " p2NoP1Size: " + p2NoP1Size;
    }



//    static <T extends JaccardStatisticDT> void  attributeQuantiles(
//            Dataset<T> jaccardStatistic, List<String> properties, String attributeIdentifier, String quantilePath, String outlierPath) {
//
//        List<OutlierDT> outlier = new ArrayList<>();
//        List<QuantileDT> quantiles = new ArrayList<>();
//        final String binIndexIdentifier = "binIndex";
//        double[] binArray = new double[] {0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1};
//        Bucketizer bucketTizer = new Bucketizer().setInputCol(ColumnIdentifier.JACCARD_INDEX)
//                .setOutputCol(binIndexIdentifier).setSplits(binArray);
//
//        for(String property : properties) {
//
//
//
//            System.out.println(property);
//            Dataset<T> filteredJaccardStatistic =
//                    jaccardStatistic.filter(col(attributeIdentifier).equalTo(property)).
//                            filter(col(ColumnIdentifier.JACCARD_INDEX).geq(0));
//            Dataset<Row> histogram = bucketTizer.transform(filteredJaccardStatistic).groupBy(binIndexIdentifier).
//                    count().withColumn("relative",(col(ColumnIdentifier.COUNT).divide(lit(filteredJaccardStatistic.count()))).multiply(100)).
//                    orderBy(binIndexIdentifier);
//            SparkComputer.showAll(histogram);
//            if(!filteredJaccardStatistic.isEmpty()) {
//
////                double[] cumDis = filteredJaccardStatistic.
////                        stat().
////                        approxQuantile(ColumnIdentifier.JACCARD_INDEX, binArray, 0);
////                for(double c : cumDis) {
////                    System.out.print(c +", ");
////                }
////                System.out.println();
//                double[] q = filteredJaccardStatistic.
//                        stat().
//                        approxQuantile(ColumnIdentifier.JACCARD_INDEX, new double[]{0.25, 0.5, 0.75}, 0);
//                double iqr = q[2]-q[0];
//                Double lowerRange = Math.max(0,q[0]- 1.5*iqr);
//                Double upperRange = Math.min(1,q[2] + 1.5*iqr);
//                System.out.println(lowerRange + " " + upperRange);
//                Double upperWhisker;
//                Double lowerWhisker;
//
//                try {
//                    upperWhisker = filteredJaccardStatistic.
//                            filter(col(ColumnIdentifier.JACCARD_INDEX).gt(q[2]).and(col(ColumnIdentifier.JACCARD_INDEX).leq(upperRange))).
//                            groupBy().max(ColumnIdentifier.JACCARD_INDEX).first().getDouble(0);
//                } catch(NullPointerException e) {
//                    upperWhisker = q[2];
//                }
//
//
//                try {
//                    lowerWhisker = filteredJaccardStatistic.
//                            filter(col(ColumnIdentifier.JACCARD_INDEX).geq(lowerRange).and(col(ColumnIdentifier.JACCARD_INDEX).leq(q[0]))).
//                            groupBy().agg(
//                            min(col(ColumnIdentifier.JACCARD_INDEX)).cast(DataTypes.DoubleType).as(ColumnIdentifier.MIN)).first().getDouble(0);
//                } catch (NullPointerException e) {
//                    lowerWhisker = q[0];
//                }
//
//                outlier.addAll(filteredJaccardStatistic.
//                        filter(col(ColumnIdentifier.JACCARD_INDEX).lt(lowerRange).
//                                or(col(ColumnIdentifier.JACCARD_INDEX).gt(upperRange))).
//                        withColumn(ColumnIdentifier.ATTRIBUTE,lit(property).cast(DataTypes.StringType)).
//                        select(col(ColumnIdentifier.JACCARD_INDEX).as(ColumnIdentifier.VALUE),col(ColumnIdentifier.ATTRIBUTE)).
//                        as(OutlierDT.ENCODER).collectAsList());
//
//                Row minMax =filteredJaccardStatistic.groupBy().
//                        agg(
//                                min(col(ColumnIdentifier.JACCARD_INDEX)).cast(DataTypes.DoubleType).as(ColumnIdentifier.MIN),
//                                max(col(ColumnIdentifier.JACCARD_INDEX)).cast(DataTypes.DoubleType).as(ColumnIdentifier.MAX)).first();
//
//                QuantileDT quantileDT = new QuantileDT(property,q,minMax.getDouble(0),minMax.getDouble(1),lowerWhisker,upperWhisker);
//                System.out.println(quantileDT);
//                quantiles.add(quantileDT);
//            }
//            else {
//                System.out.println("not contained");
//            }
//            System.out.println();
//        }
//        try {
//            SparkComputer.writeToCSV(SparkComputer.createDataFrame(quantiles,QuantileDT.ENCODER),quantilePath);
//            SparkComputer.writeToCSV(SparkComputer.createDataFrame(outlier,OutlierDT.ENCODER),outlierPath);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }

    public static String quantilePropertyPath(String subFolderName) {
        new File(PROPERTY_QUANTILES_PATH + "/" + subFolderName ).mkdirs();
        return PROPERTY_QUANTILES_PATH + "/" + subFolderName + "/quantiles";
    }

    public static String quantileCategoryPath(String subFolderName) {
        new File(CATEGORY_QUANTILES_PATH + "/" + subFolderName ).mkdirs();
        return CATEGORY_QUANTILES_PATH + "/" + subFolderName + "/quantiles";
    }

    public static String outlierPropertyPath(String subFolderName) {
        new File(PROPERTY_QUANTILES_PATH + "/" + subFolderName ).mkdirs();
        return PROPERTY_QUANTILES_PATH + "/" + subFolderName + "/outlier";
    }

    public static String outlierCategoryPath(String subFolderName) {
        new File(CATEGORY_QUANTILES_PATH + "/" + subFolderName ).mkdirs();
        return CATEGORY_QUANTILES_PATH + "/" + subFolderName + "/outlier";
    }

//    public static <T extends JaccardStatisticDT> void propertyQuantiles(Dataset<T> jaccardStatistic, String subFolderName) {
//        List<String> properties = new ArrayList<>();
//        FileIO.readSeqFromFile(ProjectDirectoryManager.getGRAPH_FILES_DIR() + "/allProperties.txt",properties,false,0,Long.MAX_VALUE,String.class);
//        attributeQuantiles(jaccardStatistic,properties, ColumnIdentifier.PROPERTY,quantilePropertyPath(subFolderName),outlierPropertyPath(subFolderName));
//
//    }
//
//    public static <T extends JaccardStatisticDT> void categoryQuantiles(Dataset<T> jaccardStatistic, String subFolderName) {
//        List<String> properties = new ArrayList<>(Arrays.asList("protein","trna","rrna","repOrigin"));
////        FileIO.readSeqFromFile(ProjectDirectoryManager.getGRAPH_FILES_DIR() + "/allCategories.txt",properties,false,0,Long.MAX_VALUE,String.class);
//        attributeQuantiles(jaccardStatistic,properties,ColumnIdentifier.CATEGORY,quantileCategoryPath(subFolderName),outlierCategoryPath(subFolderName));
//
//    }

    public static <T extends JaccardStatisticTaxonomyDT> void attributeMissMatches(Dataset<T> jaccardStatistic, List<String> categories,  String subFolderName) {
        Dataset<T> filteredJaccardStatistic =
                jaccardStatistic.
                        filter(col(ColumnIdentifier.JACCARD_INDEX).lt(0));
        String attributeIdentifier = ColumnIdentifier.CATEGORY;
        for(String property : categories) {
            System.out.println(property);

            Dataset<T> propertyFilteredJaccardStatistic = filteredJaccardStatistic.filter(col(attributeIdentifier).equalTo(property));

            SparkComputer.showAll(propertyFilteredJaccardStatistic.orderBy(col(ColumnIdentifier.PROPERTY)));
            Dataset<T> p1Filtered = propertyFilteredJaccardStatistic.filter(col(ColumnIdentifier.JACCARD_INDEX).equalTo(-1));
            Dataset<T> p2Filtered = propertyFilteredJaccardStatistic.filter(col(ColumnIdentifier.JACCARD_INDEX).equalTo(-2));
            long falsePositiveCount = p1Filtered.count();
            long falseNegativeCount = p2Filtered.count();
            System.out.println("FalsePositiveCount: " + falsePositiveCount + "\nFalseNegativeCount: " + falseNegativeCount);
            SparkComputer.showAll(p1Filtered.groupBy(col(ColumnIdentifier.P1_SIZE)).count().
                    orderBy(ColumnIdentifier.P1_SIZE));
            System.out.println("properties only  p1");
            SparkComputer.showAll(p1Filtered.groupBy(col(ColumnIdentifier.PROPERTY)).count().orderBy(ColumnIdentifier.COUNT));
            System.out.println("properties only p2");
            SparkComputer.showAll(p2Filtered.groupBy(col(ColumnIdentifier.PROPERTY)).count().orderBy(ColumnIdentifier.COUNT));
            System.out.println("Taxonomic distribution p1");
            SparkComputer.showAll(p1Filtered.groupBy(ColumnIdentifier.TAXONOMIC_GROUP_NAME).count().orderBy(ColumnIdentifier.COUNT));
            System.out.println("Taxonomic distribution p2");
            SparkComputer.showAll(p2Filtered.groupBy(ColumnIdentifier.TAXONOMIC_GROUP_NAME).count().orderBy(ColumnIdentifier.COUNT));

        }
    }

    public static double computeJaccardIndex(List<Integer> positions1, List<Integer> positions2) {

        List<Integer> p1Positions = Auxiliary.deepCopy(positions1);
        List<Integer> p2Positions = Auxiliary.deepCopy(positions2);


        List<Integer> intersection = new ArrayList<>(Auxiliary.deepCopy(p1Positions));
        intersection.retainAll(p2Positions);



        List<Integer> union = new ArrayList<>(Auxiliary.deepCopy(p1Positions));
        union.addAll(p2Positions);
        union = union.stream().distinct().collect(Collectors.toList());

        return  ((double)intersection.size())/union.size();

    }

}
