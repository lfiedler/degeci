package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class RecordWeightCountDT extends WeightCountDT{
    protected int recordId;
    public static final Encoder<RecordWeightCountDT> ENCODER = Encoders.bean(RecordWeightCountDT.class);
    public RecordWeightCountDT() {
    }

    public RecordWeightCountDT(long weight, long count, int recordId) {
        super(weight, count);
        this.recordId = recordId;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }
}
