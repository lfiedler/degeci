package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.IdentifierInterface;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.PositionInterface;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import javax.xml.crypto.Data;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class PositionStrandCountIdentifierDT implements PositionInterface, IdentifierInterface, Serializable {
    private String identifier;
    private int countPlus;
    private int countMinus;
    private int position;
    public static final StructType SCHMEA;

    static {
        List<StructField> structFields = new ArrayList<>();
        structFields.add(DataTypes.createStructField(ColumnIdentifier.IDENTIFIER,DataTypes.StringType,true));
        structFields.add(DataTypes.createStructField(COLUMN_NAME.COUNT_MINUS.identifier,DataTypes.IntegerType,true));
        structFields.add(DataTypes.createStructField(ColumnIdentifier.POSITION, DataTypes.IntegerType,true));
        structFields.add(DataTypes.createStructField(COLUMN_NAME.COUNT_PLUS.identifier,DataTypes.IntegerType,true));
        SCHMEA = DataTypes.createStructType(structFields);
    }

    public static final Encoder<PositionStrandCountIdentifierDT> ENCODER = Encoders.bean(PositionStrandCountIdentifierDT.class);

    public enum COLUMN_NAME implements DBColumnName {
        IDENTIFIER("identifier"),
        COUNT_MINUS("countMinus"),
        COUNT_PLUS("countPlus"),
        POSITION("position");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }


    public PositionStrandCountIdentifierDT() {

    }

    public PositionStrandCountIdentifierDT(String identifier, int countPlus, int countMinus, int position) {
        this.identifier = identifier;
        this.countPlus = countPlus;
        this.countMinus = countMinus;
        this.position = position;
    }

    public static Dataset<PositionCountIdentifierDT> filterForStrand(Dataset<PositionStrandCountIdentifierDT> dataset, boolean strand) {
        return dataset.filter(strand ? (FilterFunction<PositionStrandCountIdentifierDT>)(s -> s.getCountPlus() > 0) :
                (FilterFunction<PositionStrandCountIdentifierDT>)(( s -> s.getCountMinus() > 0))).
                withColumnRenamed(
                        strand
                                ? ColumnIdentifier.COUNT_PLUS
                                : ColumnIdentifier.COUNT_MINUS,
                        ColumnIdentifier.COUNT
                ).
                select(ColumnIdentifier.IDENTIFIER,ColumnIdentifier.POSITION, ColumnIdentifier.COUNT).
                as(PositionCountIdentifierDT.ENCODER).
                orderBy(ColumnIdentifier.POSITION);
    }

    public static FilterFunction<PositionStrandCountIdentifierDT> minusCountGtFilter(int minCount) {
        return k -> k.getCountMinus() > minCount;
    }

    public static FilterFunction<PositionStrandCountIdentifierDT> plusCountGtFilter(int minCount) {
        return k -> k.getCountPlus() > minCount;
    }

    public static FilterFunction<PositionStrandCountIdentifierDT> minusCountGtZeroFilter() {
        return minusCountGtFilter(0);
    }

    public static FilterFunction<PositionStrandCountIdentifierDT> plusCountGtZeroFilter() {
        return plusCountGtFilter(0);
    }

    public static Dataset<PositionStrandCountIdentifierDT> convert(Dataset<?> dataset) {
        return dataset.select(ColumnIdentifier.IDENTIFIER,
                ColumnIdentifier.POSITION,
                ColumnIdentifier.COUNT_MINUS,
                ColumnIdentifier.COUNT_PLUS).as(ENCODER);
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    @Override
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public int getCountPlus() {
        return countPlus;
    }

    public void setCountPlus(int countPlus) {
        this.countPlus = countPlus;
    }

    public int getCountMinus() {
        return countMinus;
    }

    public void setCountMinus(int countMinus) {
        this.countMinus = countMinus;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public void setPosition(int position) {
        this.position = position;
    }
}
