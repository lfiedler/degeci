package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

public interface DBColumnName {
    String getIdentifier();

    static String rawIdentifier(DBColumnName column_name) {
        return column_name.getIdentifier().replaceAll("\"","");
    }

    static String getValuesAsString(DBColumnName[] dbColumnNames) {
        String columnStr = "";
        for(int i = 0; i < dbColumnNames.length; i++) {
            columnStr += dbColumnNames[i].getIdentifier() + ",";
        }
        return columnStr.substring(columnStr.length()-1);
    }

    default String rawIdentifier() {
        return rawIdentifier(this);
    }

    static String [] getStringArray(DBColumnName[] dbColumnNames) {
        String [] string_column_names = new String [dbColumnNames.length];
        for(int i = 0; i < dbColumnNames.length; i++) {
            string_column_names[i] = dbColumnNames[i].getIdentifier();
        }
        return string_column_names;
    }

    static void printColumnNames( DBColumnName[] column_names) {

        for(int i = 0; i < column_names.length; i++) {
            System.out.println(column_names[i].getIdentifier());
        }
    }


}
