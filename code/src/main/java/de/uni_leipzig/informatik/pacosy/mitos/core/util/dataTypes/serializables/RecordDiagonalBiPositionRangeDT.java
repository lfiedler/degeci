package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;

import static org.apache.spark.sql.functions.*;

public class RecordDiagonalBiPositionRangeDT extends RecordDiagonalPositionRangeDT {
    public static final Encoder<RecordDiagonalBiPositionRangeDT> ENCODER = Encoders.bean(RecordDiagonalBiPositionRangeDT.class);
    private int recordStart;
    private int recordEnd;
    public enum COLUMN_NAME implements DBColumnName {
        RECORD_START("\"recordStart\""),
        RECORD_END("\"recordEnd\""),
        RECORDID("\"recordId\""),
        DIAGONAL_IDENTIFIER("\"diagonalValue\""),
        START("start"),
        END("end"),
        LENGTH("length");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public RecordDiagonalBiPositionRangeDT() {
    }

    public RecordDiagonalBiPositionRangeDT(int start, int end, int recordId, int diagonalValue, int recordLength, int recordStart, int recordEnd) {
        super(start, end, recordId, diagonalValue,recordLength);
        this.recordStart = recordStart;
        this.recordEnd = recordEnd;
    }

    public int getRecordStart() {
        return recordStart;
    }

    public void setRecordStart(int recordStart) {
        this.recordStart = recordStart;
    }

    public int getRecordEnd() {
        return recordEnd;
    }

    public void setRecordEnd(int recordEnd) {
        this.recordEnd = recordEnd;
    }

    public static Dataset<RecordDiagonalBiPositionRangeDT> convertToRecordDiagonalBiPositionRange(Dataset<?> dataset) {
        return dataset.select(
                COLUMN_NAME.RECORDID.rawIdentifier(),
                ColumnIdentifier.RECORD_LENGTH,
                COLUMN_NAME.DIAGONAL_IDENTIFIER.rawIdentifier(),
                COLUMN_NAME.START.identifier,
                COLUMN_NAME.END.identifier,
                COLUMN_NAME.RECORD_START.rawIdentifier(),
                COLUMN_NAME.RECORD_END.rawIdentifier(),
                COLUMN_NAME.LENGTH.identifier,
                ColumnIdentifier.STRAND).
                as(ENCODER);
    }

    public static Dataset<Row> getStatistics(Dataset<RecordDiagonalBiPositionRangeDT> recordDiagonalBiPositionRanges) {
        return recordDiagonalBiPositionRanges.groupBy(ColumnIdentifier.RECORD_ID).
                agg(sum(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.SUM),
                        round(mean(col(ColumnIdentifier.LENGTH)),2).as(ColumnIdentifier.MEAN),
                        min(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.MIN),
                        max(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.MAX),
                        collect_list(col(ColumnIdentifier.LENGTH)));
    }
}
