package de.uni_leipzig.informatik.pacosy.mitos.core.graphs;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class GraphFactory {

    private static Logger LOGGER = LoggerFactory.getLogger(GraphFactory.class);

    private GraphFactory() {

    }

    public static Graph createGraph(String keyspace) {

        if(keyspace.equals(TaxonomyGraph.KEYSPACE)) {
            return new TaxonomyGraph();
        }
        else {
            LOGGER.error("Invalid keyspace " + keyspace);
            System.exit(-1);
        }
        return null;
    }
}
