package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class GapStatisticIdDT extends GapStatisticDT{
    String name;
    public static final Encoder<GapStatisticIdDT> ENCODER = Encoders.bean(GapStatisticIdDT.class);

//    public GapStatisticIdDT(long gapCount, long unbridgedCount, String name) {
//        super(gapCount, unbridgedCount);
//        this.name = name;
//    }

    public GapStatisticIdDT(String id, GapStatisticDT gapStatistic) {
        this.gapCount = gapStatistic.gapCount;
        this.unbridgedCount = gapStatistic.unbridgedCount;
        this.name = id;
    }

    public GapStatisticIdDT() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
