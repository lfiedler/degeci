package de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess;

import com.sun.org.apache.bcel.internal.generic.BREAKPOINT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.ColumnIdentifier;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.RangeDT;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.execution.columnar.INT;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;

import java.sql.DatabaseMetaData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.apache.spark.sql.functions.*;

public class ColumnFunctions {

//    public static Column meanSquare(Column arrayColumn, int offset) {
//        return element_at(arrayColumn,size(arrayColumn).minus(offset));
//    }

    public static Column arrayElementFromBack(Column arrayColumn, int offset) {
        return element_at(arrayColumn,size(arrayColumn).minus(offset));
    }

    public static Column lastArrayElement(Column arrayColumn) {
        return arrayElementFromBack(arrayColumn,0);
    }

    public static Column lastArrayElement(String arrayColumn) {
        return lastArrayElement(col(arrayColumn));
    }

    public static Column preLastArrayElement(Column arrayColumn) {
        return arrayElementFromBack(arrayColumn,1);
    }

    public static Column preLastArrayElement(String arrayColumn) {
        return preLastArrayElement(col(arrayColumn));
    }

    public static Column arrayRemoveLastElement(String arrayColumn, String sizeColumn) {
        return expr("slice("+ arrayColumn + ",1,"+ sizeColumn + "-1)");
    }

    public static Column arrayRemoveLastElement(String arrayColumn) {
        return expr("slice("+ arrayColumn + ",1,"+ size(col(arrayColumn)) + "-1)");
    }

    public static Column rangeFilter(String column, int start, int stop) {
        return col(column).geq(start).and(col(column).leq(stop));
    }

    public static Column getLinearRangeLength(String startColumn, String endColumn) {
        return getLinearRangeLength(col(startColumn),col(endColumn));
    }

    public static Column getLinearRangeLength(Column startColumn, Column endColumn) {
        return endColumn.minus(startColumn).plus(1);
    }

    public static Column getCyclicRangeLength(String startColumn, String endColumn, String lengthColumn) {
        return getCyclicRangeLength(col(startColumn),col(endColumn),col(lengthColumn));
    }

    public static Column getCyclicRangeLength(Column startColumn, Column endColumn, Column lengthColumn) {
        return pmod(endColumn.minus(startColumn),lengthColumn).plus(1);
//        return when(startColumn.gt(endColumn),lengthColumn.minus(startColumn).plus(endColumn).plus(1)).otherwise(endColumn.minus(startColumn).plus(1));
    }

    public static Column getCyclicRangeLength(Column startColumn, Column endColumn, Column lengthColumn, Column strandColumn) {
        return when(strandColumn,getCyclicRangeLength(startColumn,endColumn,lengthColumn)).otherwise(getCyclicRangeLength(endColumn,startColumn,lengthColumn));
//        return when(startColumn.gt(endColumn),lengthColumn.minus(startColumn).plus(endColumn).plus(1)).otherwise(endColumn.minus(startColumn).plus(1));
    }

    public static Column cyclicAdd(Column p1, Column p2, Column length) {
        return pmod(p1.plus(p2),length);
    }

    public static Column cyclicSubstract(Column p1, Column p2, Column length) {
        return pmod(p1.minus(p2),length);
    }

    public static Column cyclicSubstract(Column p1, Column p2, Column length, Column strand) {
        return
                when(strand,cyclicSubstract(p1,p2,length)).otherwise(cyclicAdd(p2,p1,length));
    }

    public static Column substring(String sequenceColumn, String positionColumn, String lengthColumn) {
        return expr("substring("+col(sequenceColumn) +","+ col(positionColumn).plus(1)+","+ col(lengthColumn)+")");
    }

    public static Column substring(Column sequenceColumn, Column positionColumn, Column lengthColumn) {
        return expr("substring("+sequenceColumn +","+ positionColumn.plus(1)+","+ lengthColumn+")");
    }

    public static Column getRightNeighbourDistance(Column positionIdentifier, Column lengthIdentifier,Column strandIdentifier,Column distance) {
        return when(strandIdentifier.equalTo(true),pmod(positionIdentifier.plus(distance),lengthIdentifier)).
                otherwise(pmod(positionIdentifier.minus(distance),lengthIdentifier) );
    }


//    public static Column getRightNeighbourDistance(Column positionIdentifier, Column lengthIdentifier,Column maxDistance) {
//        return when(col(strandIdentifier).equalTo(true),getRightNeighbour(positionIdentifier,lengthIdentifier)).
//                otherwise(getLeftNeighbour(positionIdentifier,lengthIdentifier));
//    }

    public static Column getRightNeighbour(String positionIdentifier, String lengthIdentifier,String strandIdentifier) {
        return when(col(strandIdentifier).equalTo(true),getRightNeighbour(positionIdentifier,lengthIdentifier)).
                otherwise(getLeftNeighbour(positionIdentifier,lengthIdentifier));
    }

    public static Column getRightNeighbour(String positionIdentifier, String lengthIdentifier) {
        return getRightNeighbour(col(positionIdentifier),col(lengthIdentifier));
//        return least(col(positionIdentifier).plus(1),col(lengthIdentifier).minus(1));
//        when(col(positionIdentifier).lt(col(lengthIdentifier).minus(1)))
    }

    public static Column getRightNeighbour(Column positionIdentifier, Column lengthIdentifier) {
        return pmod(positionIdentifier.plus(1),lengthIdentifier);
//                when(positionIdentifier.equalTo(lengthIdentifier.minus(1)),0).otherwise(positionIdentifier.plus(1));
//                least(positionIdentifier.plus(1),lengthIdentifier.minus(1));
//        when(col(positionIdentifier).lt(col(lengthIdentifier).minus(1)))
    }

    public static Column getLeftNeighbour(String positionIdentifier, String lengthIdentifier, String strandIdentifier) {
        return when(col(strandIdentifier).equalTo(true),getLeftNeighbour(positionIdentifier,lengthIdentifier)).
                otherwise(getRightNeighbour(positionIdentifier,lengthIdentifier));
    }

    public static Column getLeftNeighbour(String positionIdentifier, String lengthIdentifier) {
        return getLeftNeighbour(col(positionIdentifier),col(lengthIdentifier));
    }

    public static Column getLeftNeighbour(Column positionIdentifier,Column lengthIdentifier) {
        return pmod(positionIdentifier.minus(1),lengthIdentifier);
//                greatest(positionIdentifier.minus(1),lit(0));
    }

    public static Column getDuplicateFreeOrderedSequence(Column start, Column end, Object na) {
        return when(start.equalTo(end),array(start)).otherwise(when(start.gt(end),na).otherwise(sequence(start,end)));
    }

    public static Column computePlusStrandCyclicDistanceExcludingBoundaryPositions(Column positionIdentifier1,Column positionIdentifier2, Column lengthIdentifier) {

        return
                when(positionIdentifier2.lt(positionIdentifier1),
                        lengthIdentifier.minus(1).minus(positionIdentifier1).plus(positionIdentifier2)
                ).otherwise(positionIdentifier2.minus(positionIdentifier1).minus(1));
    }

    public static Column computeDistanceExcludingBoundaryPositions(Column positionIdentifier1,Column positionIdentifier2, Column lengthIdentifier,
                                                                   Column topologyIdentifier, Column strandIdentifier) {

        return
                when(topologyIdentifier.equalTo(true),
                        when(strandIdentifier.equalTo(true),
                                pmod(positionIdentifier2.minus(positionIdentifier1),lengthIdentifier).minus(1)
//                                when(positionIdentifier2.lt(positionIdentifier1),
//                                        lengthIdentifier.minus(1).minus(positionIdentifier1).plus(positionIdentifier2)
//                                ).otherwise(positionIdentifier2.minus(positionIdentifier1).minus(1))

                        ).
                                otherwise(
                                        when(positionIdentifier1.lt(positionIdentifier2),
                                                lengthIdentifier.minus(1).minus(positionIdentifier2).plus(positionIdentifier1)
                                        ).otherwise(positionIdentifier1.minus(positionIdentifier2).minus(1)))
                ).
                        otherwise(
                                when(strandIdentifier.equalTo(true),
                                        when(positionIdentifier2.lt(positionIdentifier1), Integer.MAX_VALUE).
                                                otherwise(positionIdentifier2.minus(positionIdentifier1).minus(1))).
                                        otherwise(
                                                when(positionIdentifier2.gt(positionIdentifier1), Integer.MAX_VALUE).
                                                        otherwise(positionIdentifier1.minus(positionIdentifier2).minus(1))
                                        )
                        );
    }

    public static Column computeSmallestDistanceExcludingBoundaryPositionsSigned(Column positionIdentifier1,Column positionIdentifier2, Column lengthIdentifier,
                                                                           Column topologyIdentifier, Column strandIdentifier) {

        return when(computeDistanceExcludingBoundaryPositions(positionIdentifier1,positionIdentifier2,lengthIdentifier,topologyIdentifier,strandIdentifier).
                leq(computeDistanceExcludingBoundaryPositions(positionIdentifier2,positionIdentifier1,lengthIdentifier,topologyIdentifier,strandIdentifier)),
                        computeDistanceExcludingBoundaryPositions(positionIdentifier1,positionIdentifier2,lengthIdentifier,topologyIdentifier,strandIdentifier)
                        ).otherwise(computeDistanceExcludingBoundaryPositions(positionIdentifier2,positionIdentifier1,lengthIdentifier,topologyIdentifier,strandIdentifier).multiply(-1));
    }

    public static Column computeSmallestDistanceExcludingBoundaryPositions(Column positionIdentifier1,Column positionIdentifier2, Column lengthIdentifier,
                                                                   Column topologyIdentifier, Column strandIdentifier) {

        return least(computeDistanceExcludingBoundaryPositions(positionIdentifier1,positionIdentifier2,lengthIdentifier,topologyIdentifier,strandIdentifier),
                computeDistanceExcludingBoundaryPositions(positionIdentifier2,positionIdentifier1,lengthIdentifier,topologyIdentifier,strandIdentifier)
                );
    }

    public static Column computeSmallestDistance(Column positionIdentifier1,Column positionIdentifier2, Column lengthIdentifier,
                                                                           Column topologyIdentifier, Column strandIdentifier) {

        return least(computeDistanceExcludingBoundaryPositions(positionIdentifier1,positionIdentifier2,lengthIdentifier,topologyIdentifier,strandIdentifier),
                computeDistanceExcludingBoundaryPositions(positionIdentifier2,positionIdentifier1,lengthIdentifier,topologyIdentifier,strandIdentifier)
        ).plus(1);
    }

    public static Column computeSmallestDistanceSigned(Column positionIdentifier1,Column positionIdentifier2, Column lengthIdentifier,
                                                                                 Column topologyIdentifier, Column strandIdentifier) {

        return when(computeSmallestDistance(positionIdentifier1,positionIdentifier2,lengthIdentifier,topologyIdentifier,strandIdentifier).
                        leq(computeSmallestDistance(positionIdentifier2,positionIdentifier1,lengthIdentifier,topologyIdentifier,strandIdentifier)),
                computeSmallestDistance(positionIdentifier1,positionIdentifier2,lengthIdentifier,topologyIdentifier,strandIdentifier)
        ).otherwise(computeSmallestDistance(positionIdentifier2,positionIdentifier1,lengthIdentifier,topologyIdentifier,strandIdentifier).multiply(-1));
    }

    public static Column computeDistanceExcludingBoundaryPositions(String positionIdentifier1,String positionIdentifier2, String lengthIdentifier,
                                                                   String topologyIdentifier, String strandIdentifier) {
        return computeDistanceExcludingBoundaryPositions(col(positionIdentifier1),col(positionIdentifier2),col(lengthIdentifier),col(topologyIdentifier),col(strandIdentifier));
//        return
//                when(col(topologyIdentifier).equalTo(true),
//                        when(col(strandIdentifier).equalTo(true),
//                                when(col(positionIdentifier2).lt(col(positionIdentifier1)),
//                                        col(lengthIdentifier).minus(1).minus(col(positionIdentifier1)).plus(col(positionIdentifier2))
//                                ).otherwise(col(positionIdentifier2).minus(col(positionIdentifier1)).minus(1))).
//                                otherwise(
//                                        when(col(positionIdentifier1).lt(col(positionIdentifier2)),
//                                                col(lengthIdentifier).minus(1).minus(col(positionIdentifier2)).plus(col(positionIdentifier1))
//                                        ).otherwise(col(positionIdentifier1).minus(col(positionIdentifier2)).minus(1)))
//                ).
//                        otherwise(
//                                when(col(strandIdentifier).equalTo(true),
//                                        when(col(positionIdentifier2).lt(col(positionIdentifier1)), Integer.MAX_VALUE).
//                                                otherwise(col(positionIdentifier2).minus(col(positionIdentifier1)).minus(1))).
//                                        otherwise(
//                                                when(col(positionIdentifier2).gt(col(positionIdentifier1)), Integer.MAX_VALUE).
//                                                        otherwise(col(positionIdentifier1).minus(col(positionIdentifier2)).minus(1))
//                                        )
//                        );
    }

    public static Column toArray(List<?> list){
        return array(list.stream().map(functions::lit).toArray(Column[]::new));
    }


    public static Column cyclicIntermediatePositions(Column positionIdentifier1, Column positionIdentifier2,
                                                      Column lengthIdentifier,  Object na) {
        return when(positionIdentifier1.equalTo(positionIdentifier2),array()). otherwise(
                when(positionIdentifier2.lt(positionIdentifier1),
                when(positionIdentifier2.equalTo(0),
                        when(positionIdentifier1.equalTo(lengthIdentifier.minus(1)),array()).
                                otherwise(getDuplicateFreeOrderedSequence(getRightNeighbour(positionIdentifier1,lengthIdentifier),lengthIdentifier.minus(1),na))).
                        otherwise(
                                when(positionIdentifier1.equalTo(lengthIdentifier.minus(1)), getDuplicateFreeOrderedSequence(lit(0),getLeftNeighbour(positionIdentifier2,lengthIdentifier),na)).
                                        otherwise(concat(getDuplicateFreeOrderedSequence(getRightNeighbour(positionIdentifier1,lengthIdentifier),lengthIdentifier.minus(1),na),
                                                getDuplicateFreeOrderedSequence(lit(0),getLeftNeighbour(positionIdentifier2,lengthIdentifier),na)))
                        )
        ).otherwise(array_distinct(getDuplicateFreeOrderedSequence(getRightNeighbour(positionIdentifier1,lengthIdentifier),getLeftNeighbour(positionIdentifier2,lengthIdentifier),na))));
    }

    private static Column cyclicIntermediatePositions(String positionIdentifier1, String positionIdentifier2,
                                                      String lengthIdentifier,  Object na) {
        return when(col(positionIdentifier1).equalTo(col(positionIdentifier2)),array()). otherwise(
                when(col(positionIdentifier2).lt(col(positionIdentifier1)),
                        when(col(positionIdentifier2).equalTo(0),
                                when(col(positionIdentifier1).equalTo(col(lengthIdentifier).minus(1)),array()).
                                        otherwise(getDuplicateFreeOrderedSequence(getRightNeighbour(positionIdentifier1,lengthIdentifier),col(lengthIdentifier).minus(1),na))).
                                otherwise(
                                        when(col(positionIdentifier1).equalTo(col(lengthIdentifier).minus(1)), getDuplicateFreeOrderedSequence(lit(0),getLeftNeighbour(positionIdentifier2,lengthIdentifier),na)).
                                                otherwise(concat(getDuplicateFreeOrderedSequence(getRightNeighbour(positionIdentifier1,lengthIdentifier),col(lengthIdentifier).minus(1),na),
                                                        getDuplicateFreeOrderedSequence(lit(0),getLeftNeighbour(positionIdentifier2,lengthIdentifier),na)))
                                )
                ).otherwise(array_distinct(getDuplicateFreeOrderedSequence(getRightNeighbour(positionIdentifier1,lengthIdentifier),getLeftNeighbour(positionIdentifier2,lengthIdentifier),na))));
    }

    /**
     * Compute Array of positions between positionIdentifier1 and positionIdentifier2, i.e. exclusive of both,
     * applying correct boundary conditions given by column topologyIdentifier
     * @param positionIdentifier1
     * @param positionIdentifier2
     * @param lengthIdentifier Length of the genome
     * @param topologyIdentifier
     * @param strandIdentifier
     * @param na Object to be inserted if there are no intermediate positions
     * @return
     */
    public static Column computeIntermediatePositions(String positionIdentifier1, String positionIdentifier2,
                                                      String lengthIdentifier, String topologyIdentifier, String strandIdentifier, Object na) {

        return
                when(col(topologyIdentifier).equalTo(true),
                        when(col(strandIdentifier).equalTo(true),cyclicIntermediatePositions(positionIdentifier1,positionIdentifier2,lengthIdentifier,na)).
                        otherwise(cyclicIntermediatePositions(positionIdentifier2,positionIdentifier1,lengthIdentifier,na))
                ).
                        otherwise(
                                when(col(strandIdentifier).equalTo(true),
                                        when(col(positionIdentifier2).lt(col(positionIdentifier1)), na).
                                                otherwise(array_distinct(getDuplicateFreeOrderedSequence(getRightNeighbour(positionIdentifier1,lengthIdentifier),getLeftNeighbour(positionIdentifier2,lengthIdentifier),na)))).
                                        otherwise(
                                                when(col(positionIdentifier2).gt(col(positionIdentifier1)), na).
                                                        otherwise(array_distinct(getDuplicateFreeOrderedSequence(getRightNeighbour(positionIdentifier2,lengthIdentifier),getLeftNeighbour(positionIdentifier1,lengthIdentifier),na)))
                                        )
                        );
    }

    /**
     * Compute Array of positions between positionIdentifier1 and positionIdentifier2, i.e. exclusive of both,
     * applying correct boundary conditions given by column topologyIdentifier
     * @param positionIdentifier1
     * @param positionIdentifier2
     * @param lengthIdentifier Length of the genome
     * @param topologyIdentifier
     * @param strandIdentifier
     * @param na Object to be inserted if there are no intermediate positions
     * @return
     */
    public static Column computeIntermediatePositions(Column positionIdentifier1, Column positionIdentifier2,
                                                      Column lengthIdentifier, Column topologyIdentifier, Column strandIdentifier, Object na) {

        return
                when(topologyIdentifier.equalTo(true),
                        when(strandIdentifier.equalTo(true),cyclicIntermediatePositions(positionIdentifier1,positionIdentifier2,lengthIdentifier,na)).
                                otherwise(cyclicIntermediatePositions(positionIdentifier2,positionIdentifier1,lengthIdentifier,na))
                ).
                        otherwise(
                                when(strandIdentifier.equalTo(true),
                                        when(positionIdentifier2.lt(positionIdentifier1), na).
                                                otherwise(array_distinct(getDuplicateFreeOrderedSequence(getRightNeighbour(positionIdentifier1,lengthIdentifier),getLeftNeighbour(positionIdentifier2,lengthIdentifier),na)))).
                                        otherwise(
                                                when(positionIdentifier2.gt(positionIdentifier1), na).
                                                        otherwise(array_distinct(getDuplicateFreeOrderedSequence(getRightNeighbour(positionIdentifier2,lengthIdentifier),getLeftNeighbour(positionIdentifier1,lengthIdentifier),na)))
                                        )
                        );
    }

    public static Dataset<Row> checkOverlap(Dataset<Row> dataset, String p1, String p2, String referenceP1, String referenceP2, String length,double overlapFraction, String overlapColumnName) {
        return checkOverlap(dataset,col(p1),col(p2),col(referenceP1),col(referenceP2),col(length),overlapFraction,overlapColumnName);
    }

    public static Dataset<Row> checkOverlap(Dataset<Row> dataset, Column p1, Column p2, Column referenceP1, Column referenceP2, Column length,double overlapFraction, String overlapColumnName) {
        return dataset.
                withColumn("dref",
                        pmod(referenceP2.minus(referenceP1),length)).
                withColumn("d",
                        pmod(p2.minus(p1),length)).
                withColumn("d52",pmod(p2.minus(referenceP1),
                        length)).
                withColumn("d51",pmod(p1.minus(referenceP1),
                        length)).
                withColumn("d15",pmod(referenceP1.minus(p1),
                        length)).
                withColumn("d62",pmod(p2.minus(referenceP2),
                        length)).
                withColumn("d16",pmod(referenceP2.minus(p1),
                        length)).
                withColumn("c1",
                        // p2 between p1ref and p2ref
                        col("d52").leq(col("dref")).
                        and(
                                // p1p2 contained in p1ref,p2ref
                                col("d51").leq(col("dref")).
                                // overlap of both ranges at least overlapfraction percent of dref
                                or(
                                        col("d52").geq(col("dref").multiply(overlapFraction))))).
                withColumn("c2",
                        // p1ref,p2ref beteween p1 and p2
                        col("d15").leq(col("d")).
                                and(col("d62").leq(col("d")))
                        ).
                withColumn("c3",
                        // p1 between p1ref and p2ref
                        col("d51").leq(col("dref")).
                        and(
                            // p2 NOT between p1ref and p2ref
                            col("d52").gt(col("dref"))).
                        and(col("d16").geq(col("dref").multiply(overlapFraction)))).
                withColumn(overlapColumnName,col("c1").or(col("c2")).or(col("c3"))).
                drop("d52","d51","d15","d62","d16","c1","c2","c3");

//                withColumn(overlapColumnName,
//                        col("d52").leq(col("dref")).
//                                and(col("d51").leq(col("dref")).
//                                        or(col("d15").geq(col("d").multiply(overlapFraction)))).
//                                or(
//                                        col("d15").leq(col("dref")).
//                                                and(col("d62").leq(col("dref")))
//                                ).
//                                or(
//                                        col("d51").leq(col("dref")) .
//                                                and(col("d62").geq(col("d").multiply(overlapFraction)))));
//                drop("d52","d51","d15","d62");
    }

    public static Dataset<Row> computeOverlap(Dataset<Row> dataset, Column p1, Column p2, Column referenceP1, Column referenceP2, Column length, String overlapColumnName) {
        return dataset.
                withColumn("dref",
                        pmod(referenceP2.minus(referenceP1),length)).
                withColumn("d",
                        pmod(p2.minus(p1),length)).
                withColumn("d52",pmod(p2.minus(referenceP1),
                        length)).
                withColumn("d51",pmod(p1.minus(referenceP1),
                        length)).
                withColumn("d15",pmod(referenceP1.minus(p1),
                        length)).
                withColumn("d62",pmod(p2.minus(referenceP2),
                        length)).
                withColumn("d16",pmod(referenceP2.minus(p1),
                        length)).
//                withColumn("c1",col("d52").leq(col("dref")).
//                        and(col("d51").leq(col("dref")))).
//                withColumn("c2",
//                        col("d15").leq(col("d")).
//                        and(col("d16").leq(col("d")))).
                withColumn(overlapColumnName,
                        when(
                                // p1p2 contained in p1ref,p2ref
                                (col("d52").leq(col("dref")).
                                and(col("d51").leq(col("dref")))).
                                        or(
                                                // p1ref,p2ref contained in p1 and p2
                                                (col("d15").leq(col("d")).
                                                        and(col("d16").leq(col("d"))))
                                        ),
                                1.0
                        ).otherwise(
                                when(
                                        // p1 between p1ref and p2ref
                                        col("d51").leq(col("dref")),
                                        col("d16").divide(least(col("dref"),col("d")))
                                        ).otherwise(
                                                when(
                                                        // p2 between p1ref and p2ref
                                                        col("d52").leq(col("dref")),
                                                            col("d52").divide(least(col("dref"),col("d")))
                                                        ).
                                                        otherwise(0.0)
                                )
                        )).
                drop("d52","d51","d15","d62","d16");

    }

    public static Dataset<Row> computeOverlap(Dataset<Row> dataset, Column p1, Column p2, Column referenceP1, Column referenceP2, Column length, String overlapColumnName,boolean withRespectToReference) {
        return dataset.
                withColumn("dref",
                        pmod(referenceP2.minus(referenceP1),length)).
                withColumn("d",
                        pmod(p2.minus(p1),length)).
                withColumn("d52",pmod(p2.minus(referenceP1),
                        length)).
                withColumn("d51",pmod(p1.minus(referenceP1),
                        length)).
                withColumn("d15",pmod(referenceP1.minus(p1),
                        length)).
                withColumn("d62",pmod(p2.minus(referenceP2),
                        length)).
                withColumn("d16",pmod(referenceP2.minus(p1),
                        length)).
                withColumn("compare",withRespectToReference ? col("dref"): col("d")).
//                withColumn("c1",col("d52").leq(col("dref")).
//                        and(col("d51").leq(col("dref")))).
//                withColumn("c2",
//                        col("d15").leq(col("d")).
//                        and(col("d16").leq(col("d")))).
        withColumn(overlapColumnName,
        when(
                // p1p2 contained in p1ref,p2ref
                (col("d52").leq(col("dref")).
                        and(col("d51").leq(col("dref")))).
                        or(
                                // p1ref,p2ref contained in p1 and p2
                                (col("d15").leq(col("d")).
                                        and(col("d16").leq(col("d"))))
                        ),
                1.0
        ).otherwise(
                when(
                        // p1 between p1ref and p2ref
                        col("d51").leq(col("dref")),
                        col("d16").divide(col("compare"))
                ).otherwise(
                        when(
                                // p2 between p1ref and p2ref
                                col("d52").leq(col("dref")),
                                col("d52").divide(col("compare"))
                        ).
                                otherwise(0.0)
                )
        )).
                drop("d52","d51","d15","d62","d16","compare");

    }

    public static Dataset<Row> computeOverlapPositions2(Dataset<Row> dataset, Column p1, Column p2, Column referenceP1, Column referenceP2,
                                                        Column length, Column refLength,
                                                        Column sequenceLength, String overlapColumnName) {
        return dataset.

                withColumn("dref1_2",pmod(p2.minus(referenceP1),
                        sequenceLength).plus(1)).
                withColumn("dref1_1",pmod(p1.minus(referenceP1),
                        sequenceLength).plus(1)).
                withColumn("d1_ref1",pmod(referenceP1.minus(p1),
                        sequenceLength).plus(1)).
//                withColumn("dref2_2",pmod(p2.minus(referenceP2),
//                        sequenceLength)).
                withColumn("d1_ref2",pmod(referenceP2.minus(p1),
                        sequenceLength).plus(1)).
                withColumn(overlapColumnName,
                        when(
                                // p1p2 contained in p1ref,p2ref
                                (col("dref1_2").leq(refLength).
                                        and(col("dref1_1").leq(refLength))),array(p1,p2)).
                                otherwise(
                                        when(
                                                // p1ref,p2ref contained in p1 and p2
                                                (col("d1_ref1").leq(length).
                                                        and(col("d1_ref2").leq(length))),
                                                array(referenceP1,referenceP2)
                                        )
                                                .otherwise(
                                                        when(
                                                                // p1 between p1ref and p2ref
                                                                col("dref1_1").leq(refLength),
                                                                array(p1,referenceP2)
                                                        ).otherwise(
                                                                when(
                                                                        // p2 between p1ref and p2ref
                                                                        col("dref1_2").leq(refLength),
                                                                        array(referenceP1,p2)
                                                                ).
                                                                        otherwise(null)
                                                        )
                                                ))

                )
                .drop("dref1_2","dref1_1","d1_ref1","d1_ref2");
//                .drop("d52","dref1_1","d1_ref1","dref2_2","d16","d","dref");

    }

    public static Dataset<Row> computeOverlapPositions(Dataset<Row> dataset, Column p1, Column p2, Column referenceP1, Column referenceP2, Column length, String overlapColumnName) {
        return dataset.
                withColumn("dref",
                        pmod(referenceP2.minus(referenceP1),length)).
                withColumn("d",
                        pmod(p2.minus(p1),length)).
                withColumn("d52",pmod(p2.minus(referenceP1),
                        length)).
                withColumn("d51",pmod(p1.minus(referenceP1),
                        length)).
                withColumn("d15",pmod(referenceP1.minus(p1),
                        length)).
                withColumn("d62",pmod(p2.minus(referenceP2),
                        length)).
                withColumn("d16",pmod(referenceP2.minus(p1),
                        length)).
        withColumn(overlapColumnName,
        when(
                // p1p2 contained in p1ref,p2ref
                (col("d52").leq(col("dref")).
                        and(col("d51").leq(col("dref")))),array(p1,p2)).
                otherwise(
                        when(
                                // p1ref,p2ref contained in p1 and p2
                                (col("d15").leq(col("d")).
                                        and(col("d16").leq(col("d")))),
                                array(referenceP1,referenceP2)
                        )
                                .otherwise(
                                        when(
                                                // p1 between p1ref and p2ref
                                                col("d51").leq(col("dref")),
                                                array(p1,referenceP2)
                                        ).otherwise(
                                                when(
                                                        // p2 between p1ref and p2ref
                                                        col("d52").leq(col("dref")),
                                                        array(referenceP1,p2)
                                                ).
                                                        otherwise(null)
                                        )
                                ))

        ).drop("d52","d51","d15","d62","d16","d","dref");

    }

    /**
     * extract subsequence form sequenceColumn, given a path of kmers with starvertex position positionColumn of length specified in lengthColumn
     * @param sequenceColumn
     * @param positionColumn
     * @param lengthColumn
     * @return
     */
    public static Column fetchSubsequence(String sequenceColumn, String positionColumn, String lengthColumn) {
        return expr("substring("+col(sequenceColumn) +","+ col(positionColumn).minus(15)+","+ col(lengthColumn)+")");
    }

    public static Column getSuffixSubstring(String stringColumn, int suffixLength) {
        return expr("substring("+col(stringColumn) +","+ length(col(stringColumn)).minus(suffixLength)+","+ length(col(stringColumn))+")");
    }

    public static Column getPrefixSubstring(String stringColumn, int prefixLength) {
        return expr("substring("+col(stringColumn) +",1,"+ prefixLength+")");
    }

    public static Column getPrefixSubstring(Column stringColumn, Column prefixLength) {
        return expr("substring("+stringColumn +",1,"+ prefixLength+")");
    }

}
