package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.ColumnIdentifier;
// import jdk.nashorn.internal.ir.EmptyNode;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;

public interface PositionInterface {

    int getPosition();

    void setPosition(int position);

    static <T extends PositionInterface> Dataset<Integer> distinctPositions(Dataset<T> dataset) {
        return dataset.select(ColumnIdentifier.IDENTIFIER).as(Encoders.INT());
    }
}
