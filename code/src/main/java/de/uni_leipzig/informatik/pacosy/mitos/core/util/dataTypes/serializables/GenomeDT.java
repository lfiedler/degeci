package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class GenomeDT implements Serializable {

    private String recordId;
    private String sequence;
    public static final Encoder<GenomeDT> ENCODER = Encoders.bean(GenomeDT.class);

    public enum COLUMN_NAME implements DBColumnName {
        RECORD_ID("\"recordId\""),
        SEQUENCE("sequence");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public GenomeDT() {

    }

    public GenomeDT(String recordId, String sequence) {
        this.recordId = recordId;
        this.sequence = sequence;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

}
