package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class BreakpointStatDT implements Serializable {
    public static final Encoder<BreakpointStatDT> ENCODER = Encoders.bean(BreakpointStatDT.class);
    protected int k;
    protected String substitionRate;
    protected int shuffleAmount;
    protected long missingCount;
    protected long totalAmount;
    protected double matchRate;
    protected long candidateAmount;
    protected long falsePositivesAmount;
    protected double falsePositiveRate;

    public BreakpointStatDT() {
    }

    public BreakpointStatDT(int k, String substitionRate, int shuffleAmount, long missingCount, long totalAmount, long candidateAmount, long falsePositivesAmount) {
        this.k = k;
        this.substitionRate = substitionRate;
        this.shuffleAmount = shuffleAmount;
        this.missingCount = missingCount;
        this.totalAmount = totalAmount;
        this.candidateAmount = candidateAmount;
        this.falsePositivesAmount = falsePositivesAmount;
        this.matchRate = 1-((double)missingCount)/totalAmount;
        this.falsePositiveRate = 1-((double)falsePositivesAmount)/candidateAmount;
    }

    public String getSubstitionRate() {
        return substitionRate;
    }

    public void setSubstitionRate(String substitionRate) {
        this.substitionRate = substitionRate;
    }

    public int getShuffleAmount() {
        return shuffleAmount;
    }

    public void setShuffleAmount(int shuffleAmount) {
        this.shuffleAmount = shuffleAmount;
    }

    public long getCandidateAmount() {
        return candidateAmount;
    }

    public void setCandidateAmount(long candidateAmount) {
        this.candidateAmount = candidateAmount;
    }

    public long getFalsePositivesAmount() {
        return falsePositivesAmount;
    }

    public void setFalsePositivesAmount(long falsePositivesAmount) {
        this.falsePositivesAmount = falsePositivesAmount;
    }

    public double getFalsePositiveRate() {
        return falsePositiveRate;
    }

    public void setFalsePositiveRate(double falsePositiveRate) {
        this.falsePositiveRate = falsePositiveRate;
    }

    public int getK() {
        return k;
    }

    public void setK(int k) {
        this.k = k;
    }

    public long getMissingCount() {
        return missingCount;
    }

    public void setMissingCount(long missingCount) {
        this.missingCount = missingCount;
    }

    public long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(long totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getMatchRate() {
        return matchRate;
    }

    public void setMatchRate(double matchRate) {
        this.matchRate = matchRate;
    }
}
