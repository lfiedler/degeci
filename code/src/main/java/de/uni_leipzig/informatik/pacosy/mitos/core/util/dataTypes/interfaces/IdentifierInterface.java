package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces;

public interface IdentifierInterface {

    String getIdentifier();

    void setIdentifier(String identifier);

}
