package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class WeightCountDT implements Serializable {
    protected long weight;
    protected long count;
    public static final Encoder<WeightCountDT> ENCODER = Encoders.bean(WeightCountDT.class);

    public WeightCountDT() {
    }

    public WeightCountDT(long weight, long count) {
        this.weight = weight;
        this.count = count;
    }

    public long getWeight() {
        return weight;
    }

    public void setWeight(long weight) {
        this.weight = weight;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

}
