package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TripletDT implements Serializable  {
    protected int t1;
    protected int t2;
    protected int t3;
    protected int amount;
    public static final Encoder<TripletDT> ENCODER = Encoders.bean(TripletDT.class);

    public TripletDT() {
    }

    public TripletDT(int t1, int t2, int t3, int amount) {
        this.t1 = t1;
        this.t2 = t2;
        this.t3 = t3;
        this.amount = amount;
    }

    public static List<TripletDT> getTriplets(List<Integer> taxids1, List<Integer> taxids2) {
        List<TripletDT> triplets = new ArrayList<>();
        for(int i = 0; i < taxids1.size(); i ++) {

            int t1 = taxids1.get(i);
            for(int j = i+1; j < taxids1.size(); j++) {
                int t2 = taxids1.get(j);
                for(int k = 0; k < taxids2.size(); k++) {
                    int t3 = taxids2.get(k);
                    TripletDT triplet = new TripletDT(t1,t2,t3,1);
                    if(!triplet.hasDuplicates()) {
                        triplets.add(triplet);
                    }
                }
            }
        }
        return triplets;
    }

    public static String tableValues() {
        return ColumnIdentifier.T1 + " integer NOT NULL,\n" +
                ColumnIdentifier.T2 + " integer NOT NULL,\n" +
                ColumnIdentifier.T3 + " integer NOT NULL,\n" +
                ColumnIdentifier.AMOUNT + " integer NOT NULL";
    }

    public int getT1() {
        return t1;
    }

    public void setT1(int t1) {
        this.t1 = t1;
    }

    public int getT2() {
        return t2;
    }

    public void setT2(int t2) {
        this.t2 = t2;
    }

    public int getT3() {
        return t3;
    }

    public void setT3(int t3) {
        this.t3 = t3;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean hasDuplicates() {
        if(t1 == t2) {
            return true;
        }
        if(t1 == t3) {
            return true;
        }
        if(t2 == t3) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "(" + t1 + "," + t2 + "|" + t3 + "): " + amount;
    }


}
