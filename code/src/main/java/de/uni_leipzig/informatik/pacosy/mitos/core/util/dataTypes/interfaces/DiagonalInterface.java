package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces;

public interface DiagonalInterface {

    int getDiagonalValue();

    void setDiagonalValue(int diagonalValue);
}
