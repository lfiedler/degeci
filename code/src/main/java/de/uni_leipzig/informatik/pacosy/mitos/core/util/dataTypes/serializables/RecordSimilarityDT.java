package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.hadoop.yarn.util.Records;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class RecordSimilarityDT implements Serializable {
    private int record1;
    private int record2;
    private int achievedCount;
    private int possibleCount;
    public static final Encoder<RecordSimilarityDT> ENCODER = Encoders.bean(RecordSimilarityDT.class);

    public enum COLUMN_NAME implements DBColumnName {
        RECORD1("record1"),
        RECORD2("record2"),
        ACHIEVED_COUNT("achievedCount"),
        POSSIBLE_COUNT("possibleCount");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public RecordSimilarityDT() {

    }

    public RecordSimilarityDT(int record1, int record2, int possibleCount, int achievedCount) {
        this.record1 = record1;
        this.record2 = record2;
        this.possibleCount = possibleCount;
        this.achievedCount = achievedCount;
    }

    public int getRecord1() {
        return record1;
    }

    public void setRecord1(int record1) {
        this.record1 = record1;
    }

    public int getRecord2() {
        return record2;
    }

    public void setRecord2(int record2) {
        this.record2 = record2;
    }

    public int getAchievedCount() {
        return achievedCount;
    }

    public void setAchievedCount(int achievedCount) {
        this.achievedCount = achievedCount;
    }

    public int getPossibleCount() {
        return possibleCount;
    }

    public void setPossibleCount(int possibleCount) {
        this.possibleCount = possibleCount;
    }
}
