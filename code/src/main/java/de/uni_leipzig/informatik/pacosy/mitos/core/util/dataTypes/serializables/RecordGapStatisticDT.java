package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.RecordIdInterface;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class RecordGapStatisticDT extends GapStatisticDT implements RecordIdInterface {
    private int recordId;
    protected int length;
    public static final Encoder<RecordGapStatisticDT> ENCODER = Encoders.bean(RecordGapStatisticDT.class);

    public RecordGapStatisticDT() {
    }

    public RecordGapStatisticDT(GapStatisticDT gapStatisticDT, int recordId, int length) {
        this(gapStatisticDT.getGapCount(),gapStatisticDT.getUnbridgedCount(),recordId, length);
    }

    public RecordGapStatisticDT(long gapCount, long unbridgedCount, int recordId, int length) {
        super(gapCount, unbridgedCount);
        this.recordId = recordId;
        this.length = length;
    }

    @Override
    public int getRecordId() {
        return recordId;
    }

    @Override
    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "RecordGapStatisticDT{" +
                "recordId=" + recordId +
                ", length=" + length +
                ", gapCount=" + gapCount +
                ", unbridgedCount=" + unbridgedCount +
                '}';
    }
}
