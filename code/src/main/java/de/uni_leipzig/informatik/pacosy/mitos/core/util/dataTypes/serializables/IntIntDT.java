package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;
import java.util.Arrays;

import static org.apache.spark.sql.functions.*;

public class IntIntDT implements Serializable {
    protected int int1;
    protected int int2;
    public static final Encoder<IntIntDT> ENCODER = Encoders.bean(IntIntDT.class);


    public IntIntDT(int int1, int int2) {
        this.int1 = int1;
        this.int2 = int2;
    }

    public IntIntDT() {
    }

    public int getInt1() {
        return int1;
    }

    public void setInt1(int int1) {
        this.int1 = int1;
    }

    public int getInt2() {
        return int2;
    }

    public void setInt2(int int2) {
        this.int2 = int2;
    }


}
