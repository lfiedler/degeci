package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.IdentifierInterface;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.PositionInterface;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import static org.apache.spark.sql.functions.col;

public class NodePairPositionDT extends NodePairDT implements PositionInterface{

    int position;
    public static final Encoder<NodePairPositionDT> ENCODER = Encoders.bean(NodePairPositionDT.class);

    public NodePairPositionDT() {
        super();
    }

    public NodePairPositionDT(String v1, String v2, int position) {
        super(v1,v2);
        this.position = position;
    }


    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public static <T extends PositionInterface & IdentifierInterface> Dataset<NodePairPositionDT> convert(Dataset<T> dataset) {
        if(dataset.isEmpty()) {
            return null;
        }
        int length = dataset.first().getIdentifier().length() -1;
        return dataset.select(col(ColumnIdentifier.IDENTIFIER).substr(0, length).as(ColumnIdentifier.V1),
                col(ColumnIdentifier.IDENTIFIER).substr(1,length).as(ColumnIdentifier.V2),col(ColumnIdentifier.POSITION)).
                as(ENCODER);
    }
}
