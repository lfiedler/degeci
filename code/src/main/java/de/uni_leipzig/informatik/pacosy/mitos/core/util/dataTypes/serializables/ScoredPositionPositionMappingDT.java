package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class ScoredPositionPositionMappingDT extends PositionPositionMappingDT {
    protected int score;
    // 1e-50 high quality,
    // 0.01 can still be considered good match
    protected double eValue;



    public static final Encoder<ScoredPositionPositionMappingDT> ENCODER = Encoders.bean(ScoredPositionPositionMappingDT.class);

    public ScoredPositionPositionMappingDT(int recordId, int position, int recordPosition, boolean strand, int score, double eValue) {
        super(recordId, position, recordPosition, strand);
        this.score = score;
        this.eValue = eValue;


    }

    public ScoredPositionPositionMappingDT() {
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public double geteValue() {
        return eValue;
    }

    public void seteValue(double eValue) {
        this.eValue = eValue;
    }

    public static String tableValues() {
        return
                "\"" +ColumnIdentifier.RECORD_ID +"\" integer," + ColumnIdentifier.POSITION + " integer," +
                        "\"" +ColumnIdentifier.RECORD_POSITION + "\" integer," +
                        ColumnIdentifier.STRAND + " boolean, " + ColumnIdentifier.SCORE +" integer," +
                        "\"" +ColumnIdentifier.EVALUE +"\" double precision";
    }
}
