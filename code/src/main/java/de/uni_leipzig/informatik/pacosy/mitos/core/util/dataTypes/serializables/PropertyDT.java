package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import org.apache.http.auth.AUTH;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class PropertyDT implements Serializable,  Comparable<PropertyDT> {

    protected String category;
    protected String property;
    protected int position;
    protected BigDecimal probability;
    final static public String VOID_IDENTIFIER = "void";
    public static final Encoder<PropertyDT> ENCODER = Encoders.bean(PropertyDT.class);

    public PropertyDT() {
    }

    public PropertyDT(String category, String property, int position, BigDecimal probability) {
        this.category = category;
        this.property = property;
        this.position = position;
        this.probability = probability;
    }

    @Override
    public int compareTo(PropertyDT propertyDT) {
        int p = Integer.valueOf(this.position).compareTo(propertyDT.position);
        if(p == 0) {
            return this.property.compareTo(propertyDT.property);
        }
        return p;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PropertyDT)) return false;
        PropertyDT that = (PropertyDT) o;
        return getPosition() == that.getPosition() &&
                Objects.equals(getCategory(), that.getCategory()) &&
                Objects.equals(getProperty(), that.getProperty());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCategory(), getProperty(), getPosition());
    }

    @Override
    public String toString() {
        return
                "category='" + category + '\'' +
                ", property='" + property + '\'' +
                ", position=" + position +
                ", probability=" + probability ;
    }

    public static String rawProperty(String property) {
        if(!property.contains("_")) {
            return property;
        }
        return property.substring(0,property.indexOf("_"));
    }

    public String rawProperty() {
        return rawProperty(this.property);
    }

    public BigDecimal getProbability() {
        return probability;
    }

    public void setProbability(BigDecimal probability) {
        this.probability = probability;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public static List<PropertyDT> convert(PropertyRangeProbDT propertyRangeProb) {
        List<PropertyDT> properties = new ArrayList<>();


        if(propertyRangeProb.start > propertyRangeProb.end){
            for(int p = propertyRangeProb.start; p <= propertyRangeProb.start+propertyRangeProb.length- propertyRangeProb.end-2; p++) {
                properties.add(new PropertyDT(propertyRangeProb.category,propertyRangeProb.property,p,propertyRangeProb.probability));
            }
            for(int p = 0; p <= propertyRangeProb.end; p++) {
                properties.add(new PropertyDT(propertyRangeProb.category,propertyRangeProb.property,p,propertyRangeProb.probability));
            }
        }
        else {
            for(int p = propertyRangeProb.start; p <= propertyRangeProb.end; p++) {
                properties.add(new PropertyDT(propertyRangeProb.category,propertyRangeProb.property,p,propertyRangeProb.probability));
            }
        }

        return properties;
    }
//    public static List<PropertyDT> convert(PropertyRangeProbDT propertyRangeProb) {
//        List<PropertyDT> properties = new ArrayList<>();
//
//        for(int p = propertyRangeProb.start; p <= propertyRangeProb.end; p++) {
//            properties.add(new PropertyDT(propertyRangeProb.category,propertyRangeProb.property,p,propertyRangeProb.probability));
//        }
//        return properties;
//    }

    public static void joinRanges(PropertyRangeProbDT range1, PropertyRangeProbDT range2) {
        if(range1.getEnd() >= range2.getStart() && range1.getEnd() <= range2.getEnd()) {
            range1.setProperty(range2.getProperty());
            range1.setEnd(range2.getEnd());
            return;
        }
        if(range1.getStart() >= range2.getStart() && range1.getStart() <= range2.getStart()) {
            range1.setProperty(range2.getProperty());
            range1.setStart(range2.getEnd());
            return;
        }

    }

    private static boolean mergeRangeLists(List<PropertyRangeProbDT> rangeList, String genericProperty,String specificProperty) {
        final int threshold = 20;
        Map<String, List<PropertyRangeProbDT>> propertyRangeMap = rangeList.stream().collect(Collectors.groupingBy(PropertyRangeDT::getProperty));
        boolean updated = false;
        if(propertyRangeMap.containsKey(genericProperty) && propertyRangeMap.containsKey(specificProperty)) {
                List<PropertyRangeProbDT> t = new ArrayList<>();
                List<PropertyRangeProbDT> propertyRangesS = propertyRangeMap.get(genericProperty);
                Collections.sort(propertyRangesS);
                List<PropertyRangeProbDT> deletes = new ArrayList<>();
                List<PropertyRangeProbDT> inserts = new ArrayList<>();
                List<PropertyRangeProbDT> propertyRangesS1 = propertyRangeMap.get(specificProperty);
                Collections.sort(propertyRangesS1);

                int s1Index = 0;
                int sIndex;
                PropertyRangeProbDT s1Element = propertyRangesS1.get(s1Index);
                PropertyRangeProbDT sElement = null;
                for(sIndex = 0; sIndex < propertyRangesS.size(); sIndex++) {
                    sElement = propertyRangesS.get(sIndex);

                    while(s1Element.getEnd() < sElement.getStart()) {
                        s1Index++;
                        if(s1Index > propertyRangesS1.size()-1) {
                            sIndex = propertyRangesS.size();
                            s1Index--;
                            break;
                        }
                        s1Element = propertyRangesS1.get(s1Index);
                    }
                    if(sIndex == propertyRangesS.size()) {
                        break;
                    }
                    if((sElement.getEnd() >= s1Element.getStart() && sElement.getEnd() <= s1Element.getEnd())
                            || (sElement.getStart() >= s1Element.getStart()&& sElement.getStart() <= s1Element.getEnd())
                            || ( s1Element.getEnd() <= sElement.getEnd())
                            || (s1Element.getStart() >= sElement.getStart() && s1Element.getStart() <= sElement.getEnd())
                    ) {// ranges overlap
//                        deletes.add(new PropertyRangeProbDT(s1Element));
                        deletes.add(new PropertyRangeProbDT(sElement));

                        s1Element.setStart(Math.min(s1Element.getStart(),sElement.getStart()));
                        s1Element.setEnd(Math.max(s1Element.getEnd(),sElement.getEnd()));
//                        inserts.add(s1Element);
                        updated = true;
                    }
//                    if(sElement.getStart() >= s1Element.getStart() || (sElement.getEnd() >= s1Element.getStart() && sElement.getEnd() <= s1Element.getEnd()) ||
//                            (s1Element.getStart() >= sElement.getStart() && s1Element.getEnd() <= sElement.getEnd())) { // ranges overlap
//                        deletes.add(new PropertyRangeProbDT(s1Element));
//                        deletes.add(new PropertyRangeProbDT(sElement));
//                        s1Element.setStart(Math.min(s1Element.getStart(),sElement.getStart()));
//                        s1Element.setEnd(Math.max(s1Element.getEnd(),sElement.getEnd()));
//                        inserts.add(s1Element);
//                        updated = true;
//                    }
                    else if(s1Element.getStart() -sElement.getEnd() <= threshold) {
                        deletes.add(new PropertyRangeProbDT(sElement));
                        sElement = new PropertyRangeProbDT(sElement);
                        sElement.setProperty(s1Element.property);
                        inserts.add(sElement);
                        updated = true;
                    }
                    else if(s1Index-1 >= 0) { // check previous interval -> cannot overlap per construction but be within threshold distance
                        s1Element = propertyRangesS1.get(s1Index-1);
                        if(sElement.getStart()-s1Element.getEnd() <= threshold) {
                            deletes.add(new PropertyRangeProbDT(sElement));
                            sElement = new PropertyRangeProbDT(sElement);
                            sElement.setProperty(s1Element.property);
                            inserts.add(sElement);
                            updated = true;

                        }
                        s1Element = propertyRangesS1.get(s1Index);
                    }
                    else { // could not be merged
                        if(!t.isEmpty() && sElement.getStart() -t.get(t.size()-1).getEnd() > threshold) { // clear  list only if not within threshold distance to last element
                            t.clear();
                        }
                        t.add(sElement);
                        updated = false;
                    }

                    if(updated && !t.isEmpty() && sElement.getStart() -t.get(t.size()-1).getEnd() <= threshold) {
                        for(PropertyRangeProbDT range : t) {
                            deletes.add(new PropertyRangeProbDT(range));
                            range = new PropertyRangeProbDT(range);
                            range.setProperty(s1Element.property);
                            inserts.add(range);
                        }
                    }

                }

                if(s1Index > 0) {
                    s1Element = propertyRangesS1.get(s1Index-1);
                    if(!sElement.getProperty().equals(s1Element.getProperty()) &&sElement.getStart()-s1Element.getEnd() <= threshold) {
                        deletes.add(new PropertyRangeProbDT(sElement));
                        sElement = new PropertyRangeProbDT(sElement);
                        sElement.setProperty(s1Element.property);
                        inserts.add(sElement);
                        if(!t.isEmpty() && sElement.getStart() -t.get(t.size()-1).getEnd() <= threshold) {
                            for(PropertyRangeProbDT range : t) {
                                deletes.add(new PropertyRangeProbDT(range));
                                range = new PropertyRangeProbDT(range);
                                range.setProperty(s1Element.property);
                                inserts.add(range);
                            }
                        }
                        for(int index = propertyRangesS.indexOf(sElement)+1; index < propertyRangesS.size(); index++){
                            PropertyRangeProbDT range = propertyRangesS.get(index);
                            if(range.getStart()-sElement.getEnd()<= threshold) {
                                deletes.add(new PropertyRangeProbDT(range));
                                range = new PropertyRangeProbDT(range);
                                range.setProperty(s1Element.property);
                                inserts.add(range);
                                sElement = range;
                            }
                            else {
                                break;
                            }
                        }
                    }
                }
                if(!deletes.isEmpty() ||!inserts.isEmpty()) {
//                    System.out.println(specificProperty + " "+ genericProperty);
//                    System.out.println("before");
//                    Collections.sort(rangeList);
//                    Auxiliary.printSeq(rangeList);
                    rangeList.removeAll(deletes);
                    rangeList.addAll(inserts);
//                    System.out.println("after");
//                    Collections.sort(rangeList);
//                    Auxiliary.printSeq(rangeList);
                    List<PropertyRangeProbDT> sList = rangeList.stream().filter(p -> p.getProperty().equals(specificProperty)).collect(Collectors.toList());
                    Collections.sort(sList);
                    deletes.clear();
                    PropertyRangeProbDT prevRange = sList.get(0);
                    PropertyRangeProbDT nextRange;
                    for(int i = 1; i < sList.size(); i++) {
                        nextRange = sList.get(i);
                        if(nextRange.getStart() != prevRange.getEnd()+1) {
                            prevRange = nextRange;

                        }
                        else {
                            prevRange.setEnd(nextRange.getEnd());
                            prevRange.setLength(prevRange.getLength()+nextRange.getLength());
                            deletes.add(nextRange);
                        }
                    }
                    rangeList.removeAll(deletes);
//                    System.out.println("after merge");
//                    Collections.sort(rangeList);
//                    Auxiliary.printSeq(rangeList);
                    return true;
                }


        }
        return false;
    }

    public static List<PropertyRangeProbDT> convert(List<PropertyDT> properties, int length) {
        final int threshold = PropertyRangeDT.THRESHOLD;

        List<PropertyDT> distinctProperties = properties.stream().distinct().collect(Collectors.toList());

        Map<String, List<PropertyDT>> propertyMap = distinctProperties.stream().collect(Collectors.groupingBy(p -> p.property));
        List<PropertyRangeProbDT> rangeListFull = new ArrayList<>();

        for(Map.Entry<String,List<PropertyDT>> property : propertyMap.entrySet()) {

            List<PropertyRangeProbDT> rangeList = new ArrayList<>();
//            System.out.println(property.getKey());
            List<PropertyDT> propertyDTList  = property.getValue();
            Collections.sort(propertyDTList);
            Iterator<PropertyDT> positionsIt = propertyDTList.iterator();
            PropertyDT prevPos = null;
            PropertyDT pos = null;



            // add first element to first range
            pos = positionsIt.next();
            PropertyRangeProbDT range = new PropertyRangeProbDT();
            range.setStart(pos.position);
            range.addProbability(pos.probability);
            range.setProperty(pos.property);
            range.setCategory(pos.category);

            while(positionsIt.hasNext()) {
                prevPos = pos;
                pos = positionsIt.next();


                if(  pos.position != prevPos.position + 1) {
                    range.setEnd(prevPos.position);
                    range.setLength(range.getEnd()-range.getStart()+1);
                    range.normalizeProbability();
                    rangeList.add(range);
//                System.out.println(range);
                    range = new PropertyRangeProbDT();
                    range.setStart(pos.position);
                    range.addProbability(pos.probability);
                    range.setProperty(pos.property);
                    range.setCategory(pos.category);
                }
                else {
                    range.addProbability(pos.probability);
                }
            }
            range.setEnd(pos.position);
            range.setLength(range.getEnd()-range.getStart()+1);
            range.normalizeProbability();
//        System.out.println(range);
            rangeList.add(range);

            Collections.sort(rangeList);
            int counter = 1;

            PropertyRangeProbDT prevRange = rangeList.get(0);
            prevRange.setProperty(rangeList.get(0).getProperty() + "_" + counter);
            for(int i = 1; i < rangeList.size(); i++) {
                PropertyRangeProbDT nextRange = rangeList.get(i);
                if(nextRange.getStart()-prevRange.getEnd()<= threshold) {
                    nextRange.setProperty(prevRange.getProperty());
                }
                else {
                    counter++;
                    nextRange.setProperty(nextRange.getProperty()+ "_" + counter);
                }
                prevRange = nextRange;
            }
            if(Auxiliary.mod(rangeList.get(0).start-rangeList.get(rangeList.size()-1).end,length) <= threshold) {
                rangeList.get(rangeList.size()-1).setProperty(rangeList.get(0).property);
            }
            rangeListFull.addAll(rangeList);
        }
        boolean s1Merged = mergeRangeLists(rangeListFull,"S","S1");
        boolean s2Merged = mergeRangeLists(rangeListFull,"S","S2");

        boolean l1Merged = mergeRangeLists(rangeListFull,"L","L1");
        boolean l2Merged = mergeRangeLists(rangeListFull,"L","L2");
        return rangeListFull;

    }

}
