package de.uni_leipzig.informatik.pacosy.mitos.core.psql;

import java.sql.SQLException;

public interface SQLTable {
     String SCHEMA_NAME = "complete";
     int CHUNK_SIZE = 10000;
     void createTable() throws SQLException;
     void dropTable() throws SQLException;

}
