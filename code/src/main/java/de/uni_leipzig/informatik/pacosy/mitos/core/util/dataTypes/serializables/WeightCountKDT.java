package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class WeightCountKDT extends WeightCountDT{
    protected int k;
    public static final Encoder<WeightCountKDT> ENCODER = Encoders.bean(WeightCountKDT.class);

    public WeightCountKDT() {
    }

    public WeightCountKDT(long weight, long count, int k) {
        super(weight, count);
        this.k = k;
    }

    public int getK() {
        return k;
    }

    public void setK(int k) {
        this.k = k;
    }
}
