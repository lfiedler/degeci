package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class PropertyRangeStrandDT extends PropertyRangeDT {
    protected boolean strand;
    public static final Encoder<PropertyRangeStrandDT> ENCODER = Encoders.bean(PropertyRangeStrandDT.class);

    public PropertyRangeStrandDT(String category, String property, int start, int end, int length, boolean strand) {
        super(category, property, start, end, length);
        this.strand = strand;
    }

    public PropertyRangeStrandDT() {
    }

    public boolean isStrand() {
        return strand;
    }

    public void setStrand(boolean strand) {
        this.strand = strand;
    }


}
