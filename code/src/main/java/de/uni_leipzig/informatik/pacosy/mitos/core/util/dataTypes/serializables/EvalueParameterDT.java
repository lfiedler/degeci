package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import org.apache.commons.lang.time.StopWatch;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;

import java.io.Serializable;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.functions.least;

public class EvalueParameterDT implements Serializable {
    private double H;
    private double K;
    private double lambda;
    public static final Encoder<EvalueParameterDT> ENCODER = Encoders.bean(EvalueParameterDT.class);

    public EvalueParameterDT() {
    }

    public EvalueParameterDT(double h, double k, double lambda) {
        H = h;
        K = k;
        this.lambda = lambda;
    }

    public double getH() {
        return H;
    }

    public void setH(double h) {
        H = h;
    }

    public double getK() {
        return K;
    }

    public void setK(double k) {
        K = k;
    }

    public double getLambda() {
        return lambda;
    }

    public void setLambda(double lambda) {
        this.lambda = lambda;
    }

    public double eValue(int l1,int l2, int score) {
        double shiftValue = Math.log(K*l1*l2)/H;
        return K*(l1-shiftValue)*(l2-shiftValue)*Math.exp(-lambda*score);
    }

    @Override
    public String toString() {
        return  "K:" + K + ",H:" + H + ",lambda:" + lambda;
    }




}
