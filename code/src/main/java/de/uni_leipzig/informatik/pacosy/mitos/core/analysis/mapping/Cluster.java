package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping;

import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.Graph;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.*;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.tinkerpop.gremlin.process.traversal.P;
import org.apache.tinkerpop.gremlin.process.traversal.Scope;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.structure.VertexProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.functions.col;

public class Cluster {

    protected GraphTraversalSource g;
    protected static final Logger LOGGER = LoggerFactory.getLogger(Cluster.class);
    public static class ClusterIdentifier{
        public static final String CLUSTER = "cluster";
        public static final String TOP_CANDIDATE_AMOUNT = "topCandidateAmount";
        public static final String RANK = "rank";
        public static final String AMOUNT = "amount";
        public static final String NORM_SCORE = "normScore";
        public static final String ROOT_BEST_NORM_SCORE = "rootBestNormScore";
        public static final String BEST_NORM_SCORE = "bestNormScore";
        public static final String BEST_SCORE = "score";
        public static final String SCORE = "score";
        public static final String HAS_PARENT = "hasParent";
        public static final String START = "start";
        public static final String END = "end";
        public static final String LENGTH = "length";
        public static final String COUNT = "count";
        public static final String ROOT_BEST_PROPERTY = "rootBestProperty";
        public static final String BEST_PROPERTY = "bestProperty";
        public static final String PROPERTY = "gene";
//        public static final String PROPERTY = "property";
        public static final String CATEGORY = "category";
        public static final String HEIGHT = "height";
    }

    public Cluster() {

    }

    public Cluster(File inputFile) {
        setG(inputFile);
    }


    private static <T extends Comparable<T>> Comparator<Map<String,Object>> getClusterComparator(String key) {
        return             (Map<String,Object> m1, Map<String,Object> m2) ->
                ((T)m1.get(key)).
                        compareTo((T)m2.get(key));
    }


    /*** Traversals ***************************************************************************************************************/
    public static GraphTraversal getRootsTraversal(GraphTraversalSource g) {
        return g.V().filter(__.out().count().is(P.eq(0)));
    }

    public static GraphTraversal getRootsBelowAlpha(GraphTraversalSource g, double alphaValue, String rootIdentifier) {
        return getRootsTraversal(g).as(rootIdentifier).properties(ClusterIdentifier.PROPERTY).has(ClusterIdentifier.RANK,1).
                filter(__.properties(ClusterIdentifier.NORM_SCORE).value().is(P.lt(alphaValue))).select(rootIdentifier);
    }

    public static GraphTraversal getRootsAtAndAboveAlpha(GraphTraversalSource g, double alphaValue, String rootIdentifier) {
        return getRootsTraversal(g).as(rootIdentifier).properties(ClusterIdentifier.PROPERTY).has(ClusterIdentifier.RANK,1).
                filter(__.properties(ClusterIdentifier.NORM_SCORE).value().is(P.gte(alphaValue))).select(rootIdentifier);
    }

    public static GraphTraversal getLeavesTraversal(GraphTraversalSource g) {
        return g.V().filter(__.in().count().is(P.eq(0)));
    }

    public static GraphTraversal getBestPropertyTraversal() {
        return __.properties(ClusterIdentifier.PROPERTY).has(ClusterIdentifier.RANK,1);
    }

    public static GraphTraversal getAllButBestPropertyTraversal() {
        return __.properties(ClusterIdentifier.PROPERTY).not(__.has(ClusterIdentifier.RANK,1));
    }

    public static GraphTraversal getBestPropertyTraversal(GraphTraversal<?,?> graphTraversal) {
        return graphTraversal.properties(ClusterIdentifier.PROPERTY).has(ClusterIdentifier.RANK,1);
    }

    public static GraphTraversal getTreeHeightTraversal() {
        return __.repeat(__.in()).emit().path().count(Scope.local).max();
    }

    public static GraphTraversal getLevelTraversal(GraphTraversalSource g,int level) {
        return getRootsTraversal(g).repeat(__.in()).times(level);
    }


    public static long getTreeHeights(GraphTraversalSource g) {
        return (long)getRootsTraversal(g).local(getTreeHeightTraversal()).next();
    }

    public static List<Map<String,Object>> getClusterListTraversal(GraphTraversal<?,?> graphTraversal, String rootIdentifier) {
        return graphTraversal.project(ClusterIdentifier.START,ClusterIdentifier.END,ClusterIdentifier.LENGTH,
                ClusterIdentifier.AMOUNT,
                ClusterIdentifier.BEST_NORM_SCORE,ClusterIdentifier.COUNT,ClusterIdentifier.BEST_PROPERTY,
                ClusterIdentifier.CATEGORY,
                ClusterIdentifier.ROOT_BEST_PROPERTY,ClusterIdentifier.ROOT_BEST_NORM_SCORE).
                by(__.values(ClusterIdentifier.START)).
                by(__.values(ClusterIdentifier.END)).
                by(__.values(ClusterIdentifier.LENGTH)).
                by(__.values(ClusterIdentifier.AMOUNT)).
                by(getBestPropertyTraversal().values(ClusterIdentifier.NORM_SCORE)).
                by(getBestPropertyTraversal().values(ClusterIdentifier.COUNT)).
                by(getBestPropertyTraversal().value()).
                by(getBestPropertyTraversal().values(ClusterIdentifier.CATEGORY)).
                by(getBestPropertyTraversal(__.select(rootIdentifier)).value()).
                by(getBestPropertyTraversal(__.select(rootIdentifier)).values(ClusterIdentifier.NORM_SCORE)).toList();
    }

    public static List<Map<String,Object>> getRootDistribution(GraphTraversalSource g) {
        return getRootsTraversal(g).order().by(__.values(ClusterIdentifier.START)).
                project(
//                ClusterIdentifier.HEIGHT,
                ClusterIdentifier.START,
                ClusterIdentifier.END,
                ClusterIdentifier.LENGTH,
                ClusterIdentifier.TOP_CANDIDATE_AMOUNT,
                ClusterIdentifier.AMOUNT,
                ClusterIdentifier.BEST_NORM_SCORE,
                ClusterIdentifier.BEST_SCORE,
                ClusterIdentifier.BEST_PROPERTY,
                ClusterIdentifier.AMOUNT).
//                by(getTreeHeightTraversal()).
                by(__.values(ClusterIdentifier.START)).
                by(__.values(ClusterIdentifier.END)).
                by(__.values(ClusterIdentifier.LENGTH)).
                by(__.values(ClusterIdentifier.TOP_CANDIDATE_AMOUNT)).
                        by(__.values(ClusterIdentifier.AMOUNT)).
                by(getBestPropertyTraversal().values(ClusterIdentifier.NORM_SCORE)).
                by(getBestPropertyTraversal().values(ClusterIdentifier.SCORE)).
                by(getBestPropertyTraversal().value()).
                by(__.values(ClusterIdentifier.AMOUNT)).
//                        order(Scope.local).by(__.select(ClusterIdentifier.START)).
                        toList();
    }


    public static List<Map<String,Object>> getDivergentPropertiesOfRootsAboveAndAtAlpha(GraphTraversalSource g, double filterAlphaValue) {
        final String rootIdentifier = "r";
        return getClusterListTraversal(getRootsAtAndAboveAlpha(g,filterAlphaValue,rootIdentifier).as(rootIdentifier).repeat(__.in()).emit(__.where(P.neq(rootIdentifier)).
                by(getBestPropertyTraversal().value()).and(getBestPropertyTraversal().values(ClusterIdentifier.NORM_SCORE).is(P.gte(filterAlphaValue)))
        ),rootIdentifier);

//        System.out.println(getBestPropertyTraversal(getRootsTraversal(g).as(rootIdentifier).repeat(__.in()).emit(__.
//                where(P.gte(rootIdentifier)).
//                by(getBestPropertyTraversal().values(ClusterIdentifier.NORM_SCORE))
//        )).values(ClusterIdentifier.NORM_SCORE).groupCount().toList());
//        System.out.println(getRootsTraversal(g).as(rootIdentifier).repeat(__.in()).emit(__.
//                where(P.gte(rootIdentifier)).
//                by(getBestPropertyTraversal().values(ClusterIdentifier.NORM_SCORE)))
//        .as("e1").
//                        where(rootIdentifier,P.gte("e1")).by(__.properties(ClusterIdentifier.PROPERTY).has(ClusterIdentifier.RANK,1).value()).toList());
//        return

//                getRootsTraversal(g).as(rootIdentifier).repeat(__.in()).emit(__.
//                where(P.gte(rootIdentifier)).
//                by(getBestPropertyTraversal().values(ClusterIdentifier.NORM_SCORE)).
//                where(P.neq(rootIdentifier)).by(getBestPropertyTraversal().value())
//        ).
//                getRootsTraversal(g).as(rootIdentifier).repeat(__.in()).emit(__.
//                        where(P.gte(rootIdentifier)).
//                        by(getBestPropertyTraversal().values(ClusterIdentifier.NORM_SCORE)))
//                        .as("e1").
//                        where(rootIdentifier,P.gte("e1")).by(__.properties(ClusterIdentifier.PROPERTY).has(ClusterIdentifier.RANK,1).value()).
//                project(ClusterIdentifier.START,ClusterIdentifier.END,ClusterIdentifier.LENGTH,
//                        ClusterIdentifier.AMOUNT,
//                        ClusterIdentifier.BEST_NORM_SCORE,ClusterIdentifier.COUNT,ClusterIdentifier.BEST_PROPERTY,
//                        ClusterIdentifier.CATEGORY,
//                        ClusterIdentifier.ROOT_BEST_PROPERTY,ClusterIdentifier.ROOT_BEST_NORM_SCORE).
//                by(__.values(ClusterIdentifier.START)).
//                by(__.values(ClusterIdentifier.END)).
//                by(__.values(ClusterIdentifier.LENGTH)).
//                by(__.values(ClusterIdentifier.AMOUNT)).
//                by(getBestPropertyTraversal().values(ClusterIdentifier.NORM_SCORE)).
//                by(getBestPropertyTraversal().values(ClusterIdentifier.COUNT)).
//                by(getBestPropertyTraversal().value()).
//                by(getBestPropertyTraversal().values(ClusterIdentifier.CATEGORY)).
//                by(getBestPropertyTraversal(__.select(rootIdentifier)).value()).
//                by(getBestPropertyTraversal(__.select(rootIdentifier)).values(ClusterIdentifier.NORM_SCORE)).
//                toList();
    }

    public static List<List<Object>> getLevelRanges(GraphTraversalSource g, int level) {
        return getLevelTraversal(g,level).order().by(__.values(ClusterIdentifier.START)).
                local(__.values(ClusterIdentifier.START,ClusterIdentifier.END).fold()).
                toList();
    }

    public static List<Map<String,Object>> getLevelDistribution(GraphTraversalSource g, int level) {
        return getLevelTraversal(g,level).project(
                ClusterIdentifier.START,
                ClusterIdentifier.END,
                ClusterIdentifier.LENGTH,
                ClusterIdentifier.TOP_CANDIDATE_AMOUNT,
                ClusterIdentifier.BEST_NORM_SCORE,
                ClusterIdentifier.BEST_SCORE,
                ClusterIdentifier.BEST_PROPERTY,
                ClusterIdentifier.AMOUNT).
                by(__.values(ClusterIdentifier.START)).
                by(__.values(ClusterIdentifier.END)).
                by(__.values(ClusterIdentifier.LENGTH)).
                by(__.values(ClusterIdentifier.TOP_CANDIDATE_AMOUNT)).
                by(getBestPropertyTraversal().values(ClusterIdentifier.NORM_SCORE)).
                by(getBestPropertyTraversal().values(ClusterIdentifier.SCORE)).
                by(getBestPropertyTraversal().value()).
                by(__.values(ClusterIdentifier.AMOUNT)).toList();
    }

    public static List<?> getNodesWithoutProperties(GraphTraversalSource g) {
        return
                g.V().as("v").properties(ClusterIdentifier.PROPERTY).where(__.local(__.values(ClusterIdentifier.RANK).min().is(P.neq(1)))).toList();
//                g.V().as("v").properties(ClusterIdentifier.PROPERTY).has(ClusterIdentifier.RANK).select("v").propertyMap().toList();
//                g.V().has(ClusterIdentifier.RANK).propertyMap().toList();
    }


    /*** Creation ***************************************************************************************************************/
    public static org.apache.tinkerpop.gremlin.structure.Vertex updateTree(TreeNode root, GraphTraversalSource g) {
        org.apache.tinkerpop.gremlin.structure.Vertex v =
                g.addV(ClusterIdentifier.CLUSTER).
                        property(ClusterIdentifier.START,root.getRange().getStart()).
                        property(ClusterIdentifier.END,root.getRange().getEnd()).
                        property(ClusterIdentifier.LENGTH,root.getRange().getLength()).
                        property(ClusterIdentifier.AMOUNT,root.getProperties().size()).
//                        property(ClusterIdentifier.AMOUNT,root.getProperties().stream().mapToLong(scoredProperty -> scoredProperty.getCount()).sum()).
                        property(ClusterIdentifier.TOP_CANDIDATE_AMOUNT,
                                root.topAmount()).
                        next();

        int counter = 1;

        for(ScoredProperty property: root.getProperties()) {

            v.property(VertexProperty.Cardinality.list,ClusterIdentifier.PROPERTY,property.getProperty(),
                    ClusterIdentifier.RANK,counter,
                    ClusterIdentifier.CATEGORY,property.getCategory(),
                    ClusterIdentifier.COUNT,property.getCount(),
                    ClusterIdentifier.SCORE,property.getScore(),
                    ClusterIdentifier.NORM_SCORE,property.getNormScore()
            );
            counter++;
        }


        return v;
    }

//    private static void createTree(TreeNode root, Vertex rootVertex, GraphTraversalSource g) {
//
////        System.out.println("Root " + root);
////        System.out.println(root.getRange());
//        for(TreeNode child: root.getChildren()) {
////            if(child.getRange().equals(new RangeDT(5793,11122))) {
////                System.out.println("Child " + child);
////            }
//            Vertex childV = updateTree(child,g);
//
//            g.addE(ClusterIdentifier.HAS_PARENT).from(childV).to(rootVertex).iterate();
//            createTree(child,childV,g);
//        }
//
//    }

    private static void createTree(TreeNode rootInitial, Vertex rootVertexInitial, GraphTraversalSource g) {


        Stack<TreeNode> roots = new Stack<>();
        Stack<Vertex> rootVerticees = new Stack<>();
        roots.push(rootInitial);
        rootVerticees.push(rootVertexInitial);
        while(!roots.isEmpty()) {
            TreeNode root = roots.pop();
//            System.out.println(root);
            Vertex rootVertex = rootVerticees.pop();
            for(TreeNode child: root.getChildren()) {
                Vertex childV = updateTree(child,g);
                g.addE(ClusterIdentifier.HAS_PARENT).from(childV).to(rootVertex).iterate();
                roots.push(child);
                rootVerticees.push(childV);
            }
        }

    }

    public static GraphTraversalSource createTree(TreeNode root) {
        return createTree(root, Graph.createEmptySubGraph());
    }

    public static GraphTraversalSource createTree(TreeNode root, GraphTraversalSource g) {

        createTree(root,updateTree(root,g),g);
        return g;
    }

    public static void persistTree(List<TreeNode> roots, File treeFile) {
        if(roots == null || roots.size() == 0) {
            LOGGER.error("No roots provided");
            return;
        }
        GraphTraversalSource g = createTree(roots.get(0));
//        System.out.println(g.V().local(__.properties("property").count()).toList());
//        createTree(roots.get(4),g);
        for(int i = 1; i < roots.size(); i++) {
//            LOGGER.info("At root " + i + ": " + roots.get(i));
            g = createTree(roots.get(i),g);
        }
//        Graph.writeToGraphML(treeFile.getAbsolutePath(),g);
        Graph.writeToGryo(treeFile.getAbsolutePath(),g);
    }

    /*** Computation ***************************************************************************************************************/

    public  static List<PropertyRangeProbDT>  extractBestPropertiesProperties(GraphTraversalSource g, int length) {
        List<Map<String,Object>> clusters = getClusterListTraversal(g.V().as("root"),"root");
        List<PropertyRangeProbDT> propertyRangeDTS = clusters.stream().map(m -> convertToPropertyRangeDT(m)).collect(Collectors.toList());
        Collections.sort(propertyRangeDTS);
        propertyRangeDTS = PropertyRangeDT.collapseRanges(propertyRangeDTS.stream().
                filter(p -> !p.getProperty().equals("void")).
                collect(Collectors.toList()),length);
        Collections.sort(propertyRangeDTS);

//        Auxiliary.printSeq(propertyRangeDTS);
        return propertyRangeDTS;

    }

    public static void analyzeOtherCandidates(GraphTraversalSource g, double filterAlphaValue, List<String> properties) {
        final String rootIdentifier = "root";

        // get subclusters with any property annotated of roots below alpha
        List<Map<String,Object>> clusters =
                getClusterListTraversal(
                        getRootsBelowAlpha(g,filterAlphaValue,rootIdentifier).repeat(__.in()).emit(__.where(
                                getBestPropertyTraversal().values(ClusterIdentifier.NORM_SCORE).is(P.gte(filterAlphaValue)))), rootIdentifier);

        // get subclusters with other properties annotated than the root best property of roots above and at alpha
        clusters.addAll(getDivergentPropertiesOfRootsAboveAndAtAlpha(g,filterAlphaValue));
        // get cluster at roots above and at alpha
        clusters.addAll(getClusterListTraversal(getRootsAtAndAboveAlpha(g,filterAlphaValue, rootIdentifier),rootIdentifier));
        clusters = clusters.stream().filter(m -> properties.contains(m.get(ClusterIdentifier.BEST_PROPERTY))).collect(Collectors.toList());
        for(Map<String,Object> cluster: clusters) {
            System.out.println("best property:" + cluster.get(ClusterIdentifier.BEST_PROPERTY));
            System.out.println(g.V().has(ClusterIdentifier.START,cluster.get(ClusterIdentifier.START)).has(ClusterIdentifier.END,cluster.get(ClusterIdentifier.END)).
                    filter(getAllButBestPropertyTraversal()).values(ClusterIdentifier.PROPERTY).toList());
        }
    }

    public static List<PropertyRangeProbDT> createAnnotatedProperties(GraphTraversalSource g, double filterAlphaValue, int length) {
        final String rootIdentifier = "root";
        final int minLength = 5;

        // get subclusters with any property annotated of roots below alpha
        List<Map<String,Object>> clusters =
                getClusterListTraversal(
                        getRootsBelowAlpha(g,filterAlphaValue,rootIdentifier).repeat(__.in()).emit(__.where(
                                getBestPropertyTraversal().values(ClusterIdentifier.NORM_SCORE).is(P.gte(filterAlphaValue)))), rootIdentifier);

        // get subclusters with other properties annotated than the root best property of roots above and at alpha
        clusters.addAll(getDivergentPropertiesOfRootsAboveAndAtAlpha(g,filterAlphaValue));
        // get cluster at roots above and at alpha
        clusters.addAll(getClusterListTraversal(getRootsAtAndAboveAlpha(g,filterAlphaValue, rootIdentifier),rootIdentifier));


        List<PropertyRangeProbDT> propertyRangeDTS = clusters.stream().map(m -> convertToPropertyRangeDT(m)).collect(Collectors.toList());
        Collections.sort(propertyRangeDTS);
//        Auxiliary.printSeq(propertyRangeDTS);


//        Auxiliary.printSeq(propertyRangeDTS);
//        System.out.println();
        propertyRangeDTS = PropertyRangeDT.collapseRanges(propertyRangeDTS.stream().
                filter(p -> !p.getProperty().equals("void")).
                collect(Collectors.toList()),length);
        Collections.sort(propertyRangeDTS);
//        System.out.println("kdkdk");
//        Auxiliary.printSeq(propertyRangeDTS);
        return propertyRangeDTS;
    }



    public static Map<String,Map<?,Long>> getCountStatistics(List<Map<String,Object>> propertyDistribution, Set<String> keys) {
        Map<String,Map<?,Long>> countMaps = new HashMap<>();
        for(String key: keys) {
            System.out.println(key);
            countMaps.put(key, propertyDistribution.stream().
                    map(p -> {
                        if(p.get(key) instanceof Double){
                            return new BigDecimal((double)p.get(key)).setScale(2, RoundingMode.HALF_UP);
                        }
                        else {
                            return p.get(key);
                        }
                    }).
                    collect(Collectors.groupingBy(Function.identity(), TreeMap::new,Collectors.counting())));
        }
        return countMaps;
    }

    public static Map<String,Map<?,Long>>  getCountStatistics(List<Map<String,Object>> propertyDistribution) {
        Set<String> keys = propertyDistribution.get(0).keySet();
        return getCountStatistics(propertyDistribution,keys);
    }

    /*** Auxiliary ***************************************************************************************************************/


//    public static List<Map<String,Object>> getDivergentClusters(GraphTraversalSource g, double filterAlphaValue) {
//
//        List<Map<String,Object>> divergentProperties = Cluster.getDivergentProperties(g,filterAlphaValue);
//        Collections.sort(divergentProperties,getClusterComparator(ClusterIdentifier.START));
//        divergentProperties.get(0).keySet().removeIf(
//                Predicate.isEqual(ClusterIdentifier.ROOT_BEST_NORM_SCORE).
//                        or(Predicate.isEqual(ClusterIdentifier.AMOUNT))
//        );
//
//        List<Map<String,Object>> divergentClusters = new ArrayList<>();
//        divergentClusters.add(divergentProperties.get(0));
//        Map<String,Object> prevCluster;
//        Map<String,Object> nextCluster;
//
//        for(int clusterIndex = 1; clusterIndex < divergentProperties.size(); clusterIndex++) {
//            prevCluster = divergentClusters.get(divergentClusters.size()-1);
//            nextCluster = divergentProperties.get(clusterIndex);
//            nextCluster.keySet().removeIf(
//                    Predicate.isEqual(ClusterIdentifier.ROOT_BEST_NORM_SCORE).
//                            or(Predicate.isEqual(ClusterIdentifier.AMOUNT))
//            );
//            int prevClusterEnd = (int)prevCluster.get(ClusterIdentifier.END);
//            if((int)nextCluster.get(ClusterIdentifier.START) == prevClusterEnd+1 &&
//                nextCluster.get(ClusterIdentifier.BEST_PROPERTY).equals(prevCluster.get(ClusterIdentifier.BEST_PROPERTY))
//            ) {
//                prevCluster.put(ClusterIdentifier.END, prevClusterEnd+1);
//                prevCluster.put(ClusterIdentifier.COUNT,(long)prevCluster.get(ClusterIdentifier.COUNT)+(long)nextCluster.get(ClusterIdentifier.COUNT));
//                prevCluster.put(ClusterIdentifier.LENGTH,(int)prevCluster.get(ClusterIdentifier.LENGTH)+ (int)nextCluster.get(ClusterIdentifier.LENGTH));
//                prevCluster.put(ClusterIdentifier.BEST_NORM_SCORE,
//                        ((BigDecimal)prevCluster.get(ClusterIdentifier.BEST_NORM_SCORE)).add((BigDecimal)nextCluster.get(ClusterIdentifier.BEST_NORM_SCORE)));
//
//            }
//            else {
//                divergentClusters.add(nextCluster);
//            }
//        }
//        for(Map<String,Object> m : divergentClusters) {
//            m.put(ClusterIdentifier.BEST_NORM_SCORE,(((BigDecimal)m.get(ClusterIdentifier.BEST_NORM_SCORE)).divide(new BigDecimal((int)m.get(ClusterIdentifier.LENGTH),MathContext.DECIMAL128),MathContext.DECIMAL128)));
//        }
//        return divergentClusters;
//    }

    public static PropertyRangeProbDT convertToPropertyRangeDT(Map<String, Object> map) {
        return  new PropertyRangeProbDT(
                (String)map.get(ClusterIdentifier.CATEGORY),
                (String)map.get(ClusterIdentifier.BEST_PROPERTY),
                (int)map.get(ClusterIdentifier.START),
                (int)map.get(ClusterIdentifier.END),
                (int)map.get(ClusterIdentifier.LENGTH),
                (BigDecimal)map.get(ClusterIdentifier.BEST_NORM_SCORE)
                );
    }

    public static void getRootBelowAlphaStatistic(GraphTraversalSource g, double alphaValue) {
//        Auxiliary.printSeq(getRootsBelowAlpha(g,alphaValue,"root").project("prob","length").
//                by(getBestPropertyTraversal().values(ClusterIdentifier.NORM_SCORE)).
//                by(__.values(ClusterIdentifier.LENGTH)).toList());

        List<Map<String,Object>> clusters =
        getClusterListTraversal(
        getRootsBelowAlpha(g,TreeNode.alphaValue.doubleValue(),"root").repeat(__.in()).emit(__.where(
                getBestPropertyTraversal().values(ClusterIdentifier.NORM_SCORE).is(P.gte(alphaValue)))), "root");

        Collections.sort(clusters,getClusterComparator("start"));
        Auxiliary.printSeq(clusters);
//        Auxiliary.printSeq(getRootsBelowAlpha(g,alphaValue,"root").values(ClusterIdentifier.LENGTH).groupCount().toList());
//        System.out.println();
//        Auxiliary.printSeq(getBestPropertyTraversal(getRootsBelowAlpha(g,alphaValue,"root")).values(ClusterIdentifier.NORM_SCORE).groupCount().toList());
    }

//    public static Dataset<PropertyRangeProbDT> getDivergentClusters(GraphTraversalSource g, double filterAlphaValue) {
//
//        List<Map<String,Object>> divergentProperties = Cluster.getDivergentPropertiesOfRootsAboveAndAtAlpha(g,filterAlphaValue);
//        Collections.sort(divergentProperties,getClusterComparator(ClusterIdentifier.START));
//        List<PropertyRangeProbDT> divergentClusters = new ArrayList<>();
//
//        divergentClusters.add(convertToPropertyRangeDT(divergentProperties.get(0)));
//        PropertyRangeProbDT prevCluster;
//        PropertyRangeProbDT nextCluster;
//
//        for(int clusterIndex = 1; clusterIndex < divergentProperties.size(); clusterIndex++) {
//            prevCluster = divergentClusters.get(divergentClusters.size()-1);
//            nextCluster = convertToPropertyRangeDT(divergentProperties.get(clusterIndex));
//
//            if(nextCluster.getStart() == prevCluster.getEnd()+1 &&
//                    nextCluster.getProperty().equals(prevCluster.getProperty())
//            ) {
//                prevCluster.setEnd(prevCluster.getEnd()+1);
//                prevCluster.setLength(prevCluster.getLength()+1);
//                prevCluster.setProbability(prevCluster.getProbability().add(nextCluster.getProbability()));
//            }
//            else {
//                divergentClusters.add(nextCluster);
//            }
//        }
//        for(PropertyRangeProbDT m : divergentClusters) {
//            m.setProbability((m.getProbability().divide(new BigDecimal(m.getLength(),MathContext.DECIMAL128),MathContext.DECIMAL128)));
//        }
//        return SparkComputer.createDataFrame(divergentClusters,PropertyRangeProbDT.ENCODER).
//                select(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY,ColumnIdentifier.PROBABILITY).as(PropertyRangeProbDT.ENCODER);
//    }
//
//
//    public static Dataset<PropertyRangeProbDT> getClusters(GraphTraversalSource g) {
//        return SparkComputer.createDataFrame(((List<Map<String, Object>>)getRootsTraversal(g).
//                project(ClusterIdentifier.START,ClusterIdentifier.END,ClusterIdentifier.LENGTH,
//                        ClusterIdentifier.BEST_NORM_SCORE,ClusterIdentifier.BEST_PROPERTY,
//                        ClusterIdentifier.CATEGORY).
//                by(__.values(ClusterIdentifier.START)).
//                by(__.values(ClusterIdentifier.END)).
//                by(__.values(ClusterIdentifier.LENGTH)).
//                by(getBestPropertyTraversal().values(ClusterIdentifier.NORM_SCORE)).
//                by(getBestPropertyTraversal().value()).
//                by(getBestPropertyTraversal().values(ClusterIdentifier.CATEGORY)).toList()).stream().
//                map(m -> convertToPropertyRangeDT(m)).collect(Collectors.toList()),PropertyRangeProbDT.ENCODER).
//                select(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY,ColumnIdentifier.PROBABILITY).as(PropertyRangeProbDT.ENCODER);
//    }
//
//    public static Dataset<PropertyRangeProbDT> getAllClusters(GraphTraversalSource g, double filterAlphaValue) {
//        return getDivergentClusters(g,filterAlphaValue).union(getClusters(g));
//    }
    /*** Getter and Setter ***************************************************************************************************************/

    public GraphTraversalSource getG() {
        return g;
    }

    public void setG(GraphTraversalSource g) {
        this.g = g;

    }

    public void setG(File inputFile) {
        this.g = Graph.readGryo(inputFile.getAbsolutePath());
//        System.out.println(getRootsTraversal(g).properties().toList());
//        System.out.println("here" + getRootsBelowAlpha(this.g,0.6,"v").toList());
    }

}