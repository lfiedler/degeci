package de.uni_leipzig.informatik.pacosy.mitos.core.util.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.Scanner;

public class Configuration {


    protected Properties properties;

    protected String propertiesFileName;

    private Scanner scanner;

    protected static final Logger LOGGER = LoggerFactory.getLogger(Configuration.class);

    public Configuration(File propertiesFile) {
        this.propertiesFileName = propertiesFile.getName();
        this.properties = new Properties();
        this.scanner = new Scanner(System.in);

        try{
            properties.load(new FileInputStream(propertiesFile));

            LOGGER.info("Successfully loaded properties file");
        } catch(IOException e) {
            LOGGER.error("Properties file "+ propertiesFileName +" could not be loaded. Maybe different properties file name?");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Configuration(InputStream in, String propertiesFileName) {
        this.propertiesFileName = propertiesFileName;
        this.properties = new Properties();
        this.scanner = new Scanner(System.in);

        try{
            properties.load(in);

            LOGGER.info("Successfully loaded properties file");
        } catch(IOException e) {
            LOGGER.error("Properties file "+ propertiesFileName +" could not be loaded. Maybe different properties file name?");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Configuration(Object o, String propertiesFileName) {

        this.propertiesFileName = propertiesFileName;
        this.properties = new Properties();
        this.scanner = new Scanner(System.in);

        try{
            properties.load(o.getClass().getClassLoader().
                    getResourceAsStream(propertiesFileName));

            LOGGER.info("Successfully loaded properties file");
        } catch(IOException e) {
            LOGGER.error("Properties file "+ propertiesFileName +" could not be loaded. Maybe different properties file name?");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static File getFile(Object o, String propertiesFileName) {
        return new File(o.getClass().getClassLoader().getResource(propertiesFileName).getFile());
    }

    /**
     * Retrieves the provided identifier from properties.
     * If the identifier is not found, a MissingResourceException is thrown.
     * @param identifier Key element of properties
     * @return The value, corresponding to the given identifier in properties.
     * @throws MissingResourceException
     */
    protected String getEntry(String identifier) throws MissingResourceException {
        String p = properties.getProperty(identifier);
        if(p == null) {
            throw new MissingResourceException("Missing property " + identifier,propertiesFileName,identifier);
        }
        return p;
    }

    /**
     * Retrieves the provided identifier from properties.
     * If the identifier is not found, defaultValue is returned
     * @param identifier Key element of properties
     * @param defaultValue
     * @return
     */
    protected String getEntry(String identifier, String defaultValue) {
        String p = properties.getProperty(identifier);
        if(p == null) {
            p = defaultValue;
        }
        return p;
    }

    /**
     * Set property element key to value.
     * If the element already exists a warning is issued.
     * @param key
     * @param value
     */
    protected void setProperty(String key,String value) {
        if(properties.getProperty(key) != null) {
            LOGGER.warn("Overriding property " + key + "=" + properties.getProperty(key) +
                    " provided in " + propertiesFileName + "file");
        }
        properties.setProperty(key,value);
    }

    /**
     * Set property element key to value specified on command line
     * @param key
     */
    protected void setPropertyFromCommandLine(String key) {
        LOGGER.error("Missing property"+key+" must be specified");
        System.out.println(key+"=");
        setProperty(key,scanner.next());
    }

    /**
     * Get the properties map
     * @return
     */
    public Properties getProperties() {
        return properties;
    }



}
