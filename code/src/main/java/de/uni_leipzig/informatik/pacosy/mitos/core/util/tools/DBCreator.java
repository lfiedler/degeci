package de.uni_leipzig.informatik.pacosy.mitos.core.util.tools;

import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.DeGeCI;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.PairwiseAligner;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.GraphConfiguration;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.Graph;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.TaxonomyGraph;
import de.uni_leipzig.informatik.pacosy.mitos.core.parser.TaxonomyParser;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.*;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.ColumnFunctions;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.*;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.InputParser;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.types.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.exit;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

public class DBCreator {

    static private final String TAXONOMY_NAMES_FILENAME = "names.dmp";

    static private final String TAXONOMY_NODES_FILENAME = "nodes.dmp";

    static private final String TAXONOMY_MERGE_FILENAME = "merged.dmp";

    static private final String TAXONOMY_DIVISION_FILENAME = "division.dmp";

    public enum ACTION {


        PERSIST_KMERS_PSQL,
        PERSIST_TAXONOMY,
        PERSIST_RECORDS,
    }

    private static Logger LOGGER = LoggerFactory.getLogger(DBCreator.class);

    private static Pattern gbPattern = Pattern.compile("^LOCUS\\p{Blank}*\\p{Upper}{2}_\\p{Digit}+\\p{Blank}*([1-9]\\p{Digit}*)" +
            "\\p{Blank}*bp\\p{Blank}*DNA\\p{Blank}*(circular|linear)\\p{Blank}*\\p{Upper}{3}\\p{Blank}" +
            "\\p{Digit}{2}-(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)-(1|2)\\p{Digit}{3}$");

    private static Pattern gbPattern2= Pattern.compile("^LOCUS.*");

    private static Pattern versionPattern = Pattern.compile("^VERSION\\p{Blank}*\\p{Upper}{2}_\\p{Digit}+\\.([1-9]\\p{Digit}*)$");

    private static Pattern taxidPattern = Pattern.compile("\\p{Blank}*/db_xref=\"taxon:([1-9]\\p{Digit}*)\"");

    private String sequenceDir;

    private int amount = -1;

    private int k;

    private DeGeCI.AmbigFilter ambigFilter;

    private List<String> ids;


    private static Dataset<RecordDT> recordData;

    private TaxonomyGraph taxonomyGraph;

    private DataDirectoryParser dataDirectoryParser;


    private ACTION action;

    private List<String> skipDirectories;

    private List<String> directories;

    static {
        Spark.getInstance();
        if(Spark.checkExisting(ProjectDirectoryManager.getRECORD_DATA_DIR())){
            recordData = SparkComputer.readORC(ProjectDirectoryManager.getRECORD_DATA_DIR(), RecordDT.ENCODER);
        }
        else {
            recordData = null;
        }
    }
    private enum SEQUENCE_FILES {
        BED("bed"),
        FASTA("fasta"),
        GB("gb"),
        RECORD("record");

        public final String identifier;
        SEQUENCE_FILES(String identifier) {
            this.identifier = identifier;
        }
    }

    public enum STATISTIC_FILE_NAMES {
        PLUS_RANDOM_RANGE_LENGTH("plusRandomRangeLength.csv"),
        MINUS_RANDOM_COUNT("minusRandomCount.csv"),
        PLUS_NUCLEOTIDE_COUNT("plusNucleotideCount.csv"),
        MINUS_NUCLEOTIDE_COUNT("minusNucleotideCount.csv"),
        SEQUENCE_TAXONOMY_COUNT_STATISTICS("countStatistic.txt");

        public final String identifier;
        STATISTIC_FILE_NAMES(String identifier) {
            this.identifier = identifier;
        }
    }

    public DBCreator(String[] args) {

        PersistenceInputParser persistenceInputParser =
                new PersistenceInputParser(args);

    }


    public DBCreator(String fileName) {

        PersistenceInputParser persistenceInputParser =
                new PersistenceInputParser(new String[] {"-f",fileName});


    }

    public DBCreator(boolean getHelp) {

        if(getHelp) {
            PersistenceInputParser persistenceInputParser =
                    new PersistenceInputParser(new String[] {"--help"});
        }


    }

    private static String readSingleFastaFile(String fileName) {
        StringBuilder contentBuilder = new StringBuilder();
        char commentCharacter = '>';

        Function<String,String> parser =  (s -> s);
        try (Stream<String> stream = Files.lines( Paths.get(fileName), StandardCharsets.UTF_8))
        {

            if (stream.filter(s -> s.charAt(0) == commentCharacter).count() != 1) {
                return null;
            }

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        try (Stream<String> stream = Files.lines( Paths.get(fileName), StandardCharsets.UTF_8))
        {

            stream.filter(s -> s.charAt(0) != commentCharacter)
                    .forEach(s ->  contentBuilder.append(parser.apply(s)));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }

    public static Dataset<Row> parseSequences(String pathToFastaFiles) throws IOException {

        List<Row> sequenceList = new ArrayList<>();
        for( File file : Files.walk(Paths.get(pathToFastaFiles),1).filter(Files::isRegularFile)
                .filter(x -> x.toString().endsWith(".fna")).map(p -> p.toFile()).collect(Collectors.toList())) {
            sequenceList.add(RowFactory.create(
                    readSingleFastaFile(file.getAbsolutePath())
                    , RecordDT.mapToRecordId(file.getName().substring(0,file.getName().indexOf(".")))
            ));
        }
        return Spark.getInstance().createDataFrame(sequenceList, new StructType(new StructField[]{
                new StructField(ColumnIdentifier.SEQUENCE, StringType,false, Metadata.empty()),
                new StructField(ColumnIdentifier.RECORD_ID, IntegerType,false, Metadata.empty()),
        }));
    }

    public static Dataset<Row> createPropertyRanges(String pathToBedFiles, Dataset<Row> recordData) {
        return
                Spark.getInstance().read().text(pathToBedFiles+"/*.bed").
                        withColumn("s",split(col(ColumnIdentifier.VALUE),"\t")).
                        filter(size(col("s")).equalTo(6)).
                        drop(ColumnIdentifier.VALUE).
//                withColumn(ColumnIdentifier.NAME,element_at(col("s"),1)).
        withColumn(ColumnIdentifier.NAME,regexp_replace(element_at(col("s"),1)," ","")).

                        withColumn(ColumnIdentifier.START,element_at(col("s"),2).cast(DataTypes.IntegerType)).
                        withColumn(ColumnIdentifier.END,element_at(col("s"),3).cast(DataTypes.IntegerType)).
                        withColumn(ColumnIdentifier.STRAND,element_at(col("s"),6)).

                        withColumn(ColumnIdentifier.PROPERTY,element_at(col("s"),4)).
                        filter(not(col(ColumnIdentifier.PROPERTY).contains("Intron")) ).
                        filter(not(col(ColumnIdentifier.PROPERTY).contains("orf")) ).
                        withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"\\(.*\\)","")).
                        withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"_.*","")).
                        withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"-.*","")).
                        withColumn(ColumnIdentifier.CATEGORY,
                                when(col(ColumnIdentifier.PROPERTY).isInCollection(Arrays.asList("OH","OL")),"repOrigin").
                                        when(substring(col(ColumnIdentifier.PROPERTY),1,3).equalTo("trn"),"trna").
                                        when(substring(col(ColumnIdentifier.PROPERTY),1,3).equalTo("rrn"),"rrna").
                                        otherwise("protein")).
//                groupBy().agg(collect_set(ColumnIdentifier.NAME)).
        join(recordData.select(col(ColumnIdentifier.NAME).as("n"),col(ColumnIdentifier.RECORD_ID),col(ColumnIdentifier.LENGTH).as(ColumnIdentifier.SEQUENCE_LENGTH)),
        col(ColumnIdentifier.NAME).equalTo(col("n"))).drop("n","s",ColumnIdentifier.NAME).
                        withColumn(ColumnIdentifier.END,
                                when(col(ColumnIdentifier.END).notEqual(col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)),col(ColumnIdentifier.END).minus(1)).
                                        otherwise(col(ColumnIdentifier.END))).
                        withColumn(ColumnIdentifier.LENGTH, ColumnFunctions.getLinearRangeLength(ColumnIdentifier.START,ColumnIdentifier.END)).
                        withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"trn","")).
                        withColumn(ColumnIdentifier.GENE,concat_ws("",col(ColumnIdentifier.PROPERTY),col(ColumnIdentifier.STRAND))).
                        withColumn(ColumnIdentifier.STRAND,when(col(ColumnIdentifier.STRAND).equalTo("+"),true).otherwise(false));
    }

    public static Dataset<Row> addVoidPropertiesPerStrand(Dataset<Row> genomeMapping) {
        String[] columns =  genomeMapping.columns();
//            Dataset<Row> voids =
//                    genomeMapping.select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.STRAND).distinct().
//                withColumn(ColumnIdentifier.POSITION,explode(sequence(lit(0),col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)))).
//                except(genomeMapping.withColumn(ColumnIdentifier.POSITION,explode(sequence(col(ColumnIdentifier.START),col(ColumnIdentifier.END)))).
//                        select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.STRAND,ColumnIdentifier.POSITION).distinct()).
//                withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,col(ColumnIdentifier.POSITION).minus(
//                        row_number().over(Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND).orderBy(ColumnIdentifier.POSITION)))).
//                groupBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.STRAND,ColumnIdentifier.DIFF_COLUMN_IDENTIFIER).
//                agg(min(col(ColumnIdentifier.POSITION)).as(ColumnIdentifier.START),max(col(ColumnIdentifier.POSITION)).as(ColumnIdentifier.END)).
//                withColumn(ColumnIdentifier.CATEGORY,lit("void")).
//                withColumn(ColumnIdentifier.PROPERTY,lit("void")).
//                withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getLinearRangeLength(ColumnIdentifier.START,ColumnIdentifier.END)).
//                withColumn(ColumnIdentifier.GENE,concat_ws("_",lit("void"),row_number().over(Window.partitionBy(ColumnIdentifier.RECORD_ID).orderBy(ColumnIdentifier.START)))).
//                select(SparkComputer.getColumns(columns));

        genomeMapping = genomeMapping.filter(col(ColumnIdentifier.CATEGORY).notEqual("void")).persist();
//
        Dataset<Row> inverseRanges =genomeMapping
                .withColumn(ColumnIdentifier.POSITION,explode(sequence(col(ColumnIdentifier.START),col(ColumnIdentifier.END))))
                .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.POSITION).distinct()
                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,col(ColumnIdentifier.POSITION).minus(
                        row_number().over(Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND).orderBy(ColumnIdentifier.POSITION)))).
                groupBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.DIFF_COLUMN_IDENTIFIER).
                agg(min(col(ColumnIdentifier.POSITION)).as(ColumnIdentifier.START),max(col(ColumnIdentifier.POSITION)).as(ColumnIdentifier.END)).
                orderBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.START);
//
//
//                      inverseRanges  .show(5000);
//        inverseRanges.
//                withColumn(ColumnIdentifier.INVERSE_START,
//                        lag(col(ColumnIdentifier.END),1).
//                                over(Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND).orderBy(ColumnIdentifier.START)).plus(1)).
//                na().fill(0,new String[]{ColumnIdentifier.INVERSE_START}).
////                withColumn(ColumnIdentifier.INVERSE_START,
////                        lag(col(ColumnIdentifier.END),1,0).
////                                over(Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND).orderBy(ColumnIdentifier.START)).plus(1)).
//                withColumn(ColumnIdentifier.INVERSE_END,
//                        when(col(ColumnIdentifier.START).gt(0),
//                                col(ColumnIdentifier.START).minus(1)).otherwise(-1)).
//                orderBy(ColumnIdentifier.STRAND,ColumnIdentifier.START).show(5000);

        inverseRanges =        inverseRanges.

//                withColumn(ColumnIdentifier.INVERSE_START,
//                        lag(col(ColumnIdentifier.END),1,0).
//                                over(Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND).orderBy(ColumnIdentifier.START)).plus(1)).
        withColumn(ColumnIdentifier.INVERSE_START,
        lag(col(ColumnIdentifier.END),1).
                over(Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND).orderBy(ColumnIdentifier.START)).plus(1)).
                na().fill(0,new String[]{ColumnIdentifier.INVERSE_START}).

                withColumn(ColumnIdentifier.INVERSE_END,
                        when(col(ColumnIdentifier.START).gt(0),
                                col(ColumnIdentifier.START).minus(1)).otherwise(-1)).
                filter(col(ColumnIdentifier.INVERSE_END).gt(-1)).
                drop(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.DIFF_COLUMN_IDENTIFIER).
                withColumn(ColumnIdentifier.CATEGORY,lit("void")).
                withColumn(ColumnIdentifier.PROPERTY,lit("void")).
                withColumnRenamed(ColumnIdentifier.INVERSE_START,ColumnIdentifier.START).
                withColumnRenamed(ColumnIdentifier.INVERSE_END,ColumnIdentifier.END).
                withColumn(ColumnIdentifier.LENGTH, ColumnFunctions.getLinearRangeLength(ColumnIdentifier.START,ColumnIdentifier.END)).
                withColumn(ColumnIdentifier.GENE,concat_ws("_",lit("void"),row_number().over(Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND)
                        .orderBy(ColumnIdentifier.START)))).
                select(SparkComputer.getColumns(genomeMapping.columns()))
                .union(genomeMapping);

        return inverseRanges.groupBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.SEQUENCE_LENGTH).
                agg(max(col(ColumnIdentifier.END)).as(ColumnIdentifier.START)).
                filter(col(ColumnIdentifier.START).notEqual(col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)))
                .withColumn(ColumnIdentifier.START,col(ColumnIdentifier.START).plus(1))
                .withColumn(ColumnIdentifier.END,col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1))
                .  withColumn(ColumnIdentifier.CATEGORY,lit("void"))
                .withColumn(ColumnIdentifier.PROPERTY,lit("void"))
                .withColumn(ColumnIdentifier.GENE,lit("void_0"))
                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getLinearRangeLength(ColumnIdentifier.START,ColumnIdentifier.END))

                .select(SparkComputer.getColumns(genomeMapping.columns()))                .union(inverseRanges);

//        return inverseRanges.union(genomeMapping);
//                select(
//                        col(ColumnIdentifier.INVERSE_START).as(ColumnIdentifier.START),
//                        col(ColumnIdentifier.INVERSE_END).as(ColumnIdentifier.END)).
//                withColumn(ColumnIdentifier.LENGTH,col(ColumnIdentifier.END).minus(col(ColumnIdentifier.START)).plus(1)).
//
//        Dataset<Row> voids =
//                genomeMapping.filter(col(ColumnIdentifier.CATEGORY).notEqual("void")).
//                        select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.SEQUENCE_LENGTH).distinct().
//                        withColumn(ColumnIdentifier.STRAND,explode(array(lit(true),lit(false)))).
//                        withColumn(ColumnIdentifier.POSITION,explode(sequence(lit(0),col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)))).
//                        select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.POSITION).
//
//                        except(genomeMapping.withColumn(ColumnIdentifier.POSITION,explode(sequence(col(ColumnIdentifier.START),col(ColumnIdentifier.END)))).
//                                select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.POSITION).distinct()).
//                        withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,col(ColumnIdentifier.POSITION).minus(
//                                row_number().over(Window.partitionBy(ColumnIdentifier.RECORD_ID).orderBy(ColumnIdentifier.POSITION)))).
//                        groupBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.DIFF_COLUMN_IDENTIFIER).
//                        agg(min(col(ColumnIdentifier.POSITION)).as(ColumnIdentifier.START),max(col(ColumnIdentifier.POSITION)).as(ColumnIdentifier.END)).
//                        withColumn(ColumnIdentifier.CATEGORY,lit("void")).
//                        withColumn(ColumnIdentifier.PROPERTY,lit("void")).
//                        withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getLinearRangeLength(ColumnIdentifier.START,ColumnIdentifier.END)).
//                        withColumn(ColumnIdentifier.GENE,concat_ws("_",lit("void"),row_number().over(Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND)
//                                .orderBy(ColumnIdentifier.START)))).
//                        select(SparkComputer.getColumns(columns));
//        return genomeMapping.union(voids);
    }

    private static RecordDT parseRecordFromGB(File gbFile) {

        Matcher m;
        RecordDT record = new RecordDT();
        try {


            BufferedReader br = new BufferedReader(new FileReader(gbFile));

            // parse length and topology
            String line = br.readLine();

            String name = gbFile.getName().substring(0,gbFile.getName().indexOf("."));
            System.out.println(name);
            record.setName(name);
            m = gbPattern.matcher(line);
            if(m.matches()) {
                record.setLength(Integer.parseInt(m.group(1)));

                record.setTopology(m.group(2).equals("circular") ? true : false);
            }
            else {
                LOGGER.error("LOCUS field " + line + " does not match");
                throw new IllegalArgumentException("LOCUS field " + line + " does not match");
            }


            while((line = br.readLine()) != null)  {
                m = versionPattern.matcher(line);
                if(m.matches()) {
                    record.setVersion(Integer.parseInt(m.group(1)));
                    break;
                }

            }

            while((line = br.readLine()) != null)  {
                m = taxidPattern.matcher(line);
                if(m.matches()) {
                    record.setTaxid(Integer.parseInt(m.group(1)));
                    break;
                }

            }
            if(line == null) {
                LOGGER.error("Taxid not specified in file " + gbFile.getName());
                throw new MissingResourceException("Taxid not specified in file " + gbFile.getName(),"Record","taxid");
            }
            br.close();

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return record;
    }




    public static void  createRecordDataset(String seqDir) throws IOException {
        LOGGER.info("Persisting record information");
        List<RecordDT> records = new ArrayList<>();
        for( File file : Files.walk(Paths.get(seqDir+"/gb"),1).filter(Files::isRegularFile)
                     .filter(x -> x.toString().endsWith(".gb")).map(p -> p.toFile()).collect(Collectors.toList())) {

            RecordDT recordDT = parseRecordFromGB(file);
            records.add(recordDT);

        }
        SparkComputer.persistDataFrameORC(SparkComputer.createDataFrame(records,RecordDT.ENCODER),ProjectDirectoryManager.getRECORD_DATA_DIR());
        LOGGER.info("Persisting Gene Annotations");
        Dataset<Row> gm = createPropertyRanges(seqDir+"/bed",SparkComputer.readORC(ProjectDirectoryManager.getRECORD_DATA_DIR()));
        SparkComputer.persistDataFrameORC(gm,ProjectDirectoryManager.getPROPERTY_DIR());
        SparkComputer.persistDataFrameORC(addVoidPropertiesPerStrand(gm),ProjectDirectoryManager.getPROPERTY_VOIDS_DIR());
        Dataset<Row> ranges = DeGeCI.createCyclicPropertyRangesWithRecords(SparkComputer.readORC(ProjectDirectoryManager.getPROPERTY_VOIDS_DIR()))
                . groupBy(ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY)
                .agg(count("*").as(ColumnIdentifier.COUNT),
                        mean(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.MEAN)
                        );
        SparkComputer.persistDataFrameORC(ranges,ProjectDirectoryManager.getGENE_LENGTHS_DIR());

        LOGGER.info("Persisting sequences");
        SparkComputer.persistDataFrameORC(
                PairwiseAligner.addReverseComplementSequences(parseSequences(seqDir+"/fasta")),
                ProjectDirectoryManager.getSEQUENCES_DIR());
    }

    public void persistKmersToSQL(int k) throws SQLException {


        List<File> files =  dataDirectoryParser.getAllFiles(null,SEQUENCE_FILES.FASTA);




        KmersTable.getInstance().setK(k);
        KmersTable.getInstance().createTable();

        Dataset<Integer> kmerRecords = null;
        LOGGER.info("Using k "+ KmersTable.getInstance().getK());
        try {
            kmerRecords = SparkComputer.createDataFrame(KmersTable.getInstance().getPersistedRecordIds(), Encoders.INT());

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        // get all recordIds whose genomes are not yet persisted
        List<String> records = recordData.join(kmerRecords,col(ColumnIdentifier.VALUE).
                equalTo(col(ColumnIdentifier.RECORD_ID)),SparkComputer.JOIN_TYPES.LEFT_ANTI).
                select(ColumnIdentifier.NAME).as(Encoders.STRING()).collectAsList();
        // get corresponding fasta files
        files = dataDirectoryParser.filterFilesInclude(files,records);
        int counter = files.size();

        for(File fastaFile: files) {

            LOGGER.info(fastaFile.getName() + " still "+ counter);

            int id = RecordDT.mapToRecordId(
                    fastaFile.getName().substring(0,fastaFile.getName().indexOf(".")));
            RecordDT record = RecordDT.getRecord(recordData,id);
//            Auxiliary.printMap(record);

            Map<String,Integer []> kmerMap = KmersTable.getKmerMap(
                    fastaFile,
                    k,
                    record.isTopology(),
                    true, DeGeCI.AmbigFilter.NO_FILTER
            );
//            Auxiliary.printMap(kmerMap,s -> s,
//                    pos -> {
//                String string = "";
//                for(int i = 0; i < pos.length; i++) string += pos[i] + ",";
//                return string;}
//                );
//            Auxiliary.printMap(kmerMap);
            try {
                KmersTable.getInstance().addKmers(true,id,kmerMap);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                exit(-1);
            }

            kmerMap= KmersTable.getKmerMap(
                    fastaFile,
                    k,
                    record.isTopology(),
                    false, DeGeCI.AmbigFilter.NO_FILTER
            );
//            Auxiliary.printMap(kmerMap);
            try {
                KmersTable.getInstance().addKmers(false,id,kmerMap);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                exit(-1);
            }
            counter--;
        }
        try {
            KmersTable.getInstance().createPrimaryKey();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }



    public void executeTaxonomy() {


        taxonomyGraph.entireDeletion();
        taxonomyGraph.createSchema();
        LOGGER.info("Done with Schema creation");
        String taxonomyDirectory = ProjectDirectoryManager.getTAXONOMIC_DATA();
        File namesFile = new File(taxonomyDirectory + "/" + TAXONOMY_NAMES_FILENAME);
        if(!namesFile.exists()) {
            LOGGER.error("File " + taxonomyDirectory + "/" + TAXONOMY_NAMES_FILENAME + " does not exist");
            System.exit(-1);
        }
        File nodesFile = new File(taxonomyDirectory + "/" + TAXONOMY_NODES_FILENAME);
        if(!nodesFile.exists()) {
            LOGGER.error("File " + taxonomyDirectory + "/" + TAXONOMY_NODES_FILENAME + " does not exist");
            System.exit(-1);
        }
        File mergeFile = new File(taxonomyDirectory + "/" + TAXONOMY_MERGE_FILENAME);
        if(!mergeFile.exists()) {
            LOGGER.error("File " + taxonomyDirectory + "/" + TAXONOMY_MERGE_FILENAME + " does not exist");
            System.exit(-1);
        }
        File divisionFile = new File(taxonomyDirectory + "/" + TAXONOMY_DIVISION_FILENAME);
        if(!divisionFile.exists()) {
            LOGGER.error("File " + taxonomyDirectory + "/" + TAXONOMY_DIVISION_FILENAME + " does not exist");
            System.exit(-1);
        }
        TaxonomyParser taxonomyParser = new TaxonomyParser(nodesFile,namesFile,mergeFile,divisionFile);
        taxonomyParser.parseNodes();
        taxonomyParser.parseNames();
        taxonomyParser.parseMergedTaxids();
        taxonomyParser.parseDivisions();

        System.out.println("Start persisting");
        taxonomyGraph.persist(taxonomyParser.getNodes(),taxonomyParser.getEdges(),taxonomyParser.getNames());


    }




    private class DataDirectoryParser {



        private String sequenceDataDirectory;

        public DataDirectoryParser() {

        }



        public void setSequenceDataDirectory(String sequenceDataDirectory) {
            this.sequenceDataDirectory = sequenceDataDirectory;
        }




        private void traverseSequenceSubDirPath(List<String> routes, SEQUENCE_FILES fileType) {
            traverseSequenceSubDirPath(routes,sequenceDataDirectory,fileType);
        }

        private void traverseSequenceSubDirPath(List<String> routes, String baseDir, SEQUENCE_FILES fileType) {
            try {
                List<String> subDirectories = getSequenceSubdirectories(baseDir,fileType).
                        map(x -> x.toString()).collect(Collectors.toList());
                if(subDirectories.size() == 0) {
//                    routes.add(baseDir.substring(0,baseDir.lastIndexOf("/")));
                    if(baseDir.endsWith(fileType.identifier)) {
//                        routes.add(baseDir);
                        routes.add(baseDir.substring(0,baseDir.lastIndexOf("/")));
                    }
                    return;
                }
                for(String subDir: subDirectories) {
                    traverseSequenceSubDirPath(routes,subDir,fileType);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }



        public Stream<Path> getSequenceSubdirectories(String baseDirectory, SEQUENCE_FILES fileType) throws IOException {
            return Files.walk(Paths.get(baseDirectory),1).filter(
                    f ->
                            (
                                    Files.isDirectory(f) &&!( f.toString().equals(baseDirectory)) //&& f.toString().equals(fileType.identifier)
                            )
            );
//            File baseDir = new File(baseDirectory);
//            if(!baseDir.isDirectory()) {
//                LOGGER.warn(baseDirectory + " is no directory");
//            }
//            else {
//                File [] subdirs = baseDir.listFiles();
//
//            }
        }

        public Stream<Path> getFileStream(String baseDirectory, SEQUENCE_FILES fileType) throws IOException {
//            Stream<Path> walk = Files.walk(Paths.get(sequenceDataDirectory + "/" + SEQUENCE_FILES.FASTA.identifier +"/"),1).filter(Files::isRegularFile).filter(x -> x.toString().contains("mitochondrion"));
            return Files.walk(Paths.get(baseDirectory + "/" + fileType.identifier +"/"),1).
                    filter(Files::isRegularFile);
        }



        public List<File> filterFilesInclude(List<File> directories, List<String> includes) {
            return directories.stream().
                    filter(d ->
                            includes.stream().anyMatch(e -> d.getAbsolutePath().contains(e))).
                    collect(Collectors.toList());
        }



        public List<File> getAllFiles(List<String> excludes, SEQUENCE_FILES fileType) {
            List<File> files = new ArrayList<>();
            List<String> directories = new ArrayList<>();
            traverseSequenceSubDirPath(directories,fileType);
            if(excludes != null && excludes.size() > 0) {
                directories = directories.stream().
                        filter(d ->
                                ! excludes.stream().anyMatch(e -> d.contains(e))).
                        collect(Collectors.toList());
            }

            Auxiliary.printSeq(directories);
            for(String directory: directories) {
                try {
                    files.addAll(getFileStream(directory, fileType).map(x -> x.toFile()).collect(Collectors.toList()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return files;
        }



    }

    private class PersistenceInputParser extends InputParser {

        protected PersistenceInputParser(String[] args) { super(args);
        }

        protected void setArguments() {
            this.parser = ArgumentParsers.newFor("Graph pesistence").
                    build().description("Specify parameters for graph persistence");



            parser.addArgument("--action").help("Specify what is to be done.\n" +
                    "Options are: \n" +
                    "PERSIST_RECORDS (persist record info (name, taxid, length)).\n" +
                    "PERSIST_KMERS_PSQL (create de-Bruijn graph and persist to psql).\n" +
                    "PERSIST_TAXONOMY (persist taxonomy, requires --taxonomy-dir to be specified).\n" )
                    .type(ACTION.class).required(true);
            parser.addArgument("--sequenceDir").help("Directory where fasta and gb files reside (in folders gb and fasta) that are to be persisted in the database.");
            parser.addArgument("--amount").
                    help("Amount of records to be persisted. If not specified all remaining (non persistent) records are persisted.").
                    type(Integer.class).setDefault(-1);
            parser.addArgument("--records").help("Specifiy records explicity").nargs("*");
            parser.addArgument("--fasta-file").help("Fasta file to be used for sequence mapping.");
            parser.addArgument("--scientific-names").nargs("*").help("Scientific name for taxonomy");
            parser.addArgument("--k").
                    help("Size of a kmer. Specifies the 'length' of a node. An edge then has a 'length' of k+1.").
                    type(Integer.class).setDefault(-1);
            parser.addArgument("--k-small").
                    help("Size of a psql kmer. Specifies the 'length' of a node. An edge then has a 'length' of k+1.").
                    type(Integer.class).setDefault(-1);
            parser.addArgument("--no-mitos-annotation").
                    help("Give notification that provided bed files are not mitos annotated.\nOtherwise mitos annotation is assumed.").
                    type(Boolean.class).action(Arguments.storeTrue()).setDefault(false);
            parser.addArgument("--keyspace").help("Name of the graph database keyspace.");
            parser.addArgument("--ambig-filter").help("Specify how ambiguous records are to be treated." +
                    "\nOptions are: NO_FILTER (replace all ambiguous nucleotides by their possible nucleotides)" +
                    "\nSKIP_FILTER (skip ambiguous records)\nCUT_FILTER (cut out all affected kmers)." +
                    "\nIf no filter is specified, SKIP_FILTER is applied.").
                    choices("NO_FILTER","SKIP_FILTER","CUT_FILTER").setDefault("SKIP_FILTER");
            parser.addArgument("--linear-topology").type(Boolean.class).setDefault(false);
            parser.addArgument("--move-ambig").type(Boolean.class).setDefault(true);
            parser.addArgument("--skip-directories").nargs("*").help("basename of directories not to consider in recursive searches");
            parser.addArgument("--gap-open").type(Integer.class).setDefault(-1);
            parser.addArgument("--gap-extend").type(Integer.class).setDefault(-1);
            parser.addArgument("--runs").type(Integer.class).setDefault(50);
            parser.addArgument("--length").type(Integer.class).setDefault(50);

        }

        private void requireKmerLengthExit() {
            if(k == -1) {
                LOGGER.error("Kmer length must be provided. Use --k.");
                exit(-1);
            }
        }

        private void requireSequenceDir() {
            if(sequenceDir == null) {
                LOGGER.error("Sequence directory must be specified. Use --sequenceDir" );
                exit(-1);
            }
        }


        @Override
        protected void executeRoutines(Namespace res) {

            dataDirectoryParser = new DataDirectoryParser();
            sequenceDir = res.getString("sequenceDir");
            String keyspace = res.getString("keyspace");

            if(keyspace != null) {
                LOGGER.info("Using keyspace " + keyspace);
            }
            k = res.getInt("k");
            if(k != -1) {
                LOGGER.info("Using kmer size of " + k);
            }
            if(sequenceDir != null){
                LOGGER.info("Using Sequence Directory:  " + sequenceDir);
                dataDirectoryParser.setSequenceDataDirectory(sequenceDir);
            }
            action = ACTION.valueOf(res.getString("action"));
            LOGGER.info("Action to perform: " + action);
            switch (action) {
                case PERSIST_RECORDS:
                    requireSequenceDir();
                    break;
                case PERSIST_KMERS_PSQL:
                    requireSequenceDir();
                    requireKmerLengthExit();
                    break;
                case PERSIST_TAXONOMY:
                    LOGGER.info("Selected Action: " + "PERSIST_TAXONOMY (persist taxonomy).");
                    break;
                default:
                    LOGGER.error("Invalid Action " + action);
                    System.exit(-1);
            }

            if(action == ACTION.PERSIST_KMERS_PSQL) {
                try {
                    persistKmersToSQL(res.getInt("k"));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            else if(action == ACTION.PERSIST_TAXONOMY) {
                taxonomyGraph = TaxonomyGraph.build();

                executeTaxonomy();
            }
            else if(action == ACTION.PERSIST_RECORDS) {
                try {
                    createRecordDataset(sequenceDir);
                } catch (IOException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
            }

        }

    }

    public static void createDataBase() {
        LOGGER.info("Persisting annotations and sequences");
        /*** Persist records in psql **/
        new DBCreator(new String[]
                {"--action","PERSIST_RECORDS","--sequenceDir",ProjectDirectoryManager.getSEQUENCE_DATA()});
        LOGGER.info("Persisting de-Bruijn graph");
        /*** Persist kmers in psql **/
        new DBCreator(new String[]
                {"--action","PERSIST_KMERS_PSQL","--sequenceDir",ProjectDirectoryManager.getSEQUENCE_DATA(),"--k","16"});

    }

    public static void createTaxonomyGraph() {
        new DBCreator(new String[]
                {"--action","PERSIST_TAXONOMY"});
    }

    public static void main(String[] args) {

//        createDataBase();
//        createTaxonomyGraph();
        System.exit(0);
    }
}
