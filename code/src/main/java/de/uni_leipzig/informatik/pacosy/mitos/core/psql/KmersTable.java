package de.uni_leipzig.informatik.pacosy.mitos.core.psql;

import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.DeGeCI;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.PairwiseAligner;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.*;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class KmersTable implements SQLTable {

    private static KmersTable kmersTable;

    private int k;
    private static String tableSuffix = "";
    private static final String KMER_BASE_TABLENAME = "complete.kmer_";
    private static final String KMER_DISTINCT_BASE_TABLENAME = "complete.distinct_kmer_";
    private static final String KMER_BASE_PREFIX_FUNCTION_NAME = "complete.extract_prefix";
    private static final Encoder<KmerDT> KMER_DT_ENCODER = Encoders.bean(KmerDT.class);
    protected static final Logger LOGGER = LoggerFactory.getLogger(KmersTable.class);
//    protected static final String EDGE_WEIGHT_DIRECTORY = "EDGE_WEIGHTS/";
//    protected static final String RECORD_EDGE_WEIGHT_DIRECTORY = EDGE_WEIGHT_DIRECTORY + "EDGE_WEIGHTS/";
    protected static final String PLUS_STRAND_EDGE_WEIGHT_BASE_NAME = "PLUS_STRAND_EDGE_WEIGHT";
    protected static final String EDGE_WEIGHT_BASE_NAME = "EDGE_WEIGHT";
    protected static final String MINUS_STRAND_EDGE_WEIGHT_BASE_NAME = "MINUS_STRAND_EDGE_WEIGHT";
    protected static final String RECORD_SIMILARITY_BASE_FUNCTION_NAME = "complete.record_similarity";
    protected static final String PLUS_STRAND_REPEAT_BASE_NAME = "PLUS_STRAND_REPEATS";
    protected static final String MINUS_STRAND_REPEAT_BASE_NAME = "MINUS_STRAND_REPEATS";
    protected static final String DISTINCT_INDEX_BASE_NAME = "DISTINCT_INDEX";
    protected static final String INDEX_BASE_NAME = "KMERS_INDEX";
    protected static final String INDEX_RECORD_BASE_NAME = "KMERS_RECORD_INDEX";
    protected static final String DISTINCT_PKEY_BASE_NAME = "DISTINCT_PKEY";
    public static final String RECORD_SIMILARITY_COUNT = "RECORD_SIMILARITY_COUNT";
    public static final int DEFAULT_K = 16;

    private static Pattern fastaHeaderPattern = Pattern.compile("^>([\\p{Upper}]+_[\\p{Digit}]+\\.[1-9]+)[\\p{Blank}](.*)(([\\p{Blank}]\\bmitochond.*\\b[\\p{Punct}])|[\\p{Blank}]genome[\\p{Punct}]).*");

    private static Pattern fastaShorthHeaderPattern = Pattern.compile("^>([\\p{Upper}]+_[\\p{Digit}]+\\.[1-9]+)[\\p{Blank}].*");

    private static Pattern fastaHeaderLiberalPattern = Pattern.compile("^>[\\p{Upper}]+_[\\p{Digit}]+\\.[1-9]+[\\p{Blank}](.*)");

    private static Pattern fastaLinePattern = Pattern.compile("\\p{Upper}+\\p{Blank}?+");


    private KmersTable() {
        k = DEFAULT_K;
    }

    public static synchronized KmersTable getInstance() {
        if(kmersTable == null) {
            kmersTable = new KmersTable();
        }
        return kmersTable;
    }
    public static String parseSequenceFromFasta(File fastaFile, Pattern fastaHaderPattern, Pattern fastaLinePattern) {

        StringBuilder sequence = new StringBuilder();
        try {

            BufferedReader br = new BufferedReader(new FileReader(fastaFile));
            String line;

            Matcher fastaHeaderMatcher;
            Matcher fastaLineMatcher;
            int lineCounter = 0;
            while((line = br.readLine()) != null)  {

                fastaLineMatcher = fastaLinePattern.matcher(line);
                if(fastaLineMatcher.matches()) {
                    sequence.append(line);
                }
                else {
                    fastaHeaderMatcher = fastaHaderPattern.matcher(line);
                    if(fastaHeaderMatcher.matches()) {
                        lineCounter++;
                    }
                }

            }
            br.close();
            LOGGER.info("Sequence length: " + sequence.length());
            if(lineCounter < 1) {
                LOGGER.error("More than one fasta record. Amount of records: " + lineCounter);
                System.exit(-1);
            }
            else if(lineCounter > 1) {
                LOGGER.error("No fasta record included");
                System.exit(-1);
            }

        } catch(Exception e) {
            e.printStackTrace();
        }

        return sequence.toString();
    }

    private static void updateKmerMap(Map<String,Integer []> kmerMap, String kmer, int position) {
        Integer [] positions = kmerMap.get(kmer);
        if(positions == null) {
            positions = new Integer[1];
            positions[0] = position;
        }
        else {
            positions = ArrayUtils.add(positions,position);
        }
        kmerMap.put(kmer,positions);

    }

    public static Map<String,Integer []> getKmerMap(File fastaFile, int k, boolean topology, boolean strand, DeGeCI.AmbigFilter ambigFilter) {

        return getKmerMap(parseSequenceFromFasta(fastaFile,fastaHeaderLiberalPattern,fastaLinePattern),k,topology,strand,ambigFilter);
    }

    public static Map<String,Integer []> getKmerMap(String sequence, int k, boolean topology, boolean strand, DeGeCI.AmbigFilter ambigFilter) {
        Map<String,Integer []> kmerMap = new HashMap<>();

        if(ambigFilter == DeGeCI.AmbigFilter.NO_FILTER) {
            String kmer;
            for(int pos = 0; pos < sequence.length()-k; pos++) {
                kmer = sequence.substring(pos,pos+k+1);

                if(!strand) {
                    updateKmerMap(kmerMap, PairwiseAligner.reverseComplement(kmer),pos);
                }
                else {
                    updateKmerMap(kmerMap,kmer,pos+k);
                }



            }
//            System.out.println("end:");
            if(topology) {
                for(int pos = k; pos > 0; pos--) {
                    kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                    if(!strand) {
                        updateKmerMap(kmerMap,PairwiseAligner.reverseComplement(kmer),sequence.length()-pos);
                    }
                    else {
                        updateKmerMap(kmerMap,kmer,-pos+k);
                    }

                }
            }
        }
        else if(ambigFilter == DeGeCI.AmbigFilter.SKIP_FILTER) {
            Matcher matcher = DeGeCI.ambigPattern.matcher(sequence);
            int ambigCounter = 0;
            Map<String,Integer> ambigNucleotides = new HashMap<>();
            while(matcher.find()) {
                Integer count = ambigNucleotides.get(matcher.group(1));
                ambigNucleotides.put(matcher.group(1), (count == null) ? 1 : count+1);
                ambigCounter++;
            }
            if(ambigCounter > 0) {
                LOGGER.info("Skipping ambiguous sequence ");
                LOGGER.info(ambigNucleotides.keySet().stream().
                        map(u -> u + ":" + ambigNucleotides.get(u) + "\n").toString());
                return kmerMap;
            }
            else {
                String kmer;

                for(int pos = 0; pos < sequence.length()-k; pos++) {
                    kmer = sequence.substring(pos,pos+k+1);
                    if(!strand) {
                        updateKmerMap(kmerMap,PairwiseAligner.reverseComplement(kmer),pos);
                    }
                    else {
                        updateKmerMap(kmerMap,kmer,pos+k);
                    }
                }

                if(topology) {
                    for(int pos = k; pos > 0; pos--) {
                        kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                        if(!strand) {
                            updateKmerMap(kmerMap,PairwiseAligner.reverseComplement(kmer),sequence.length()-pos);
                        }
                        else {
                            updateKmerMap(kmerMap,kmer,-pos+k);
                        }

                    }
                }
            }

        } else {

            String kmer;
            Matcher m;
            for(int pos = 0; pos < sequence.length()-k; pos++) {
                kmer = sequence.substring(pos,pos+k+1);
                m = DeGeCI.ambigPatternGlobal.matcher(kmer);
                if(!m.matches()) {
                    if(!strand) {
                        updateKmerMap(kmerMap,PairwiseAligner.reverseComplement(kmer),pos);
                    }
                    else {
                        updateKmerMap(kmerMap,kmer,pos+k);
                    }
                } else {
                    LOGGER.info("Skipping ambiguous kmer " + kmer);
                }


            }
            System.out.println("end:");
            if(topology) {
                for(int pos = k; pos > 0; pos--) {
                    kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                    m = DeGeCI.ambigPattern.matcher(kmer);
                    if(!m.matches()) {
                        if(!strand) {
                            updateKmerMap(kmerMap,PairwiseAligner.reverseComplement(kmer),sequence.length()-pos);
                        }
                        else {
                            updateKmerMap(kmerMap,kmer,-pos+k);
                        }
                    } else {
                        LOGGER.info("Skipping ambiguous kmer " + kmer);
                    }

                }
            }
        }


        return kmerMap;
    }

    /****CREATION********************************************************************************/
    public static void persistRefseq(String refseq, Dataset<Row> sequences, boolean strand, int k) throws SQLException {

        tableSuffix = "_"+refseq;
        KmersTable kmersTable = getInstance();
        kmersTable.createTable(strand);
        int counter =0;
        for(Row row: sequences.collectAsList()) {
            System.out.println(counter);
            Map<String,Integer []> kmermap = getKmerMap(row.getString(row.fieldIndex(ColumnIdentifier.SEQUENCE)),
                    k, true, strand,DeGeCI.AmbigFilter.NO_FILTER);
            kmersTable.addKmers(strand,row.getInt(row.fieldIndex(ColumnIdentifier.RECORD_ID)),kmermap );
            counter++;
        }
        kmersTable.createPrimaryKey();
    }

    public void createTable() throws SQLException {
        createTable(true);
        createTable(false);
    }

    public void createTable(boolean strand) throws SQLException {
        String tableName = getTableName(strand);
        JDBJInterface.getInstance().createTable(tableName,
                KmerDT.getKmerName() + " TEXT,\n" +
                        KmerDT.COLUMN_NAME.RECORD_ID.identifier + " integer,\n" +
                        KmerDT.COLUMN_NAME.POSITIONS.identifier + " integer[]", true);
    }

    public void createSubTable(String baseTablename, List<Integer> records) throws SQLException {

        for(boolean strand: Arrays.asList(true,false)) {
            String tableName = baseTablename+ getStrandIdentifier(strand);
            String query = "SELECT "+ KmerDT.getColumnNames() +" FROM " + getTableName(strand) +
                    " WHERE \"" + KmerDT.getRecordIdName() + "\" IN " + JDBJInterface.parseToColumnFormat(records,Integer.class) ;
            System.out.println(                    "CREATE TABLE complete."+ tableName +
                    " AS " + query);
            JDBJInterface.getInstance().executeUpdate(
                    "CREATE TABLE complete."+ tableName +
                            " AS " + query
            );


            JDBJInterface.getInstance().executeUpdate("ALTER TABLE IF EXISTS complete." + tableName+
                    " ADD CONSTRAINT "+tableName + getPrimaryKeyConstraintIdentifier(strand,k) +
                    " PRIMARY KEY (" + KmerDT.getKmerName() + "," + KmerDT.COLUMN_NAME.RECORD_ID.identifier + ")");
        }



    }

    public void createTableDistinct(boolean strand) throws SQLException {
        String tableName = getDistinctTableName(strand);

        JDBJInterface.getInstance().executeUpdate("CREATE TABLE " + tableName +
                " AS SELECT kmer, COUNT(*) \n" +
                        "FROM "+ getTableName(strand)+"\n"+
                " GROUP BY kmer");
    }

    public void createDistinctIndex() throws SQLException {
        createDistinctIndex(true);
        createDistinctIndex(false);
    }

    public void createIndex(boolean strand) throws SQLException {
//        JDBJInterface.getInstance().executeUpdate(
//                "CREATE INDEX " + getIndexName(strand) +
//                        " ON " + getTableName(strand) + " (kmer)"
//        );

        JDBJInterface.getInstance().executeUpdate(
                "CREATE INDEX " + getRecordIndexName(strand) +
                        " ON " + getTableName(strand) + " (\"recordId\")"
        );
    }

    public void createIndex() throws SQLException {
        createIndex(true);
        createIndex(false);
    }

    public void createDistinctIndex(boolean strand) throws SQLException {
        JDBJInterface.getInstance().executeUpdate(
                "CREATE INDEX " + getDistinctIndexName(strand) +
                        " ON " + getDistinctTableName(strand) + " (count)"
        );
    }

    public void createTableDistinct() throws SQLException {
        createTableDistinct(true);
        createTableDistinct(false);
    }

    public void dropTable() throws SQLException {
        dropTable(true);
        dropTable(false);
    }

    public void dropTable(boolean strand) throws SQLException {
        JDBJInterface.getInstance().dropTable(getTableName(strand), true);
    }

    public void dropDistinctTable() throws SQLException {
        dropDistinctTable(true);
        dropDistinctTable(false);
    }

    public void dropDistinctTable(boolean strand) throws SQLException {
        JDBJInterface.getInstance().dropTable(getDistinctTableName(strand), true);
    }

    private void createDistinctPrimaryKey(boolean strand) throws SQLException {
        JDBJInterface.getInstance().executeUpdate("ALTER TABLE IF EXISTS " + getDistinctTableName(strand) +
                " ADD CONSTRAINT " + getDistinctPkeyName(strand) +
                " PRIMARY KEY (" + KmerDT.getKmerName() +")");
    }

    private void createDistinctPrimaryKey() throws SQLException {
        createDistinctPrimaryKey(true);
        createDistinctPrimaryKey(false);
    }

    public void createPrimaryKey(boolean strand) throws SQLException {
        JDBJInterface.getInstance().executeUpdate("ALTER TABLE IF EXISTS " + getTableName(strand) +
                " ADD CONSTRAINT " + getPrimaryKeyConstraintIdentifier(strand,k) +
                " PRIMARY KEY (" + KmerDT.getKmerName() + "," + KmerDT.COLUMN_NAME.RECORD_ID.identifier + ")");
    }

    public void createPrimaryKey() throws SQLException {
        createPrimaryKey(true);
        createPrimaryKey(false);
//        createIndex();
    }

    public void dropPrimaryKey(boolean strand) throws SQLException {
        try {
            JDBJInterface.getInstance().executeUpdate("ALTER TABLE IF EXISTS " + getTableName(strand) +
                    " DROP CONSTRAINT IF EXISTS " + getPrimaryKeyConstraintIdentifier(strand,k));

        } catch (SQLException s) {
            if (s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable(strand);
            } else {
                throw s;
            }
        }

    }

    public void dropPrimaryKey() throws SQLException {
        dropPrimaryKey(true);
        dropPrimaryKey(false);
    }

    private void createRecordSimilarityFunction(boolean strand) throws SQLException{
        JDBJInterface.getInstance().executeUpdate(
            "CREATE OR REPLACE FUNCTION " + getRecordSimilarityFunctionName(strand)+ "(r INTEGER)\n" +
                    "RETURNS TABLE ( \n" +
                    "\t\"recordId\" INTEGER,\n" +
                    "\tcount BIGINT)\n" +
                    "AS $$\n" +
                    "DECLARE\n" +
                    "\trec ALIAS FOR $1;\n" +
                    "BEGIN\n" +
                    "RETURN QUERY\n"+
                    "\tSELECT t2.\"recordId\", COUNT(*) FROM\n" +
                    "\t(SELECT * \n" +
                    "\tFROM "+getTableName(strand)+" kp1\n" +
                    "\tWHERE kp1.\"recordId\" = rec\n" +
                    "\t) AS t1\n" +
                    "\t\tINNER JOIN \n" +
                    "\t(SELECT * \n" +
                    "\tFROM "+getTableName(strand)+" kp2\n" +
                    "\tWHERE kp2.\"recordId\" <> rec) AS t2\n" +
                    "\tON (t1.kmer = t2.kmer)\n" +
                    "\tGROUP BY t2.\"recordId\"\n" +
                    "\tORDER BY 2;\n" +
                    "END;\n" +
                    "$$ LANGUAGE plpgsql;"
        );
    }

    private void createRecordSimilarityFunction() throws SQLException{
        createRecordSimilarityFunction(true);
        createRecordSimilarityFunction(false);
    }

    private void dropRecordSimilarityFunction() throws SQLException {
        JDBJInterface.getInstance().dropFunction(getRecordSimilarityFunctionName(true));
        JDBJInterface.getInstance().dropFunction(getRecordSimilarityFunctionName(false));
    }

    private void createExtractPrefixFunction(boolean strand) throws SQLException{
        JDBJInterface.getInstance().executeUpdate(
                "CREATE OR REPLACE FUNCTION "+ getPrefixFunctionName(strand)+"(prefix_array TEXT[], k INTEGER)\n" +
                        "RETURNS TABLE (\n" +
                        "\t\tkmer TEXT,\n" +
                        "\t\t\"recordId\" INTEGER,\n" +
                        "\t\tpositions INTEGER[]\n" +
                        ")\n" +
                        "AS $$\n" +
                        "DECLARE\n" +
                        "\tprefix_ ALIAS FOR $1;\n" +
                        "BEGIN\n" +
                        "\n" +
                        "\tRETURN QUERY \n" +
                        "\tSELECT substring(kmertable.kmer,1,k), kmertable.\"recordId\", kmertable.positions \n" +
                        "\tFROM "+getTableName(strand)+ " kmertable\n" +
                        "\tWHERE \n" +
                        "\tsubstring(kmertable.kmer,1,k) = ANY(prefix_);\n" +
                        "\n" +
                        "END;\n" +
                        "\n" +
                        "$$ LANGUAGE plpgsql;"

        );
    }



    public void createExtractPrefixFunction() throws SQLException {
        createExtractPrefixFunction(true);
        createExtractPrefixFunction(false);
    }

    private void dropExtractPrefixFunction() throws SQLException {
        JDBJInterface.getInstance().dropFunction(getPrefixFunctionName(true));
        JDBJInterface.getInstance().dropFunction(getPrefixFunctionName(false));
    }

    public void dropRecords(List<Integer> recordIds) throws SQLException {
        try {
            JDBJInterface.getInstance().removeDataFromTable(getTableName(true), RecordDT.COLUMN_NAME.RECORDID.identifier + " IN " +
                    JDBJInterface.parseToColumnFormat(recordIds,Integer.class));
            JDBJInterface.getInstance().removeDataFromTable(getTableName(false),RecordDT.COLUMN_NAME.RECORDID.identifier + " IN " +
                    JDBJInterface.parseToColumnFormat(recordIds,Integer.class));

        } catch (SQLException s) {
            if (s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
            } else {
                throw s;
            }
        }
    }


    public void addKmers(boolean strand, int recordId, Map<String, Integer[]> kmerMap) throws SQLException {
        String values = "";
        try {
            for (Map.Entry<String, Integer[]> entry : kmerMap.entrySet()) {
                values += "('" + entry.getKey() + "'," + recordId + ",'" + JDBJInterface.getInstance().parseToSQLArray(entry.getValue()) + "'),";
            }
            values = values.substring(0, values.length() - 1);
            JDBJInterface.getInstance().insertDataIntoTable(getTableName(strand), values);

        } catch (SQLException s) {
            if (s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable(strand);
                values = "";
                for (Map.Entry<String, Integer[]> entry : kmerMap.entrySet()) {
                    values += "('" + entry.getKey() + "'," + recordId + ",'" + JDBJInterface.getInstance().parseToSQLArray(entry.getValue()) + "'),";
                }
                values = values.substring(0, values.length() - 1);
                JDBJInterface.getInstance().insertDataIntoTable(getTableName(strand), values);
            } else {
                throw s;
            }
        }
    }

    public void addKmer(boolean strand, int recordId, String kmer, Integer[] positions) throws SQLException {
        JDBJInterface.getInstance().insertDataIntoTable(getTableName(strand), "'" + kmer + "'," + recordId + ",'" +
                JDBJInterface.getInstance().parseToSQLArray(positions) + "'");
    }

    /****QUERIES********************************************************************************/
    public List<Integer> getPersistedRecordIds() throws SQLException {

        List<Integer> recordIds = new ArrayList<>();
        try {
            ResultSet resultSet = JDBJInterface.getInstance().
                    executeQuery("SELECT DISTINCT(" + RecordDT.COLUMN_NAME.RECORDID.identifier +
                            ") FROM " +getTableName(true) +"");
            while (resultSet.next()) {
                recordIds.add(resultSet.getInt("recordId"));
            }
        } catch (SQLException e) {
            if (e.getMessage().contains("ERROR: relation \"" + getTableName(true) + "\" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
            } else {
                throw e;
            }
        }

        return recordIds;
    }

//    public Dataset<KmerDT> getKmerMatches(boolean strand, Collection<String> kmers) {
//
//        String query = "SELECT * FROM " + getTableName(strand) + " WHERE " + KmerDT.getKmerName() + " IN " +
//                JDBJInterface.parseToColumnFormat(kmers,String.class);
//
//        return JDBJInterface.getInstance().fetchQueryResult(query).as(KMER_DT_ENCODER);
//    }

    public Dataset<KmerDT> getKmerMatches(boolean strand, Collection<String> kmers) {
        String query = "SELECT "+ KmerDT.getColumnNames() +" FROM " + getTableName(strand) +
                " WHERE " + KmerDT.getKmerName() + " IN " + JDBJInterface.parseToColumnFormat(kmers,String.class) ;

        return JDBJInterface.getInstance().fetchQueryResult(query).as(KMER_DT_ENCODER);
    }

    public Dataset<String> getKmersAsString(boolean strand, int recordId) {
        String query = "SELECT kmer FROM " + getTableName(strand) +
                " WHERE " + RecordDT.COLUMN_NAME.RECORDID.identifier + " = " + recordId;
        return JDBJInterface.getInstance().fetchQueryResult(query).as(Encoders.STRING());
    }

    public Dataset<Row> getKmers(boolean strand, int recordId) {
        String query = "SELECT kmer, unnest(positions) AS position FROM " + getTableName(strand) +
                " WHERE " + RecordDT.COLUMN_NAME.RECORDID.identifier + " = " + recordId;
        return JDBJInterface.getInstance().fetchQueryResult(query);
    }

    public Dataset<KmerDT> getKmerMatches(boolean strand, int recordId) {

        List<String> kmers = getKmersAsString(strand,recordId).distinct().collectAsList();
        LOGGER.info("Done extracting kmers");
        return getKmerMatches(strand,kmers);
    }

    public Dataset<RecordCountDT> getRecordSimilarity(boolean strand, int recordId) {


//        String query = "SELECT * FROM " + getRecordSimilarityFunctionName(strand) + "(" + recordId + ")";
        String query =  "\tSELECT t2.\"recordId\", COUNT(*) FROM\n" +
                "\t(SELECT * \n" +
                "\tFROM "+getTableName(strand)+" kp1\n" +
                "\tWHERE kp1.\"recordId\" = "+recordId+"\n" +
                "\t) AS t1\n" +
                "\t\tINNER JOIN \n" +
                "\t(SELECT * \n" +
                "\tFROM "+getTableName(strand)+" kp2\n" +
                "\tWHERE kp2.\"recordId\" <> "+recordId+") AS t2\n" +
                "\tON (t1.kmer = t2.kmer)\n" +
                "\tGROUP BY t2.\"recordId\"\n" +
                "\tORDER BY 2\n" ;
//        System.out.println(query);

        return JDBJInterface.getInstance().fetchQueryResult(query).as(RecordCountDT.ENCODER);
    }

    public Dataset<String> getDistinctKmers(boolean strand) {


        String query = "SELECT "+ KmerDT.getColumnNames() +")FROM " + getDistinctTableName(strand);
//        System.out.println(query);

        return JDBJInterface.getInstance().fetchQueryResult(query).as(Encoders.STRING());
    }

    public Dataset<WeightCountDT> getEdgeWeightDistribution(boolean strand, Collection<String> kmers) {
        String values = " (VALUES " + kmers.stream().map(k -> "('"+k+"')").collect(Collectors.joining(",")) + ") AS t (s) ";
        String query =
                "SELECT t.weight, COUNT(*)\n" +
                        "FROM\n" +
                        "(SELECT COUNT(*) AS "+ ColumnIdentifier.WEIGHT+" FROM " + getTableName(strand) +
                " INNER JOIN" +
                values +
                " ON  (" + KmerDT.COLUMN_NAME.KMER.identifier + " = s)" +
                " GROUP BY " + KmerDT.COLUMN_NAME.KMER.identifier+") AS t\n" +
        "GROUP BY t.weight";
//        System.out.println(query);
        return JDBJInterface.getInstance().fetchQueryResult(query).as(WeightCountDT.ENCODER);
    }

    public long getMaxWeight(boolean strand, Collection<String> kmers) {
        String values = " (VALUES " + kmers.stream().map(k -> "('"+k+"')").collect(Collectors.joining(",")) + ") AS t (s) ";
//        String query =
//                "SELECT MAX(t.weight)\n" +
//                        "FROM\n" +
//                        "(SELECT COUNT(*) AS "+ ColumnIdentifier.WEIGHT+" FROM " + getTableName(strand) +
//                        " INNER JOIN" +
//                        values +
//                        " ON  (" + KmerDT.COLUMN_NAME.KMER.identifier + " = s)" +
//                        " GROUP BY " + KmerDT.COLUMN_NAME.KMER.identifier+") AS t\n" +
//                        "GROUP BY t.weight";
        String query = "SELECT MAX(count) FROM " + getDistinctTableName(strand) +
                " INNER JOIN" +
                values +
                " ON  (" + KmerDT.COLUMN_NAME.KMER.identifier + " = s)";
//        System.out.println(query);
        return SparkComputer.getFirstOrElseNull(JDBJInterface.getInstance().fetchQueryResult(query).as(Encoders.LONG()));
    }

    public Dataset<Row> getWeightStatistic(boolean strand, Collection<String> kmers) {
        String values = " (VALUES " + kmers.stream().map(k -> "('"+k+"')").collect(Collectors.joining(",")) + ") AS t (s) ";
//        String query =
//                "SELECT MAX(t.weight)\n" +
//                        "FROM\n" +
//                        "(SELECT COUNT(*) AS "+ ColumnIdentifier.WEIGHT+" FROM " + getTableName(strand) +
//                        " INNER JOIN" +
//                        values +
//                        " ON  (" + KmerDT.COLUMN_NAME.KMER.identifier + " = s)" +
//                        " GROUP BY " + KmerDT.COLUMN_NAME.KMER.identifier+") AS t\n" +
//                        "GROUP BY t.weight";
        String query = "SELECT MAX(COALESCE(count,0))+1  AS " + ColumnIdentifier.WEIGHT_MAX +
        ", MIN(COALESCE(count,0))+1  AS "+ ColumnIdentifier.WEIGHT_MIN +
//                ", COUNT(MIN(COALESCE(count,0))) AS "+ ColumnIdentifier.COUNT_MIN +
                ", COUNT(*) AS "+ ColumnIdentifier.COUNT +
                ", AVG(COALESCE(count,0)+1 ) AS "  + ColumnIdentifier.WEIGHT_MEAN +
                ", stddev(COALESCE(count,0)+1 ) AS "  + ColumnIdentifier.WEIGHT_STD_DEVIATION +
                " FROM "+
                getDistinctTableName(strand) +
                " RIGHT OUTER JOIN" +
                values +
                " ON  (" + KmerDT.COLUMN_NAME.KMER.identifier + " = s)" //) t" +
//                " GROUP BY t.count"
                ;
//        System.out.println(query);
        Dataset<Row> result = JDBJInterface.getInstance().fetchQueryResult(query);
        result.show();
        return result;
    }

//    public Dataset<Row> getKmerWeightStatistic()

    public Dataset<IdentifierCountDT> getKmerMatchCount(boolean strand, Collection<String> kmers) {

//        String query = "SELECT "+ KmerDT.COLUMN_NAME.KMER.identifier +" AS " + ColumnIdentifier.IDENTIFIER +
//                ",COUNT(*) AS "+ ColumnIdentifier.COUNT+" FROM " + getTableName(strand) +
//                " LEFT OUTER JOIN" +
//                " (VALUES " + kmers.stream().map(k -> "('"+k+"')").collect(Collectors.joining(",")) + ") AS t (s) " +
//                " ON  (" + KmerDT.COLUMN_NAME.KMER.identifier + " = s)" +
//                " GROUP BY " + KmerDT.COLUMN_NAME.KMER.identifier;
        String values = " (VALUES " + kmers.stream().map(k -> "('"+k+"')").collect(Collectors.joining(",")) + ") AS t (s) ";
//        String query = "SELECT  "+ KmerDT.COLUMN_NAME.KMER.identifier +" AS " + ColumnIdentifier.IDENTIFIER +
//                ",COUNT(*) AS "+ ColumnIdentifier.COUNT+" FROM " + getTableName(strand) +
//                " INNER JOIN" +
//                 values +
//                " ON  (" + KmerDT.COLUMN_NAME.KMER.identifier + " = s)" +
//                " GROUP BY " + KmerDT.COLUMN_NAME.KMER.identifier;
        String query = "SELECT  "+ KmerDT.COLUMN_NAME.KMER.identifier +" AS " + ColumnIdentifier.IDENTIFIER +
                ", count FROM " + getDistinctTableName(strand) +
                " LEFT OUTER JOIN" +
                values +
                " ON  (" + KmerDT.COLUMN_NAME.KMER.identifier + " = s)";
//        System.out.println(query);
        return JDBJInterface.getInstance().fetchQueryResult(query).as(IdentifierCountDT.ENCODER);
    }


    public Dataset<KmerDT> getPrefixMatches(boolean strand, String[] prefixes) throws SQLException {
        if(prefixes.length < 1) {
            LOGGER.warn("No prefixes contained in prefix match query." +
                    " Returning empty Dataset.");
            return Spark.getInstance().emptyDataFrame().as(KMER_DT_ENCODER);
        }
        if(!consistentPrefixLength(prefixes)) {
            LOGGER.error("Prefixes are not of same length!");
            return null;
        }
        JDBJInterface jdbjInterface = JDBJInterface.getInstance();
        String query = "SELECT * FROM " + getPrefixFunctionName(strand) + "( '" +
                jdbjInterface.parseToSQLArray(prefixes) + "'," + prefixes[0].length() + ")";
//        System.out.println(query);
        return jdbjInterface.fetchQueryResult(query).as(KMER_DT_ENCODER);
    }

    public Dataset<KmerDT> getPrefixMatches(boolean strand, Collection<String> prefixes) throws SQLException {
        return getPrefixMatches(strand,prefixes.toArray(new String[prefixes.size()]));
    }

    public Dataset<WeightCountDT> getEdgeWeightDistribution(boolean strand) {
        String query = "SELECT t.weight, COUNT(*)\n" +
                "FROM\n" +
                "(SELECT COUNT(*) AS weight\n" +
                "FROM " + getTableName(strand) +
                " GROUP BY kmer) AS t\n" +
                "GROUP BY t.weight";
        return JDBJInterface.getInstance().fetchQueryResult(query).as(WeightCountDT.ENCODER);
    }

    public Dataset<Row> getEdges(boolean strand) {
        String query = "SELECT substring(kmer,1,16) AS src, substring(kmer,2,16) AS dst, \"recordId\", "+ (strand ==true ? " TRUE " : " FALSE ") + " AS strand, positions\n" +
                "FROM " + getTableName(strand) + " LIMIT 20";
        return JDBJInterface.getInstance().fetchQueryResult(query);
    }

    public Dataset<Row> getVertices(boolean strand) {
        String query = "SELECT DISTINCT(id) FROM " +
                "((SELECT substring(kmer,1,16) AS id " +
                "FROM " + getTableName(strand) + " LIMIT 20) " +
                " UNION " +
                "(SELECT substring(kmer,2,16) AS id " +
                "FROM " + getTableName(strand) + " LIMIT 20 )) AS t";
//        System.out.println(query);
        return JDBJInterface.getInstance().fetchQueryResult(query);
    }

    public Dataset<WeightCountDT> getEdgeWeightDistribution() {
        String query =
//                "SELECT t.weight, COUNT(*) FROM\n" +
//                        "(SELECT COALESCE( t1.count,0) + COALESCE( t2.count,0) AS weight\n" +
//                        "FROM \n" +
//                        "(SELECT * FROM "+getDistinctTableName(true)+ " ) t1\n" +
//                        " FULL OUTER JOIN\n" +
//                        "(SELECT * FROM "+ getDistinctTableName(false)+"") AS t2\n" +
//                        "ON (t1.kmer = t2.kmer)) t\n" +
//                        "GROUP BY t.weight"

                "SELECT t.weight, COUNT(*) \n" +
                "FROM\n" +
                "(SELECT COALESCE( t1.count,0) + COALESCE( t2.count,0) AS weight\n" +
                "FROM \n" +
                getDistinctTableName(true) +" t1\n" +
                "FULL OUTER JOIN\n" +
                getDistinctTableName(false) +" t2\n" +
                "ON (t1.kmer = t2.kmer)) AS t\n" +
                "GROUP BY t.weight";
        return JDBJInterface.getInstance().fetchQueryResult(query).as(WeightCountDT.ENCODER);
    }

    public Dataset<RecordWeightCountDT> getRecordEdgeWeightDistribution(boolean strand) {
        String query = "SELECT k.\"recordId\", t.weight, COUNT(*)\n" +
                "FROM\n" +
                "(SELECT kmer, COUNT(*) AS weight\n" +
                "FROM " + getTableName(strand) +
                " GROUP BY kmer) AS t\n" +
                "\tINNER JOIN \n" +
                getTableName(strand)+" AS k\n" +
                "ON (t.kmer = k.kmer)\n" +
                "GROUP BY \n" +
                "k.\"recordId\", t.weight";
//        LOGGER.info(query);
        return JDBJInterface.getInstance().fetchQueryResult(query).as(RecordWeightCountDT.ENCODER);
    }

    public Dataset<RecordWeightCountDT> getRecordRepeatCountDistribution(boolean strand) {
        String query = "SELECT \"recordId\", array_length(positions,1) AS weight, COUNT(*) FROM \n" +
                getTableName(strand) + " WHERE\n" +
                " array_length(positions,1) > 1\n" +
                "GROUP BY \"recordId\", array_length(positions,1) ";
//        LOGGER.info(query);
        return JDBJInterface.getInstance().fetchQueryResult(query).as(RecordWeightCountDT.ENCODER);
    }

    /****Auxiliary********************************************************************************/

    private static boolean consistentPrefixLength(String[] prefixes) {
        int k = prefixes[0].length();
        for(int i = 1; i < prefixes.length; i++) {
            if(prefixes[i].length() != k) {
                return false;
            }
        }
        return true;
    }

    private static String getStrandIdentifier(boolean strand) {
        return (strand? "plus" : "minus");
    }

    private static String getPrimaryKeyConstraintIdentifier(boolean strand, int k) {
        return "kmer_" +getStrandIdentifier(strand) + k + "_pkey"+ tableSuffix;
    }

    private String getEdgeWeightDirectory() {
        return ProjectDirectoryManager.getEDGE_WEIGHTS_DIR() + "/" +
                getEdgeWeightTableName();
    }

    private String getEdgeWeightDirectory(boolean strand) {
        return ProjectDirectoryManager.getEDGE_WEIGHTS_DIR() + "/" +
                 getEdgeWeightTableName(strand);
    }

    private String getRecordEdgeWeightDirectory(boolean strand) {
        return ProjectDirectoryManager.getEDGE_WEIGHTS_DIR() + "/" +
                 getRecordEdgeWeightTableName(strand);
    }

    private String getEdgeWeightTableName() {
        return     EDGE_WEIGHT_BASE_NAME + "_" + k;
    }

    private String getEdgeWeightTableName(boolean strand) {
        return     (strand ? PLUS_STRAND_EDGE_WEIGHT_BASE_NAME :
                MINUS_STRAND_EDGE_WEIGHT_BASE_NAME) + "_" + k;
    }

    private String getRecordEdgeWeightTableName(boolean strand) {
        return  "RECORD_"+   (strand ? PLUS_STRAND_EDGE_WEIGHT_BASE_NAME :
                MINUS_STRAND_EDGE_WEIGHT_BASE_NAME) + "_" + k;
    }

    private String getRecordRepeatsDirectory(boolean strand) {
        return  ProjectDirectoryManager.getRECORD_DATA_DIR() +"/" +
                getRecordRepeatsTableName(strand);
    }

    private String getRecordRepeatsTableName(boolean strand) {
        return  "RECORD_"+   (strand ? PLUS_STRAND_REPEAT_BASE_NAME :
                MINUS_STRAND_REPEAT_BASE_NAME) + "_" + k;
    }

    private String getDistinctTableName(boolean strand) {
        return KMER_DISTINCT_BASE_TABLENAME + getStrandIdentifier(strand) + "_" + k;
    }

    private String getRecordSimilarityDirectory(boolean strand,int recordId) {
        return ProjectDirectoryManager.getRECORD_DATA_DIR(strand,recordId) + "/" +
            RECORD_SIMILARITY_COUNT;
    }

    public String getTableName(boolean strand) {
        return KMER_BASE_TABLENAME + getStrandIdentifier(strand) + "_" + k + tableSuffix;
    }

    private String getIndexName(boolean strand) {
        return INDEX_BASE_NAME + getStrandIdentifier(strand) + "_" + k + tableSuffix;
    }

    private String getRecordIndexName(boolean strand) {
        return INDEX_RECORD_BASE_NAME + getStrandIdentifier(strand) + "_" + k;
    }

    private String getDistinctIndexName(boolean strand) {
        return DISTINCT_INDEX_BASE_NAME + getStrandIdentifier(strand) + "_" + k;
    }

    private String getDistinctPkeyName(boolean strand) {
        return DISTINCT_PKEY_BASE_NAME + getStrandIdentifier(strand) + "_" + k;
    }

    private String getRecordSimilarityFunctionName(boolean strand) {
        return RECORD_SIMILARITY_BASE_FUNCTION_NAME + "_" + getStrandIdentifier(strand) + "_" + k;
    }

    private String getPrefixFunctionName(boolean strand) {
        return KMER_BASE_PREFIX_FUNCTION_NAME + "_" + getStrandIdentifier(strand) + "_" + k;
    }

    public void persistRecordSimilarity(boolean strand, int recordId){
        SparkComputer.persistDataFrame(getRecordSimilarity(strand,recordId),getRecordSimilarityDirectory(strand,recordId));
    }

    public void persistEdgeWeightDistribution(){
        getEdgeWeightDistribution().write().
                bucketBy(4,ColumnIdentifier.COUNT).
                sortBy(ColumnIdentifier.WEIGHT).
                option(Spark.PATH_OPTION_KEY,getEdgeWeightDirectory()).
                saveAsTable(getEdgeWeightTableName());
    }

    public void persistEdgeWeightDistribution(boolean strand){
        getEdgeWeightDistribution(strand).write().
                bucketBy(4,ColumnIdentifier.COUNT).
                sortBy(ColumnIdentifier.WEIGHT).
                option(Spark.PATH_OPTION_KEY,getEdgeWeightDirectory(strand)).
                saveAsTable(getEdgeWeightTableName(strand));
    }

    public void persistRecordEdgeWeightDistribution(boolean strand){
        getRecordEdgeWeightDistribution(strand).write().
                bucketBy(4,ColumnIdentifier.RECORD_ID).
                sortBy(ColumnIdentifier.WEIGHT).
                option(Spark.PATH_OPTION_KEY,getRecordEdgeWeightDirectory(strand)).
                saveAsTable(getRecordEdgeWeightTableName(strand));
    }

    public void persistRecordRepeatCountDistribution(boolean strand){
        getRecordRepeatCountDistribution(strand).write().
                bucketBy(4,ColumnIdentifier.RECORD_ID).
                sortBy(ColumnIdentifier.WEIGHT).
                option(Spark.PATH_OPTION_KEY,getRecordRepeatsDirectory(strand)).
                saveAsTable(getRecordRepeatsTableName(strand));
    }

    public Dataset<WeightCountDT> fetchRecordRepeatCountDistribution(boolean strand) {
        return SparkComputer.read(getRecordRepeatsDirectory(strand),WeightCountDT.ENCODER);
    }

    public Dataset<WeightCountDT> fetchEdgeWeightDistribution(boolean strand) {
        return SparkComputer.read(getEdgeWeightDirectory(strand),WeightCountDT.ENCODER);
    }

    public Dataset<WeightCountDT> fetchEdgeWeightDistribution() {
        return SparkComputer.read(getEdgeWeightDirectory(),WeightCountDT.ENCODER);
    }

    public Dataset<RecordWeightCountDT> fetchRecordEdgeWeightDistribution(boolean strand) {
        return SparkComputer.read(getRecordEdgeWeightDirectory(strand),RecordWeightCountDT.ENCODER);
    }

    public Dataset<RecordCountDT> fetchRecordSimilarity(boolean strand, int recordId){
        return SparkComputer.read(getRecordSimilarityDirectory(strand,recordId)).as(RecordCountDT.ENCODER);
    }

    public static List<StructField> getWeightStatisticsStructFields() {
        List<StructField> structFields = new ArrayList<>();
        structFields.add(DataTypes.createStructField(ColumnIdentifier.WEIGHT_MAX, DataTypes.LongType,false));
        structFields.add(DataTypes.createStructField(ColumnIdentifier.WEIGHT_MIN, DataTypes.LongType,false));
//        structFields.add(DataTypes.createStructField(ColumnIdentifier.COUNT_MIN, DataTypes.LongType,false));
        structFields.add(DataTypes.createStructField(ColumnIdentifier.COUNT, DataTypes.LongType,false));
        structFields.add(DataTypes.createStructField(ColumnIdentifier.WEIGHT_MEAN, DataTypes.createDecimalType(15,4),false));
        structFields.add(DataTypes.createStructField(ColumnIdentifier.WEIGHT_STD_DEVIATION, DataTypes.createDecimalType(15,4),false));
        return structFields;
    }

    /****Getter and Setter********************************************************************************/

    public int getK() {
        return k;
    }

    public void setK(int k) {

        if(k > DEFAULT_K) {
            LOGGER.error("K must be smaller or equal to default value: "+ DEFAULT_K );
            return;
        }
//        if(k == this.k) {
//            LOGGER.info("Provide K value is same as before");
//        }
        this.k = k;
    }

    public static String getTableSuffix() {
        return tableSuffix;
    }

    public static void setTableSuffix(String tableSuffix) {
        KmersTable.tableSuffix = tableSuffix;
    }

}
