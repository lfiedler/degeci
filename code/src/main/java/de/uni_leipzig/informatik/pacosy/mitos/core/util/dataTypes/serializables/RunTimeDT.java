package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class RunTimeDT implements Serializable {
    public static final Encoder<RunTimeDT> ENCODER = Encoders.bean(RunTimeDT.class);
    protected double time;
    protected String name;

    public RunTimeDT() {
    }

    public RunTimeDT(long time, String name) {
        this.time = asMinutes(time);
        this.name = name;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return  name + ": " + time;
    }

    public static double asMinutes(long time) {
        return time/60000.0;
    }
}
