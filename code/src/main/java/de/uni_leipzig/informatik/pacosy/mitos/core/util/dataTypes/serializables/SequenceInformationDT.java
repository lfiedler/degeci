package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class SequenceInformationDT implements Serializable {
    public static final Encoder<SequenceInformationDT> ENCODER = Encoders.bean(SequenceInformationDT.class);
    private int length;
    private String id;
    private boolean topology;
    private int taxId;
    private String taxName;

    public enum COLUMN_NAME implements DBColumnName {
        ID("id"),
        TOPOLOGY("topology"),
        TAXID("taxId"),
        TAX_NAME("taxName");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public SequenceInformationDT() {
        topology = true;
        taxId = -1;
    }

    public SequenceInformationDT(String id, boolean topology) {
        this.id = id;
        this.topology = topology;
    }

    public SequenceInformationDT(int length, String id, boolean topology, int taxId, String taxName) {
        this.length = length;
        this.id = id;
        this.topology = topology;
        this.taxId = taxId;
        this.taxName = taxName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isTopology() {
        return topology;
    }

    public void setTopology(boolean topology) {
        this.topology = topology;
    }

    public int getTaxId() {
        return taxId;
    }

    public void setTaxId(int taxId) {
        this.taxId = taxId;
    }

    public String getTaxName() {
        return taxName;
    }

    public void setTaxName(String taxName) {
        this.taxName = taxName;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String toString() {
        return "Id: " + id + "\n" +
                "Length: " + length + "\n"+
                "Topology: " + (topology ? "circular" : "linear") + "\n" ;
//                "TaxId: " + taxId + "\n"
    }
}
