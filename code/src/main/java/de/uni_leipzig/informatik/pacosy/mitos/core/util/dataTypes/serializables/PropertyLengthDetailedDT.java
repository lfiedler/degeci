package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class PropertyLengthDetailedDT extends PropertyLengthDT{
    private int minLength;
    private int maxLength;
    public static final Encoder<PropertyLengthDetailedDT> ENCODER = Encoders.bean(PropertyLengthDetailedDT.class);
    public PropertyLengthDetailedDT() {
    }

    public PropertyLengthDetailedDT(String category, String property, int meanLength, int count, int minLength, int maxLength) {
        super(category, property, meanLength, count);
        this.minLength = minLength;
        this.maxLength = maxLength;
    }

    public int getMinLength() {
        return minLength;
    }

    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }
}
