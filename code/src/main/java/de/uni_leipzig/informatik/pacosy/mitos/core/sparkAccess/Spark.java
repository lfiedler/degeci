package de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess;

import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.PairwiseAligner;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.ColumnIdentifier;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkContext;
import org.apache.spark.SparkStageInfo;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.JavaSparkStatusTracker;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import static de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.PairwiseAligner.*;


public class Spark {

    public static int threads;
    static {
        initThreads();
    }
    private static Spark spark;
    private static SparkSession sparkSession;
    private static final String SPARK_APP_NAME = "MITOS_DBG";
    private static final String MASTER = "local["+threads+"]";
    public static final String PATH_OPTION_KEY = "path";
    public static JavaSparkContext javaSparkContext;
    public static final String SPARK_DIR = ProjectDirectoryManager.getPROJECT_DIR() + "/spark";
//    public static final String SPARK_DIR = "/mnt/data-usb/spark";
//    public static final String SPARK_CHECKPOINT_DIR = "/tmp/checkpoint";
    public static final String SPARK_CHECKPOINT_DIR = SPARK_DIR + "/checkpoint";
    public static AtomicBoolean lock = new AtomicBoolean(false);

    private Spark() {

    }

    private static SparkContext init() {

        setSparkSession(MASTER);
        SparkContext sc = sparkSession.sparkContext();

        sc.setCheckpointDir(SPARK_CHECKPOINT_DIR);
        return sc;
    }

    private static void initThreads() {
        Properties connectionProperties = new Properties();

        try {
            connectionProperties.load(new FileInputStream(ProjectDirectoryManager.getACCESS_DIR()+"/spark.properties"));
            threads = Integer.parseInt(connectionProperties.getProperty("threads"));
//            connectionProperties.load(Resources.getResource(psqlPropertiesFileName).openStream());

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static SparkSession getInstance() {
        if(sparkSession == null) {
//            setSparkSession(MASTER);
//            SparkContext sc = sparkSession.sparkContext();
//            sc.setCheckpointDir(SPARK_CHECKPOINT_DIR);
            javaSparkContext = new JavaSparkContext(init());

        }
        return sparkSession;
    }

    public static void reset() {
        sparkSession.close();
        javaSparkContext = new JavaSparkContext(init());
    }

    public static FileSystem getFileSystem() throws IOException {
        return LocalFileSystem.get(javaSparkContext.hadoopConfiguration());
    }

    public static void clearCheckPointDir(){

        clearCheckPointDir(getCheckPointDir());

    }


    public static String getCheckPointDir() {
        return javaSparkContext.getCheckpointDir().get();
    }

    public static Dataset<Row> checkPoint(Dataset<Row> dataset, int index, int checkpointInterval, String prefix) {
        String dir = SPARK_CHECKPOINT_DIR + "/"+prefix+"_" + index;
        SparkComputer.persistDataFrame(dataset,dir);

        if(index >= checkpointInterval) {
            clearDirectory(SPARK_CHECKPOINT_DIR+ "/"+prefix+"_" + (index-checkpointInterval));
        }

        return SparkComputer.read(dir);
//            System.out.println(fileSystem.exists(new Path( checkPointDir)));

    }

    public static Dataset<Row> checkPoint2(Dataset<Row> dataset, int index, int checkpointInterval, String prefix) {
        String dir = SPARK_CHECKPOINT_DIR + "/"+prefix+"_" + index;
        SparkComputer.persistDataFrame(dataset,dir);

        if(index >= checkpointInterval) {
            clearDirectory(SPARK_CHECKPOINT_DIR+ "/"+prefix+"_" + (index-checkpointInterval-2));
        }

        return SparkComputer.read(dir);
//            System.out.println(fileSystem.exists(new Path( checkPointDir)));

    }

    public static Dataset<Row> checkPoint(Dataset<Row> dataset, int index, int checkpointInterval) {
        String dir = SPARK_CHECKPOINT_DIR + "/" + index;
        SparkComputer.persistDataFrame(dataset,dir);

        if(index >= checkpointInterval) {
            clearDirectory(SPARK_CHECKPOINT_DIR+ "/" + (index-checkpointInterval));
        }

        return SparkComputer.read(dir);
//            System.out.println(fileSystem.exists(new Path( checkPointDir)));

    }

    public static Dataset<Row> checkPoint(Dataset<Row> dataset, String name) {
        String dir = SPARK_CHECKPOINT_DIR + "/" + name;
        SparkComputer.persistDataFrame(dataset,dir);

        return SparkComputer.read(dir);
//            System.out.println(fileSystem.exists(new Path( checkPointDir)));

    }

    public static Dataset<Row> checkPointWithRandomAccessor(Dataset<Row> dataset, String name) {
        String dir = SPARK_CHECKPOINT_DIR + "/" + name+"/"+UUID.randomUUID().toString();
        SparkComputer.persistDataFrame(dataset,dir);

        return SparkComputer.read(dir);
//            System.out.println(fileSystem.exists(new Path( checkPointDir)));

    }

    public static Dataset<Row> checkPointORC(Dataset<Row> dataset, String name) {

        SparkComputer.persistDataFrameORC(dataset,name);

        return SparkComputer.readORC(name);
//            System.out.println(fileSystem.exists(new Path( checkPointDir)));

    }

    public static void printProgress(int waitSeconds) {
        getInstance();
        JavaSparkStatusTracker sparkStatusTracker =  javaSparkContext.statusTracker();
        synchronized (lock) {
            while (!lock.get()) {
//                System.out.println("test");
//                System.out.println(sparkStatusTracker.getActiveStageIds());
                for(int stageId : sparkStatusTracker.getActiveStageIds()) {
                    SparkStageInfo stageInfo = sparkStatusTracker.getStageInfo(stageId);
                    System.out.println(stageInfo.numTasks() + " tasks total: " + stageInfo.numActiveTasks() +
                            " active, " + stageInfo.numCompletedTasks() + " complete");
                }
                try {
                    lock.wait(waitSeconds*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static void clearEntireCheckPointDir() {
        clearDirectory(SPARK_CHECKPOINT_DIR);
    }

    public static void clearDirectory(String directory) {
        FileSystem fileSystem = null;
        try {
            fileSystem = getFileSystem();
//                System.out.println(SPARK_CHECKPOINT_DIR+ "/" + (index-checkpointInterval));
            Path path = new Path( directory);
//                System.out.println(fileSystem.exists(path));
            fileSystem.delete(path,true);

//                System.out.println(fileSystem.exists(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean checkExisting(String directory) {
        FileSystem fileSystem = null;
        try {
            fileSystem = getFileSystem();

            return fileSystem.exists(new Path(directory));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void renameDirectory(String directoryOld, String directoryNew) {
        FileSystem fileSystem = null;
        try {
            fileSystem = getFileSystem();
//                System.out.println(SPARK_CHECKPOINT_DIR+ "/" + (index-checkpointInterval));

//                System.out.println(fileSystem.exists(path));
            fileSystem.rename(new Path(directoryOld),new Path(directoryNew));

//                System.out.println(fileSystem.exists(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void replaceDataset( Dataset<Row> newDataset, String path) {
        clearDirectory(path);
        SparkComputer.persistDataFrameORC(newDataset,path);

    }

    public static void clearCheckPointDir(String checkPointDir){

//        System.out.println(checkPointDir);
        try {
            FileSystem fileSystem = getFileSystem();
//            System.out.println(fileSystem.exists(new Path( checkPointDir)));
            Path path = new Path( checkPointDir);
            fileSystem.delete(path,true);

//            System.out.println(fileSystem.exists(new Path( checkPointDir)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static JavaSparkContext getJavaSparkContext() {
        return javaSparkContext;
    }

    public static void setSparkSession(String master) {
        if(sparkSession != null) {
            sparkSession.close();
        }

//        System.out.println("in here " + SPARK_DIR + " " );
        sparkSession =  SparkSession.builder().
                config("spark.driver.maxResultSize", "5g").
                config("spark.cleaner.referenceTracking.cleanCheckpoints",true).
//                config("spark.scheduler.listenerbus.eventqueue.size", "1000000").
//                config("spark.rpc.askTimeout", "10000s").
                config("spark.driver.memory","15G").
//                config("spark.network.timeout",10000000).
                config("spark.executor.memory","8G").
                config("spark.sql.broadcastTimeout",-1).
                config("spark.sql.shuffle.partitions",8).
                config("spark.worker.cleanup.enabled",true).
                config("spark.worker.cleanup.interval",300).
                config("spark.sql.autoBroadcastJoinThreshold",-1).
//                config("spark.sql.codegen.wholeStage", false).
//                config("spark.cleaner.periodicGC.interval","2min").
                config("spark.local.dir",SPARK_DIR).
                        master(master).
                appName(SPARK_APP_NAME).getOrCreate();
        sparkSession.udf().register(ALIGN_DEGECCI, alignDeGECCI, DEGECCI_ALIGN_STRUCT);


        sparkSession.udf().register(ALIGN_LOCAL_SCORE_UDF, alignScore, DataTypes.DoubleType);
        sparkSession.udf().register(ALIGN_LOCAL_SCORE_EVALUE, PairwiseAligner.alignEValue, DataTypes.DoubleType);
        sparkSession.udf().register(PERCENTAGE_IDENTITY_UDF, PairwiseAligner.percentageIdentity, DataTypes.DoubleType);


        sparkSession.sqlContext().setConf("spark.sql.orc.filterPushdown", "true");
//        sparkSession.conf().set("spark.driver.maxResultSize", "2g");
//        sparkSession.conf().set("spark.scheduler.listenerbus.eventqueue.size", "1000000");
//        sparkSession.conf().set("spark.rpc.askTimeout", "10000s");
//        sparkSession.conf().set("spark.sql.autoBroadcastJoinThreshold","-1");
//        sparkSession.conf().set("spark.driver.memory","1G");
//        sparkSession.conf().set("spark.executor.memory","2G");
//        sparkSession.conf().set("spark.sql.broadcastTimeout",8000);
//        sparkSession.conf().set("spark.cleaner.periodicGC.interval","2min");

//        System.out.println(sparkSession.conf().get("spark.sql.broadcastTimeout"));
    }


    public static Dataset<Row> createRecordDataset(List<Integer> recordIds) {
        return getInstance().createDataset(recordIds,Encoders.INT()).withColumnRenamed(ColumnIdentifier.VALUE,ColumnIdentifier.RECORD_ID);
    }




}
