package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.ColumnIdentifier;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;

import static org.apache.spark.sql.functions.col;

public interface RecordIdInterface {

    int getRecordId();

    void setRecordId(int recordId);

    static <T extends RecordIdInterface> Dataset<Integer> getDistinctRecords(Dataset<T> dataset) {
        return dataset.select(ColumnIdentifier.RECORD_ID).distinct().as(Encoders.INT());
    }

    static <T extends RecordIdInterface> Dataset<T> fiterForRecord(Dataset<T> dataset, int recordId) {
            return dataset.filter(col(ColumnIdentifier.RECORD_ID).equalTo(recordId));
    }

    static <T extends RecordIdInterface> FilterFunction<T> recordFilter(int recordId, Class<T> tClass) {
        return k -> k.getRecordId() == recordId;
    }

}
