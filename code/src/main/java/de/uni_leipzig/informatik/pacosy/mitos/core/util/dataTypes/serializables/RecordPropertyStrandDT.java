package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class RecordPropertyStrandDT extends RecordPropertyDT{
    protected boolean strand;
    public static final Encoder<RecordPropertyStrandDT> ENCODER = Encoders.bean(RecordPropertyStrandDT.class);

    public RecordPropertyStrandDT() {
    }

    public RecordPropertyStrandDT(int recordId, int position, String category, String property, boolean strand) {
        super(recordId, position, category, property);
        this.strand = strand;
    }

    public boolean isStrand() {
        return strand;
    }

    public void setStrand(boolean strand) {
        this.strand = strand;
    }
}
