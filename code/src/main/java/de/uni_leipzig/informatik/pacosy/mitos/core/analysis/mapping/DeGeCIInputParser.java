package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping;

import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.TaxonomyGraph;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.FileIO;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.InputParser;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.eclipse.jdt.internal.compiler.ast.Argument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataInput;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DeGeCIInputParser extends InputParser {
    private static Logger LOGGER = LoggerFactory.getLogger(DeGeCIInputParser.class);

    protected DeGeCIInputParser(String[] args) {
        super(args);
    }

    protected static void generateParameterFile(Namespace res) throws IOException {
        LOGGER.info("Generating parameter file");
        String parameter =
                "name: " + res.getString("name") + "\n"+

                "refseq version: " + res.getInt("refseq") + "\n"+
                "circular: " + !res.getBoolean("treat_linear") + "\n"+
                "count: " + res.getInt("count")+ "\n"+
                "weight: " + res.getInt("weight") +"\n"+
                        "alpha: " + res.getDouble("alpha")  + "\n"+
                "translation table: " + res.getInt("trans_table") + "\n"+
                "shift: " + res.getInt("shift") + "\n";

                if(res.getList("taxids") != null || res.getList("taxonomic_groups") != null) {

                    parameter += "exclude taxids: " + res.getBoolean("exclude_taxids") + "\n" +
                            ((res.getList("taxids") != null) ? "taxids: " + res.getList("taxids") +"\n":
                            "taxonomic groups: " + res.getList("taxonomic_groups"));
                }

//        System.out.println(parameter);
        FileIO.writeToFile(parameter,res.getString("result_directory")+"/parameterSettings.txt",false,"");

//                "sequence: " + res.getString("sequence")+ "\n"+
    }

    protected void setArguments() {
        this.parser = ArgumentParsers.newFor("Mitochondrial genome annotation").
                build().description("Specify parameters for annotation pipe line");
        parser.addArgument("--debug").action(Arguments.storeTrue()).type(Boolean.class).setDefault(false).help("Only for debbuging purposes");
        parser.addArgument("--sequence").type(String.class).help("Nucleotide sequence");
        parser.addArgument("--fasta-file").type(String.class).help("path to fasta file. Multi fasta files are NOT supported");
        parser.addArgument("--name").type(String.class).setDefault("inputGenome").help("name of input genome");
//        parser.addArgument("--id").type(String.class).setDefault(3).help("job id").required(true);
        parser.addArgument("--treat-linear").type(Boolean.class).setDefault(false).help("treat sequence as linear");
        parser.addArgument("--weight").type(Integer.class).setDefault(1).help("Minimal edge weight for mapping (k+1)-mers");
        parser.addArgument("--trans-table").type(Integer.class).choices(Arrays.asList(1,2,4,5,9,13,14))
                .help("Specifies translation table to optimize gene boundary predictions. Possible options are 1,2, 4,5, 9, 13 or 14 ");
        parser.addArgument("--shift").type(Integer.class).choices(Arguments.range(0,10)).setDefault(6)
                .help("Window size to search for start and stop codons. Only used if the translation table is specified.");
        parser.addArgument("--count").type(Integer.class).setDefault(2).help("Minimal count of predictions");
        parser.addArgument("--refseq").type(Integer.class).setDefault(89).help("Refseq release");


        parser.addArgument("--taxid").type(Integer.class).setDefault(-1).help("TaxId of the input genome");
        parser.addArgument("--exclude-taxids").type(Boolean.class).setDefault(false).help("exclude all genomes with taxids contained in the taxonomic tree spawned by taxids.");
        parser.addArgument("--taxids").type(Integer.class).nargs("*").help("Taxids for taxonomy filter");
        parser.addArgument("--taxonomic-groups").type(String.class).nargs("*").help("Taxonomic Groups for taxonomy filter");

        parser.addArgument("--alpha").type(Double.class).setDefault(0.7).help("alpha");
        parser.addArgument("--result-directory").help("Result directory").required(true);

    }

    protected void executeRoutines(Namespace res)  {


        try {

            if (res.getString("sequence") == null && res.getString("fasta_file") == null) {

                LOGGER.error("Sequence must be supplied");
                System.exit(1);
            }
            else if(res.getString("sequence") != null && res.getString("sequence").length() <  DeGeCI.MIN_SEQUENCE_LENGTH) {

                LOGGER.error("Sequence too short must be at least " + DeGeCI.MIN_SEQUENCE_LENGTH + " nt long.");
                System.exit(1);
            }
            generateParameterFile(res);
            if (res.getBoolean("debug") != null) {
                DeGeCI.DEBUG = res.getBoolean("debug");
            }
            if (res.getInt("trans_table") != null) {

                DeGeCI.transTable = res.getInt("trans_table");
                DeGeCI.shift = res.getInt("shift");
                LOGGER.info("Using translation table " + DeGeCI.transTable);
            }
            List<Integer> refseqRealses = Arrays.asList(89, 204);
            if (!refseqRealses.contains(res.getInt("refseq"))) {
                LOGGER.error("Invalid refseq release. Valid options are " + refseqRealses);
                System.exit(2);
            }

            TaxonomyGraph taxonomyGraph = null;
            List<Integer> taxids =null;
            if(res.get("taxids") != null) {
                taxids = new ArrayList<>( res.get("taxids"));
            }

            List<Integer> filterRecords = null;
            if (taxids != null || res.get("taxonomic_groups") != null) {
//                LOGGER.error("Currently not yet implemented.");
//                System.exit(0);
                taxonomyGraph = TaxonomyGraph.build();
            }
            if (taxids == null && res.get("taxonomic_groups") != null) {
                taxids = taxonomyGraph.getTaxidsForTaxonomicName(res.get("taxonomic_groups"));
                if (taxids == null) {
                    LOGGER.error("No taxids found for provided taxonomic groups");
                    System.exit(3);
                }
            }
            if (taxids != null) {
                LOGGER.info("Launching Taxonomic Filtering");
                filterRecords = taxonomyGraph.getRecordsToFilteredTaxIds(taxids, res.getBoolean("exclude_taxids"));
                if(filterRecords == null) {
                    filterRecords = new ArrayList<>();
                }
                LOGGER.info("Using " + filterRecords.size() + " genomes for mapping");
            }
            if (res.getString("sequence") != null) {
                if (res.getDouble("alpha") < 0 | res.getDouble("alpha") > 1) {
                    LOGGER.error("Alpha must be between 0 and 1");
                    System.exit(4);
                }

                try {
                    if (taxids != null) {
                        DeGeCI.annotateRefseq(res.getString("name"), res.getString("sequence"), res.getInt("taxid"),
                                !res.getBoolean("treat_linear"), res.getString("result_directory"),
                                res.getInt("weight"), res.getInt("count"), 16, res.getInt("refseq") + "",
                                res.getDouble("alpha"), false, filterRecords
                        );
                    } else {
                        DeGeCI.annotateRefseq(res.getString("name"), res.getString("sequence"), res.getInt("taxid"),
                                !res.getBoolean("treat_linear"), res.getString("result_directory"),
                                res.getInt("weight"), res.getInt("count"), 16, res.getInt("refseq") + "",
                                res.getDouble("alpha"), false
                        );
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                File fastaFile = new File(res.getString("fasta_file"));
                if (!fastaFile.exists()) {
                    LOGGER.info("Could not find fasta file " + fastaFile.getAbsolutePath());
                    System.exit(1);
                }
                try {
                    if (taxids != null) {
                        DeGeCI.annotateRefseq(res.getString("name"), DeGeCI.readSingleFastaFile(fastaFile.getAbsolutePath()), res.getInt("taxid"),
                                !res.getBoolean("treat_linear"), res.getString("result_directory"),
                                res.getInt("weight"), res.getInt("count"), 16, res.getInt("refseq") + "",
                                res.getDouble("alpha"), false, filterRecords
                        );
                    } else {
                        DeGeCI.annotateRefseq(res.getString("name"), fastaFile, res.getInt("taxid"),
                                !res.getBoolean("treat_linear"), res.getString("result_directory"),
                                res.getInt("weight"), res.getInt("count"), 16, res.getInt("refseq") + "",
                                res.getDouble("alpha"), false
                        );
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
       catch (Exception e) {
            LOGGER.error("No matches found");
//            e.printStackTrace();
            DeGeCI.clear(res.getString("result_directory"),res.getInt("refseq")+"");
            System.exit(-1);
        }
        DeGeCI.clear(res.getString("result_directory"),res.getInt("refseq")+"");




    }

    public static void main(String[] args) {

        Spark.getInstance();

        new DeGeCIInputParser(args);

        System.exit(0);
    }
}
