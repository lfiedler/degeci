package de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.ColumnIdentifier;
import org.apache.commons.io.FileUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.*;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.expressions.WindowSpec;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.Tuple2;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.*;

public class SparkComputer {

    protected String directory;
    public static final String PARQUET_EXTENSION = ".parquet";
    public static final String PARQUET_PATH = "/parquet";
    private static final Class showStringTypes[] = new Class[] {Integer.TYPE,Integer.TYPE,Boolean.TYPE};
    private static final String showStringMethodIdentifier = "showString";

    public static class JOIN_TYPES {
        public static final String INNER = "inner";
        public static final String LEFT = "left";
        public static final String RIGHT = "right";
        public static final String SEMI = "semi";
        public static final String LEFT_ANTI = "leftanti";
        public static final String RIGHT_ANTI = "rightanti";
    }

    protected interface PARQUET_NAMES {

    }

    public static void showStructFields(List<StructField> structFields) {
        for(StructField structField: structFields) {
            System.out.print(structField +", ");
        }
    }

    public String getDirectory() {
        return directory;
    }

    public SparkComputer(String directory) {
        this.directory = directory;
    }

    public static <T extends Serializable> Dataset<T> createDataFrame(List<T> serializables, Class<T> tClass){
        return createDataFrame(serializables,Encoders.bean(tClass));
    }

    public static <T extends Serializable> Dataset<T> createDataFrame(List<T> serializables, Encoder<T> encoder){
//        return Spark.getInstance().createDataFrame(serializables,tClass).as(encoder);
        return Spark.getInstance().createDataset(serializables,encoder);
    }

    public  static <T> void showAll(Dataset<T> dataset) {
        dataset.show((int)dataset.count());
    }

    public static  <T> void persistDataFrame(Dataset<T> dataset, String parquetName) {
        dataset.write().parquet(parquetName);
    }

    public static  <T> void persistDataFrameORC(Dataset<T> dataset, List<String> indexColumns, String fileName) {
        dataset.write().format("orc").
                option("orc.bloom.filter.columns",indexColumns.stream().collect(Collectors.joining(","))).
                save(fileName);
    }

    public static  <T> void persistDataFrameORC(Dataset<T> dataset, String fileName) {
        dataset.write().format("orc").
                save(fileName);
    }

    public static  <T> void persistDataFrameORCAppend(Dataset<T> dataset, String fileName) {
        dataset.write().mode("append").format("orc").
                save(fileName);
    }

    public static <T> Dataset<T> samplePerGroup(Dataset<T> dataset, Class<T> tClass, String groupName,int sampleSize) {
        WindowSpec w = Window.partitionBy(groupName).orderBy(functions.rand());
        String rank = "rank";

        return dataset.withColumn(rank,functions.rank().over(w)).filter(col(rank).leq( sampleSize)).drop(rank).
                as(Encoders.bean(tClass));
    }

    public static void checkDuplicates(Dataset<Row> dataset) {
        final String dupIdentifier = "dup";
        System.out.println("Duplicates");
        dataset.groupBy(SparkComputer.getColumns(dataset.columns())).
                agg(count("*").as(dupIdentifier)).
                filter(col(dupIdentifier).gt(1)).
//                withColumn(dupIdentifier,count("*").over()).
        show(100);
    }

    public static Dataset<Row> createDuplicateFreeDataset(Dataset<Row> dataset) {
        final String dupIdentifier = "dup";

        return dataset.groupBy(SparkComputer.getColumns(dataset.columns())).
                agg(count("*").as(dupIdentifier)).drop(dupIdentifier).distinct();
    }

    public static  <T> void persistOverwriteDataFrame(Dataset<T> dataset, String parquetName) {

        File f = new File(parquetName);
        if(f.exists()) {
            try {
                FileUtils.deleteDirectory(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        dataset.write().parquet(parquetName);
    }

    public static  <T> void persistOverwriteDataFrameORC(Dataset<T> dataset, String fileName) {
        Spark.clearDirectory(fileName);
        persistDataFrameORC(dataset,fileName);
    }

    public static void writeToCSV(Dataset<?> dataset, String path)  {
        File f = new File(path);
        if(f.exists()) {
            try {
                FileUtils.deleteDirectory(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        dataset.repartition(1).write().
        format("com.databricks.spark.csv").
        option("header", "true")
        .save(path);
    }

    public static void writeToJSon(Dataset<?> dataset, String path)  {
        File f = new File(path);
        if(f.exists()) {
            try {
                FileUtils.deleteDirectory(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        dataset.repartition(1).write().
                format("com.databricks.spark.csv").
                option("header", "true")
                .save(path);
    }

    public static  <T> void appendToPersistedDataFrame(Dataset<T> dataset, String parquetName) {
        dataset.write().mode("append").parquet(parquetName);
    }

    public static <T> T getFirstOrElseNull(Dataset<T> dataset) {
        try {
            return dataset.first();
        }catch (NoSuchElementException e) {
            return null;
        }
    }

    public static <T> T getFirstOrElse(Dataset<T> dataset,T elseElement) {
        try {
            return dataset.first();
        }catch (NoSuchElementException e) {
            return elseElement;
        }
    }



    public static Dataset<Row> swapRows(Dataset<Row> dataset, String col1, String col2){
        String randomString = String.valueOf(UUID.randomUUID());
        return dataset.withColumn(col1+randomString,col(col2)).
                withColumn(col2,col(col1)).
                withColumn(col1,col(col1+randomString)).drop(col1+randomString);
    }

    public static Dataset<Row> swapRows(Dataset<Row> dataset, String col1, String col2,Column condition){
        return swapRows(dataset.filter(condition),col1,col2).union(
                dataset.filter(not(condition))
        );
    }

    public static Dataset<Row> read(String pathToFile, StructType schema) {
        try {
            return Spark.getInstance().read().schema(schema).parquet(pathToFile);
        } catch (Exception a) {
            return null;
        }
    }

    public static Dataset<Row> readORC(String pathToFile) {
        return Spark.getInstance().read().format("orc").load(pathToFile);
    }

    public static <T> Dataset<T> readORC(String pathToFile,Encoder<T> encoder) {
        return readORC(pathToFile).as(encoder);
    }

    public static Dataset<Row> read(String pathToFile) {
        return Spark.getInstance().read().parquet(pathToFile);
    }

    public static <T> Dataset<T> read(String pathToFile, Class<T> tClass) {
        return read(pathToFile).as(Encoders.bean(tClass));
    }

    public static <T> Dataset<T> read(String pathToFile, Encoder<T> encoder) {
        return read(pathToFile).as(encoder);
    }

    public static <T> Dataset<T> read(String pathToFile, Encoder<T> encoder, StructType schema) {
        try{
            return read(pathToFile,schema).as(encoder);
        } catch (NullPointerException nullPointerException) {
            return null;
        }
    }

    public static void changeShufflePartition(String partitionValue) {
        Spark.getInstance().sqlContext().setConf("spark.sql.shuffle.partitions",partitionValue);
    }

    public static <T> void showDatTypes(Dataset<T> dataset) {
        Tuple2<String, String>[] t = dataset.dtypes();
        for(Tuple2<String,String> tuple2 : t) {
            System.out.println(tuple2._1 + " " + tuple2._2);
        }
    }

    public static String getString(Dataset<?> dataset) {

        String o = null;
        try {
            Method method = Dataset.class.getMethod(showStringMethodIdentifier,showStringTypes);
            o = (String)method.invoke(dataset,new Object[] {(int)dataset.count(),0,true});
            System.out.println(o);
        } catch (NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            return o;
        }

    }

    public static String getString(Dataset<?> dataset, int rows) {

        String o = null;
        try {
            Method method = Dataset.class.getMethod(showStringMethodIdentifier,showStringTypes);
            o = (String)method.invoke(dataset,new Object[] {rows,0,true});
            System.out.println(o);
        } catch (NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            return o;
        }

    }

    public static <T> Dataset<Row> convert(Dataset<T> dataset, String[] columnNames) {
        Column[] columns = new Column[columnNames.length];
        for(int i = 0; i < columns.length; i++) {
            columns[i] = col(columnNames[i]);
        }
        return dataset.select(columns);
    }

    public static <T,K> Dataset<K> convert(Dataset<T> dataset, String[] columnNames, Encoder<K> encoder) {
        Column[] columns = new Column[columnNames.length];
        for(int i = 0; i < columns.length; i++) {
            columns[i] = col(columnNames[i]);
        }
        return dataset.select(columns).as(encoder);
    }

    public static Column[] getColumns(String... columnNames) {
        org.apache.spark.sql.Column[] columns = new org.apache.spark.sql.Column[columnNames.length];
        for(int i = 0; i < columnNames.length; i++) {
            columns[i] = col(columnNames[i]);
        }
        return columns;
    }

    public static Column[] toLiterals(Object... objects) {
        org.apache.spark.sql.Column[] columns = new org.apache.spark.sql.Column[objects.length];
        for(int i = 0; i < objects.length; i++) {
            columns[i] = lit(objects[i]);
        }
        return columns;
    }

    public static  Column[] getJoinedColumns(Column [] c1, Column... c2) {
        org.apache.spark.sql.Column[] columns = new org.apache.spark.sql.Column[c1.length+c2.length];
//        System.out.println("c1 Size" + c1.length);
//        System.out.println("c2 Size" + c2.length);
//        System.out.println("joined Size" + columns.length);
        for(int i = 0; i < c1.length; i++) {

            columns[i] = c1[i];
        }
        for(int i = c1.length, j = 0; i < c1.length+c2.length; i++,j++) {

            columns[i] = c2[j];
        }
        return columns;
    }



    public static boolean containsColumn(Dataset<Row> dataset, String column) {
        for(String col : dataset.columns()) {
            if(col.equals(column)) {
                return true;
            }
        }
        return false;
    }


    public static Dataset<Row> filterRange(Dataset<Row> dataset, int start, int amount, Column... ordercolumns) {
        return dataset.withColumn(ColumnIdentifier.INDEX,row_number().over(Window.orderBy(ordercolumns))).
                filter(col(ColumnIdentifier.INDEX).between(start,start+amount)).drop(ColumnIdentifier.INDEX);
    }
//    public static  Dataset<Row> appendIdentifier(Dataset<?> dataset, String identifier) {
//        String[] columns = dataset.columns();
//        if(columns.length == 0) {
//            return dataset.toDF();
//        }
//        Dataset<Row> appendedDataset = dataset.withColumnRenamed(columns[0],columns[0]+identifier);
//        for(int i = 1; i < columns.length; i++) {
//            appendedDataset = appendedDataset.withColumnRenamed(columns[i],columns[i]+identifier);
//        }
//        return appendedDataset;
//    }

    public static Dataset<Row> splitCyclicRange(Dataset<Row> dataset,Column start, Column end, Column sequenceLength) {
        String[] colNames = dataset.columns();
        return dataset.filter(start.gt(end))
                .withColumn("z",explode(arrays_zip(array(start,lit(0)),array(sequenceLength.minus(1),end))))
                .withColumn(start.toString(),col("z.0")).withColumn(end.toString(),col("z.1"))
                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getLinearRangeLength(start,end))
                .select(getColumns(colNames));
//                union(dataset.filter(col(ColumnIdentifier.START).gt(col(ColumnIdentifier.END))))
    }
    public static Column toArray(String... cols) {
        return array(getColumns(cols));
    }

    public static Column toArrayFromObjects(Object... objects) {
        return array(toLiterals(objects));
    }

    public static  Dataset<Row> appendIdentifier( Dataset<?> dataset, String identifier,String... columns) {
//
        if(columns.length == 0) {
            columns = dataset.columns();
            if(columns.length == 0) {
                return dataset.toDF();
            }
        }
        Dataset<Row> appendedDataset = dataset.withColumnRenamed(columns[0],columns[0]+identifier);
        for(int i = 1; i < columns.length; i++) {

            appendedDataset = appendedDataset.withColumnRenamed(columns[i],columns[i]+identifier);
        }
        return appendedDataset;
    }

    public static <T> Dataset<T>[] equalSplit(Dataset<T> dataset, long maxSize) {
        long count = dataset.count();
        double[] weights;
        if(maxSize >= count) {
            weights = new double[1];
            weights[0] = 1.0;
        }
        else {
            System.out.println("Count " + count);
            System.out.println("Amount " + (count/maxSize));
            weights = new double[(int)(count/maxSize)];
            double factor = (double)maxSize/count;
            System.out.println("Factor " + factor);
            for(int i = 0; i < weights.length; i++) {
                weights[i] = factor;
            }
        }
        return dataset.randomSplit(weights);
    }

    public static <T> List<Dataset<T>> equalSplitList(Dataset<T> dataset, long maxSize) {
        List<Dataset<T>> datasetList = new ArrayList<>();
        Dataset<T>[] datasets = equalSplit(dataset,maxSize);
        for(Dataset<T> dataset1: datasets) {
            if(!dataset1.isEmpty()) {
                datasetList.add(dataset1);
            }
        }

        return datasetList;
    }

    public static <T> Dataset<T> union(JavaRDD<T>[] javaRDDS, Encoder<T> tEncoder) {
            return Spark.getInstance().createDataset(Spark.getJavaSparkContext().union(javaRDDS).rdd(),tEncoder);
    }

    public static <T> Dataset<T> union(List<Dataset<T>> datasets, Encoder<T> tEncoder) {
        if(datasets.size() == 0) {
            return null;
        }
        return union(datasets.stream().map(dataset -> dataset.javaRDD()).toArray(JavaRDD[]::new),tEncoder);
    }

    public static String getBoxPlotData(Dataset<?> dataset, String groupColumnName) {

        if(dataset.isEmpty()) {
            return "";
        }
        double[] quantiles = dataset.stat().
                approxQuantile(groupColumnName, new double[]{0.25, 0.5, 0.75}, 0);
        Row rows = dataset.groupBy().
                agg(
                        min(col(groupColumnName)).as(ColumnIdentifier.MIN),
                        max(col(groupColumnName)).as(ColumnIdentifier.MAX)).first();
        return quantiles[0]+"," +quantiles[1]+","+quantiles[2]+","+rows.get(0) +","+rows.get(1);

    }

    public static Dataset<Row> addRowNumber(Dataset<Row> dataset) {
        return addRowNumber(dataset,ColumnIdentifier.ROW_NUMBER);
    }

    public static Dataset<Row> addRowNumber(Dataset<Row> dataset,String rowNumberIdentifier) {
        return dataset.withColumn(rowNumberIdentifier,lit(1)).
                withColumn(rowNumberIdentifier,row_number().over(Window.orderBy(rowNumberIdentifier)));
    }

}
