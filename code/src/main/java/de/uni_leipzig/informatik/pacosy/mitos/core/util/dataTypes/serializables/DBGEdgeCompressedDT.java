package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class DBGEdgeCompressedDT extends EdgeDT {


    protected int recordId;
    private Integer[] positions;
    private boolean strand;
    public static final Encoder<DBGEdgeCompressedDT> ENCODER = Encoders.bean(DBGEdgeCompressedDT.class);

    public DBGEdgeCompressedDT(String src, String dst, int recordId, boolean strand, Integer[] positions) {
        super(src,dst);
        this.recordId = recordId;
        this.positions = positions;
        this.strand = strand;
    }

    public Integer[] getPositions() {
        return positions;
    }

    public void setPositions(Integer[] positions) {
        this.positions = positions;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public boolean isStrand() {
        return strand;
    }

    public void setStrand(boolean strand) {
        this.strand = strand;
    }
}
