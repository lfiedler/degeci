package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class TopologyEdgeDT extends EdgeDT{
    protected int weight;

    public static final Encoder<TopologyEdgeDT> ENCODER = Encoders.bean(TopologyEdgeDT.class);

    public TopologyEdgeDT(String src, String dst, int weight) {
        super(src, dst);
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
