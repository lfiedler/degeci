package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.RecordIdInterface;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.features.FeatureInterface;
import org.biojava.nbio.core.sequence.features.Qualifier;
import org.biojava.nbio.core.sequence.io.GenbankReaderHelper;
import org.biojava.nbio.core.sequence.template.AbstractSequence;
import org.biojava.nbio.core.sequence.template.SequenceMixin;

import java.io.*;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class RecordKmerDT implements RecordIdInterface, Serializable {
    protected String kmer;
    protected int recordId;
    protected int recordPosition;
    protected int recordLength;
    public static final Encoder<RecordKmerDT> ENCODER = Encoders.bean(RecordKmerDT.class);

    public RecordKmerDT() {
    }

    public RecordKmerDT(String kmer, int recordId, int recordPosition, int recordLength) {
        this.kmer = kmer;
        this.recordId = recordId;
        this.recordPosition = recordPosition;
        this.recordLength = recordLength;
    }

    public String getKmer() {
        return kmer;
    }

    public void setKmer(String kmer) {
        this.kmer = kmer;
    }

    @Override
    public int getRecordId() {
        return recordId;
    }

    @Override
    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public int getRecordPosition() {
        return recordPosition;
    }

    public void setRecordPosition(int recordPosition) {
        this.recordPosition = recordPosition;
    }

    public int getRecordLength() {
        return recordLength;
    }

    public void setRecordLength(int recordLength) {
        this.recordLength = recordLength;
    }


}
