package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class NodePairDT implements Serializable {
    protected String v1;
    protected String v2;
    public static final Encoder<NodePairDT> ENCODER = Encoders.bean(NodePairDT.class);
    public NodePairDT() {
    }

    public NodePairDT(String v1, String v2) {
        this.v1 = v1;
        this.v2 = v2;
    }

    public String getV1() {
        return v1;
    }

    public void setV1(String v1) {
        this.v1 = v1;
    }

    public String getV2() {
        return v2;
    }

    public void setV2(String v2) {
        this.v2 = v2;
    }


    public static <T extends NodePairDT> List<String> getDistinctNodes(Collection<T> nodePairs) {
        Set<String> nodes = new HashSet<>(nodePairs.stream().map(n -> n.v1).collect(Collectors.toList()));
        nodes.addAll(nodePairs.stream().map(n -> n.v2).collect(Collectors.toList()));
        return new ArrayList<>(nodes);
    }
}
