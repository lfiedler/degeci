package de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess;


import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.Argument;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.ArgumentType;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LocalProperty extends AccessProperty {

    // Remark: If memory issues arise, the cache settings ars inadequate. First transaction level cache (via cache.tx-cache-size/MAX_TRANSACTION_LEVEL_CACHE_OF_RECENTLY_USED_VERTICES) should be lowered.
    // if this does not help, database level cache may be adapted via (cache.db-cache/ENABLE_CACHING, cache.db-cache-time (global), cache.db-cache-size/DATABASE_LEVEL_CACHE_PERCENTAGE)
    // The database level cache retains adjacency lists (or subsets thereof) across multiple transactions and beyond the duration of a single transaction.
    // The database level cache is shared by all transactions across a database. It is more space efficient than the transaction level caches but also slightly slower to access.
    // In contrast to the transaction level caches, the database level caches do not expire immediately after closing a transaction. Hence, the database level cache significantly speeds up graph traversals for read heavy workloads across transactions.
    public enum ALLOWED_KEYS implements ALLOWED_KEYS_INTERFACE{
        ENABLE_CACHING("cache.db-cache"),
        DATABASE_LEVEL_CACHE_PERCENTAGE("cache.db-cache-size"),
        MAX_TRANSACTION_LEVEL_CACHE_OF_RECENTLY_USED_VERTICES("cache.tx-cache-size"),
        TRANSACTION_LEVEL_CACHE_DIRTY_VERTICES_SIZE("cache.tx-dirty-size"),
        IDS_RENEW_TIMEOUT("ids.renew-timeout"),
        IDS_RENEW_PERECENTAGE("ids.renew-percentage"),
        ENABLE_QUERY_BATCH("query.batch"),
        ENABLE_PRE_FETCH_PROPERTIES_TO_ADJACENT_VERTICES("query.batch-property-prefetch"),
        ENABLE_PRE_FETCH_ALL_PROPERTIES("query.fast-property"),
        DISABLE_DATA_CONSISTENCY_CHECKS("storage.batch-loading"),
        STORAGE_CONNECTION_TIMEOUT("storage.connection-timeout"),
        STORAGE_WRITE_TIME("storage.write-time"),
        STORAGE_READ_TIME("storage.read-time"),
        STORAGE_HOSTNAME("storage.hostname"),
        ES_MAX_RETRY_TIMEOUT("index.search.elasticsearch.max-retry-timeout"),
        ES_MAX_RESULT_SIZE("index.search.max-result-set-size"),
        ES_STORAGE_PORT("index.search.port")
        ;
        public final String identifier;
        public String getIdentifier() {
            return identifier;
        }


        ALLOWED_KEYS(String identifier) {
            this.identifier = identifier;
        }
    }

    private enum MUTABILITY implements MUTABILITY_INTERFACE {
        LOCAL,
        MASKABLE;
    }


    private ALLOWED_KEYS propertyKey;
    private static Map<ALLOWED_KEYS,PropertyAttribute> properties;

    private Object value;
    private static String splitValue = ":";

    static {
        setProperties();
    }

    public LocalProperty() {

    }

    public LocalProperty(ALLOWED_KEYS key, Object value) {
        super(key,value);
    }

    public LocalProperty(String str) {
        super(str);
    }

    protected static void setProperties() {

        properties = new HashMap<>();
        properties.put(ALLOWED_KEYS.ENABLE_CACHING,new PropertyAttribute(Boolean.class,false,MUTABILITY.MASKABLE,"Whether to enable JanusGraph's database-level cache, which is shared across all transactions. Enabling this option speeds up traversals by holding hot graph elements in memory, but also increases the likelihood of reading stale data. Disabling it forces each transaction to independently fetch graph elements from storage before reading/writing them."));
        properties.put(ALLOWED_KEYS.DATABASE_LEVEL_CACHE_PERCENTAGE,new PropertyAttribute(Double.class,0.3,MUTABILITY.MASKABLE,"Size of JanusGraph's database level cache. Values between 0 and 1 are interpreted as a percentage of VM heap, while larger values are interpreted as an absolute size in bytes. " +
                "Controls how much heap space JanusGraph’s database level cache is allowed to consume. The larger the cache, the more effective it will be. However, large cache sizes can lead to excessive GC and poor performance. " +
                "Configuring a cache that is too large can lead to out-of-memory exceptions and excessive GC."));
        properties.put(ALLOWED_KEYS.MAX_TRANSACTION_LEVEL_CACHE_OF_RECENTLY_USED_VERTICES,new PropertyAttribute(Integer.class,20000,MUTABILITY.MASKABLE,"Maximum size of the transaction-level cache of recently-used vertices. Tunes the vertex and index cache during transactions. If the same vertex is not accessed again in the transaction, this setting will make no difference for the vertex cache." +
                " The size of the vertex cache on heap is not only determined by the number of vertices it may hold but also by the size of their adjacency list. " +
                "In other words, vertices with large adjacency lists (i.e. many incident edges) will consume more space in this cache than those with smaller lists. " +
                "Modified vertices are pinned in the cache, which means they cannot be evicted since that would entail loosing their changes. Therefore, transaction which contain a lot of modifications may end up with a larger than configured vertex cache. " +
                "If the same index call never occurs twice in the same transaction, the index cache makes no difference."));
        properties.put(ALLOWED_KEYS.TRANSACTION_LEVEL_CACHE_DIRTY_VERTICES_SIZE,new PropertyAttribute(Integer.class,"no-default",MUTABILITY.MASKABLE,"Initial size of the transaction-level cache of uncommitted dirty vertices. This is a performance hint for write-heavy, performance-sensitive transactional workloads. If set, it should roughly match the median vertices modified per transaction."));
        properties.put(ALLOWED_KEYS.IDS_RENEW_TIMEOUT,new PropertyAttribute(Duration.class,Duration.ofMillis(120000),MUTABILITY.MASKABLE,"The number of milliseconds that the JanusGraph id pool manager will wait before giving up on allocating a new block of ids."));
        properties.put(ALLOWED_KEYS.IDS_RENEW_PERECENTAGE,new PropertyAttribute(Double.class,0.3,MUTABILITY.MASKABLE,"When the most-recently-reserved ID block has only this percentage of its total IDs remaining (expressed as a value between 0 and 1), JanusGraph asynchronously begins reserving another block. This helps avoid transaction commits waiting on ID reservation even if the block size is relatively small."));
        properties.put(ALLOWED_KEYS.ENABLE_QUERY_BATCH,new PropertyAttribute(Boolean.class,false,MUTABILITY.MASKABLE,"Whether traversal queries should be batched when executed against the storage backend. This can lead to significant performance improvement if there is a non-trivial latency to the backend."));
        properties.put(ALLOWED_KEYS.ENABLE_PRE_FETCH_PROPERTIES_TO_ADJACENT_VERTICES, new PropertyAttribute(Boolean.class,false,MUTABILITY.MASKABLE,"Whether to do a batched pre-fetch of all properties on adjacent vertices against the storage backend prior to evaluating a has condition against those vertices. Because these vertex properties will be loaded into the transaction-level cache of recently-used vertices when the condition is evaluated this can lead to significant performance improvement if there are many edges to adjacent vertices and there is a non-trivial latency to the backend."));
        properties.put(ALLOWED_KEYS.ENABLE_PRE_FETCH_ALL_PROPERTIES, new PropertyAttribute(Boolean.class,true,MUTABILITY.MASKABLE,"Whether to pre-fetch all properties on first singular vertex property access. This can eliminate backend calls on subsequentproperty access for the same vertex at the expense of retrieving all properties at once. This can be expensive for vertices with many properties"));
        properties.put(ALLOWED_KEYS.DISABLE_DATA_CONSISTENCY_CHECKS, new PropertyAttribute(Boolean.class,false,MUTABILITY.LOCAL,"Whether to enable batch loading into the storage backend"));
        properties.put(ALLOWED_KEYS.STORAGE_CONNECTION_TIMEOUT,new PropertyAttribute(Duration.class,Duration.ofMillis(10000),MUTABILITY.MASKABLE,"Default timeout, in milliseconds, when connecting to a remote database instance."));
        properties.put(ALLOWED_KEYS.STORAGE_WRITE_TIME,new PropertyAttribute(Duration.class,Duration.ofMillis(100000),MUTABILITY.MASKABLE,"Maximum time (in ms) to wait for a backend write operation to complete successfully. If a backend write operationfails temporarily, JanusGraph will backoff exponentially and retry the operation until the wait time has been exhausted."));
        properties.put(ALLOWED_KEYS.STORAGE_READ_TIME,new PropertyAttribute(Duration.class,Duration.ofMillis(10000),MUTABILITY.MASKABLE,"Maximum time (in ms) to wait for a backend read operation to complete successfully. If a backend read operationfails temporarily, JanusGraph will backoff exponentially and retry the operation until the wait time has been exhausted."));
        properties.put(ALLOWED_KEYS.STORAGE_HOSTNAME,new PropertyAttribute(String.class,new String[] {"127.0.0.1"},MUTABILITY.LOCAL,"The hostname or comma-separated list of hostnames of storage backend servers. This is only applicable to some storage backends, such as cassandra and hbase."));
        properties.put(ALLOWED_KEYS.ES_MAX_RETRY_TIMEOUT,new PropertyAttribute(Integer.class,null,MUTABILITY.MASKABLE,"Sets the maximum timeout (in milliseconds) to honour in case of multiple retries of the same request sent using the ElasticSearch Rest Client by JanusGraph."));
        properties.put(ALLOWED_KEYS.ES_MAX_RESULT_SIZE,new PropertyAttribute(Integer.class,50,MUTABILITY.MASKABLE,"Maximum number of results to return if no limit is specified. For index backends that support scrolling, it represents the number of results in each batch"));
        properties.put(ALLOWED_KEYS.ES_STORAGE_PORT,new PropertyAttribute(Integer.class,null,MUTABILITY.MASKABLE,"The port on which to connect to index backend servers"));

    }

    public Map<ALLOWED_KEYS,PropertyAttribute> getProperties() {
        return properties;
    }

    protected static List<String> getKeyIdentifier() {
        return properties.keySet().stream().map( key -> key.getIdentifier()).collect(Collectors.toList());
    }

}
