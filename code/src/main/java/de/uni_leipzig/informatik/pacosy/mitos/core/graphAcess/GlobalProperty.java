package de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GlobalProperty extends AccessProperty {

    public enum ALLOWED_KEYS implements ALLOWED_KEYS_INTERFACE{

        CACHE_DB_CACHE_TIME("cache.db-cache-time"),
        IDS_BLOCK_SIZE("ids.block-size"),
        SEARCH_BACKEND("index.search.backend");
        public final String identifier;
        public String getIdentifier() {
            return identifier;
        }
        ALLOWED_KEYS(String identifier) {
            this.identifier = identifier;
        }
    }

    private enum MUTABILITY implements MUTABILITY_INTERFACE {
        GLOBAL,
        GLOBAL_OFFLINE;
    }


    private ALLOWED_KEYS propertyKey;
    private static Map<ALLOWED_KEYS,PropertyAttribute> properties;

    private Object value;
    private static String splitValue = ":";

    static {
        setProperties();
    }

    public GlobalProperty() {
    }

    public GlobalProperty(ALLOWED_KEYS key, Object value) {
        super(key,value);
    }

    public GlobalProperty(String str) {
        super(str);
    }

    protected static void setProperties() {

        properties = new HashMap<>();
        properties.put(ALLOWED_KEYS.SEARCH_BACKEND,new PropertyAttribute(String.class,"elasticsearch",MUTABILITY.GLOBAL_OFFLINE
                ,"Search engine to use."));
        properties.put(ALLOWED_KEYS.CACHE_DB_CACHE_TIME,new PropertyAttribute(Long.class,10000,MUTABILITY.GLOBAL_OFFLINE
                ,"Default expiration time, in milliseconds, for entries in the database-level cache. Entries are evicted when they reach this age even if the cache has room to spare. " +
                "Set to 0 to disable expiration (cache entries live forever or until memory pressure triggers eviction when set to 0)."));
        properties.put(ALLOWED_KEYS.IDS_BLOCK_SIZE,new PropertyAttribute(Integer.class,10000,MUTABILITY.GLOBAL_OFFLINE
                ,"Globally reserve graph element IDs in chunks of this size. Setting this too low will make commits frequently block on slow reservation requests. " +
                "Setting it too high will result in IDs wasted when a graph instance shuts down with reserved but mostly-unused blocks."));
    }

    public Map<ALLOWED_KEYS,PropertyAttribute> getProperties() {
        return properties;
    }

//    public List<GlobalProperty> parseConfiguration(GraphConfiguration configuration) {
//        List<GlobalProperty> properties = new ArrayList<>();
//        List<GlobalProperty> keys = getKeyIdentifier();
//        keys.retainAll(configuration.getKeysAsList());
//        for(GlobalProperty key: keys) {
//            configuration.getP
//        }
//    }

    protected static List<String> getKeyIdentifier() {
        return properties.keySet().
                stream().map( key -> key.getIdentifier()).collect(Collectors.toList());
    }
}
