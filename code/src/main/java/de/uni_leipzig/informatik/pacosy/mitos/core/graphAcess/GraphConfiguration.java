package de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess;

import com.google.common.io.Resources;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.tinkerpop.gremlin.driver.remote.DriverRemoteConnection;
import org.apache.tinkerpop.gremlin.process.traversal.AnonymousTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphFactory;
import org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import static org.apache.tinkerpop.gremlin.process.traversal.AnonymousTraversalSource.traversal;

public class GraphConfiguration extends org.apache.commons.configuration.PropertiesConfiguration {

//    private static final String GRAPH_CONNECTION_DIR = Resources.getResource("graphConnectionFiles").getFile();
    private static final String GRAPH_CONNECTION_DIR = "/graphConnectionFiles";
//    private static final GraphConfiguration oltpMinimalTemplateConfiguration = loadConfiguration(new File(GRAPH_CONNECTION_DIR + "/oltpMinimal.properties"));
//    private static final GraphConfiguration oltpStandardTemplateConfiguration = loadConfiguration(new File(GRAPH_CONNECTION_DIR + "/oltp.properties"));
//    private static final GraphConfiguration oltpBulkLoadTemplateConfiguration = loadConfiguration(new File(GRAPH_CONNECTION_DIR + "/oltpBulkLoad.properties"));
//    private static final GraphConfiguration oltpLowCacheTemplateConfiguration = loadConfiguration(new File(GRAPH_CONNECTION_DIR + "/oltpLowCache.properties"));

    private static final GraphConfiguration oltpMinimalTemplateConfiguration = loadConfiguration(GraphConfiguration.class.getResourceAsStream(GRAPH_CONNECTION_DIR + "/oltpMinimal.properties"));
    private static final GraphConfiguration oltpStandardTemplateConfiguration = loadConfiguration(GraphConfiguration.class.getResourceAsStream(GRAPH_CONNECTION_DIR + "/oltp.properties"));
    private static final GraphConfiguration oltpBulkLoadTemplateConfiguration = loadConfiguration(GraphConfiguration.class.getResourceAsStream(GRAPH_CONNECTION_DIR + "/oltpBulkLoad.properties"));
    private static final GraphConfiguration oltpLowCacheTemplateConfiguration = loadConfiguration(GraphConfiguration.class.getResourceAsStream(GRAPH_CONNECTION_DIR + "/oltpLowCache.properties"));
//    private static final GraphConfiguration remoteConfiguration =  loadConfiguration(GraphConfiguration.class.getResourceAsStream(GRAPH_CONNECTION_DIR + "/remote-graph.properties"));

    private static Logger LOGGER = LoggerFactory.getLogger(GraphConfiguration.class);
    private static final String CQL_KEY_TAG = "storage.cql.keyspace";
    private List<GlobalProperty> globalPropertiesToCheck;

    static {
        oltpStandardTemplateConfiguration.copy(oltpMinimalTemplateConfiguration);
        oltpLowCacheTemplateConfiguration.copy(oltpMinimalTemplateConfiguration);
        oltpBulkLoadTemplateConfiguration.copy(oltpMinimalTemplateConfiguration);
    }

    public enum CONNECTION_TYPE {
        STANDARD_OLTP,

        BULK_LOAD_OLTP,
        LOW_CACHE_OLTP,
        MINIMAL_OLTP;
    }

    private GraphConfiguration(PropertiesConfiguration propertiesConfiguration) {
        setProperty(propertiesConfiguration);
    }

    private GraphConfiguration(File graphConfigurationFile) throws ConfigurationException {
        super(graphConfigurationFile);
    }

    private GraphConfiguration() {
        super();
    }

    public List<String> getKeysAsList() {
        List<String> keys = new ArrayList<>();
        getKeys().forEachRemaining(k -> keys.add(k));
        return keys;
    }

    private static GraphConfiguration mergeGraphConfigurations(GraphConfiguration configuration1, GraphConfiguration configuration2) {
        GraphConfiguration graphConfiguration = new GraphConfiguration(configuration1);
        graphConfiguration.setProperty(configuration2);
        return graphConfiguration;
    }

    private static GraphConfiguration loadConfiguration(InputStream configurationFile) {
        GraphConfiguration configuration = null;

        try {
            configuration = new GraphConfiguration();


            configuration.load(configurationFile);

            // make sure path is absolute one (necessary for yaml Configuration)
            Matcher m;
            Pattern yamlPattern = Pattern.compile( ".*"+GraphDatabaseConfiguration.STORAGE_CONF_FILE.getName());
            Iterator<String> it = configuration.getKeys();
            while(it.hasNext()) {

                String keyId = it.next();

                m = yamlPattern.matcher(keyId);
                if(m.matches()) {
                    String newValue = configuration.getBasePath()
                            .substring(0,configuration.getBasePath().lastIndexOf("/")+1) +
                            configuration.getString(keyId);
                    configuration.setProperty(keyId,newValue);
                }

            }

        } catch (ConfigurationException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return configuration;
    }

    private static GraphConfiguration loadConfiguration(File configurationFile) {
        GraphConfiguration configuration = null;

        try {
            configuration = new GraphConfiguration(configurationFile);



            // make sure path is absolute one (necessary for yaml Configuration)
            Matcher m;
            Pattern yamlPattern = Pattern.compile( ".*"+GraphDatabaseConfiguration.STORAGE_CONF_FILE.getName());
            Iterator<String> it = configuration.getKeys();
            while(it.hasNext()) {

                String keyId = it.next();

                m = yamlPattern.matcher(keyId);
                if(m.matches()) {
                    String newValue = configuration.getBasePath()
                            .substring(0,configuration.getBasePath().lastIndexOf("/")+1) +
                            configuration.getString(keyId);
                    configuration.setProperty(keyId,newValue);
                }

            }

        } catch (ConfigurationException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return configuration;
    }

    private void setProperty(PropertiesConfiguration propertiesConfiguration) {
        propertiesConfiguration.getKeys().forEachRemaining(k -> this.setProperty(k,propertiesConfiguration.getProperty(k)));
    }

    public void setProperty(AccessProperty accessProperty) {

        setProperty(
                accessProperty.getPropertyKey().getIdentifier(),
                accessProperty.getValue());


    }

    public List<Object> getProperties(List<String> keys) {
        final List<Object> properties = new ArrayList<>();
        keys.stream().forEach(k -> properties.add(getProperty(k)));
        return properties;
    }

    public List<String> getPropertiesAsStringList(List<String> keys) {
        final List<String> properties = new ArrayList<>();
        keys.stream().forEach(k -> properties.add(k + AccessProperty.SPLIT_VALUE+ getProperty(k)));
        return properties;
    }

    public List<GlobalProperty> getGlobalPropertiesToCheck() {
        return globalPropertiesToCheck;
    }

    public static String getKeySpaceIdentifier() {
        return CQL_KEY_TAG;
    }

    public String toString() {
        return getKeysAsList().stream().map(key -> key + ": " + getProperty(key)).collect(Collectors.joining("\n"));
    }

    public static class Builder {

        private String keyspace;
        private CONNECTION_TYPE connectionType;
        private PropertiesConfiguration propertiesConfiguration;
        private boolean useElasticSearch;
        private List<? extends AccessProperty> properties;

        private Builder() {
            this.keyspace = null;
            useElasticSearch = false;
            this.properties = null;
        }

        public Builder(String keySpace, CONNECTION_TYPE connectionType) {
            this();
            this.keyspace = keySpace;
            this.connectionType = connectionType;
            assert(connectionType != null);

        }

        public Builder(PropertiesConfiguration propertiesConfiguration) {
            this();
            this.propertiesConfiguration = propertiesConfiguration;
        }

        public <T extends AccessProperty> Builder properties(List<T> properties) {
            this.properties = properties;
            return this;
        }

        public Builder useElasticSearch() {
            useElasticSearch = true;
            return this;
        }

        public GraphConfiguration build() {
            GraphConfiguration graphConfiguration = null;
            if(keyspace != null) {

                switch (connectionType) {
                    case STANDARD_OLTP:
                        graphConfiguration = (GraphConfiguration) oltpStandardTemplateConfiguration.clone();
//                        graphConfiguration.setProperty(oltpStandardTemplateConfiguration);
                        break;
                    case BULK_LOAD_OLTP:
                        graphConfiguration = (GraphConfiguration) oltpBulkLoadTemplateConfiguration.clone();
//                        graphConfiguration.setProperty(oltpBulkLoadTemplateConfiguration);
                        break;
                    case MINIMAL_OLTP:
                        graphConfiguration = (GraphConfiguration) oltpMinimalTemplateConfiguration.clone();
//                        graphConfiguration.setProperty(oltpMinimalTemplateConfiguration);
                        break;
                    case LOW_CACHE_OLTP:
                        graphConfiguration = (GraphConfiguration) oltpLowCacheTemplateConfiguration.clone();
//                        graphConfiguration.setProperty(oltpLowCacheTemplateConfiguration);
                    default:
                        break;
                }
                graphConfiguration.setProperty(CQL_KEY_TAG,keyspace);
            }
            else {
                graphConfiguration = new GraphConfiguration(propertiesConfiguration);
            }
            if(useElasticSearch) {
                graphConfiguration.setProperty(new GlobalProperty(GlobalProperty.ALLOWED_KEYS.SEARCH_BACKEND,"elasticsearch"));
            }
            if(properties != null) {
                for(AccessProperty property: properties) {
                    graphConfiguration.setProperty(property);
                }
            }
            List<String> keys = GlobalProperty.getKeyIdentifier();
            keys.retainAll(graphConfiguration.getKeysAsList());
            keys.remove(GlobalProperty.ALLOWED_KEYS.SEARCH_BACKEND.identifier);
            List<String> properties = graphConfiguration.getPropertiesAsStringList(keys);
            graphConfiguration.globalPropertiesToCheck =
                    properties.stream().map(p -> new GlobalProperty(p)).collect(Collectors.toList());

//            System.out.println(graphConfiguration);

            return graphConfiguration;
        }

    }

    public void printConfiguration() {
        Iterator<String> it = getKeys();
        while(it.hasNext()) {
            String keyId = it.next();
            System.out.println(keyId + ": "+ getProperty(keyId));
        }
    }


}
