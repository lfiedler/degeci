package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.RangeDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.SequenceInformationDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.FileIO;
import org.apache.commons.collections.list.TreeList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TreeNode implements Comparable<TreeNode> {

    protected static final Logger LOGGER = LoggerFactory.getLogger(TreeNode.class);
    @JsonBackReference
    private TreeNode parent;
    @JsonManagedReference
    private List<TreeNode> children;
    private List<ScoredProperty> properties;
    private transient List<ScoredProperty> mergeProperties;
    private RangeDT range;
    private static final int maxRangeDistance = 1;
    public static BigDecimal alphaValue = new BigDecimal(0.95);
    private transient int topCandidatesAmount;




    /*** Initialization ***************************************************************************************************************/
    public TreeNode() {
        this.children = new ArrayList<>();
        this.properties = new TreeList();
        this.mergeProperties = new TreeList();
    }

    public TreeNode(RangeDT range) {
        this();
        this.range = range;
    }

    public void reset() {
        mergeProperties = new TreeList();
        topCandidatesAmount = 0;
    }

    @Override
    public int compareTo(TreeNode treeNode) {
        return Integer.valueOf(getRange().getStart()).compareTo(treeNode.getRange().getStart());
    }

    @Override
    public boolean equals(Object o) {

        if(o == this) {
            return true;
        }
        if(!(o instanceof TreeNode)) {
            return false;
        }
        TreeNode s = (TreeNode) o;
        return s.getRange().equals(range);
    }

    /*** Checks ***************************************************************************************************************/
    private boolean validCandidate(TreeNode treeNode, SequenceInformationDT sequenceInformation) {
        if(sequenceInformation.isTopology()) {
            if(Auxiliary.mod(treeNode.getRange().getStart()-this.getRange().getEnd(),sequenceInformation.getLength())
                    <= maxRangeDistance) {
                return true;
            }
            return false;
        }
        if(0 < (treeNode.getRange().getStart()-this.getRange().getEnd()) &&(treeNode.getRange().getStart()-this.getRange().getEnd())<=maxRangeDistance) {
            return true;
        }
        return false;
    }
    /*** Traversal ***************************************************************************************************************/

    public static int treeHeight(TreeNode root){
        if(root == null) {
            return 0;
        }
        else {
            int lHeight = treeHeight(
                    root.children.size() > 0 ?
                    root.children.get(0) : null);
            int rHeight = treeHeight(
                    root.children.size() > 1 ?
                    root.children.get(1): null);

            if(lHeight > rHeight) {
                return lHeight+1;
            }
            return rHeight+1;
        }
    }

    private static void fetchLevel(TreeNode root, List<TreeNode> newTree, int level) {
        if(root == null){
            return;
        }
        if(level == 1) {
            newTree.add(root);
            return;
        }
        if(root.getChildren().size() == 0) {

            return;
        }
        fetchLevel(root.children.get(0),newTree,level-1);
        fetchLevel(root.children.get(1),newTree,level-1);
    }

    public static List<TreeNode> fetchLevel(TreeNode root, int level) {
        List<TreeNode> treeAtLevel = new ArrayList<>();
        fetchLevel(root,treeAtLevel,level);
        return treeAtLevel;
    }



    /*** Computation ***************************************************************************************************************/

    public  TreeNode merge(TreeNode treeNode) {

//        if(range.getStart() > treeNode.range.getEnd()) {
//            System.out.println("weird range");
//        }
        TreeNode mergeNode = new TreeNode(new RangeDT(range.getStart(),treeNode.range.getEnd(),range.getLength()+treeNode.range.getLength()));
        mergeNode.addChild(treeNode);
        mergeNode.addChild(this);
        Collections.sort(mergeNode.getChildren());
        mergeNode.setProperties(mergeProperties);
        treeNode.setParent(mergeNode);
        setParent(mergeNode);
        return mergeNode;
    }

    public boolean updateNeighbourMerge(TreeNode treeNode, SequenceInformationDT sequenceInformation) {
        if(!validCandidate(treeNode,sequenceInformation)) {
//            LOGGER.debug("Not a validate candidate "+ this.range + " " + treeNode.range);
            reset();
            return false;
        }
        mergeProperties = ScoredProperty.merge(treeNode.getProperties(),properties);
        topCandidatesAmount = contributionCount(mergeProperties);
//        LOGGER.info("topCandidateAmount: " + topCandidatesAmount);
//        if(topCandidatesAmount == 0) {
//            LOGGER.error("No contributing count " + range);
//
//        }
        return true;
    }

    public boolean beats(TreeNode treeNode) {
        if(topCandidatesAmount == 0) {
            return false;
        }
        if(treeNode.topCandidatesAmount == 0) {
            return true;
        }
        if(topCandidatesAmount > treeNode.topCandidatesAmount) {
            return false;
        }
        if(topCandidatesAmount < treeNode.topCandidatesAmount) {
            return true;
        }
        if(treeNode.topCandidateScore().compareTo(topCandidateScore()) == 1) {
            return false;
        }
        if(treeNode.topCandidateScore().compareTo(topCandidateScore()) == -1) {
            return true;
        }
        if(treeNode.topCandidateCount() < topCandidateCount()) {
            return true;
        }
        return false;
    }

    public void normalizeProperties() {
        ScoredProperty.setNormWeights(properties);
    }

    /*** Quality attributes ***************************************************************************************************************/

    public BigDecimal topCandidateScore() {
        return mergeProperties != null ?
                mergeProperties.get(0).getScore():
                BigDecimal.ZERO;
    }

    public long topCandidateCount() {
        return mergeProperties != null ?
                mergeProperties.get(0).getCount():
                0;
    }

    public long topAmount() {
        return contributionCount(properties);
    }

    public BigDecimal topScore() {
        return properties.get(0).getScore();
    }

    public long topCount() {
        return mergeProperties.get(0).getCount();
    }

    public static int contributionCount(List<ScoredProperty> properties) {
        BigDecimal cumCount = BigDecimal.ZERO;
        for(int i = 0; i < properties.size(); i++) {
            cumCount.add(properties.get(i).getNormScore());
            if(cumCount.compareTo(alphaValue) == 1 || cumCount.compareTo(alphaValue) == 0) {
                return i+1;
            }
        }
        return properties.size();
    }


    /*** Getter and setter ***************************************************************************************************************/
    public void addChild(TreeNode child) {
        this.children.add(child);
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }
    public void addProperty(String property, String category, BigDecimal weight, long count) {
        ScoredProperty scoredProperty = new ScoredProperty(property,category,weight,count);
        properties.add(scoredProperty);
    }

    public void addProperty(String property, String category, BigDecimal weight) {
        ScoredProperty scoredProperty = new ScoredProperty(property,category,weight,1);
        int index = properties.indexOf(scoredProperty);
        if(index > -1) {
            ScoredProperty s = properties.get(index);
            s.setScore(s.getScore().add(scoredProperty.getScore()));
            s.setCount(s.getCount() +1);
//                properties.set(index,s);
        }
        else {
            properties.add(new ScoredProperty(scoredProperty));
        }
    }

    public void setProperties(List<ScoredProperty> properties) {
        Collections.sort(properties);
        this.properties = properties;
    }

    public List<ScoredProperty> getProperties() {
        return properties;
    }

    public TreeNode getParent() {
        return parent;
    }

    public void setParent(TreeNode parent) {
        this.parent = parent;
    }

    public RangeDT getRange() {
        return range;
    }

    public void setRange(RangeDT range) {
        this.range = range;
    }

    public int getTopCandidatesAmount() {
        return topCandidatesAmount;
    }

    public void setTopCandidatesAmount(int topCandidatesAmount) {
        this.topCandidatesAmount = topCandidatesAmount;
    }

    public List<ScoredProperty> getMergeProperties() {
        return mergeProperties;
    }

    public void setMergeProperties(List<ScoredProperty> mergeProperties) {
        this.mergeProperties = mergeProperties;
    }
    /*** String routines ***************************************************************************************************************/

    public String qualityCandidateAsString() {
        return "TopcanditateAmount: " + topCandidatesAmount + ", " +
                "TopcanditateScore: " + topCandidateScore() + ", " +
                "topCandidateCount: " + topCandidateCount();
    }

    public String qualityAsString() {
        return "TopAmount: " + topAmount() + ", " +
                "TopScore: " + topScore() + ", " +
                "topCount: " + topCount();
    }

    public String propertiesAsString() {
        return "Properties: " + properties;
    }

    public String propertiesAndRangesAsString() {
        return range + ": " + properties;
    }

    public String toString() {
        return range +
                ", Children: " + ((children != null) ?
                children.stream().map(treeNode -> treeNode.getRange()+"").collect(Collectors.joining(",")) :
                "") + ", " +
                "Parent: " + ((parent != null) ? parent.getRange()+"" : "");
    }

    public String detailsAsString() {
        return range  +"\n" +
                propertiesAsString() + "\n" +
                "Children: " + ((children != null) ?
                children.stream().map(treeNode -> treeNode.getRange()+"").collect(Collectors.joining(",")) :
                "") + "\n" +
                "Parent: " + ((parent != null) ? parent.getRange()+"" : "") + "\n" +
                qualityAsString();
    }



    public static void serialize(File file, List<TreeNode> treeNodes) {
        FileIO.serializeJson(file,treeNodes);
    }

    public static List<TreeNode> deSerialize(File file) {
        return FileIO.deSerializeJsonList(file,TreeNode.class);
    }

}
