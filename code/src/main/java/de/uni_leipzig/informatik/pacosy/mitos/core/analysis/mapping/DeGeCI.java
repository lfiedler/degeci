package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping;

import de.uni_leipzig.informatik.pacosy.mitos.core.psql.KmersTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.ColumnFunctions;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.*;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.FileIO;
import org.apache.commons.lang.time.StopWatch;
import org.apache.spark.sql.*;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.expressions.WindowSpec;
import org.apache.spark.sql.types.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.PairwiseAligner.reverseComplement;
import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.IntegerType;
import static org.apache.spark.sql.types.DataTypes.StringType;


public class DeGeCI {
    protected static final Logger LOGGER = LoggerFactory.getLogger(DeGeCI.class);
    protected static Dataset<Row> geneLengths;
    protected static Dataset<Row> taxonomicGeneLengths;
    protected static Dataset<Row> taxData;
    protected static int maxDTrna = 40;
    protected static int maxD = 100;
    public static int transTable = -1;
    public static int shift = 6;
    public static boolean DEBUG = false;
    public static int MIN_SEQUENCE_LENGTH = 50;
    static public enum AmbigFilter {
        REPLACE_FILTER,
        NO_FILTER,
        SKIP_FILTER,
        CUT_FILTER
    }
    public static Map<Integer,List<String>> initiationCodes = new HashMap<>();
    public static Map<Integer,List<String>> terminationCodes= new HashMap<>();
    static {
        // standard code 1
        initiationCodes.put(1,new ArrayList<>(Arrays.asList("ATG","TTG","CTG")));
        // vertebrate code 2
        initiationCodes.put(2,new ArrayList<>(Arrays.asList("ATG","ATA","ATT","ATC","GTG")));
        // mold code 4
        initiationCodes.put(4,new ArrayList<>(Arrays.asList("ATG","TTA","TTG","CTG","ATT","ATA","ATC","GTG")));
        // invertebrate code 5
        initiationCodes.put(5,new ArrayList<>(Arrays.asList("ATG","ATA","ATT","ATC","GTG","TTG")));
        // Echinoderm code 9
        initiationCodes.put(9,new ArrayList<>(Arrays.asList("ATG","GTG")));
        // Ascidian code 13
        initiationCodes.put(13,new ArrayList<>(Arrays.asList("ATG","ATA","GTG","TTG")));
        // Alternative Flatworm code 14
        initiationCodes.put(14,new ArrayList<>(Arrays.asList("ATG")));

        // standard code 1
        terminationCodes.put(1,new ArrayList<>(Arrays.asList("TTA","TAG")));
        // vertebrate code 2
        terminationCodes.put(2,new ArrayList<>(Arrays.asList("AGA","AGG","TAA","TAG")));
        // mold code 4
        terminationCodes.put(4,new ArrayList<>(Arrays.asList("TAA","TAG")));
        // invertebrate code 5
        terminationCodes.put(5,new ArrayList<>(Arrays.asList("TAA","TAG")));
        // Echinoderm code 9
        terminationCodes.put(9,new ArrayList<>(Arrays.asList("TAA","TAG")));
        // Ascidian code 13
        terminationCodes.put(13,new ArrayList<>(Arrays.asList("TAA","TAG")));
        // Alternative Flatworm code 14
        terminationCodes.put(14,new ArrayList<>(Arrays.asList("TAG")));
    }

    public static Pattern ambigPattern = Pattern.compile("([^ACGT])");
    public static Pattern ambigPatternGlobal = Pattern.compile("(.*[^ACGT].*)");
    private static StructType bedStruct = new StructType(new StructField[]{
            new StructField(ColumnIdentifier.NAME, StringType,true, Metadata.empty()),
//            new StructField(ColumnIdentifier.SCORE,DataTypes.DoubleType,true, Metadata.empty()),
            new StructField(ColumnIdentifier.START, IntegerType,true, Metadata.empty()),
            new StructField(ColumnIdentifier.END, IntegerType,true, Metadata.empty()),
            new StructField(ColumnIdentifier.PROPERTY, StringType,true, Metadata.empty()),
            new StructField("m", IntegerType,true, Metadata.empty()),
            new StructField(ColumnIdentifier.STRAND, StringType,true, Metadata.empty()),
//            new StructField(ColumnIdentifier.IDENTICAL,DataTypes.IntegerType,true, Metadata.empty()),
    });

    /*** auxilary ***/


    public static List<PositionCountIdentifierDT> getMappedKmers(String sequence, int k, boolean topology, boolean strand, AmbigFilter ambigFilter) {
        List<PositionCountIdentifierDT> mappedKmerDTS = new ArrayList<>();
        try {
            if(ambigFilter == AmbigFilter.NO_FILTER) {

                String kmer;

                for(int pos = 0; pos < sequence.length()-k; pos++) {
                    kmer = sequence.substring(pos,pos+k+1);
                    if(!strand) {
                        mappedKmerDTS.add(new PositionCountIdentifierDT(reverseComplement(kmer),pos));
                    }
                    else {
                        mappedKmerDTS.add(new PositionCountIdentifierDT(kmer,pos+k));

                    }
                }

                if(topology) {
                    for(int pos = k; pos > 0; pos--) {
                        kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                        if(!strand) {
                            mappedKmerDTS.add(new PositionCountIdentifierDT(reverseComplement(kmer),sequence.length()-pos));
                        }
                        else {
                            mappedKmerDTS.add(new PositionCountIdentifierDT(kmer,-pos+k));
                        }

                    }
                }

            }

            else if(ambigFilter == AmbigFilter.SKIP_FILTER) {
                Matcher matcher = ambigPattern.matcher(sequence);
                int ambigCounter = 0;
                Map<String,Integer> ambigNucleotides = new HashMap<>();
                while(matcher.find()) {
                    Integer count = ambigNucleotides.get(matcher.group(1));
                    ambigNucleotides.put(matcher.group(1), (count == null) ? 1 : count+1);
                    ambigCounter++;
                }
                if(ambigCounter > 0) {
                    LOGGER.info("Skipping ambiguous sequence ");
                    LOGGER.info(ambigNucleotides.keySet().stream().
                            map(u -> u + ":" + ambigNucleotides.get(u) + "\n").toString());
                    return mappedKmerDTS;
                }
                else {
                    String kmer;

                    for(int pos = 0; pos < sequence.length()-k; pos++) {
                        kmer = sequence.substring(pos,pos+k+1);
                        if(!strand) {
                            mappedKmerDTS.add(new PositionCountIdentifierDT(reverseComplement(kmer),pos));
                        }
                        else {
                            mappedKmerDTS.add(new PositionCountIdentifierDT(kmer,pos+k));

                        }
                    }

                    if(topology) {
                        for(int pos = k; pos > 0; pos--) {
                            kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                            if(!strand) {
                                mappedKmerDTS.add(new PositionCountIdentifierDT(reverseComplement(kmer),sequence.length()-pos));
                            }
                            else {
                                mappedKmerDTS.add(new PositionCountIdentifierDT(kmer,-pos+k));
                            }

                        }
                    }
                }

            } else {

                String kmer;
                Matcher m;
                for(int pos = 0; pos < sequence.length()-k; pos++) {
                    kmer = sequence.substring(pos,pos+k+1);
                    m = ambigPatternGlobal.matcher(kmer);
                    if(!m.matches()) {
                        if(!strand) {
                            mappedKmerDTS.add(new PositionCountIdentifierDT(reverseComplement(kmer),pos));
                        }
                        else {
                            mappedKmerDTS.add(new PositionCountIdentifierDT(kmer,pos+k));

                        }
                    } else {
                        LOGGER.info("Skipping ambiguous kmer " + kmer);
                    }


                }
                System.out.println("end:");
                if(topology) {
                    for(int pos = k; pos > 0; pos--) {
                        kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                        m = ambigPattern.matcher(kmer);
                        if(!m.matches()) {
                            if(!strand) {
                                mappedKmerDTS.add(new PositionCountIdentifierDT(reverseComplement(kmer),sequence.length()-pos));
                            }
                            else {
                                mappedKmerDTS.add(new PositionCountIdentifierDT(kmer,-pos+k));
                            }
                        } else {
                            LOGGER.info("Skipping ambiguous kmer " + kmer);
                        }

                    }
                }
            }



        } catch(Exception e) {
            e.printStackTrace();
        }

        return mappedKmerDTS;
    }

    public static Dataset<Row> getGeneCategories() {
        return SparkComputer.readORC(ProjectDirectoryManager.getGRAPH_DATA_DIR()+"/GENE_CATEGORIES");
    }

    public static Dataset<Row> createPropertyRanges(String pathToBedFiles, Dataset<Row> recordData) {


        return
                Spark.getInstance().read().format("csv")
                        .option("sep","\t").schema(bedStruct)
//                        .text(pathToBedFiles+"/*.bed")
                        .load(pathToBedFiles+"/*.bed")
                        .withColumn(ColumnIdentifier.NAME,regexp_replace(col(ColumnIdentifier.NAME)," ",""))
                        .withColumn(ColumnIdentifier.NAME,regexp_replace(col(ColumnIdentifier.NAME),"\\.[1-9]+$",""))
                        .withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"\\(.*\\)","")).
//                        withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"_.*","")).
        withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"[-_].*","")).
                        withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"trna","")).
//                        withColumn(ColumnIdentifier.CATEGORY,
//                                when(col(ColumnIdentifier.PROPERTY).isInCollection(Arrays.asList("OH","OL")),"repOrigin").
//                                        when(substring(col(ColumnIdentifier.PROPERTY),1,3).equalTo("trn"),"trna").
//                                        when(substring(col(ColumnIdentifier.PROPERTY),1,3).equalTo("rrn"),"rrna").
//                                        otherwise("protein")).
//                groupBy().agg(collect_set(ColumnIdentifier.NAME)).
        join(recordData.select(col(ColumnIdentifier.NAME).as("n"),col(ColumnIdentifier.RECORD_ID),col(ColumnIdentifier.LENGTH).as(ColumnIdentifier.SEQUENCE_LENGTH)),
        col(ColumnIdentifier.NAME).equalTo(col("n"))).drop("n","s",ColumnIdentifier.NAME).
                        withColumn(ColumnIdentifier.END,
                                when(col(ColumnIdentifier.END).notEqual(col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)),col(ColumnIdentifier.END).minus(1)).
                                        otherwise(col(ColumnIdentifier.END))).
                        withColumn(ColumnIdentifier.LENGTH, ColumnFunctions.getLinearRangeLength(ColumnIdentifier.START,ColumnIdentifier.END)).
                        withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"trn","")).
                        withColumn(ColumnIdentifier.GENE,concat_ws("",col(ColumnIdentifier.PROPERTY),col(ColumnIdentifier.STRAND))).
                        withColumn(ColumnIdentifier.STRAND,when(col(ColumnIdentifier.STRAND).equalTo("+"),true).otherwise(false))
                        .join(getGeneCategories().withColumnRenamed(ColumnIdentifier.PROPERTY,"prop"),
                                col(ColumnIdentifier.PROPERTY).equalTo(col("prop")),SparkComputer.JOIN_TYPES.LEFT
                        ).drop("prop","m").na().fill("other");
    }

    public static Dataset<Row> createPropertyRange(String pathToBedFile, Dataset<Row> recordData) {


        return
                Spark.getInstance().read().format("csv")
                        .option("sep","\t").schema(bedStruct)
//                        .text(pathToBedFiles+"/*.bed")
                        .load(pathToBedFile)
                        .withColumn(ColumnIdentifier.NAME,regexp_replace(col(ColumnIdentifier.NAME)," ",""))
                        .withColumn(ColumnIdentifier.NAME,regexp_replace(col(ColumnIdentifier.NAME),"\\.[1-9]+$",""))
                        .withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"\\(.*\\)","")).
//                        withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"_.*","")).
        withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"[-_].*","")).
                        withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"trna","")).
//                        withColumn(ColumnIdentifier.CATEGORY,
//                                when(col(ColumnIdentifier.PROPERTY).isInCollection(Arrays.asList("OH","OL")),"repOrigin").
//                                        when(substring(col(ColumnIdentifier.PROPERTY),1,3).equalTo("trn"),"trna").
//                                        when(substring(col(ColumnIdentifier.PROPERTY),1,3).equalTo("rrn"),"rrna").
//                                        otherwise("protein")).
//                groupBy().agg(collect_set(ColumnIdentifier.NAME)).
        join(recordData.select(col(ColumnIdentifier.NAME).as("n"),col(ColumnIdentifier.RECORD_ID),col(ColumnIdentifier.LENGTH).as(ColumnIdentifier.SEQUENCE_LENGTH)),
        col(ColumnIdentifier.NAME).equalTo(col("n"))).drop("n","s",ColumnIdentifier.NAME).
                        withColumn(ColumnIdentifier.END,
                                when(col(ColumnIdentifier.END).notEqual(col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)),col(ColumnIdentifier.END).minus(1)).
                                        otherwise(col(ColumnIdentifier.END))).
                        withColumn(ColumnIdentifier.LENGTH, ColumnFunctions.getLinearRangeLength(ColumnIdentifier.START,ColumnIdentifier.END)).
                        withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"trn","")).
                        withColumn(ColumnIdentifier.GENE,concat_ws("",col(ColumnIdentifier.PROPERTY),col(ColumnIdentifier.STRAND))).
                        withColumn(ColumnIdentifier.STRAND,when(col(ColumnIdentifier.STRAND).equalTo("+"),true).otherwise(false))
                        .join(getGeneCategories().withColumnRenamed(ColumnIdentifier.PROPERTY,"prop"),
                                col(ColumnIdentifier.PROPERTY).equalTo(col("prop")),SparkComputer.JOIN_TYPES.LEFT
                        ).drop("prop","m").na().fill("other");
    }

    /***  bridging ***/



    public static Dataset<Row> getGeneLengths() {
        if(geneLengths == null) {
            geneLengths = SparkComputer.readORC(ProjectDirectoryManager.getGRAPH_DATA_DIR()+"/GENE_LENGTHS");
        }
        return geneLengths;
    }

    public static Dataset<Row> splitCyclicRanges(Dataset<Row> ranges) {
//        SparkComputer.addRowNumber(ranges)
////                .filter(col(ColumnIdentifier.RECORD_START).gt(col(ColumnIdentifier.RECORD_END)).or((col(ColumnIdentifier.START)).gt(col(ColumnIdentifier.END))))
//                .filter(col(ColumnIdentifier.RECORD_START).gt(col(ColumnIdentifier.RECORD_END)).and((col(ColumnIdentifier.START)).gt(col(ColumnIdentifier.END))))
//                .orderBy(ColumnIdentifier.ROW_NUMBER).show(500);
        return ranges
//                SparkComputer.addRowNumber(ranges)
                .filter(col(ColumnIdentifier.RECORD_START).gt(col(ColumnIdentifier.RECORD_END)).or((col(ColumnIdentifier.START)).gt(col(ColumnIdentifier.END))))
                .withColumn("d",col(ColumnIdentifier.SEQUENCE_LENGTH).minus(col(ColumnIdentifier.START)))
                .withColumn("dRecord",col(ColumnIdentifier.RECORD_LENGTH).minus(col(ColumnIdentifier.RECORD_START)))
                .withColumn("z",
                        // refernce cyclic bounds
                        when(col("d").lt(col(ColumnIdentifier.LENGTH)).and(col("dRecord").geq(col(ColumnIdentifier.LENGTH))),
                                (array(
                                        array(col(ColumnIdentifier.START),col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)),
                                        array(lit(0),col(ColumnIdentifier.END) )
                                ))).

                                otherwise(
                                        // mapping cyclic bounds
                                        when(col("d").geq(col(ColumnIdentifier.LENGTH)).and(col("dRecord").lt(col(ColumnIdentifier.LENGTH))),
                                                (array(
                                                        array(col(ColumnIdentifier.START),col(ColumnIdentifier.START).plus(col("dRecord")).minus(1)),
                                                        array(col(ColumnIdentifier.START).plus(col("dRecord")),col(ColumnIdentifier.END) )
                                                )

                                                )
                                        )
                                                //both beyond cyclic bounds
                                                .otherwise(
                                                        when(col("d").equalTo(col("dRecord")),
                                                                array(array(col(ColumnIdentifier.START), col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)),
                                                                        array(lit(0),col(ColumnIdentifier.END) )
                                                                        )
                                                                ).otherwise(
                                                                when(col("d").lt(col("dRecord")),
                                                                        (array(
                                                                                array(col(ColumnIdentifier.START), col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)),
                                                                                array(lit(0),col("dRecord").minus(col("d")).plus(2)),
                                                                                array(col("dRecord").minus(col("d")).plus(1),col(ColumnIdentifier.END) )
                                                                        ))
                                                                ).otherwise(
                                                                        (array(
                                                                                array(col(ColumnIdentifier.START), col(ColumnIdentifier.START).plus(col("dRecord")).minus(1)),
                                                                                array(col(ColumnIdentifier.START).plus(col("dRecord")),col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)),
                                                                                array(lit(0),col("dRecord").minus(col("d")).plus(1),col(ColumnIdentifier.END) )
                                                                        ))
                                                                )
                                                        )

                                                )
                                )
                )
                .withColumn("z2",
                        // refernce cyclic bounds
                        when(col("d").lt(col(ColumnIdentifier.LENGTH)).and(col("dRecord").geq(col(ColumnIdentifier.LENGTH))),
                                (array(
                                        array(col(ColumnIdentifier.RECORD_START),col(ColumnIdentifier.RECORD_START).plus(col("d")).minus(1)),
                                        array(col(ColumnIdentifier.RECORD_START).plus(col("d")),col(ColumnIdentifier.RECORD_END) )


                                ))).

                                otherwise(
                                        // mapping cyclic bounds
                                        when(col("d").geq(col(ColumnIdentifier.LENGTH)).and(col("dRecord").lt(col(ColumnIdentifier.LENGTH))),
                                                (array(
                                                        array(col(ColumnIdentifier.RECORD_START),col(ColumnIdentifier.RECORD_LENGTH).minus(1)),
                                                        array(lit(0),col(ColumnIdentifier.RECORD_END) )
                                                )

                                                )
                                        )
                                                //both beyond cyclic bounds
                                                .otherwise(
                                                        when(col("d").equalTo(col("dRecord")),
                                                                array(array(col(ColumnIdentifier.RECORD_START), col(ColumnIdentifier.RECORD_LENGTH).minus(1)),
                                                                        array(lit(0),col(ColumnIdentifier.RECORD_END) )
                                                                )
                                                        ).otherwise(
                                                                when(col("d").lt(col("dRecord")),
                                                                        (array(
                                                                                array(col(ColumnIdentifier.RECORD_START), col(ColumnIdentifier.RECORD_START).plus(col("d")).minus(1)),
                                                                                array(col(ColumnIdentifier.RECORD_START).plus(col("d")),col(ColumnIdentifier.RECORD_LENGTH).minus(1)),
                                                                                array(lit(0),col("d").minus(col("dRecord")).plus(1),col(ColumnIdentifier.RECORD_END) )


                                                                        ))
                                                                ).otherwise(
                                                                        (array(
                                                                                array(col(ColumnIdentifier.START), col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)),
                                                                                array(lit(0),col("dRecord").minus(col("d")).plus(2)),
                                                                                array(col("dRecord").minus(col("d")).plus(1),col(ColumnIdentifier.END) )
                                                                        ))
                                                                )
                                                        )

                                                )
                                )
                )
                .select(col("*"),explode_outer(map_from_arrays(col("z"),col("z2"))))
//                withColumn(ColumnIdentifier.START+"new",element_at(col("key"),1))
//                .withColumn(ColumnIdentifier.END+"new",element_at(col("key"),2))
                .withColumn(ColumnIdentifier.START,element_at(col("key"),1))
                .withColumn(ColumnIdentifier.END,element_at(col("key"),2))
//                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getLinearRangeLength(ColumnIdentifier.START,ColumnIdentifier.END))
                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getLinearRangeLength(ColumnIdentifier.START,ColumnIdentifier.END))
                .withColumn(ColumnIdentifier.RECORD_START,element_at(col("value"),1))
                .withColumn(ColumnIdentifier.RECORD_END,element_at(col("value"),2))
                .select(SparkComputer.getColumns(ranges.columns()));
//                .withColumn(ColumnIdentifier.RECORD_START+"new",element_at(col("value"),1))
//                .withColumn(ColumnIdentifier.RECORD_END+"new",element_at(col("value"),2));
//        return ranges
//                .filter(col(ColumnIdentifier.RECORD_START).gt(col(ColumnIdentifier.RECORD_END)).or((col(ColumnIdentifier.START)).gt(col(ColumnIdentifier.END))))
//                .withColumn("d",col(ColumnIdentifier.SEQUENCE_LENGTH).minus(col(ColumnIdentifier.START)))
//                .withColumn("dRecord",col(ColumnIdentifier.RECORD_LENGTH).minus(col(ColumnIdentifier.RECORD_START)))
//                .withColumn("z",
//                        // refernce cyclic bounds
//                        when(col("d").lt(col(ColumnIdentifier.LENGTH)).and(col("dRecord").geq(col(ColumnIdentifier.LENGTH))),
//                                (array(
//                                        array(col(ColumnIdentifier.START),col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)),
//                                        array(lit(0),col(ColumnIdentifier.END) )
//                                ))).
//
//                                otherwise(
//                                        // mapping cyclic bounds
//                                        when(col("d").geq(col(ColumnIdentifier.LENGTH)).and(col("dRecord").lt(col(ColumnIdentifier.LENGTH))),
//                                                (array(
//                                                        array(col(ColumnIdentifier.START),col(ColumnIdentifier.START).plus(col("dRecord")).minus(1)),
//                                                        array(col(ColumnIdentifier.START).plus(col("dRecord")),col(ColumnIdentifier.END) )
//                                                )
//
//                                                )
//                                        )
//                                                //both beyond cyclic bounds
//                                                .otherwise(
//                                                        when(col("d").lt("dRecord"),
//                                                                (array(
//                                                                        array(col(ColumnIdentifier.START), col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)),
//                                                                        array(lit(0),col("dRecord").minus(col("d")).plus(2)),
//                                                                        array(col("dRecord").minus(col("d")).plus(1),col(ColumnIdentifier.END) )
//                                                                ))
//                                                        ).otherwise(
//                                                                (array(
//                                                                        array(col(ColumnIdentifier.START), col(ColumnIdentifier.START).plus(col("dRecord")).minus(1)),
//                                                                        array(col(ColumnIdentifier.START).plus(col("dRecord")),col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)),
//                                                                        array(lit(0),col("dRecord").minus(col("d")).plus(1),col(ColumnIdentifier.END) )
//                                                                ))
//                                                        )
//                                                )
//                                )
//                )
//                .withColumn("z2",
//                        // refernce cyclic bounds
//                        when(col("d").lt(col(ColumnIdentifier.LENGTH)).and(col("dRecord").geq(col(ColumnIdentifier.LENGTH))),
//                                (array(
//                                        array(col(ColumnIdentifier.RECORD_START),col(ColumnIdentifier.RECORD_START).plus(col("d")).minus(1)),
//                                        array(col(ColumnIdentifier.RECORD_START).plus(col("d")),col(ColumnIdentifier.RECORD_END) )
//
//
//                                ))).
//
//                                otherwise(
//                                        // mapping cyclic bounds
//                                        when(col("d").geq(col(ColumnIdentifier.LENGTH)).and(col("dRecord").lt(col(ColumnIdentifier.LENGTH))),
//                                                (array(
//                                                        array(col(ColumnIdentifier.RECORD_START),col(ColumnIdentifier.RECORD_LENGTH).minus(1)),
//                                                        array(lit(0),col(ColumnIdentifier.RECORD_END) )
//                                                )
//
//                                                )
//                                        )
//                                                //both beyond cyclic bounds
//                                                .otherwise(
//                                                        when(col("d").lt("dRecord"),
//                                                                (array(
//                                                                        array(col(ColumnIdentifier.RECORD_START), col(ColumnIdentifier.RECORD_START).plus(col("d")).minus(1)),
//                                                                        array(col(ColumnIdentifier.RECORD_START).plus(col("d")),col(ColumnIdentifier.RECORD_LENGTH).minus(1)),
//                                                                        array(lit(0),col("d").minus(col("dRecord")).plus(1),col(ColumnIdentifier.RECORD_END) )
//
//
//                                                                ))
//                                                        ).otherwise(
//                                                                (array(
//                                                                        array(col(ColumnIdentifier.START), col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)),
//                                                                        array(lit(0),col("dRecord").minus(col("d")).plus(2)),
//                                                                        array(col("dRecord").minus(col("d")).plus(1),col(ColumnIdentifier.END) )
//                                                                ))
//                                                        )
//                                                )
//                                )
//                )
//                .select(col("*"),explode_outer(map_from_arrays(col("z"),col("z2")))).
//                withColumn(ColumnIdentifier.START,element_at(col("key"),1))
//                .withColumn(ColumnIdentifier.END,element_at(col("key"),2))
//                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getLinearRangeLength(ColumnIdentifier.START,ColumnIdentifier.END))
//                .withColumn(ColumnIdentifier.RECORD_START,element_at(col("value"),1))
//                .withColumn(ColumnIdentifier.RECORD_END,element_at(col("value"),2)).select(SparkComputer.getColumns(ranges.columns()));
    }

    public static Dataset<Row> annotateBridgedRangesWithOverlap(Dataset<Row> bridgedRanges,Dataset<Row> propertyRangesWithVoidsPerStrand) {
        WindowSpec wRef = Window.partitionBy(ColumnIdentifier.ROW_NUMBER).orderBy(ColumnIdentifier.POSITION);
        WindowSpec wRec = Window.partitionBy(ColumnIdentifier.ROW_NUMBER).orderBy(ColumnIdentifier.RECORD_POSITION,ColumnIdentifier.POSITION);

        bridgedRanges = SparkComputer.addRowNumber(bridgedRanges)
                .withColumn("aQ",col(ColumnIdentifier.ALIGN_RESULT+".aQ"))
                .withColumn("aT",col(ColumnIdentifier.ALIGN_RESULT+".aT"))
                .withColumn("sQ",col(ColumnIdentifier.ALIGN_RESULT+".sQ"))
                .withColumn("sT",col(ColumnIdentifier.ALIGN_RESULT+".sT"))
                .select(col("*"),posexplode(split(col("sT"),"")).as(new String[]{ColumnIdentifier.INDEX,ColumnIdentifier.NUCLEOTIDE}))
                .filter(not(col(ColumnIdentifier.NUCLEOTIDE).isin("","-")))
                .withColumn(ColumnIdentifier.RECORD_POSITION,
                        when(col(ColumnIdentifier.STRAND),
                                pmod(col(ColumnIdentifier.RECORD_END).plus(expr("element_at("+ col("aQ") + ","+ col(ColumnIdentifier.INDEX).plus(1) + ")")),
                                        col(ColumnIdentifier.RECORD_LENGTH)) )
                                .otherwise(pmod(col(ColumnIdentifier.RECORD_END).minus(expr("element_at("+ col("aQ") + ","+ col(ColumnIdentifier.INDEX).plus(1) + ")")),col(ColumnIdentifier.RECORD_LENGTH)))
                )
                .withColumn(ColumnIdentifier.POSITION,pmod(col(ColumnIdentifier.END).plus(expr("element_at("+ col("aT") + ","+
                        col(ColumnIdentifier.INDEX).plus(1) + ")")),col(ColumnIdentifier.SEQUENCE_LENGTH)));

//        properties = SparkComputer.appendIdentifier(properties,"GM")
//                .withColumn("pGM",explode(sequence(col(ColumnIdentifier.START+"GM"),col(ColumnIdentifier.END+"GM"))))
//                .select(ColumnIdentifier.RECORD_ID+"GM","pGM",ColumnIdentifier.STRAND+"GM",ColumnIdentifier.PROPERTY+"GM",ColumnIdentifier.CATEGORY+"GM",
//                        ColumnIdentifier.SEQUENCE_LENGTH+"GM").persist();
        bridgedRanges = bridgedRanges
                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,col(ColumnIdentifier.POSITION).minus(dense_rank().over(wRef)))
                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2",
                        col(ColumnIdentifier.RECORD_POSITION).minus(dense_rank().over(wRec)))
//                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"3",
//                        when(col(ColumnIdentifier.STRAND),
//                                col(ColumnIdentifier.POSITION).minus(col(ColumnIdentifier.RECORD_POSITION)))
//                                .otherwise(col(ColumnIdentifier.RECORD_POSITION).plus(col(ColumnIdentifier.POSITION))))
                .withColumn(ColumnIdentifier.POSITIONS,struct(col(ColumnIdentifier.POSITION),col(ColumnIdentifier.RECORD_POSITION)))
                .groupBy(ColumnIdentifier.ROW_NUMBER,ColumnIdentifier.EVALUE,ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.RECORD_LENGTH,
                        "d","dref","g",
                        ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2")
                .agg(min(col(ColumnIdentifier.POSITIONS)).as(ColumnIdentifier.START),max(col(ColumnIdentifier.POSITIONS)).as(ColumnIdentifier.END)).orderBy(ColumnIdentifier.START)
                .withColumn(ColumnIdentifier.RECORD_START,col(ColumnIdentifier.START+"."+ColumnIdentifier.RECORD_POSITION))
                .withColumn(ColumnIdentifier.RECORD_END,col(ColumnIdentifier.END+"."+ColumnIdentifier.RECORD_POSITION))
                .withColumn(ColumnIdentifier.START,col(ColumnIdentifier.START+"."+ColumnIdentifier.POSITION))
                .withColumn(ColumnIdentifier.END,col(ColumnIdentifier.END+"."+ColumnIdentifier.POSITION)).drop(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2")
                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getCyclicRangeLength(col(ColumnIdentifier.RECORD_START),col(ColumnIdentifier.RECORD_END),col(ColumnIdentifier.RECORD_LENGTH),col(ColumnIdentifier.STRAND)));

//        bridgedRanges.filter(col(ColumnIdentifier.RECORD_ID).equalTo(24571)).orderBy(ColumnIdentifier.STRAND,ColumnIdentifier.START).show(500);

        return createGenomeMappingFromOverlap(bridgedRanges,propertyRangesWithVoidsPerStrand);

    }

    public static Dataset<Row> createGenomeMappingFromOverlap(Dataset<Row> ranges, Dataset<Row> genomeMapping) {
        ranges = SparkComputer.swapRows(ranges,ColumnIdentifier.RECORD_START,ColumnIdentifier.RECORD_END,not(col(ColumnIdentifier.STRAND)));
        ranges = SparkComputer.swapRows(ranges,ColumnIdentifier.START,ColumnIdentifier.END,not(col(ColumnIdentifier.STRAND)));

        return ColumnFunctions.computeOverlapPositions2(ranges
                .join(SparkComputer.appendIdentifier(genomeMapping,"GM"),
                col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"GM"))
                        .and((pmod(col(ColumnIdentifier.RECORD_START).minus(col(ColumnIdentifier.START+"GM")),col(ColumnIdentifier.RECORD_LENGTH)).plus(1)
                        .leq(col(ColumnIdentifier.LENGTH+"GM")))
                        .or(
                                (pmod(col(ColumnIdentifier.START+"GM").minus(col(ColumnIdentifier.RECORD_START)),col(ColumnIdentifier.RECORD_LENGTH)).plus(1)
                                        .leq(col(ColumnIdentifier.LENGTH)))
                        ))),
                col(ColumnIdentifier.RECORD_START),col(ColumnIdentifier.RECORD_END),
                col(ColumnIdentifier.START+"GM"),col(ColumnIdentifier.END+"GM"),
                col(ColumnIdentifier.LENGTH),col(ColumnIdentifier.LENGTH+"GM"),
                col(ColumnIdentifier.RECORD_LENGTH),ColumnIdentifier.OVERLAP
        )                // if the negative strand of the mapping genome r maps to the reference, the strand annotations of r must be switched
                .withColumn(ColumnIdentifier.STRAND+"GM",when(col(ColumnIdentifier.STRAND),col(ColumnIdentifier.STRAND+"GM")).otherwise(not(col(ColumnIdentifier.STRAND+"GM"))))
                .withColumn("s",element_at(col(ColumnIdentifier.OVERLAP),1))
                .withColumn("e",element_at(col(ColumnIdentifier.OVERLAP),2))
                .withColumn("d1",pmod(col("s").minus(col(ColumnIdentifier.RECORD_START)),col(ColumnIdentifier.RECORD_LENGTH) ))
                .withColumn("d2",pmod(col(ColumnIdentifier.RECORD_END).minus(col("e")),col(ColumnIdentifier.RECORD_LENGTH) ))

                .withColumn("sref",
                        when(col(ColumnIdentifier.STRAND),pmod(col(ColumnIdentifier.START).plus(col("d1")),col(ColumnIdentifier.SEQUENCE_LENGTH))).
                                otherwise(pmod(col(ColumnIdentifier.START).minus(col("d1")),col(ColumnIdentifier.SEQUENCE_LENGTH)))
                )
                .withColumn("eref",
                        when(col(ColumnIdentifier.STRAND),pmod(col(ColumnIdentifier.END).minus(col("d2")),col(ColumnIdentifier.SEQUENCE_LENGTH))
                        ).
                                otherwise(pmod(col(ColumnIdentifier.END).plus(col("d2")),col(ColumnIdentifier.SEQUENCE_LENGTH)))
                )
//                .withColumn("c1",
//                        (col("dref1_2").leq(refLength).
//                        and(col("dref1_1").leq(refLength))))
//                .withColumn("c2",
//                        (col("d1_ref1").leq(length).
//                                and(col("d1_ref2").leq(length))))
                .withColumn("l",ColumnFunctions.getCyclicRangeLength(col("s"),col("e"),col(ColumnIdentifier.RECORD_LENGTH)))
                .withColumn("lref",ColumnFunctions.getCyclicRangeLength(col("sref"),col("eref"),col(ColumnIdentifier.SEQUENCE_LENGTH),col(ColumnIdentifier.STRAND)));
    }

    public static Dataset<Row> createCyclicRanges(Dataset<?> sequenceKmers, int minCount, RangeDT boundaryRange) {
        return   RangeDT.createCyclicRangesFromPositions(
                sequenceKmers.filter(col(ColumnIdentifier.COUNT_PLUS).gt(minCount).or(col(ColumnIdentifier.COUNT_MINUS).gt(minCount))).
                        orderBy(ColumnIdentifier.POSITION),boundaryRange).toDF();
    }

    public static Dataset<Row> createMappingRanges(Dataset<Row> kmerMapping, int k) throws Exception {
        WindowSpec wRef = Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND).orderBy(ColumnIdentifier.POSITION);
        WindowSpec wRec = Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND).orderBy(ColumnIdentifier.POSITION+"2");

        if(kmerMapping.isEmpty()){
            LOGGER.error("No matching kmers found");
            System.exit(6);
//            throw new Exception("No matching kmers found");
        }
//        kmerMapping.filter(col(ColumnIdentifier.RECORD_ID).equalTo(931))
//                .withColumn("r1",dense_rank().over(wRef))
//                .withColumn("r2",dense_rank().over(wRec))
//                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,col(ColumnIdentifier.POSITION).minus(dense_rank().over(wRef)))
////                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2",
////                        when(col(ColumnIdentifier.STRAND),
////                                col(ColumnIdentifier.POSITION+"2").minus(dense_rank().over(wRec))).
////                                otherwise(col(ColumnIdentifier.POSITION+"2").minus(dense_rank().over(wRec))))
//                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2",
//                        col(ColumnIdentifier.POSITION+"2").minus(dense_rank().over(wRec)))
//                .withColumn(ColumnIdentifier.POSITIONS,struct(col(ColumnIdentifier.POSITION),col(ColumnIdentifier.POSITION+"2")))
//                .orderBy(ColumnIdentifier.STRAND,ColumnIdentifier.POSITION,ColumnIdentifier.POSITION+"2").show(5000);
//        kmerMapping.filter(col(ColumnIdentifier.RECORD_ID).equalTo(931))
//                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,col(ColumnIdentifier.POSITION).minus(dense_rank().over(wRef)))
////                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2",
////                        when(col(ColumnIdentifier.STRAND),
////                                col(ColumnIdentifier.POSITION+"2").minus(dense_rank().over(wRec))).
////                                otherwise(col(ColumnIdentifier.POSITION+"2").minus(dense_rank().over(wRec))))
//                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2",
//                        col(ColumnIdentifier.POSITION+"2").minus(dense_rank().over(wRec)))
//                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"3",
//                        when(col(ColumnIdentifier.STRAND),
//                                col(ColumnIdentifier.POSITION).minus(col(ColumnIdentifier.POSITION+"2")))
//                                .otherwise(col(ColumnIdentifier.POSITION+"2").plus(col(ColumnIdentifier.POSITION)) ))
//                .withColumn(ColumnIdentifier.POSITIONS,struct(col(ColumnIdentifier.POSITION),col(ColumnIdentifier.POSITION+"2")))
//                .orderBy(ColumnIdentifier.STRAND,ColumnIdentifier.POSITION,ColumnIdentifier.POSITION+"2").show(5000);
//
//        kmerMapping.filter(col(ColumnIdentifier.RECORD_ID).equalTo(931))
//                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,col(ColumnIdentifier.POSITION).minus(dense_rank().over(wRef)))
////                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2",
////                        when(col(ColumnIdentifier.STRAND),
////                                col(ColumnIdentifier.POSITION+"2").minus(dense_rank().over(wRec))).
////                                otherwise(col(ColumnIdentifier.POSITION+"2").minus(dense_rank().over(wRec))))
//                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2",
//                        col(ColumnIdentifier.POSITION+"2").minus(dense_rank().over(wRec)))
//                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"3",
//                        when(col(ColumnIdentifier.STRAND),
//                                col(ColumnIdentifier.POSITION).minus(col(ColumnIdentifier.POSITION+"2")))
//                                .otherwise(col(ColumnIdentifier.POSITION+"2").plus(col(ColumnIdentifier.POSITION))))
//                .withColumn(ColumnIdentifier.POSITIONS,struct(col(ColumnIdentifier.POSITION),col(ColumnIdentifier.POSITION+"2")))
//                .groupBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.RECORD_LENGTH,
//                        ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2",ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"3")
//                .agg(min(col(ColumnIdentifier.POSITIONS)).as(ColumnIdentifier.START),max(col(ColumnIdentifier.POSITIONS)).as(ColumnIdentifier.END)).orderBy(ColumnIdentifier.START)
////                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getLinearRangeLength(
////                        col(ColumnIdentifier.START+"."+ColumnIdentifier.POSITION),col(ColumnIdentifier.END+"."+ColumnIdentifier.POSITION) ))
//                .withColumn(ColumnIdentifier.RECORD_START,col(ColumnIdentifier.START+"."+ColumnIdentifier.POSITION+"2"))
//                .withColumn(ColumnIdentifier.RECORD_END,col(ColumnIdentifier.END+"."+ColumnIdentifier.POSITION+"2"))
//                .withColumn(ColumnIdentifier.START,col(ColumnIdentifier.START+"."+ColumnIdentifier.POSITION))
//                .withColumn(ColumnIdentifier.END,col(ColumnIdentifier.END+"."+ColumnIdentifier.POSITION))
//                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getCyclicRangeLength(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH)))
//                .withColumn("abc",ColumnFunctions.getCyclicRangeLength(col(ColumnIdentifier.RECORD_START),col(ColumnIdentifier.RECORD_END),col(ColumnIdentifier.RECORD_LENGTH),col(ColumnIdentifier.STRAND)))
//                .orderBy(ColumnIdentifier.STRAND,ColumnIdentifier.START).show(5000);

        return kmerMapping
                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,col(ColumnIdentifier.POSITION).minus(dense_rank().over(wRef)))
//                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2",
//                        when(col(ColumnIdentifier.STRAND),
//                                col(ColumnIdentifier.POSITION+"2").minus(dense_rank().over(wRec))).
//                                otherwise(col(ColumnIdentifier.POSITION+"2").minus(dense_rank().over(wRec))))
                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2",
                                col(ColumnIdentifier.POSITION+"2").minus(dense_rank().over(wRec)))
                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"3",
                        when(col(ColumnIdentifier.STRAND),
                                col(ColumnIdentifier.POSITION).minus(col(ColumnIdentifier.POSITION+"2")))
                                .otherwise(col(ColumnIdentifier.POSITION+"2").plus(col(ColumnIdentifier.POSITION))))
                .withColumn(ColumnIdentifier.POSITIONS,struct(col(ColumnIdentifier.POSITION),col(ColumnIdentifier.POSITION+"2")))

                .groupBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.RECORD_LENGTH,
                        ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2",ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"3")
                .agg(min(col(ColumnIdentifier.POSITIONS)).as(ColumnIdentifier.START),max(col(ColumnIdentifier.POSITIONS)).as(ColumnIdentifier.END)).orderBy(ColumnIdentifier.START)
//                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getLinearRangeLength(
//                        col(ColumnIdentifier.START+"."+ColumnIdentifier.POSITION),col(ColumnIdentifier.END+"."+ColumnIdentifier.POSITION) ))
                .withColumn(ColumnIdentifier.RECORD_START,col(ColumnIdentifier.START+"."+ColumnIdentifier.POSITION+"2"))
                .withColumn(ColumnIdentifier.RECORD_END,col(ColumnIdentifier.END+"."+ColumnIdentifier.POSITION+"2"))
                .withColumn(ColumnIdentifier.START,col(ColumnIdentifier.START+"."+ColumnIdentifier.POSITION))
                .withColumn(ColumnIdentifier.END,col(ColumnIdentifier.END+"."+ColumnIdentifier.POSITION))
                .withColumn(ColumnIdentifier.RECORD_START,
                        when(col(ColumnIdentifier.STRAND),
                                pmod(col(ColumnIdentifier.RECORD_START).minus(k),col(ColumnIdentifier.RECORD_LENGTH))).
                                otherwise(col(ColumnIdentifier.RECORD_START)))
                .withColumn(ColumnIdentifier.RECORD_END,
                        when(not(col(ColumnIdentifier.STRAND)),
                                pmod(col(ColumnIdentifier.RECORD_END).minus(k),col(ColumnIdentifier.RECORD_LENGTH))).
                                otherwise(col(ColumnIdentifier.RECORD_END)))
                .withColumn(ColumnIdentifier.START,
                        when(col(ColumnIdentifier.STRAND),
                                pmod(col(ColumnIdentifier.START).minus(k),col(ColumnIdentifier.SEQUENCE_LENGTH))).
                                otherwise(col(ColumnIdentifier.START)))
                .withColumn(ColumnIdentifier.END,
                        when(not(col(ColumnIdentifier.STRAND)),
                                pmod(col(ColumnIdentifier.END).plus(k),col(ColumnIdentifier.SEQUENCE_LENGTH))).
                                otherwise(col(ColumnIdentifier.END)))
                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getCyclicRangeLength(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH)))
                .drop(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2",ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"3");
//        return kmerMapping
//                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,col(ColumnIdentifier.POSITION).minus(dense_rank().over(wRef)))
//                .withColumn(ColumnIdentifier.RANK,dense_rank().over(wRef))
//                .withColumn(ColumnIdentifier.RANK+"2",
//                        when(col(ColumnIdentifier.STRAND),
//                                dense_rank().over(wRecPlus)).
//                        otherwise(dense_rank().over(wRecMinus)))
//                .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2",
//                        when(col(ColumnIdentifier.STRAND),
//                        col(ColumnIdentifier.POSITION+"2").minus(dense_rank().over(wRecPlus))).
//                        otherwise(col(ColumnIdentifier.POSITION+"2").minus(dense_rank().over(wRecMinus))));
    }

    private static void fillPropertyCluster(Dataset<Row> properties, List<TreeNode> propertyCluster) {

    //        properties.show();
        Iterator<Row> propertyIterator = properties.toLocalIterator();

        while(propertyIterator.hasNext()) {
            Row recordProperty = propertyIterator.next();
            int i = recordProperty.getInt(recordProperty.fieldIndex(ColumnIdentifier.POSITION));
            String property = recordProperty.getString(recordProperty.fieldIndex(ColumnIdentifier.PROPERTY));
            String category = recordProperty.getString(recordProperty.fieldIndex(ColumnIdentifier.CATEGORY));
//            BigDecimal weight = new BigDecimal(recordProperty.getLong(recordProperty.fieldIndex(ColumnIdentifier.WEIGHT)), MathContext.DECIMAL128);
            BigDecimal weight = new BigDecimal(recordProperty.getDouble(recordProperty.fieldIndex(ColumnIdentifier.WEIGHT)), MathContext.DECIMAL128);
            long count = recordProperty.getLong(recordProperty.fieldIndex(ColumnIdentifier.COUNT));
            propertyCluster.get(i).addProperty(property, category, weight,count);
        }
    }

    public static int getClusterSuccessorIndex(int currentIndex,int clusterSize, boolean circular) {
        if(clusterSize == 1) {
            return -1;
        }
        if(currentIndex < clusterSize-1) {
            return currentIndex+1;
        }
        if(circular) {
            return 0;
        }
        return -1;
    }

    public static int getClusterPredecessorIndex(int currentIndex, int clusterSize, boolean circular) {
        if(clusterSize == 1) {
            return -1;
        }
        if(currentIndex > 0) {
            return currentIndex-1;
        }
        if(circular) {
            return clusterSize-1;
        }
        return -1;
    }

    public static TreeNode getClusterSuccessor(TreeNode currentCluster, List<TreeNode> propertyCluster, boolean circular) {
        int index = propertyCluster.indexOf(currentCluster);
        int successorIndex = getClusterSuccessorIndex(index,propertyCluster.size(),circular);
        if(successorIndex == -1) {
            return null;
        }
        return propertyCluster.get(successorIndex);
    }

    public static TreeNode getClusterPredecessor(TreeNode currentCluster, List<TreeNode> propertyCluster, boolean circular) {
        int index = propertyCluster.indexOf(currentCluster);
        int predecessorIndex = getClusterPredecessorIndex(index,propertyCluster.size(),circular);
        if(predecessorIndex == -1) {
            return null;
        }
        return propertyCluster.get(predecessorIndex);
    }

    public static void computeCluster(List<TreeNode> propertyCluster,boolean sequenceStrand, SequenceInformationDT sequenceInformation, String path) {

//        LOGGER.debug("Cluster start "+ propertyCluster.get(0).getRange());
//        LOGGER.debug("Cluster end "+ propertyCluster.get(propertyCluster.size()-1).getRange());
//        int topCandidateIndex = 0;
        // compute pairwise cluster candidates -> set their merged properties
        for(int clusterIndex = 0; clusterIndex < propertyCluster.size(); clusterIndex++) {

//            if(clusterIndex == 15264) {
//                System.out.println(clusterIndex);
//            }
            TreeNode cluster = propertyCluster.get(clusterIndex);
            int neighborIndex = getClusterSuccessorIndex(clusterIndex,propertyCluster.size(),sequenceInformation.isTopology());
            if(neighborIndex == -1) {
                break;
            }
            TreeNode neighbourCluster = propertyCluster.get(neighborIndex);
            cluster.updateNeighbourMerge(neighbourCluster,sequenceInformation);
        }

        int counter = 0;
        TreeNode winner = propertyCluster.get(0);
//        if(!checkConsistentRanges(propertyCluster)) {
//            LOGGER.error("Start");
//        }
        do {
            // find winner amongst cluster candidates
            for(int clusterIndex = 0; clusterIndex < propertyCluster.size(); clusterIndex++) {
                TreeNode cluster = propertyCluster.get(clusterIndex);
                if(cluster.beats(winner)){
                    winner = cluster;
                }
            }
            if(winner.getMergeProperties().size() == 0) {
                break;
            }

            TreeNode successorCluster = getClusterSuccessor(winner,propertyCluster,sequenceInformation.isTopology());
            TreeNode mergedCluster = winner.merge(successorCluster);
//            System.out.println("Merged: " + mergedCluster);
            propertyCluster.set(propertyCluster.indexOf(winner),mergedCluster);
            propertyCluster.remove(successorCluster);
            if(propertyCluster.size() == 1) {
                break;
            }
//            System.out.println("in list "+ propertyCluster.get(topCandidateIndex));
//            System.out.println("in list "+ propertyCluster.get(successorIndex));
            TreeNode predecessorCluster = getClusterPredecessor(mergedCluster,propertyCluster,sequenceInformation.isTopology());
            if(predecessorCluster != null) {
                predecessorCluster.updateNeighbourMerge(mergedCluster,sequenceInformation);
            }
            successorCluster = getClusterSuccessor(mergedCluster,propertyCluster,sequenceInformation.isTopology());
            if(successorCluster != null) {
                mergedCluster.updateNeighbourMerge(successorCluster,sequenceInformation);
            }
            else{
                mergedCluster.reset();
            }
//            if(!checkConsistentRanges(propertyCluster)) {
//                LOGGER.error("At " + counter);
//            }
            counter++;
//            if(propertyCluster.size() <= 14) {
//                printPropertyCluster(propertyCluster);
//                propertyCluster.forEach(treeNode -> System.out.println(treeNode.getRange() + " " + TreeNode.treeHeight(treeNode)));
//                break;
//            }
//            if(propertyCluster.size() <= 1600) {
//                break;
//            }
//            if(topCandidateIndex >= propertyCluster.size()) {
//                System.out.println(topCandidateIndex);
//            }
            winner = mergedCluster;


//            LOGGER.debug("cluster size: " + propertyCluster.size());
////            LOGGER.debug("Winnder index: " + topCandidateIndex);
//            LOGGER.debug("Winner: "+ winner.getRange() );
//            LOGGER.debug(winner.qualityCandidateAsString());
//            printPropertyCluster(propertyCluster);


        } while (true);

//        LOGGER.debug("root amount " + propertyCluster.size());
        Cluster.persistTree(propertyCluster,new File(path+"/"+(sequenceStrand?"plus":"minus")+ "ClusterTree.gryo"));


    }


    public static void createCluster(Dataset<Row> recordGeneMapping, String path, SequenceInformationDT sequenceInformation) {


        long length = recordGeneMapping.select(ColumnIdentifier.SEQUENCE_LENGTH).as(Encoders.LONG()).first();

        new File(path).mkdirs();

//        LOGGER.info("Fetched aggregated properties");
        for(int resultIndex = 0; resultIndex < 2; resultIndex++) {
            List<TreeNode> propertyCluster = new ArrayList<>();
            boolean sequenceStrand = resultIndex == 0 ? true : false;
//            LOGGER.info("SequenceStrand: " + sequenceStrand);
            for(int pos = 0; pos < length; pos++) {
                propertyCluster.add(new TreeNode(new RangeDT(pos,pos)));
            }
            fillPropertyCluster(recordGeneMapping.filter(col(ColumnIdentifier.STRAND).equalTo(sequenceStrand)),propertyCluster);
            int p = 0;
            Iterator<TreeNode> clusterIterator = propertyCluster.iterator();
            while (clusterIterator.hasNext()) {
                TreeNode treeNode = clusterIterator.next();
                if(treeNode.getProperties().size() == 0) {
                    clusterIterator.remove();
//                    LOGGER.info("Removed " + p);
                }
                else {
                    treeNode.normalizeProperties();
//                if(!ScoredProperty.normedCorrectly(treeNode.getProperties())) {
//                    LOGGER.error("Normalization Error " + treeNode);
//                }
                }
                p++;
            }
//            LOGGER.info("Filled propertycluster");
            FileIO.serializeJson(
                    new File(path+"/INIT_CLUSTER_"+sequenceStrand),propertyCluster);
//            LOGGER.info("Serialized propertycluster");
            computeCluster(propertyCluster,sequenceStrand,sequenceInformation, path);
//            LOGGER.info("Persisted propertycluster");
//            System.out.println("Plus annotation");
//            results.get(0).orderBy("start","end","property","recordId").show(150);
//            System.out.println("Minus annotation");
//            results.get(1).orderBy("start","end","property","recordId").show(150);
//            properties.orderBy(ColumnIdentifier.START).show(50);


        }
    }


    public static long getNextIndex(long index, long maxIndex, boolean circular) {
        if(circular) {
            return (index+1 > maxIndex) ? 1 : index+1;
        }
        return (index+1 > maxIndex) ? -1 : index+1;
    }

    public static void bridge(Dataset<Row> mappingRanges, Dataset<Row> sequences, int minCount, SequenceInformationDT sequenceInformation, String sequence, String path) {

        Dataset<Row> refSequence = PairwiseAligner.addReverseComplementSequences(SparkComputer.createDataFrame(Arrays.asList(new RecordSequenceDT(-1,sequence)),RecordSequenceDT.ENCODER).toDF());
        mappingRanges = Spark.checkPointWithRandomAccessor(createConnectedComponentsWithMappedRanges(mappingRanges,minCount),sequenceInformation.getId()).persist();

        long maxIndex = mappingRanges.groupBy().agg(max(col(ColumnIdentifier.ROW_NUMBER)).as(ColumnIdentifier.MAX)).select(ColumnIdentifier.MAX).as(Encoders.LONG()).first();


        int g = 2;
        int minLength = 15;

        int persmissableDistance = 200;
        double gapSizeFactor = 0.5;
        int incosistencyBound = 10;
        double eValueCutOff = 1e-3;



        boolean cyclicJoin = false;

        // only one connected component
        if(maxIndex== 1) {
            // Still to be done -> do not return but try selfjoin if possible
            return;
        }

        if(mappingRanges.filter(col(ColumnIdentifier.ROW_NUMBER).equalTo(1)).select(col(ColumnIdentifier.START+"ref")).as(Encoders.INT()).first() == 0 &
                mappingRanges.filter(col(ColumnIdentifier.ROW_NUMBER).equalTo(maxIndex)).select(col(ColumnIdentifier.END+"ref")).
                        as(Encoders.INT()).first() == sequenceInformation.getLength()-1) {
            cyclicJoin = true;
//            mappingRanges = mappingRanges.withColumn(ColumnIdentifier.ROW_NUMBER,when(col(ColumnIdentifier.ROW_NUMBER).equalTo(maxIndex),lit(1)).otherwise(col(ColumnIdentifier.ROW_NUMBER)));
            mappingRanges = Spark.checkPointWithRandomAccessor(mappingRanges.filter(col(ColumnIdentifier.ROW_NUMBER).equalTo(maxIndex))
                    .join(mappingRanges.filter(col(ColumnIdentifier.ROW_NUMBER).equalTo(1))
                            .select(col(ColumnIdentifier.END+"ref").as("re")).distinct())
                    .withColumn(ColumnIdentifier.END+"ref",col("re"))
                    .withColumn(ColumnIdentifier.LENGTH+"ref",ColumnFunctions.getCyclicRangeLength(ColumnIdentifier.START+"ref",ColumnIdentifier.END+"ref",ColumnIdentifier.SEQUENCE_LENGTH))
                    .withColumn(ColumnIdentifier.ROW_NUMBER,lit(1)).select(SparkComputer.getColumns(mappingRanges.columns())).
                    union(
                            mappingRanges.filter(col(ColumnIdentifier.ROW_NUMBER).equalTo(1))
                                    .join(mappingRanges.filter(col(ColumnIdentifier.ROW_NUMBER).equalTo(maxIndex))
                                            .select(col(ColumnIdentifier.START+"ref").as("rs")).distinct())
                                    .withColumn(ColumnIdentifier.START+"ref",col("rs"))
                                    .withColumn(ColumnIdentifier.LENGTH+"ref",ColumnFunctions.getCyclicRangeLength(ColumnIdentifier.START+"ref",ColumnIdentifier.END+"ref",ColumnIdentifier.SEQUENCE_LENGTH))
                                    .withColumn(ColumnIdentifier.ROW_NUMBER,lit(1)).select(SparkComputer.getColumns(mappingRanges.columns()))
                    ).
                    union(mappingRanges.filter(not(col(ColumnIdentifier.ROW_NUMBER).isin(1,maxIndex)))).distinct()
                    .withColumn(ColumnIdentifier.TOPOLOGY,lit(sequenceInformation.isTopology())),sequenceInformation.getId()).persist();
            maxIndex--;
//            LOGGER.info("Joined periodic CC");
        }
        if(maxIndex== 1) {
            return;
        }


        Dataset<Row> componentIds = createConnectedComponentIds(mappingRanges).persist();

//        componentIds.orderBy(ColumnIdentifier.ROW_NUMBER).show(500);


        for(long startIndex =1; startIndex < maxIndex; startIndex++) {

            long nextIndex = getNextIndex(startIndex,maxIndex,sequenceInformation.isTopology());
            if(nextIndex== -1) {
                break;
            }


            Dataset<Row> accepted = null;


            while (true) {

                int delta = getDelta(componentIds.filter(col(ColumnIdentifier.ROW_NUMBER).equalTo(startIndex)).first(),
                        componentIds.filter(col(ColumnIdentifier.ROW_NUMBER).equalTo(nextIndex)).first(),sequenceInformation);

                if(delta >= persmissableDistance) {
//                    LOGGER.info("Gap too large");
                    break;
                }
                Dataset<Row> candidates = null;
                if(!skipComponents(componentIds.filter(col(ColumnIdentifier.ROW_NUMBER).equalTo(startIndex)).first(),
                        componentIds.filter(col(ColumnIdentifier.ROW_NUMBER).equalTo(nextIndex)).first(),persmissableDistance,delta)) {
//                    LOGGER.info("Searching for candidates");
                    Dataset<Row> left = mappingRanges.filter(col(ColumnIdentifier.ROW_NUMBER).equalTo(startIndex).and(col(ColumnIdentifier.LENGTH).gt(minLength)) )
                            .persist();
                    Dataset<Row> right = mappingRanges.filter(col(ColumnIdentifier.ROW_NUMBER).equalTo(nextIndex).and(col(ColumnIdentifier.LENGTH).gt(minLength)) )
                            .persist();

                    candidates = Spark.checkPointWithRandomAccessor(left.filter(ColumnFunctions.computeSmallestDistanceExcludingBoundaryPositions(col(ColumnIdentifier.END),col(ColumnIdentifier.END+"ref"),
                                    col(ColumnIdentifier.SEQUENCE_LENGTH),col(ColumnIdentifier.TOPOLOGY),lit(true)).lt(persmissableDistance))
                            .join(SparkComputer.appendIdentifier(right.filter(ColumnFunctions.computeSmallestDistanceExcludingBoundaryPositions(col(ColumnIdentifier.START+"ref"),col(ColumnIdentifier.START),
                                                    col(ColumnIdentifier.SEQUENCE_LENGTH),col(ColumnIdentifier.TOPOLOGY),lit(true)).lt(persmissableDistance)),
                                            "r"),
                                    col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"r"))
                                            .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"r")))
                                            .and(ColumnFunctions.computeSmallestDistanceExcludingBoundaryPositions(col(ColumnIdentifier.END),col(ColumnIdentifier.START+"r"),
                                                            col(ColumnIdentifier.SEQUENCE_LENGTH),col(ColumnIdentifier.TOPOLOGY),lit(true)).
                                                    leq(persmissableDistance))
                            )
                            .withColumn("dref",ColumnFunctions.computeSmallestDistanceExcludingBoundaryPositions(
                                    col(ColumnIdentifier.START+"r"),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH),col(ColumnIdentifier.TOPOLOGY),lit(true)))
                            .withColumn("d",
                                    ColumnFunctions.computeSmallestDistanceExcludingBoundaryPositions(col(ColumnIdentifier.RECORD_END),col(ColumnIdentifier.RECORD_START+"r"),
                                            col(ColumnIdentifier.RECORD_LENGTH),col(ColumnIdentifier.TOPOLOGY),col(ColumnIdentifier.STRAND)))
                            .filter((abs(col("dref").minus(col("d"))).divide(col("d"))).leq(0.2))
                            .withColumn("g",greatest(col("d"),col("dref")))
                            .withColumn(ColumnIdentifier.RANK,dense_rank().over(Window.partitionBy(ColumnIdentifier.RECORD_ID).orderBy("g")))
                            .filter(col(ColumnIdentifier.RANK).equalTo(1)).drop(ColumnIdentifier.RANK),sequenceInformation.getId());

//                    accepted = candidates.filter(col("g").leq(incosistencyBound));
//                    candidates = addSubsequencesToRight(refSequence,
//                            addSubsequencesToRight(sequences,
//                                    candidates,
////                                            .filter(col("g").gt(incosistencyBound)),
//                                    col("d"),col(ColumnIdentifier.RECORD_END),col(ColumnIdentifier.RECORD_ID),col(ColumnIdentifier.STRAND),
//                            col(ColumnIdentifier.RECORD_LENGTH)).withColumnRenamed(ColumnIdentifier.SUBSEQUENCE,"s"),
//                            col("dref"),col(ColumnIdentifier.END),lit(-1),lit(true),col(ColumnIdentifier.SEQUENCE_LENGTH))
//                            .withColumnRenamed(ColumnIdentifier.SUBSEQUENCE,"sref")
//                            .withColumn(ColumnIdentifier.ALIGN_RESULT,callUDF(PairwiseAligner.ALIGN_DEGECCI,col("s"),col("sref"),lit(eValueCutOff)))
//                            .filter(not(isnull(col(ColumnIdentifier.ALIGN_RESULT+".sQ"))))
////                            .filter(col(ColumnIdentifier.ALIGN_RESULT+"."+ColumnIdentifier.EVALUE).leq(eValueCutOff))
//                            .withColumn("q",col(ColumnIdentifier.ALIGN_RESULT+".sQ"))
//                            .withColumn("t",col(ColumnIdentifier.ALIGN_RESULT+".sT"))
////                            .withColumn("e",format_string("%e",col(ColumnIdentifier.ALIGN_RESULT+"."+ColumnIdentifier.EVALUE)))
//                            .withColumn("l",length(col("t")))
//                            .withColumn(ColumnIdentifier.RATIO,col("l").divide(col("g")))
//                                    .orderBy("g");

                    candidates =
                            Spark.checkPointWithRandomAccessor(
                            addSubsequencesToRight(refSequence,
                            addSubsequencesToRight(sequences,
                                    candidates,
//                                            .filter(col("g").gt(incosistencyBound)),
                                    col("d"),col(ColumnIdentifier.RECORD_END),col(ColumnIdentifier.RECORD_ID),col(ColumnIdentifier.STRAND),
                                    col(ColumnIdentifier.RECORD_LENGTH)).withColumnRenamed(ColumnIdentifier.SUBSEQUENCE,"s"),
                            col("dref"),col(ColumnIdentifier.END),lit(-1),lit(true),col(ColumnIdentifier.SEQUENCE_LENGTH))
                            .withColumnRenamed(ColumnIdentifier.SUBSEQUENCE,"sref")
                            .withColumn(ColumnIdentifier.ALIGN_RESULT,callUDF(PairwiseAligner.ALIGN_DEGECCI,col("s"),col("sref"),lit(eValueCutOff)))
                            .filter(not(isnull(col(ColumnIdentifier.ALIGN_RESULT+".sQ"))))
                            .select(
                                    ColumnIdentifier.SEQUENCE_LENGTH,
                                    ColumnIdentifier.TOPOLOGY,

                                    ColumnIdentifier.RECORD_ID,
                                    ColumnIdentifier.STRAND,
                                    ColumnIdentifier.RECORD_LENGTH,

                                    ColumnIdentifier.START,
                                    ColumnIdentifier.END,
                                    ColumnIdentifier.RECORD_START,
                                    ColumnIdentifier.RECORD_END,
                                    ColumnIdentifier.START+"r",
                                    ColumnIdentifier.END+"r",
                                    ColumnIdentifier.RECORD_START+"r",
                                    ColumnIdentifier.RECORD_END+"r",
                                    "d","dref","g",
                                    ColumnIdentifier.ALIGN_RESULT),sequenceInformation.getId()).persist();
                    SparkComputer.persistDataFrameORCAppend(
                            candidates,
                            path+"/BRIDGED"
                    );

                    left = left.unpersist();
                    right = right.unpersist();
//                    candidates.show();
//                        .orderBy("d").show(5000);

//                Dataset<Row> d2 = left.filter(pmod(col(ColumnIdentifier.END+"ref").minus(col(ColumnIdentifier.END)).minus(1),col(ColumnIdentifier.SEQUENCE_LENGTH)).lt(persmissableDistance))
//                        .join(SparkComputer.appendIdentifier(right.filter(pmod(col(ColumnIdentifier.START).minus(col(ColumnIdentifier.START+"ref")).minus(1),col(ColumnIdentifier.SEQUENCE_LENGTH)).lt(persmissableDistance)),
//                                        "r"),
//                                col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"r"))
//                                        .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"r")))
//                                        .and(pmod(col(ColumnIdentifier.START+"r").minus(col(ColumnIdentifier.END)).minus(1),col(ColumnIdentifier.SEQUENCE_LENGTH)).leq(persmissableDistance))
//                        )
//                        .withColumn("dref",pmod(col(ColumnIdentifier.START+"r").minus(col(ColumnIdentifier.END)).minus(1),col(ColumnIdentifier.SEQUENCE_LENGTH)))
//                        .withColumn("d",
//                                ColumnFunctions.computeDistanceExcludingBoundaryPositions(col(ColumnIdentifier.RECORD_END),col(ColumnIdentifier.RECORD_START+"r"),
//                                        col(ColumnIdentifier.RECORD_LENGTH),col(ColumnIdentifier.TOPOLOGY),col(ColumnIdentifier.STRAND)))
//                        .filter((abs(col("dref").minus(col("d"))).divide(col("d"))).leq(0.2))
//                        .withColumn("g",greatest(col("d"),col("dref")))
//                        .withColumn(ColumnIdentifier.RANK,dense_rank().over(Window.partitionBy(ColumnIdentifier.RECORD_ID).orderBy("g")))
//                        .filter(col(ColumnIdentifier.RANK).equalTo(1));
////                        .orderBy("d").show(5000);
//                d1.orderBy(ColumnIdentifier.RECORD_ID).show(500);
//                d2.orderBy(ColumnIdentifier.RECORD_ID).show(500);
//                d1.except(d2).orderBy(ColumnIdentifier.RECORD_ID).show();
//                d2.except(d1).orderBy(ColumnIdentifier.RECORD_ID).show();

//                candidates = computeBridgingCandidates(left,right,g,delta);
                }
                if(candidates == null || candidates.isEmpty()){
//                    LOGGER.info("No candidates found");
                    nextIndex = getNextIndex(nextIndex,maxIndex,sequenceInformation.isTopology());}

                else {
//                    LOGGER.info("found candidastes");
//                    candidates.orderBy(ColumnIdentifier.START).show(500);
                    break;

                }
                if(candidates != null) {
                    candidates = candidates.unpersist();
                }

            }






        }
    }

    public static Dataset<Row> createConnectedComponentIds(Dataset<Row> components) {
        return   components.select(ColumnIdentifier.ROW_NUMBER,ColumnIdentifier.START+"ref",ColumnIdentifier.END+"ref",ColumnIdentifier.LENGTH+"ref").distinct();

    }

    public static int getDelta(Row c1, Row c2, SequenceInformationDT sequenceInformation) {
        int p1 = c1.getInt(c1.fieldIndex(ColumnIdentifier.END+"ref"));
        int p2 = c2.getInt(c2.fieldIndex(ColumnIdentifier.START+"ref"));
        int delta;
        if(sequenceInformation.isTopology()){
            delta=Auxiliary.mod(p2-p1,sequenceInformation.getLength());
        }
        else{
            delta=p2-p2;
        }
        LOGGER.debug("delta: "+ delta+" p1: "+ p1 + " p2: "+ p2);
        return delta;
    }

    public static boolean skipComponents(Row c1, Row c2, int searchDistance, int delta) {
        int l1 = c1.getInt(c1.fieldIndex(ColumnIdentifier.LENGTH+"ref"));
        int l2 = c2.getInt(c2.fieldIndex(ColumnIdentifier.LENGTH+"ref"));
        LOGGER.debug("l1: " + l1 + " l2: " + l2);

        if(l1 >= 2*delta & l2 >= 2*delta ) {

            return false;
        }
        LOGGER.debug("Skipping");
        return true;
    }

    public static Dataset<Row> createConnectedComponents(Dataset<Row> mappingRanges,int minCount) {
            return splitReferenceRanges(mappingRanges).withColumn(ColumnIdentifier.POSITION,explode(sequence(col(ColumnIdentifier.START),col(ColumnIdentifier.END))))
                    .groupBy(ColumnIdentifier.POSITION).count().filter(col(ColumnIdentifier.COUNT).geq(minCount)).
                    withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,col(ColumnIdentifier.POSITION).minus(row_number().over(Window.orderBy(ColumnIdentifier.POSITION))))
                    .groupBy(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER).agg(min(col(ColumnIdentifier.POSITION)).as(ColumnIdentifier.START),max(col(ColumnIdentifier.POSITION)).as(ColumnIdentifier.END))
                    .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getLinearRangeLength(ColumnIdentifier.START,ColumnIdentifier.END))
                    .drop(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER)
                    .withColumn(ColumnIdentifier.ROW_NUMBER,row_number().over(Window.orderBy(ColumnIdentifier.START)));
    }

    public static Dataset<Row> createConnectedComponentsWithMappedRanges(Dataset<Row> mappingRanges,int minCount) {
        return SparkComputer.appendIdentifier(createConnectedComponents(mappingRanges,minCount),"ref",
                        ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH).
                join(mappingRanges,
                        pmod(col(ColumnIdentifier.START).minus(col(ColumnIdentifier.START+"ref")),col(ColumnIdentifier.SEQUENCE_LENGTH)).plus(1)
                                .leq(col(ColumnIdentifier.LENGTH+"ref"))
                                        .or(
                                                pmod(col(ColumnIdentifier.START+"ref").minus(col(ColumnIdentifier.START)),col(ColumnIdentifier.SEQUENCE_LENGTH)).plus(1)
                                                        .leq(col(ColumnIdentifier.LENGTH))
                                        )
                        );
    }

    public static Dataset<Row> splitReferenceRanges(Dataset<Row> mappingRanges) {
        Dataset<Row> positions = mappingRanges.select(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.SEQUENCE_LENGTH);

       return positions.filter(col(ColumnIdentifier.START).gt(col(ColumnIdentifier.END)))
               .withColumn("e",explode(array(array(col(ColumnIdentifier.START),col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)),array(lit(0),col(ColumnIdentifier.END)) )))
               .withColumn(ColumnIdentifier.START,element_at(col("e"),1))
               .withColumn(ColumnIdentifier.END,element_at(col("e"),1))
               .select(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.SEQUENCE_LENGTH)
               .union(positions.filter(col(ColumnIdentifier.START).leq(col(ColumnIdentifier.END)))
                       .select(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.SEQUENCE_LENGTH));
    }



    public static Dataset<Row>  createKmerDatasets(int k,String sequence,String refseq, SequenceInformationDT sequenceInformation,int recordId,Dataset<Row> recordData) {


//        LOGGER.info("Persisting psql kmers: ");
        KmersTable kmersTable = KmersTable.getInstance();
        if(refseq!= "") {
            KmersTable.setTableSuffix("_"+refseq);
        }


        kmersTable.setK(k);
//        LOGGER.info("Using k: " + kmersTable.getK() + " Refseq: " + KmersTable.getTableSuffix());
        final String psqlTableIdentifier = "psql"  + "Kmers" +  sequenceInformation.getId().replaceAll("-",""),
                kmersTableIdentifier = "sequence"  +"Kmers" + sequenceInformation.getId().replaceAll("-","");


//        LOGGER.info("Getting sequence kmers");
        // get all kmers of the sequence on the plus strand
        Dataset<PositionCountIdentifierDT> sequenceKmerDataset =
                SparkComputer.createDataFrame(getMappedKmers(sequence,k,
                                sequenceInformation.isTopology(),true, AmbigFilter.NO_FILTER),
                        PositionCountIdentifierDT.ENCODER).cache();


//        LOGGER.info("Getting record kmers");
        // extract all occuring (dropping duplicates) kmers of the sequence
        List<String> sequenceKmersList = sequenceKmerDataset.
                select(ColumnIdentifier.IDENTIFIER).
                distinct().as(Encoders.STRING()).collectAsList();

        // get psql kmers on both strands
        Dataset<KmerDT> k1 = kmersTable.getKmerMatches(true,sequenceKmersList);
//        LOGGER.info("First done");
        Dataset<KmerDT> k2 = kmersTable.getKmerMatches(false,sequenceKmersList);
//        LOGGER.info("Second done");
        if(recordId != -1) {
            LOGGER.info("Removing record");
            k1 = k1.filter(col(ColumnIdentifier.RECORD_ID).notEqual(recordId));
            k2 = k2.filter(col(ColumnIdentifier.RECORD_ID).notEqual(recordId));
        }

        return k1.withColumn(ColumnIdentifier.STRAND,lit(true)).
                union(k2.withColumn(ColumnIdentifier.STRAND,lit(false)))

                .join(sequenceKmerDataset,
                        col(ColumnIdentifier.KMER).equalTo(col(ColumnIdentifier.IDENTIFIER)))
                .withColumn(ColumnIdentifier.SEQUENCE_LENGTH,lit(sequence.length()))
                .withColumn(ColumnIdentifier.POSITION+"2",explode(col(ColumnIdentifier.POSITIONS)))
                .join(recordData.select(col(ColumnIdentifier.RECORD_ID).as("r"),col(ColumnIdentifier.LENGTH).as(ColumnIdentifier.RECORD_LENGTH)),
                        col("r").equalTo(col(ColumnIdentifier.RECORD_ID)))
                .select(ColumnIdentifier.POSITION,ColumnIdentifier.POSITION+"2",ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,
                        ColumnIdentifier.RECORD_LENGTH,ColumnIdentifier.SEQUENCE_LENGTH);


    }

    public static void mitosCompare(List<String> ids, String refseq, int sequenceRefseq, String mitosPath) throws Exception {


        String mitosScript = mitosPath+"/runMitos.sh";

        String dir = ProjectDirectoryManager.getPROJECT_DIR()+"/sequenceData/refseq"+sequenceRefseq;
        File[] fastaFiles = new File(dir+"/fasta").listFiles();
        File[] gbFiles = new File(dir+"/gb").listFiles();
//
        List<RunTimeDT> times = new ArrayList<>();
//        File[] fastaFiles = new File(paths).listFiles(filter);

        Dataset<Row> tax = TaxonomicGroupMappingDT.fetchTaxonomicGroupMapping().toDF();
        for(String id: ids) {
            StopWatch stopwatch = new StopWatch();
            stopwatch.start();
            File fastaFile = null;
            int transTable = -1;
            for(File fsa: fastaFiles){
                if(fsa.getName().startsWith(id)) {
                    fastaFile = fsa;
                }
            }
            for(File gb: gbFiles){
                if(gb.getName().startsWith(id)) {
                    transTable = PairwiseAligner.fetchTranslationTable(gb);
                }
            }

            tax.filter(col(ColumnIdentifier.NAME).equalTo(id)).show();
            System.out.println(fastaFile.getName() + " " +transTable);
            String runCmnd =mitosScript+ " " + fastaFile + " " + refseq +" "+ transTable;


//            System.out.println(runCmnd);

;
            try {

                String cmd[] = {"bash","-c",runCmnd};

                Process process = Runtime.getRuntime().exec(cmd);
                Auxiliary.printErrors(process);
                Auxiliary.printResults(process);
                process.waitFor();
                if(process.exitValue() != 0) {
                    System.out.println(process.exitValue());
                    System.exit(-1);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            RunTimeDT time = new RunTimeDT(stopwatch.getTime(),id);
            LOGGER.info(time+"");
            times.add(time);
            stopwatch.reset();
        }

        SparkComputer.persistDataFrameORCAppend(SparkComputer.createDataFrame(times,RunTimeDT.ENCODER).withColumn("refseq",lit(refseq)),mitosPath+"/TIMES");


    }

    public static void computeSample(String path, List<String> ids, String dbDir) throws Exception {


        File[] fastaFiles = new File(dbDir+"/fasta").listFiles();
        File[] gbFiles = new File(dbDir+"/gb").listFiles();


//        Dataset<Row> tax = TaxonomicGroupMappingDT.fetchTaxonomicGroupMapping().toDF();
        for(String id: ids) {

            File fastaFile = null;
            int transTable = -1;
            for(File fsa: fastaFiles){
                if(fsa.getName().startsWith(id)) {
                    fastaFile = fsa;
                }
            }
            for(File gb: gbFiles){
                if(gb.getName().startsWith(id)) {
                    transTable = PairwiseAligner.fetchTranslationTable(gb);
                }
            }

//            tax.filter(col(ColumnIdentifier.NAME).equalTo(id)).show();
//            System.out.println(fastaFile.getName() + " " +transTable);

            String dir = path+"/"+id;
            if(new File(dir).exists()) {
                continue;
            }
            new File(dir).mkdirs();

            String [] data = {
                "--fasta-file" , fastaFile.getAbsolutePath(),
                "--result-directory", dir ,
                    "--name" , id ,
                    "--trans-table",transTable+""
            };

            new DeGeCIInputParser(data);
        }


    }

    public static void evaluateSample(String path, List<String> ids, Dataset<Row> recordData, Dataset<Row> genomeMapping)  {


        for(String id: ids) {

            String dir = path+"/"+id;

            Dataset<Row> annotations1 = createPropertyRange(dir+"/" + id + ".bed", recordData).withColumn(ColumnIdentifier.TOPOLOGY,lit(true));
//            Dataset<Row> annotations2 = createPropertyRange(dir+"/noTrans.bed", recordData).withColumn(ColumnIdentifier.TOPOLOGY,lit(true));

            evaluateOverlap(genomeMapping,annotations1,0.75,true,RecordDT.mapToRecordId(id),dir);
//            evaluateOverlap(genomeMapping,annotations2,0.75,true,RecordDT.mapToRecordId(id),dir+"/noTransTable");

        }


    }

    public static void gatherSample(String path, List<String> ids) {
        for(String id: ids) {

            String dir = path+"/"+id;

            SparkComputer.persistDataFrameORCAppend(SparkComputer.readORC(dir+"/CORRECTED_PREDICTIONS").withColumn(ColumnIdentifier.RECORD_ID,lit(RecordDT.mapToRecordId(id))),
                    path+"/CORRECTED_PREDICTIONS");

//            SparkComputer.persistDataFrameORCAppend(SparkComputer.readORC(dir+"/GENE_PRECISION"),path+"/GENE_PRECISION");
//            SparkComputer.persistDataFrameORCAppend(SparkComputer.readORC(dir+"/FALSE_POSITIVES"),path+"/FALSE_POSITIVES");
//            SparkComputer.persistDataFrameORCAppend(SparkComputer.readORC(dir+"/FALSE_NEGATIVES"),path+"/FALSE_NEGATIVES");
//
//            SparkComputer.persistDataFrameORCAppend(SparkComputer.readORC(dir+"/noTransTable/GENE_PRECISION"),path+"/GENE_PRECISION_N");
//            SparkComputer.persistDataFrameORCAppend(SparkComputer.readORC(dir+"/noTransTable/FALSE_POSITIVES"),path+"/FALSE_POSITIVES_N");
//            SparkComputer.persistDataFrameORCAppend(SparkComputer.readORC(dir+"/noTransTable/FALSE_NEGATIVES"),path+"/FALSE_NEGATIVES_N");
        }
    }

    public static Dataset<Row> addSubsequencesToRight(Dataset<Row> sequences, Dataset<Row> dataset, Column distance, Column position, Column record,
                                                      Column strand,Column length) {

        String sequenceColumnName = "seq";
        return dataset.
                join(sequences.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),record.equalTo(col("r"))).
                drop("r").
                withColumn(sequenceColumnName,when(col(ColumnIdentifier.STRAND),col(ColumnIdentifier.SEQUENCE))
                        .otherwise(col(ColumnIdentifier.RC_SEQUENCE)))
                .withColumn(sequenceColumnName,concat_ws("",col(sequenceColumnName),col(sequenceColumnName))).
                withColumn(ColumnIdentifier.SUBSEQUENCE,
                        when(strand,
                                ColumnFunctions.substring(col(sequenceColumnName),position.plus(1),distance))
                                .otherwise(ColumnFunctions.substring(col(sequenceColumnName),length.minus(position).plus(1),distance))).
                drop(ColumnIdentifier.SEQUENCE,ColumnIdentifier.RC_SEQUENCE,sequenceColumnName);

    }

    private static Dataset<Row> prepareAnnotatedData(Dataset<Row> annotatedRanges) {
        return
                SparkComputer.swapRows(annotatedRanges,"sref","eref",not(col(ColumnIdentifier.STRAND))).
                withColumnRenamed(ColumnIdentifier.PROPERTY+"GM",ColumnIdentifier.PROPERTY)
                .                withColumnRenamed(ColumnIdentifier.GENE+"GM",ColumnIdentifier.GENE)
                .withColumnRenamed(ColumnIdentifier.CATEGORY+"GM",ColumnIdentifier.CATEGORY)
                .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.STRAND+"GM",
                        ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.RECORD_LENGTH,
                        ColumnIdentifier.TOPOLOGY,
                        ColumnIdentifier.CATEGORY,
                        ColumnIdentifier.PROPERTY,ColumnIdentifier.GENE,"s","e","sref","eref")
                .withColumnRenamed("sref",ColumnIdentifier.START)
                .withColumnRenamed("eref",ColumnIdentifier.END);
    }

    public static Dataset<Row> joinFinalClosePredictions(Dataset<Row> predictions, Dataset<Row> ranges, Dataset<Row> meanGeneLengths, int maxDistance, String id){
        double upperRatio = 1.5;
        double lowerRatio = 0.3;
        WindowSpec neighborCheckWindow =
                Window.partitionBy(
                        ColumnIdentifier.STRAND,
                        ColumnIdentifier.PROPERTY).orderBy(ColumnIdentifier.START);


        predictions = predictions.join(meanGeneLengths.select(col(ColumnIdentifier.PROPERTY).as("prop"),col(ColumnIdentifier.MEAN)),
                col("prop").equalTo(col(ColumnIdentifier.PROPERTY))
                )
                .withColumn("max",col(ColumnIdentifier.MEAN).multiply(upperRatio))
                .withColumn("min",col(ColumnIdentifier.MEAN).multiply(lowerRatio))
//                .withColumn("min",greatest(lit(18),col(ColumnIdentifier.MEAN).multiply(lowerRatio)))
                .drop("prop","str",ColumnIdentifier.MEAN);
//        predictions.filter(col(ColumnIdentifier.PROPERTY).equalTo("rrnS")).show();
//        predictions.orderBy(ColumnIdentifier.START).show(5000);

        predictions = Spark.checkPointWithRandomAccessor(annotateAnticodonType("L",ranges,annotateAnticodonType("S",ranges,predictions)),id);

        predictions =
                predictions

                        .withColumn("l",lead(col(ColumnIdentifier.LENGTH),1).over(neighborCheckWindow))
                        .withColumn(ColumnIdentifier.NEXT+"ref",lead(col(ColumnIdentifier.START),1).over(neighborCheckWindow))
//                        .withColumn(ColumnIdentifier.DISTANCE+"ref",ColumnFunctions.computeSmallestDistanceExcludingBoundaryPositions(col(ColumnIdentifier.END),
//                                col(ColumnIdentifier.NEXT+"ref"),
//                                col(ColumnIdentifier.SEQUENCE_LENGTH),col(ColumnIdentifier.TOPOLOGY),lit(true)))
                        .withColumn(ColumnIdentifier.DISTANCE+"ref",col(ColumnIdentifier.NEXT+"ref").minus(
                                col(ColumnIdentifier.END)))
                        .withColumn("maxD",when(col(ColumnIdentifier.CATEGORY).equalTo("trna"),maxDTrna).otherwise(200) )
                        .withColumn(ColumnIdentifier.MATCH_FLAG,when(isnull(col(ColumnIdentifier.NEXT+"ref")),false).
                                otherwise(col(ColumnIdentifier.DISTANCE+"ref").leq(col("maxD"))
                                        .and( (col("l").plus(col(ColumnIdentifier.LENGTH)).plus(col(ColumnIdentifier.DISTANCE+"ref"))   ).leq(col("max")) ))
                        )
                        .withColumn(ColumnIdentifier.MATCH_FLAG+"2",lag(col(ColumnIdentifier.MATCH_FLAG),1,false).over(neighborCheckWindow));
//
//        predictions.filter(col(ColumnIdentifier.PROPERTY).equalTo("L")).orderBy(ColumnIdentifier.START).show(500);
//        predictions.filter(col(ColumnIdentifier.PROPERTY).equalTo("L1")).orderBy(ColumnIdentifier.START).show(500);
        predictions =  predictions
                .filter(not(col(ColumnIdentifier.MATCH_FLAG)).and(not(col(ColumnIdentifier.MATCH_FLAG+"2"))))
                .select(ColumnIdentifier.STRAND,
                        ColumnIdentifier.SEQUENCE_LENGTH,
                        ColumnIdentifier.TOPOLOGY,
                        ColumnIdentifier.CATEGORY,
                        ColumnIdentifier.PROPERTY,
                        ColumnIdentifier.START,ColumnIdentifier.END,"max","min")
                .union(
                        predictions
                                .filter(col(ColumnIdentifier.MATCH_FLAG).and(not(col(ColumnIdentifier.MATCH_FLAG+"2"))).or(
                                        (not(col(ColumnIdentifier.MATCH_FLAG)).and((col(ColumnIdentifier.MATCH_FLAG+"2"))))))
                                .withColumn("follow",lead(col(ColumnIdentifier.END),1).over(neighborCheckWindow))
                                .filter(col(ColumnIdentifier.MATCH_FLAG))
                                .withColumn(ColumnIdentifier.END,col("follow"))
                                .select(ColumnIdentifier.STRAND,
                                        ColumnIdentifier.SEQUENCE_LENGTH,
                                        ColumnIdentifier.TOPOLOGY,
                                        ColumnIdentifier.CATEGORY,
                                        ColumnIdentifier.PROPERTY,
                                        ColumnIdentifier.START,ColumnIdentifier.END,"max","min")
                )
                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getCyclicRangeLength(col(ColumnIdentifier.START),
                        col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH))).persist();


//
////        System.out.println(predictions.count());

        if(predictions.select(ColumnIdentifier.TOPOLOGY).as(Encoders.BOOLEAN()).first()) {
            Dataset<Row> ends  = predictions.filter(
                    col(ColumnIdentifier.SEQUENCE_LENGTH).minus(col(ColumnIdentifier.END)).leq(maxDistance)
            );
            Dataset<Row> starts  = predictions.filter(
                    col(ColumnIdentifier.START).leq(maxDistance)
            );
            Dataset<Row> cyclicRanges = ends.join(SparkComputer.appendIdentifier(starts,"2"),
                    col(ColumnIdentifier.PROPERTY).equalTo(col(ColumnIdentifier.PROPERTY+"2"))
                            .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
                            .and(ColumnFunctions.computeDistanceExcludingBoundaryPositions(
                                    col(ColumnIdentifier.END),col(ColumnIdentifier.START+"2"),col(ColumnIdentifier.SEQUENCE_LENGTH),
                                    col(ColumnIdentifier.TOPOLOGY),lit(true)).leq(maxDistance))
                            .and((col(ColumnIdentifier.LENGTH+"2").plus(col(ColumnIdentifier.LENGTH))
                                    .plus(ColumnFunctions.computeDistanceExcludingBoundaryPositions(
                                            col(ColumnIdentifier.END),col(ColumnIdentifier.START+"2"),col(ColumnIdentifier.SEQUENCE_LENGTH),
                                            col(ColumnIdentifier.TOPOLOGY),lit(true)))).leq(col("max")))
            );

            predictions = predictions.join(
                    SparkComputer.appendIdentifier(cyclicRanges.select(SparkComputer.getColumns(predictions.columns()))
                            ,"2"),
                    col(ColumnIdentifier.PROPERTY).equalTo(col(ColumnIdentifier.PROPERTY+"2"))
                            .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
                            .and(col(ColumnIdentifier.START).equalTo(col(ColumnIdentifier.START+"2")))
                            .and(col(ColumnIdentifier.END).equalTo(col(ColumnIdentifier.END+"2")))
                    ,
                    SparkComputer.JOIN_TYPES.LEFT_ANTI
            );
            predictions = predictions.join(
                    cyclicRanges.select(ColumnIdentifier.PROPERTY+"2",ColumnIdentifier.STRAND+"2",ColumnIdentifier.START+"2",ColumnIdentifier.END+"2"),
                    col(ColumnIdentifier.PROPERTY).equalTo(col(ColumnIdentifier.PROPERTY+"2"))
                            .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
                            .and(col(ColumnIdentifier.START).equalTo(col(ColumnIdentifier.START+"2")))
                            .and(col(ColumnIdentifier.END).equalTo(col(ColumnIdentifier.END+"2")))
                    ,
                    SparkComputer.JOIN_TYPES.LEFT_ANTI
            );
            predictions = predictions.union(
                    cyclicRanges.withColumn(ColumnIdentifier.END,col(ColumnIdentifier.END+"2"))
                            .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getCyclicRangeLength(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH)))
                            .select(SparkComputer.getColumns(predictions.columns()))
            );
        }


//        System.out.println("cleansed");
//        predictions.orderBy(ColumnIdentifier.START).show(500);
//        cyclicRanges.withColumn(ColumnIdentifier.END,col(ColumnIdentifier.END+"2"))
//                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getCyclicRangeLength(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH)))
//                .select(SparkComputer.getColumns(predictions.columns())).show();

//        System.out.println(predictions.count());
        return predictions.filter(col(ColumnIdentifier.LENGTH).geq(col("min")));

//        return predictions;
    }

    public static Dataset<Row> joinClosePredictions(Dataset<Row> annotatedRanges, Dataset<Row> bridgedAnnotatedRanges, int maxDistance, String id) {
        WindowSpec neighborCheckWindow =
                Window.partitionBy(
                            ColumnIdentifier.RECORD_ID,
                        ColumnIdentifier.STRAND+"GM",
                        ColumnIdentifier.GENE).orderBy(ColumnIdentifier.START);
//        SparkComputer.swapRows(predictions,"sref","eref",not(col(ColumnIdentifier.STRAND))).
//                withColumnRenamed(ColumnIdentifier.PROPERTY+"GM",ColumnIdentifier.PROPERTY)
//                .                withColumnRenamed(ColumnIdentifier.GENE+"GM",ColumnIdentifier.GENE)
//                .withColumnRenamed(ColumnIdentifier.CATEGORY+"GM",ColumnIdentifier.CATEGORY)
//                .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.STRAND+"GM",
//                        ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.RECORD_LENGTH,
//                        ColumnIdentifier.TOPOLOGY,
//                        ColumnIdentifier.CATEGORY,
//                        ColumnIdentifier.PROPERTY,ColumnIdentifier.GENE,"s","e","sref","eref")
//                .withColumnRenamed("sref",ColumnIdentifier.START)
//                .withColumnRenamed("eref",ColumnIdentifier.END)
//                .orderBy(
//                        ColumnIdentifier.RECORD_ID,
//                        ColumnIdentifier.STRAND+"GM",
//                        ColumnIdentifier.GENE,ColumnIdentifier.START).show(5000);

        Dataset<Row> predictions =
                        prepareAnnotatedData(annotatedRanges);

        if(bridgedAnnotatedRanges != null) {
              predictions = predictions
                      .union(
                    prepareAnnotatedData(bridgedAnnotatedRanges));
        }


        predictions =
                predictions
                        // start of the successor
                .withColumn(ColumnIdentifier.NEXT+"ref",lead(col(ColumnIdentifier.START),1).over(neighborCheckWindow))
                .withColumn("maxD",when(col(ColumnIdentifier.CATEGORY).equalTo("trna"),maxDTrna).otherwise(maxD) )
                .withColumn(ColumnIdentifier.DISTANCE+"ref",abs(col(ColumnIdentifier.NEXT+"ref").minus(col(ColumnIdentifier.END))))
//                .withColumn(ColumnIdentifier.DISTANCE+"ref",pmod(col(ColumnIdentifier.NEXT+"ref").minus(col(ColumnIdentifier.END)),
//                        col(ColumnIdentifier.SEQUENCE_LENGTH)))
//                .withColumn(ColumnIdentifier.DISTANCE+"ref",ColumnFunctions.computeSmallestDistanceExcludingBoundaryPositions(col(ColumnIdentifier.END),
//                        col(ColumnIdentifier.NEXT+"ref"),
//                        col(ColumnIdentifier.SEQUENCE_LENGTH),col(ColumnIdentifier.TOPOLOGY),lit(true)))
//                .withColumn(ColumnIdentifier.DISTANCE,ColumnFunctions.computeSmallestDistanceExcludingBoundaryPositions(col("e"),col(ColumnIdentifier.NEXT),
//                        col(ColumnIdentifier.SEQUENCE_LENGTH),col(ColumnIdentifier.TOPOLOGY),lit(true)))
                        // matchflag is is true if successor is less than maxDistance apart
                .withColumn(ColumnIdentifier.MATCH_FLAG,when(isnull(col(ColumnIdentifier.NEXT+"ref")),false).
                        otherwise(col(ColumnIdentifier.DISTANCE+"ref").leq(col("maxD"))))
                .withColumn(ColumnIdentifier.MATCH_FLAG+"2",lag(col(ColumnIdentifier.MATCH_FLAG),1,false).over(neighborCheckWindow));
//
//        predictions.filter(col(ColumnIdentifier.RECORD_ID).isin(24532,24428))
//
//                .filter(col(ColumnIdentifier.STRAND+"GM")).filter(col(ColumnIdentifier.PROPERTY).equalTo("M"))
//                .orderBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.START).show(50000);

        predictions =  predictions
                .filter(not(col(ColumnIdentifier.MATCH_FLAG)).and(not(col(ColumnIdentifier.MATCH_FLAG+"2"))))
                .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.STRAND+"GM",
                        ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.RECORD_LENGTH,
                        ColumnIdentifier.TOPOLOGY,
                        ColumnIdentifier.CATEGORY,
                        ColumnIdentifier.PROPERTY,ColumnIdentifier.GENE,
                        ColumnIdentifier.START,ColumnIdentifier.END)

                .union(
                        predictions
                                .filter(col(ColumnIdentifier.MATCH_FLAG).and(not(col(ColumnIdentifier.MATCH_FLAG+"2"))).or(
                                        (not(col(ColumnIdentifier.MATCH_FLAG)).and((col(ColumnIdentifier.MATCH_FLAG+"2"))))))
                                .withColumn("follow",lead(col(ColumnIdentifier.END),1).over(neighborCheckWindow))
                                .filter(col(ColumnIdentifier.MATCH_FLAG))
                                .withColumn("lFollow",ColumnFunctions.getCyclicRangeLength(col(ColumnIdentifier.START),
                                        col("follow"),col(ColumnIdentifier.SEQUENCE_LENGTH)))
                                .withColumn(ColumnIdentifier.END,
                                        when(col("lfollow").gt(ColumnFunctions.getCyclicRangeLength(col(ColumnIdentifier.START),
                                                col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH))), col("follow")).
                                                otherwise(col(ColumnIdentifier.END)))

                                .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.STRAND+"GM",
                                        ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.RECORD_LENGTH,
                                        ColumnIdentifier.TOPOLOGY,
                                        ColumnIdentifier.CATEGORY,
                                        ColumnIdentifier.PROPERTY,ColumnIdentifier.GENE,
                                        ColumnIdentifier.START,ColumnIdentifier.END)
                )
                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getCyclicRangeLength(col(ColumnIdentifier.START),
                        col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH))).persist();
//        return predictions;
//        Dataset<Row> ends  = predictions.filter(
//                col(ColumnIdentifier.SEQUENCE_LENGTH).minus(col(ColumnIdentifier.END)).leq(maxDistance)
//        );
//        Dataset<Row> starts  = predictions.filter(
//                col(ColumnIdentifier.START).leq(maxDistance)
//        );


//        System.out.println(predictions.count());
//
//        Dataset<Row> cyclicRanges = ends.join(SparkComputer.appendIdentifier(starts,"2"),
//                col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2"))
//                        .and(col(ColumnIdentifier.GENE).equalTo(col(ColumnIdentifier.GENE+"2")))
//                        .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
//                        .and(ColumnFunctions.computeDistanceExcludingBoundaryPositions(
//                                col(ColumnIdentifier.END),col(ColumnIdentifier.START+"2"),col(ColumnIdentifier.SEQUENCE_LENGTH),
//                                col(ColumnIdentifier.TOPOLOGY),lit(true)).leq(maxDistance)));
//
//
//        cyclicRanges.show();
//        predictions.join(
//                SparkComputer.appendIdentifier(cyclicRanges.select(SparkComputer.getColumns(predictions.columns()))
//                        ,"2"),
//                col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2"))
//                        .and(col(ColumnIdentifier.GENE).equalTo(col(ColumnIdentifier.GENE+"2")))
//                        .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
//                        .and(col(ColumnIdentifier.START).equalTo(col(ColumnIdentifier.START+"2")))
//                        .and(col(ColumnIdentifier.END).equalTo(col(ColumnIdentifier.END+"2")))
//        ).show();
//        Dataset<Row> retain = predictions.join(
//                SparkComputer.appendIdentifier(cyclicRanges.select(SparkComputer.getColumns(predictions.columns()))
//                        ,"2"),
//                col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2"))
//                        .and(col(ColumnIdentifier.GENE).equalTo(col(ColumnIdentifier.GENE+"2")))
//                        .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
//                        .and(col(ColumnIdentifier.START).equalTo(col(ColumnIdentifier.START+"2")))
//                        .and(col(ColumnIdentifier.END).equalTo(col(ColumnIdentifier.END+"2")))
//                ,
//                SparkComputer.JOIN_TYPES.LEFT_ANTI
//        );
//        System.out.println(retain.count());
//        return predictions;

//        predictions
//                .withColumn(ColumnIdentifier.STRAND,col(ColumnIdentifier.STRAND+"GM"))
//                .filter(col(ColumnIdentifier.STRAND)).filter(col(ColumnIdentifier.PROPERTY).equalTo("M")).orderBy(ColumnIdentifier.START).show(50000);

        if(predictions.select(ColumnIdentifier.TOPOLOGY).as(Encoders.BOOLEAN()).first()) {
            Dataset<Row> ends  = predictions.filter(
                    col(ColumnIdentifier.SEQUENCE_LENGTH).minus(col(ColumnIdentifier.END)).leq(maxDistance)
            );
            Dataset<Row> starts  = predictions.filter(
                    col(ColumnIdentifier.START).leq(maxDistance)
            );
//
////        System.out.println(predictions.count());

            Dataset<Row> cyclicRanges = ends.join(SparkComputer.appendIdentifier(starts,"2"),
                    col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2"))
                            .and(col(ColumnIdentifier.GENE).equalTo(col(ColumnIdentifier.GENE+"2")))
                            .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
                            .and(ColumnFunctions.computeDistanceExcludingBoundaryPositions(
                                    col(ColumnIdentifier.END),col(ColumnIdentifier.START+"2"),col(ColumnIdentifier.SEQUENCE_LENGTH),
                                    col(ColumnIdentifier.TOPOLOGY),lit(true)).leq(maxDistance))).persist();



            predictions = predictions.join(
                    SparkComputer.appendIdentifier(cyclicRanges.select(SparkComputer.getColumns(predictions.columns()))
                            ,"2"),
                    col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2"))
                            .and(col(ColumnIdentifier.GENE).equalTo(col(ColumnIdentifier.GENE+"2")))
                            .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
                            .and(col(ColumnIdentifier.START).equalTo(col(ColumnIdentifier.START+"2")))
                            .and(col(ColumnIdentifier.END).equalTo(col(ColumnIdentifier.END+"2")))
                    ,
                    SparkComputer.JOIN_TYPES.LEFT_ANTI
            );
            predictions = predictions.join(
                    cyclicRanges.select(ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.GENE+"2",ColumnIdentifier.STRAND+"2",ColumnIdentifier.START+"2",ColumnIdentifier.END+"2"),
                    col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2"))
                            .and(col(ColumnIdentifier.GENE).equalTo(col(ColumnIdentifier.GENE+"2")))
                            .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
                            .and(col(ColumnIdentifier.START).equalTo(col(ColumnIdentifier.START+"2")))
                            .and(col(ColumnIdentifier.END).equalTo(col(ColumnIdentifier.END+"2")))
                    ,
                    SparkComputer.JOIN_TYPES.LEFT_ANTI
            );
            predictions = predictions.union(
                    cyclicRanges.withColumn(ColumnIdentifier.END,col(ColumnIdentifier.END+"2"))
                            .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getCyclicRangeLength(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH)))
                            .select(SparkComputer.getColumns(predictions.columns()))
            );
        }

//        cyclicRanges.withColumn(ColumnIdentifier.END,col(ColumnIdentifier.END+"2"))
//                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getCyclicRangeLength(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH)))
//                .select(SparkComputer.getColumns(predictions.columns())).show();

//        System.out.println(predictions.count());
        return Spark.checkPointWithRandomAccessor(predictions,id).persist();

//        return predictions;
    }

    public static Dataset<Row> annotateAnticodonType(String trnaType,Dataset<Row> predictions, Dataset<Row> annotations) {
        if(!trnaType.equals("S") & !trnaType.equals("L")) {
            LOGGER.error("INVALID trna must be S or L");
            return null;
        }
        Dataset<Row> specifiedTypes =
                annotations.
                        filter(col(ColumnIdentifier.PROPERTY).isin(trnaType))
                        .join(SparkComputer.appendIdentifier(predictions.filter(col(ColumnIdentifier.PROPERTY).isin(trnaType+"1",trnaType+"2"))
                                        .drop(ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.CATEGORY),"2"),
                                col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2"))
                                        .and(pmod(col(ColumnIdentifier.END).minus(col(ColumnIdentifier.POSITION+"2")),
                                                col(ColumnIdentifier.SEQUENCE_LENGTH)).plus(1).leq(col(ColumnIdentifier.LENGTH)))
                        ).withColumnRenamed(ColumnIdentifier.POSITION+"2",ColumnIdentifier.POSITION)
                        .withColumn(ColumnIdentifier.RANK,dense_rank().over(Window.partitionBy(ColumnIdentifier.POSITION).orderBy(desc((ColumnIdentifier.WEIGHT+"2")))))
                        .filter(col(ColumnIdentifier.RANK).equalTo(1)).drop(ColumnIdentifier.RANK)
                        .withColumn(ColumnIdentifier.PROPERTY,col(ColumnIdentifier.PROPERTY+"2"))
                        .withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,col(ColumnIdentifier.POSITION).minus(row_number().over(Window.partitionBy(ColumnIdentifier.STRAND,ColumnIdentifier.PROPERTY).orderBy(ColumnIdentifier.POSITION))))
                        .groupBy(ColumnIdentifier.STRAND,ColumnIdentifier.PROPERTY,ColumnIdentifier.CATEGORY,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.TOPOLOGY,"min","max",
                                ColumnIdentifier.DIFF_COLUMN_IDENTIFIER)
                        .agg(min(col(ColumnIdentifier.POSITION)).as(ColumnIdentifier.START),max(col(ColumnIdentifier.POSITION)).as(ColumnIdentifier.END))
                        .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getLinearRangeLength(col(ColumnIdentifier.START),col(ColumnIdentifier.END)))
                        .drop(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER).persist();
//        specifiedTypes.show();
        if(annotations.select(ColumnIdentifier.TOPOLOGY).as(Encoders.BOOLEAN()).first()) {
            Dataset<Row> ends  = specifiedTypes.filter(
                    col(ColumnIdentifier.SEQUENCE_LENGTH).equalTo(col(ColumnIdentifier.END).plus(1))
            );
            Dataset<Row> starts  = specifiedTypes.filter(
                    col(ColumnIdentifier.START).equalTo(0)
            );


            Dataset<Row> cyclicRanges = ends.join(SparkComputer.appendIdentifier(starts,"2"),
                    col(ColumnIdentifier.PROPERTY).equalTo(col(ColumnIdentifier.PROPERTY+"2"))
                            .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
            ).persist();


            specifiedTypes = specifiedTypes.join(
                    SparkComputer.appendIdentifier(cyclicRanges.select(SparkComputer.getColumns(specifiedTypes.columns()))
                            ,"2"),
                    col(ColumnIdentifier.PROPERTY).equalTo(col(ColumnIdentifier.PROPERTY+"2"))
                            .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
                            .and(col(ColumnIdentifier.START).equalTo(col(ColumnIdentifier.START+"2")))
                            .and(col(ColumnIdentifier.END).equalTo(col(ColumnIdentifier.END+"2")))
                    ,
                    SparkComputer.JOIN_TYPES.LEFT_ANTI
            );
            specifiedTypes = specifiedTypes.join(
                    cyclicRanges.select(ColumnIdentifier.PROPERTY+"2",ColumnIdentifier.STRAND+"2",ColumnIdentifier.START+"2",ColumnIdentifier.END+"2"),
                    col(ColumnIdentifier.PROPERTY).equalTo(col(ColumnIdentifier.PROPERTY+"2"))
                            .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
                            .and(col(ColumnIdentifier.START).equalTo(col(ColumnIdentifier.START+"2")))
                            .and(col(ColumnIdentifier.END).equalTo(col(ColumnIdentifier.END+"2")))
                    ,
                    SparkComputer.JOIN_TYPES.LEFT_ANTI
            );
            specifiedTypes = specifiedTypes.
                    union(
                    cyclicRanges.withColumn(ColumnIdentifier.END,col(ColumnIdentifier.END+"2"))
                            .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getCyclicRangeLength(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH)))
                            .select(SparkComputer.getColumns(specifiedTypes.columns()))
            );
        }
//        specifiedTypes.show();

        return annotations.filter(col(ColumnIdentifier.PROPERTY).notEqual(trnaType))
                .select(SparkComputer.getColumns(specifiedTypes.columns()))
                .union(specifiedTypes.filter(col("min").leq(col(ColumnIdentifier.LENGTH))));

    }

    public static Dataset<Row> createGeneDistribution(Dataset<Row> predictions, int minRangeLength, int minCount, String id){
//        predictions
//                .filter(col(ColumnIdentifier.LENGTH).gt(minRangeLength))
//                .join(getTaxData().withColumnRenamed(ColumnIdentifier.RECORD_ID,"r").select(ColumnIdentifier.TAXONOMIC_GROUP_NAME,"r"),
//                        col("r").equalTo(col(ColumnIdentifier.RECORD_ID)),SparkComputer.JOIN_TYPES.LEFT)
//                .na().fill("other")
//                .drop("r")
//
//                .join(getGeneLengths().select(col(ColumnIdentifier.PROPERTY).as("prop"),col(ColumnIdentifier.MEAN),col(ColumnIdentifier.STRAND).as("str"),col(ColumnIdentifier.TAXONOMIC_GROUP_NAME).as("tax")),
//                        col(ColumnIdentifier.TAXONOMIC_GROUP_NAME).equalTo(col("tax"))
//                                .and(col("prop").equalTo(col(ColumnIdentifier.PROPERTY)))
//                                .and(col(ColumnIdentifier.STRAND).equalTo(col("str")))
//                ).drop("r","prop","tax",ColumnIdentifier.TAXONOMIC_GROUP_NAME)
//                .withColumn(ColumnIdentifier.STRAND,col(ColumnIdentifier.STRAND+"GM"))
//                .drop(ColumnIdentifier.STRAND+"GM")
////                . withColumn(ColumnIdentifier.POSITION,explode(sequence(col(ColumnIdentifier.START),col(ColumnIdentifier.END))))
//                . withColumn(ColumnIdentifier.POSITION,explode(
//                        ColumnFunctions.cyclicIntermediatePositions(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH),null))
//                ).orderBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.PROPERTY).show(5000);
//        predictions
//                .filter(col(ColumnIdentifier.LENGTH).gt(minRangeLength))
//                .join(getTaxData().withColumnRenamed(ColumnIdentifier.RECORD_ID,"r").select(ColumnIdentifier.TAXONOMIC_GROUP_NAME,"r"),
//                        col("r").equalTo(col(ColumnIdentifier.RECORD_ID)),SparkComputer.JOIN_TYPES.LEFT)
//                .na().fill("other")
//                .drop("r")
//
//                .join(getGeneLengths().select(col(ColumnIdentifier.PROPERTY).as("prop"),col(ColumnIdentifier.MEAN),col(ColumnIdentifier.STRAND).as("str"),col(ColumnIdentifier.TAXONOMIC_GROUP_NAME).as("tax")),
//                        col(ColumnIdentifier.TAXONOMIC_GROUP_NAME).equalTo(col("tax"))
//                                .and(col("prop").equalTo(col(ColumnIdentifier.PROPERTY)))
//                                .and(col(ColumnIdentifier.STRAND).equalTo(col("str")))
//                ).drop("r","prop","tax",ColumnIdentifier.TAXONOMIC_GROUP_NAME)
//                .withColumn(ColumnIdentifier.STRAND,col(ColumnIdentifier.STRAND+"GM"))
//                .drop(ColumnIdentifier.STRAND+"GM")
////                . withColumn(ColumnIdentifier.POSITION,explode(sequence(col(ColumnIdentifier.START),col(ColumnIdentifier.END))))
//                . withColumn(ColumnIdentifier.POSITION,explode(
//                        ColumnFunctions.cyclicIntermediatePositions(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH),null))
//                )
//
//                .groupBy(ColumnIdentifier.POSITION,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.STRAND,ColumnIdentifier.PROPERTY,ColumnIdentifier.CATEGORY,ColumnIdentifier.MEAN)
//                .agg(sum(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.WEIGHT),count("*").as(ColumnIdentifier.COUNT) )
//                .withColumn(ColumnIdentifier.WEIGHT,col(ColumnIdentifier.WEIGHT).divide(col(ColumnIdentifier.MEAN)))
//                .filter(col(ColumnIdentifier.COUNT).geq(minCount)).orderBy(ColumnIdentifier.STRAND,ColumnIdentifier.POSITION).show(5000);
//        SparkComputer.checkDuplicates(        predictions
//                .filter(col(ColumnIdentifier.LENGTH).gt(minRangeLength))
//                .withColumn(ColumnIdentifier.STRAND,col(ColumnIdentifier.STRAND+"GM"))
//                .drop(ColumnIdentifier.STRAND+"GM")
////                . withColumn(ColumnIdentifier.POSITION,explode(sequence(col(ColumnIdentifier.START),col(ColumnIdentifier.END))))
//                . withColumn(ColumnIdentifier.POSITION,explode(
//                        ColumnFunctions.cyclicIntermediatePositions(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH),null))
//                )
//
//                .groupBy(ColumnIdentifier.POSITION,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.STRAND,ColumnIdentifier.PROPERTY,ColumnIdentifier.CATEGORY)
//                .agg(sum(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.WEIGHT),count("*").as(ColumnIdentifier.COUNT) ));
//        SparkComputer.checkDuplicates(        predictions
//                .filter(col(ColumnIdentifier.LENGTH).gt(minRangeLength))
//                .withColumn(ColumnIdentifier.STRAND,col(ColumnIdentifier.STRAND+"GM"))
//                .drop(ColumnIdentifier.STRAND+"GM")
////                . withColumn(ColumnIdentifier.POSITION,explode(sequence(col(ColumnIdentifier.START),col(ColumnIdentifier.END))))
//                . withColumn(ColumnIdentifier.POSITION,explode(
//                        ColumnFunctions.cyclicIntermediatePositions(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH),null))
//                )
//
//                .groupBy(ColumnIdentifier.POSITION,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.STRAND,ColumnIdentifier.PROPERTY,ColumnIdentifier.CATEGORY)
//                .agg(sum(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.WEIGHT),count("*").as(ColumnIdentifier.COUNT) )
//                .join(getGeneLengths().filter(col(ColumnIdentifier.TAXONOMIC_GROUP_NAME).equalTo(taxonomicGroup))
//                                .select(col(ColumnIdentifier.PROPERTY).as("prop"),col(ColumnIdentifier.MEAN),col(ColumnIdentifier.STRAND).as("str")),
//                        col("prop").equalTo(col(ColumnIdentifier.PROPERTY))
//                                .and(col(ColumnIdentifier.STRAND).equalTo(col("str")))
//                ));
//        SparkComputer.checkDuplicates(        predictions
//                .filter(col(ColumnIdentifier.LENGTH).gt(minRangeLength))
//                .withColumn(ColumnIdentifier.STRAND,col(ColumnIdentifier.STRAND+"GM"))
//                .drop(ColumnIdentifier.STRAND+"GM")
////                . withColumn(ColumnIdentifier.POSITION,explode(sequence(col(ColumnIdentifier.START),col(ColumnIdentifier.END))))
//                . withColumn(ColumnIdentifier.POSITION,explode(
//                        ColumnFunctions.cyclicIntermediatePositions(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH),null))
//                )
//
//                .groupBy(ColumnIdentifier.POSITION,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.STRAND,ColumnIdentifier.PROPERTY,ColumnIdentifier.CATEGORY)
//                .agg(sum(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.WEIGHT),count("*").as(ColumnIdentifier.COUNT) )
//                .join(getGeneLengths().filter(col(ColumnIdentifier.TAXONOMIC_GROUP_NAME).equalTo(taxonomicGroup))
//                                .select(col(ColumnIdentifier.PROPERTY).as("prop"),col(ColumnIdentifier.MEAN),col(ColumnIdentifier.STRAND).as("str")),
//                        col("prop").equalTo(col(ColumnIdentifier.PROPERTY))
//                                .and(col(ColumnIdentifier.STRAND).equalTo(col("str")))
//                ).drop("r","prop")
//                .withColumn(ColumnIdentifier.WEIGHT,col(ColumnIdentifier.WEIGHT).divide(col(ColumnIdentifier.MEAN)))
//                .filter(col(ColumnIdentifier.COUNT).geq(minCount)));

//        predictions
//                .filter(col(ColumnIdentifier.LENGTH).gt(minRangeLength))
//                .withColumn(ColumnIdentifier.STRAND,col(ColumnIdentifier.STRAND+"GM"))
//                .drop(ColumnIdentifier.STRAND+"GM")
////                . withColumn(ColumnIdentifier.POSITION,explode(sequence(col(ColumnIdentifier.START),col(ColumnIdentifier.END))))
//                . withColumn(ColumnIdentifier.POSITION,explode(
//                        ColumnFunctions.cyclicIntermediatePositions(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH),null))
//                )
//
//                .groupBy(ColumnIdentifier.POSITION,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.STRAND,ColumnIdentifier.PROPERTY,ColumnIdentifier.CATEGORY)
//                .agg(sum(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.WEIGHT),count("*").as(ColumnIdentifier.COUNT) )
//                .join(getGeneLengths().filter(col(ColumnIdentifier.TAXONOMIC_GROUP_NAME).equalTo(taxonomicGroup))
//                                .select(col(ColumnIdentifier.PROPERTY).as("prop"),col(ColumnIdentifier.MEAN),col(ColumnIdentifier.STRAND).as("str")),
//                        col("prop").equalTo(col(ColumnIdentifier.PROPERTY))
//                                .and(col(ColumnIdentifier.STRAND).equalTo(col("str")))
//                ).drop("r","prop")
//                .withColumn(ColumnIdentifier.WEIGHT,col(ColumnIdentifier.WEIGHT).divide(col(ColumnIdentifier.MEAN)))
//                .filter(col(ColumnIdentifier.COUNT).geq(minCount))
//                .orderBy(ColumnIdentifier.STRAND,ColumnIdentifier.POSITION).show(5000);
//        SparkComputer.checkDuplicates(        predictions
//                        .filter(col(ColumnIdentifier.LENGTH).gt(minRangeLength))
//                        .withColumn(ColumnIdentifier.STRAND,col(ColumnIdentifier.STRAND+"GM"))
//                        .drop(ColumnIdentifier.STRAND+"GM")
////                . withColumn(ColumnIdentifier.POSITION,explode(sequence(col(ColumnIdentifier.START),col(ColumnIdentifier.END))))
//                        . withColumn(ColumnIdentifier.POSITION,explode(
//                                ColumnFunctions.cyclicIntermediatePositions(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH),null))
//                        )
//
//                        .groupBy(ColumnIdentifier.POSITION,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.STRAND,ColumnIdentifier.PROPERTY,ColumnIdentifier.CATEGORY)
//                        .agg(sum(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.WEIGHT),count("*").as(ColumnIdentifier.COUNT) )
//                        .join(getGeneLengths().filter(col(ColumnIdentifier.TAXONOMIC_GROUP_NAME).equalTo(taxonomicGroup))
//                                        .select(col(ColumnIdentifier.PROPERTY).as("prop"),col(ColumnIdentifier.MEAN),col(ColumnIdentifier.STRAND).as("str")),
//                                col("prop").equalTo(col(ColumnIdentifier.PROPERTY))
//                                        .and(col(ColumnIdentifier.STRAND).equalTo(col("str")))
//                        ).drop("r","prop")
//                        .select(ColumnIdentifier.STRAND,ColumnIdentifier.POSITION,ColumnIdentifier.PROPERTY)
//
//        );
//        predictions
//                .filter(col(ColumnIdentifier.LENGTH).gt(minRangeLength))
//                .withColumn(ColumnIdentifier.STRAND,col(ColumnIdentifier.STRAND+"GM"))
//                .drop(ColumnIdentifier.STRAND+"GM")
////                . withColumn(ColumnIdentifier.POSITION,explode(sequence(col(ColumnIdentifier.START),col(ColumnIdentifier.END))))
//                . withColumn(ColumnIdentifier.POSITION,explode(
//                        ColumnFunctions.cyclicIntermediatePositions(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH),null))
//                )
//
//                .groupBy(ColumnIdentifier.POSITION,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.STRAND,ColumnIdentifier.PROPERTY,ColumnIdentifier.CATEGORY)
//                .agg(sum(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.WEIGHT),count("*").as(ColumnIdentifier.COUNT) )
//                .join(getGeneLengths().filter(col(ColumnIdentifier.TAXONOMIC_GROUP_NAME).equalTo(taxonomicGroup))
//                                .select(col(ColumnIdentifier.PROPERTY).as("prop"),col(ColumnIdentifier.MEAN),col(ColumnIdentifier.STRAND).as("str")),
//                        col("prop").equalTo(col(ColumnIdentifier.PROPERTY))
//                                .and(col(ColumnIdentifier.STRAND).equalTo(col("str")))
//                ).drop("r","prop")
//                .withColumn(ColumnIdentifier.WEIGHT,col(ColumnIdentifier.WEIGHT).divide(col(ColumnIdentifier.MEAN)))
//                .filter(col(ColumnIdentifier.COUNT).geq(minCount))
//                .orderBy(ColumnIdentifier.STRAND,ColumnIdentifier.POSITION,ColumnIdentifier.PROPERTY).show(5000);
        predictions = Spark.checkPointWithRandomAccessor(predictions
                .filter(col(ColumnIdentifier.LENGTH).gt(minRangeLength))
                .withColumn(ColumnIdentifier.STRAND,col(ColumnIdentifier.STRAND+"GM"))
                .drop(ColumnIdentifier.STRAND+"GM")
//                . withColumn(ColumnIdentifier.POSITION,explode(sequence(col(ColumnIdentifier.START),col(ColumnIdentifier.END))))
                . withColumn(ColumnIdentifier.POSITION,explode(
                        ColumnFunctions.cyclicIntermediatePositions(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH),null))
                )

                .groupBy(ColumnIdentifier.POSITION,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.STRAND,ColumnIdentifier.PROPERTY,ColumnIdentifier.CATEGORY)
                .agg(sum(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.WEIGHT),count("*").as(ColumnIdentifier.COUNT) )
                .filter(col(ColumnIdentifier.COUNT).geq(minCount))
                .join(getGeneLengths()
                                .select(col(ColumnIdentifier.PROPERTY).as("prop"),col(ColumnIdentifier.MEAN)),
                                col("prop").equalTo(col(ColumnIdentifier.PROPERTY))
                ).drop("r","prop")
                .withColumn(ColumnIdentifier.WEIGHT,
                        col(ColumnIdentifier.WEIGHT).divide(col(ColumnIdentifier.MEAN)))
        ,id).persist();

        WindowSpec w2 = Window.partitionBy(ColumnIdentifier.STRAND,ColumnIdentifier.POSITION).orderBy(desc(ColumnIdentifier.WEIGHT),desc((ColumnIdentifier.COUNT)));

//        WindowSpec w2 = Window.partitionBy(ColumnIdentifier.STRAND,ColumnIdentifier.POSITION).orderBy(asc(ColumnIdentifier.COUNT),desc(col(ColumnIdentifier.PROPERTY)));
        Dataset<Row> voids = predictions
                .withColumn(ColumnIdentifier.WEIGHT, when(col(ColumnIdentifier.PROPERTY).equalTo("void"),lit(0)).otherwise(col(ColumnIdentifier.WEIGHT)))
                .withColumn("voidCount",when(col(ColumnIdentifier.PROPERTY).equalTo("void"),col(ColumnIdentifier.COUNT)).otherwise(lit(0)))
                .withColumn("voidCount",sum(col("voidCount"))
                        .over(w2
                                .rowsBetween(Window.unboundedPreceding(), Window.unboundedFollowing())) )
                .filter(col("voidCount").notEqual(0))


                .withColumn(ColumnIdentifier.RANK,dense_rank().
                        over(w2))
                .withColumn("filterRank",max(col(ColumnIdentifier.RANK)).over(w2.rowsBetween(Window.unboundedPreceding(), Window.unboundedFollowing())))
//                .withColumn("a",col("filterRank"))
                .withColumn("filterRank",when(col("filterRank").lt(3),1).otherwise(2))

                .withColumn("nonVoidCount",when(col(ColumnIdentifier.PROPERTY).equalTo("void"),lit(0)).otherwise(col(ColumnIdentifier.COUNT)))
                .withColumn("nonVoidCount",sum(col("nonVoidCount")).over(w2))



                .withColumn("weightSum",sum(col(ColumnIdentifier.WEIGHT)).over(w2))
                .withColumn("totalWeight",sum(col(ColumnIdentifier.WEIGHT)).over(w2.rowsBetween(Window.unboundedPreceding(), Window.unboundedFollowing())))
                .filter(col(ColumnIdentifier.RANK).equalTo(col("filterRank")))
                .withColumn(ColumnIdentifier.RATIO,col("weightSum").divide(col("totalWeight")))

                .withColumn(ColumnIdentifier.FILTER,
                        col("voidCount").gt(col("nonVoidCount"))
                                .or(col(ColumnIdentifier.RATIO).lt(0.8))
                )
                .filter(col(ColumnIdentifier.FILTER))
                .select(col(ColumnIdentifier.POSITION).as("p"),col(ColumnIdentifier.STRAND).as("str"));

//        voids
//                .orderBy(ColumnIdentifier.STRAND,ColumnIdentifier.POSITION)
//                .show(50000);

//        predictions.filter(not(col(ColumnIdentifier.STRAND)).and(col(ColumnIdentifier.POSITION).between(133,201))).orderBy(ColumnIdentifier.POSITION,ColumnIdentifier.WEIGHT).show(5000);

//        predictions.select(ColumnIdentifier.POSITION,ColumnIdentifier.STRAND).except(voids).show();
//        predictions.join(voids,col(ColumnIdentifier.POSITION).equalTo(col("p")).and(col(ColumnIdentifier.STRAND).equalTo(col("str"))),
//                SparkComputer.JOIN_TYPES.LEFT_ANTI).show();


        return predictions.join(voids,col(ColumnIdentifier.POSITION).equalTo(col("p")).and(col(ColumnIdentifier.STRAND).equalTo(col("str"))),
            SparkComputer.JOIN_TYPES.LEFT_ANTI)
                .filter(col(ColumnIdentifier.PROPERTY).notEqual("void"));
//                .filter(col(ColumnIdentifier.COUNT).geq(minCount));


    }

    public static void createPredictions(Dataset<Row> bridgedRanges, Dataset<Row> mappingRanges, Dataset<Row> gmV, SequenceInformationDT seqInfo, int maxDistance,int k,
                                         int minRangeLength, int minCount,String path ) {


        Dataset<Row> predictions = null;
//        Dataset<Row> mappingRanges = createMappingRanges(kmermapping,k);


        if(bridgedRanges != null) {
            Dataset<Row> overlaps = Spark.checkPointWithRandomAccessor(createGenomeMappingFromOverlap(mappingRanges,gmV),seqInfo.getId()).persist();
//            SparkComputer.persistDataFrameORC(overlaps,path+"/OVERLAPS");
            Dataset<Row> bAnnotated = Spark.checkPointWithRandomAccessor(annotateBridgedRangesWithOverlap(
                    bridgedRanges.withColumn(ColumnIdentifier.EVALUE,col(ColumnIdentifier.ALIGN_RESULT+"."+ColumnIdentifier.EVALUE)),gmV),seqInfo.getId());

            predictions = Spark.checkPointWithRandomAccessor(joinClosePredictions(overlaps.withColumn(ColumnIdentifier.TOPOLOGY,lit(seqInfo.isTopology())),
                    bAnnotated.withColumn(ColumnIdentifier.TOPOLOGY,lit(seqInfo.isTopology())),
                    maxDistance,seqInfo.getId()),seqInfo.getId()).persist();
        }
        else {
            Dataset<Row> overlaps = Spark.checkPointWithRandomAccessor(createGenomeMappingFromOverlap(mappingRanges,gmV).withColumn(ColumnIdentifier.TOPOLOGY,lit(seqInfo.isTopology())),seqInfo.getId()).persist();
//            SparkComputer.persistDataFrameORC(overlaps,path+"/OVERLAPS");
//            overlaps.filter(col("l").notEqual(col("lref"))).show();
//            overlaps.filter(col("l").gt(10000)).show();
            predictions = joinClosePredictions(Spark.checkPointWithRandomAccessor(overlaps,seqInfo.getId()) ,
                   null,
                    maxDistance,seqInfo.getId());
        }

//        predictions
//                .withColumn(ColumnIdentifier.STRAND,col(ColumnIdentifier.STRAND+"GM"))
//                .filter(col(ColumnIdentifier.STRAND)).filter(col(ColumnIdentifier.PROPERTY).equalTo("M")).orderBy(ColumnIdentifier.START).show(50000);

        SparkComputer.persistDataFrameORC(createGeneDistribution(predictions,minRangeLength,minCount,seqInfo.getId()),path+"/JOINED_GENE_RANGES");
        SparkComputer.persistDataFrameORC(createGeneDistribution(predictions
                .withColumn(ColumnIdentifier.PROPERTY,when(col(ColumnIdentifier.PROPERTY).isin("L1","L2"),lit("L")).otherwise(when(col(ColumnIdentifier.PROPERTY)
                        .isin("S1","S2"),lit("S")).otherwise(col(ColumnIdentifier.PROPERTY)))),minRangeLength,minCount,seqInfo.getId()),path+"/JOINED_GENE_RANGES_LS");

        createCluster(SparkComputer.readORC(path+"/JOINED_GENE_RANGES_LS"),path+"/CLUSTER",seqInfo);

    }

    private static void extractFastaFromBED(String fastaFile, String bedFile, String outPutFile) {
        String runCmnd =ProjectDirectoryManager.getSCRIPTS_DIR()+"/bedtools getfasta -fi "+ fastaFile+ " -bed " + bedFile + " -fo " + outPutFile + " -name -s";

        try {
            Process process = Runtime.getRuntime().exec(runCmnd);

            process.waitFor();
            if(process.exitValue() != 0) {
                LOGGER.error("Bedtools annotation did not work");
                System.exit(5);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static String parseToBED(Dataset<Row> propertyRanges, String identifier) {
        String bedAnnotation = "";
//        Auxiliary.printSeq(propertyRanges);
        for(Row propertyRange: propertyRanges.orderBy(ColumnIdentifier.START).collectAsList()) {
            bedAnnotation += identifier + "\t" + propertyRange.getInt(propertyRange.fieldIndex(ColumnIdentifier.START)) + "\t" +
                    (propertyRange.getInt(propertyRange.fieldIndex(ColumnIdentifier.END))+1) + "\t" +
                    propertyRange.getString(propertyRange.fieldIndex(ColumnIdentifier.PROPERTY)) +  "\t" +
                    "1\t" +
                    (propertyRange.getBoolean(propertyRange.fieldIndex(ColumnIdentifier.STRAND))? "+"  : "-") + "\n";
        }
        return bedAnnotation;

//        String bedAnnotation = "";
////        Auxiliary.printSeq(propertyRanges);
//        for(Row propertyRange: propertyRanges.orderBy(ColumnIdentifier.START).collectAsList()) {
//            bedAnnotation += identifier + "\t" + propertyRange.getInt(propertyRange.fieldIndex(ColumnIdentifier.START)) + "\t" +
//                    propertyRange.getInt(propertyRange.fieldIndex(ColumnIdentifier.START)) + "\t" +
//                    propertyRange.getString(propertyRange.fieldIndex(ColumnIdentifier.PROPERTY)) +  "\t" + (propertyRange.getBoolean(propertyRange.fieldIndex(ColumnIdentifier.STRAND))? "+"  : "-") + "\n";
//        }
//        return bedAnnotation;
    }

    private static String extractGeneOrder(Dataset<Row> propertyRanges) {
        return propertyRanges.select(ColumnIdentifier.PROPERTY).as(Encoders.STRING()).collectAsList().stream().collect(Collectors.joining(" "));
    }

    public static String createBEDPropertyAnnotations(double alphaValue, SequenceInformationDT sequenceInformationDT, String path, Dataset<Row> meanGeneLengths, Dataset<Row> ranges, String sequence) {
        Cluster cluster = new Cluster();


        for(boolean strand: Arrays.asList(true,false)) {

//            System.out.println(resultDirectoryTree.getPropertyClusterTreeFile(strand));
//            System.out.println(path+"/"+(strand ?"plus":"minus")+ "ClusterTree.gryo");
            cluster.setG(new File(path+"/"+(strand ?"plus":"minus")+ "ClusterTree.gryo"));

//            System.out.println(strand);
            Dataset<PropertyRangeProbDT> propertyRangesAnnotated =
                    SparkComputer.createDataFrame(
                            Cluster.createAnnotatedProperties(cluster.g, alphaValue,sequenceInformationDT.getLength()),PropertyRangeProbDT.ENCODER).
                            filter(col(ColumnIdentifier.CATEGORY).isInCollection(Arrays.asList("trna","rrna","protein"))).
                            select(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,
                                    ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY,ColumnIdentifier.PROBABILITY).
                            as(PropertyRangeProbDT.ENCODER);
//            propertyRangesAnnotated.show();
//            System.out.println(path+"/"+(strand ?"plusAnnotation":"minusAnnotation"));
            SparkComputer.persistOverwriteDataFrame(
                    propertyRangesAnnotated,path+"/"+(strand ?"plusAnnotation":"minusAnnotation")+Math.round(alphaValue*100));



        }
        boolean strand= true;
        Dataset<PropertyRangeProbDT> propertyRangesAnnotatedPlus =
                SparkComputer.read(path+"/"+(strand ?"plusAnnotation":"minusAnnotation")+Math.round(alphaValue*100),
                        PropertyRangeProbDT.ENCODER);

//        propertyRangesAnnotatedPlus.orderBy(ColumnIdentifier.START).show(500);
        strand = false;
        Dataset<PropertyRangeProbDT> propertyRangesAnnotatedMinus =
                SparkComputer.read(path+"/"+(strand ?"plusAnnotation":"minusAnnotation")+Math.round(alphaValue*100),
                        PropertyRangeProbDT.ENCODER);
//        propertyRangesAnnotatedMinus.orderBy(ColumnIdentifier.START).show(500);

        Dataset<PropertyRangeProbStrandDT> gm =                 PropertyRangeProbStrandDT.convert(propertyRangesAnnotatedPlus,true).
                union(PropertyRangeProbStrandDT.convert(propertyRangesAnnotatedMinus,false));

//        gm.show();
//        System.out.println(sequenceInformationDT.isTopology());
//        List<PropertyRangeProbStrandDT> propertyRangesAnnotated = gm.collectAsList();
//        gm.orderBy(ColumnIdentifier.START).show(500);

//        Dataset<Row> annotationDF = annotateAnticodonType("L",ranges,annotateAnticodonType("S",ranges,gm.withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"_.*",""))
//                .withColumn(ColumnIdentifier.SEQUENCE_LENGTH,lit(sequenceInformationDT.getLength()))
//                .withColumn(ColumnIdentifier.TOPOLOGY,lit(sequenceInformationDT.isTopology())))).checkpoint();
//        annotationDF = joinFinalClosePredictions(annotationDF
//                ,meanGeneLengths,150).checkpoint();

//        gm.filter(col(ColumnIdentifier.PROPERTY).equalTo("rrnS")).show();
//        gm.withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"_.*","")).orderBy(ColumnIdentifier.START).show(500);

        Dataset<Row> annotationDF = Spark.checkPointWithRandomAccessor(joinFinalClosePredictions(gm.withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"_.*",""))
                .withColumn(ColumnIdentifier.SEQUENCE_LENGTH,lit(sequenceInformationDT.getLength()))
                        .withColumn(ColumnIdentifier.TOPOLOGY,lit(sequenceInformationDT.isTopology())),
                ranges,
                meanGeneLengths,150,sequenceInformationDT.getId()),sequenceInformationDT.getId()

        );

        annotationDF.withColumn(ColumnIdentifier.PROPERTY+"2",when(col(ColumnIdentifier.CATEGORY).equalTo("trna"),"yes").otherwise(col(ColumnIdentifier.PROPERTY)));

        annotationDF = annotationDF.withColumn(ColumnIdentifier.PROPERTY,when(col(ColumnIdentifier.CATEGORY).equalTo("trna"),
                concat(lit("trna"),col(ColumnIdentifier.PROPERTY))).otherwise(col(ColumnIdentifier.PROPERTY)));


        try {
            FileIO.writeToFile(extractGeneOrder(annotationDF),path+"/../"+sequenceInformationDT.getId()+".txt",false,">"+sequenceInformationDT.getId());
        } catch (IOException e) {
            e.printStackTrace();
        }


        if(DEBUG) {
            SparkComputer.persistDataFrameORC(annotationDF,path+"/../PREDICTIONS");
        }

        if(transTable != -1) {
//            String bed = parseToBED(annotationDF,sequenceInformationDT.getId());
//
//            try {
//                FileIO.writeToFile(bed ,
//                        path+"/../noTrans.bed",false,""
//                );
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            annotationDF = improveGeneEnds(annotationDF,transTable,shift,sequence);

            if(DEBUG) SparkComputer.persistDataFrameORC(annotationDF,path+"/../CORRECTED_PREDICTIONS");
        }

        return parseToBED(annotationDF,sequenceInformationDT.getId());

    }

    public static Dataset<Row> createCyclicPropertyRangesWithRecords(Dataset<Row> ranges){
        Dataset<Row> ends  = ranges.filter(
                col(ColumnIdentifier.SEQUENCE_LENGTH).equalTo(col(ColumnIdentifier.END).plus(1))
        );
        Dataset<Row> starts  = ranges.filter(
                col(ColumnIdentifier.START).equalTo(0)
        );


        Dataset<Row> cyclicRanges = ends.join(SparkComputer.appendIdentifier(starts,"2"),
                col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2"))
                        .and(col(ColumnIdentifier.GENE).equalTo(col(ColumnIdentifier.GENE+"2")))
                        .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
        );

//        cyclicRanges.show();

        ranges = ranges.join(
                SparkComputer.appendIdentifier(cyclicRanges.select(SparkComputer.getColumns(ranges.columns()))
                        ,"2"),
                col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2"))
                        .and(col(ColumnIdentifier.PROPERTY).equalTo(col(ColumnIdentifier.PROPERTY+"2")))
                        .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
                        .and(col(ColumnIdentifier.START).equalTo(col(ColumnIdentifier.START+"2")))
                        .and(col(ColumnIdentifier.END).equalTo(col(ColumnIdentifier.END+"2")))
                ,
                SparkComputer.JOIN_TYPES.LEFT_ANTI
        );
//        ranges.show();

        ranges = ranges.join(
                cyclicRanges.select(ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.PROPERTY+"2",ColumnIdentifier.STRAND+"2",ColumnIdentifier.START+"2",ColumnIdentifier.END+"2"),
                col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2"))
                        .and(col(ColumnIdentifier.PROPERTY).equalTo(col(ColumnIdentifier.PROPERTY+"2")))
                        .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
                        .and(col(ColumnIdentifier.START).equalTo(col(ColumnIdentifier.START+"2")))
                        .and(col(ColumnIdentifier.END).equalTo(col(ColumnIdentifier.END+"2")))
                ,
                SparkComputer.JOIN_TYPES.LEFT_ANTI
        );
//        ranges.show();

        return ranges.

                union(
                        cyclicRanges.withColumn(ColumnIdentifier.END,col(ColumnIdentifier.END+"2"))
                                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getCyclicRangeLength(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH)))
                                .select(SparkComputer.getColumns(ranges.columns()))
                );
    }

    public static Dataset<Row> createCyclicPropertyRanges(Dataset<Row> ranges){
        Dataset<Row> ends  = ranges.filter(
                col(ColumnIdentifier.SEQUENCE_LENGTH).equalTo(col(ColumnIdentifier.END).plus(1))
        );
        Dataset<Row> starts  = ranges.filter(
                col(ColumnIdentifier.START).equalTo(0)
        );


        Dataset<Row> cyclicRanges = ends.join(SparkComputer.appendIdentifier(starts,"2"),
                col(ColumnIdentifier.PROPERTY).equalTo(col(ColumnIdentifier.PROPERTY+"2"))
                        .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
        );

//        cyclicRanges.show();

        ranges = ranges.join(
                SparkComputer.appendIdentifier(cyclicRanges.select(SparkComputer.getColumns(ranges.columns()))
                        ,"2"),
                col(ColumnIdentifier.PROPERTY).equalTo(col(ColumnIdentifier.PROPERTY+"2"))
                        .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
                        .and(col(ColumnIdentifier.START).equalTo(col(ColumnIdentifier.START+"2")))
                        .and(col(ColumnIdentifier.END).equalTo(col(ColumnIdentifier.END+"2")))
                ,
                SparkComputer.JOIN_TYPES.LEFT_ANTI
        );
//        ranges.show();

        ranges = ranges.join(
                cyclicRanges.select(ColumnIdentifier.PROPERTY+"2",ColumnIdentifier.STRAND+"2",ColumnIdentifier.START+"2",ColumnIdentifier.END+"2"),
                col(ColumnIdentifier.PROPERTY).equalTo(col(ColumnIdentifier.PROPERTY+"2"))
                        .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
                        .and(col(ColumnIdentifier.START).equalTo(col(ColumnIdentifier.START+"2")))
                        .and(col(ColumnIdentifier.END).equalTo(col(ColumnIdentifier.END+"2")))
                ,
                SparkComputer.JOIN_TYPES.LEFT_ANTI
        );
//        ranges.show();

        return ranges.

                union(
                        cyclicRanges.withColumn(ColumnIdentifier.END,col(ColumnIdentifier.END+"2"))
                                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getCyclicRangeLength(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH)))
                                .select(SparkComputer.getColumns(ranges.columns()))
                );
    }

    public static void evaluateOverlap(Dataset<Row> genomeMapping, Dataset<Row> annotations, double overlapeRate, boolean cyclic, int recordId, String path) {

            genomeMapping = genomeMapping.filter(col(ColumnIdentifier.RECORD_ID).equalTo(recordId).and(col(ColumnIdentifier.CATEGORY).isin("rrna","trna","protein"))).persist();

            genomeMapping = genomeMapping
                    .withColumn(ColumnIdentifier.ROW_NUMBER, row_number().over(Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.PROPERTY).orderBy(ColumnIdentifier.START)))
                .withColumn(ColumnIdentifier.GENE,concat_ws("-",col(ColumnIdentifier.PROPERTY),col(ColumnIdentifier.ROW_NUMBER))).drop(ColumnIdentifier.ROW_NUMBER);

            annotations = annotations
                    .withColumn(ColumnIdentifier.ROW_NUMBER, row_number().over(Window.partitionBy(ColumnIdentifier.STRAND,ColumnIdentifier.PROPERTY).orderBy(ColumnIdentifier.START)))
                    .withColumn(ColumnIdentifier.GENE,concat_ws("-",col(ColumnIdentifier.PROPERTY),col(ColumnIdentifier.ROW_NUMBER))).drop(ColumnIdentifier.ROW_NUMBER);
//        genomeMapping.orderBy(ColumnIdentifier.START).show(500);
            if(cyclic){
                genomeMapping = createCyclicPropertyRanges(genomeMapping).persist();
            }

//            System.out.println("refseq");
//            genomeMapping.orderBy(ColumnIdentifier.START).show(500);
//            System.out.println("annotation");
//            annotations.orderBy(ColumnIdentifier.START).show(500);

            Dataset<Row> matches = ColumnFunctions.computeOverlapPositions2(annotations
                    .join(SparkComputer.appendIdentifier(genomeMapping,"GM"),
//                                    col(ColumnIdentifier.STRAND+"GM").equalTo(col(ColumnIdentifier.STRAND))
                                    ((pmod(col(ColumnIdentifier.START).minus(col(ColumnIdentifier.START+"GM")),col(ColumnIdentifier.SEQUENCE_LENGTH)).plus(1)
                                            .leq(col(ColumnIdentifier.LENGTH+"GM")))
                                            .or(
                                                    (pmod(col(ColumnIdentifier.START+"GM").minus(col(ColumnIdentifier.START)),col(ColumnIdentifier.SEQUENCE_LENGTH)).plus(1)
                                                            .leq(col(ColumnIdentifier.LENGTH)))
                                            ))),
                col(ColumnIdentifier.START),col(ColumnIdentifier.END),
                col(ColumnIdentifier.START+"GM"),col(ColumnIdentifier.END+"GM"),
                col(ColumnIdentifier.LENGTH),col(ColumnIdentifier.LENGTH+"GM"),
                col(ColumnIdentifier.SEQUENCE_LENGTH),ColumnIdentifier.OVERLAP)
            .withColumn("s",element_at(col(ColumnIdentifier.OVERLAP),1))
            .withColumn("e",element_at(col(ColumnIdentifier.OVERLAP),2))
            .withColumn("l",ColumnFunctions.getCyclicRangeLength(col("s"),col("e"),col(ColumnIdentifier.SEQUENCE_LENGTH)))
            .withColumn(ColumnIdentifier.MATCH_FLAG+"strand", col(ColumnIdentifier.STRAND+"GM").equalTo(col(ColumnIdentifier.STRAND)))
            .withColumn(ColumnIdentifier.MATCH_FLAG,
                    when(col(ColumnIdentifier.PROPERTY).isin("L1","L2"),
                            col(ColumnIdentifier.PROPERTY+"GM").isin("L","L1","L2")
                            )
                            .otherwise(
                                    when(col(ColumnIdentifier.PROPERTY).isin("S1","S2"),
                                            col(ColumnIdentifier.PROPERTY+"GM").isin("S","S1","S2")
                                    )
                                            .otherwise(col(ColumnIdentifier.PROPERTY).equalTo(col(ColumnIdentifier.PROPERTY+"GM")))
                            )
                    )
//            .withColumn(ColumnIdentifier.RATIO,(col("l").divide(least(col(ColumnIdentifier.LENGTH+"GM"),col(ColumnIdentifier.LENGTH)) )))
                    .withColumn(ColumnIdentifier.RATIO+"refseq",(col("l").divide(col(ColumnIdentifier.LENGTH+"GM")) ))
                    .withColumn(ColumnIdentifier.RATIO,(col("l").divide(col(ColumnIdentifier.LENGTH)) ))
//                    .withColumn(ColumnIdentifier.JACCARD_INDEX,round((col("l").divide((col(ColumnIdentifier.LENGTH).plus(col(ColumnIdentifier.LENGTH+"GM")).minus(col("l")) ))).multiply(100)))
                    .withColumn(ColumnIdentifier.JACCARD_INDEX,(col("l").divide((col(ColumnIdentifier.LENGTH).plus(col(ColumnIdentifier.LENGTH+"GM")).minus(col("l")) ))))
                    .withColumn(ColumnIdentifier.RANK,dense_rank().over(Window.partitionBy(ColumnIdentifier.STRAND,ColumnIdentifier.GENE)
                            .orderBy(desc((ColumnIdentifier.JACCARD_INDEX)))))
                    .withColumn(ColumnIdentifier.RANK+"2",dense_rank().over(Window.partitionBy(ColumnIdentifier.STRAND+"GM",ColumnIdentifier.GENE+"GM")
                            .orderBy(desc((ColumnIdentifier.JACCARD_INDEX)))))
//                    .withColumn(ColumnIdentifier.RANK,dense_rank().over(Window.partitionBy(ColumnIdentifier.STRAND,ColumnIdentifier.GENE)
//                            .orderBy(desc((ColumnIdentifier.RATIO)),desc(ColumnIdentifier.RATIO+"refseq")) ))
//                    .withColumn(ColumnIdentifier.RANK+"2",
//                            dense_rank().over(Window.partitionBy(ColumnIdentifier.STRAND+"GM",ColumnIdentifier.GENE+"GM")
//                            .orderBy(desc((ColumnIdentifier.RATIO)),desc(ColumnIdentifier.RATIO+"refseq")) ))

//            .filter(col(ColumnIdentifier.RATIO).geq(overlapeRate))
            .orderBy(ColumnIdentifier.START);

            LOGGER.info("Matches");
            matches
                    .show(500);


            Dataset<Row> truePos = matches
//                    .filter(col(ColumnIdentifier.JACCARD_INDEX).geq(overlapeRate))
                    .filter(col(ColumnIdentifier.RATIO).geq(overlapeRate))
                    .filter(col(ColumnIdentifier.RANK).equalTo(1))
                    .filter(col(ColumnIdentifier.RANK+"2").equalTo(1)).drop(ColumnIdentifier.RANK,ColumnIdentifier.RANK+"2",ColumnIdentifier.OVERLAP);
            LOGGER.info("True Positives");
//            truePos
//                    .show(500);

//            truePos.filter(col(ColumnIdentifier.LENGTH).lt(20)).show();
            Dataset<Row> genePrecision = truePos
                    .withColumn(
                            "d_s",ColumnFunctions.computeSmallestDistanceExcludingBoundaryPositions(col(ColumnIdentifier.START+"GM"),
                                    col(ColumnIdentifier.START),col(ColumnIdentifier.SEQUENCE_LENGTH),col(ColumnIdentifier.TOPOLOGY),lit(true)).plus(1))
                    .withColumn(
                            "d_e",ColumnFunctions.computeSmallestDistanceExcludingBoundaryPositions(col(ColumnIdentifier.END+"GM"),
                                    col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH),col(ColumnIdentifier.TOPOLOGY),lit(true)).plus(1))
                    .withColumn("d",greatest(col("d_s"),col("d_e"))).orderBy(ColumnIdentifier.PROPERTY,ColumnIdentifier.START)
//                    .withColumn(
//                    "d_s",ColumnFunctions.computeSmallestDistanceExcludingBoundaryPositions(col(ColumnIdentifier.START+"GM"),
//                            col("s"),col(ColumnIdentifier.SEQUENCE_LENGTH),col(ColumnIdentifier.TOPOLOGY),lit(true)).plus(1))
//                    .withColumn(
//                            "d_e",ColumnFunctions.computeSmallestDistanceExcludingBoundaryPositions(col(ColumnIdentifier.END+"GM"),
//                                    col("e"),col(ColumnIdentifier.SEQUENCE_LENGTH),col(ColumnIdentifier.TOPOLOGY),lit(true)).plus(1))
                    ;

            genePrecision.show(500);
            SparkComputer.persistOverwriteDataFrameORC(genePrecision,path + "/GENE_PRECISION");

            Dataset<Row> falsePos =
                    annotations.join(
                    SparkComputer.appendIdentifier(truePos.select(ColumnIdentifier.STRAND,ColumnIdentifier.PROPERTY,ColumnIdentifier.START,ColumnIdentifier.END).distinct(),"2"),
                            col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2"))
                                    .and(col(ColumnIdentifier.START).equalTo(col(ColumnIdentifier.START+"2")))
                                    .and(col(ColumnIdentifier.END).equalTo(col(ColumnIdentifier.END+"2")))
                                    .and(col(ColumnIdentifier.PROPERTY).equalTo(col(ColumnIdentifier.PROPERTY+"2"))),
                            SparkComputer.JOIN_TYPES.LEFT_ANTI);
            LOGGER.info("False Positives");
            falsePos
                .show(500);
            SparkComputer.persistOverwriteDataFrameORC(falsePos,path + "/FALSE_POSITIVES");

            Dataset<Row> falseNeg =
                        genomeMapping.join(
                                truePos.select(ColumnIdentifier.STRAND+"GM",ColumnIdentifier.PROPERTY+"GM",ColumnIdentifier.START+"GM",ColumnIdentifier.END+"GM").distinct(),
                                col(ColumnIdentifier.STRAND+"GM").equalTo(col(ColumnIdentifier.STRAND))
                                        .and(col(ColumnIdentifier.START+"GM").equalTo(col(ColumnIdentifier.START)))
                                        .and(col(ColumnIdentifier.END+"GM").equalTo(col(ColumnIdentifier.END)))
                                        .and(col(ColumnIdentifier.PROPERTY+"GM").equalTo(col(ColumnIdentifier.PROPERTY))),
                                SparkComputer.JOIN_TYPES.LEFT_ANTI);

            LOGGER.info("False Negatives");
            falseNeg.show(500);
            SparkComputer.persistOverwriteDataFrameORC(falseNeg,path + "/FALSE_NEGATIVES");



    }

    public static String readSingleFastaFile(String fileName) {
        StringBuilder contentBuilder = new StringBuilder();
        char commentCharacter = '>';

        Function<String,String> parser =  (s -> s);
        try (Stream<String> stream = Files.lines( Paths.get(fileName), StandardCharsets.UTF_8))
        {

            if (stream.filter(s -> s.charAt(0) == commentCharacter).count() != 1) {
                return null;
            }

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        try (Stream<String> stream = Files.lines( Paths.get(fileName), StandardCharsets.UTF_8))
        {

            stream.filter(s -> s.charAt(0) != commentCharacter)
                    .forEach(s ->  contentBuilder.append(parser.apply(s)));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }

    public static void createFastaFile(String sequence, String name, String fastaFile) throws IOException {
        FileIO.writeToFile(sequence,fastaFile,false,">"+ name);
    }


    public static Dataset<Row> improveGeneEnds(Dataset<Row> propertyRanges, int transTable, int shift, String sequence) {

        List<Row> preditions = new ArrayList<>();

        Column[] columns = SparkComputer.getColumns(propertyRanges.columns());
        if(DEBUG) propertyRanges.orderBy(ColumnIdentifier.START).show(5000);
        propertyRanges = propertyRanges
                // only check end
                .filter(col(ColumnIdentifier.START).gt(col(ColumnIdentifier.END)))
                .withColumn(ColumnIdentifier.END,col(ColumnIdentifier.END))
                .withColumn(ColumnIdentifier.START,lit(0))
                .select(columns).withColumn(ColumnIdentifier.FLAG,lit(1))
                .union(
                        // only check start
                        propertyRanges.filter(col(ColumnIdentifier.START).gt(col(ColumnIdentifier.END)))
                                .withColumn(ColumnIdentifier.START,col(ColumnIdentifier.START))
                                .withColumn(ColumnIdentifier.END,lit(sequence.length()-1))
                                .select(columns)   .withColumn(ColumnIdentifier.FLAG,lit(2))
                )
                .union(
                        //  check both
                        propertyRanges.filter(col(ColumnIdentifier.START).leq(col(ColumnIdentifier.END))).withColumn(ColumnIdentifier.FLAG,lit(3))
                );

        if(DEBUG) propertyRanges.orderBy(ColumnIdentifier.START).show(5000);


        Dataset<Row> predictionsPlus = improveGeneEndsSingleStrand(propertyRanges.filter(col(ColumnIdentifier.STRAND)),transTable,shift,sequence);

        if(DEBUG) predictionsPlus.orderBy(ColumnIdentifier.START).show(5000);

        Dataset<Row> reverseComplementPredictions = propertyRanges.filter(not(col(ColumnIdentifier.STRAND)))
                .withColumn("sn",col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1).minus(col(ColumnIdentifier.END)))
                .withColumn("en",col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1).minus(col(ColumnIdentifier.START)))
                .withColumn(ColumnIdentifier.START,col("sn")).withColumn(ColumnIdentifier.END,col("en"))
                .select(SparkComputer.getColumns(propertyRanges.columns()));
        if(DEBUG) System.out.println("reverse complement");
        if(DEBUG) reverseComplementPredictions.orderBy(ColumnIdentifier.START).show(5000);

        Dataset<Row> predictionsMinus = improveGeneEndsSingleStrand(reverseComplementPredictions,
                transTable,shift,PairwiseAligner.reverseComplement( sequence));

        if(DEBUG) predictionsMinus.orderBy(ColumnIdentifier.START).show(5000);

        predictionsMinus = predictionsMinus.withColumn(ColumnIdentifier.SEQUENCE_LENGTH,lit(sequence.length()))
                .withColumn("sn",col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1).minus(col(ColumnIdentifier.END)))
                .withColumn("en",col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1).minus(col(ColumnIdentifier.START)))
                .withColumn(ColumnIdentifier.START,col("sn")).withColumn(ColumnIdentifier.END,col("en"))
                .select(SparkComputer.getColumns(predictionsPlus.columns()));

        if(DEBUG) predictionsMinus.orderBy(ColumnIdentifier.START).show(5000);

        return predictionsPlus.union(predictionsMinus);
    }

    public static Dataset<Row> improveGeneEndsSingleStrand(Dataset<Row> propertyRanges, int transTable, int shift, String sequence) {

        List<Row> preditions = new ArrayList<>();


        if(DEBUG) System.out.println(sequence.length() + " " +sequence);

        for (Row propertyRange: propertyRanges.collectAsList()){
            int start = propertyRange.getInt(propertyRange.fieldIndex(ColumnIdentifier.START));
            int stop = propertyRange.getInt(propertyRange.fieldIndex(ColumnIdentifier.END))+1;
            int offsetInit = 0;
            int offsetTerm = 0;
            boolean hasStartCodon = false;
            boolean hasStopCodon = false;



            if(propertyRange.getString(propertyRange.fieldIndex(ColumnIdentifier.CATEGORY)).equals( "protein")) {

                if(DEBUG) System.out.println(propertyRange.getString(propertyRange.fieldIndex(ColumnIdentifier.PROPERTY)));
                String subsequence = sequence.substring(start,stop);
//                System.out.println(start+ " " + stop);

//                System.out.println(subsequence);

                String startCodon = subsequence.substring(0,3);
                String stopCodon = subsequence.substring(subsequence.length()-3);

                if(DEBUG) System.out.println("Start codon " + startCodon);
                if(DEBUG) System.out.println("Stop Codon" + stopCodon);



                if(Arrays.asList(2,3).contains(propertyRange.getInt(propertyRange.fieldIndex(ColumnIdentifier.FLAG))) && !initiationCodes.get(transTable).contains(startCodon)) {
                    if(DEBUG) System.out.println("no start codon " + startCodon);

                    for (int i = 1; i <= shift; i++) {
                        if(DEBUG) System.out.println(Auxiliary.mod(start+i,sequence.length()) +" " +Auxiliary.mod(start+i+3,sequence.length()));

                         String codon = extractSubsequence(sequence,Auxiliary.mod(start+i,sequence.length()),Auxiliary.mod(start+i+3,sequence.length()));
                        if(DEBUG) System.out.println(codon);

                        if(terminationCodes.get(transTable).contains(codon)) {
                            if(DEBUG) System.out.println("Termination codon encountered");
                            if(DEBUG) System.out.println("Codon: " +codon);
                            break;
                        }
                        if(initiationCodes.get(transTable).contains(codon)) {
                            if(DEBUG) System.out.println("Inition codon encountered");
                            if(DEBUG) System.out.println("Codon: " +codon);
                            offsetInit = i;
                            break;
                        }
                    }
                    if(offsetInit== 0){
                        for (int i = 1; i <= shift; i++) {
                            String codon = extractSubsequence(sequence,Auxiliary.mod(start-i,sequence.length()),Auxiliary.mod(start-i+3,sequence.length()));
                            if(DEBUG) System.out.println(codon);

                            if(terminationCodes.get(transTable).contains(codon)) {
                                if(DEBUG) System.out.println("Termination codon encountered");
                                if(DEBUG) System.out.println("Codon: " +codon);
                                break;
                            }
                            if(initiationCodes.get(transTable).contains(codon)) {
                                if(DEBUG) System.out.println("Inition codon encountered");
                                if(DEBUG) System.out.println("Codon: " +codon);
                                offsetInit = -i;
                                break;
                            }
                        }
                    }


                }
                if(offsetInit != 0 || initiationCodes.get(transTable).contains(startCodon)) {
                    hasStartCodon = true;
                }
                if(Arrays.asList(1,3).contains(propertyRange.getInt(propertyRange.fieldIndex(ColumnIdentifier.FLAG))) && !terminationCodes.get(transTable).contains(stopCodon)) {
                    if(DEBUG) System.out.println("no stop codon " + stopCodon);

                    for (int i = 1; i <= shift; i++) {

                        if(DEBUG) System.out.println(Auxiliary.mod(stop-3+i,sequence.length()) + " " +Auxiliary.mod(stop-1+i,sequence.length()));
                        String codon = extractSubsequence(sequence,Auxiliary.mod(stop-3+i,sequence.length()),Auxiliary.mod(stop+i,sequence.length()));
                        if(DEBUG) System.out.println(codon);

                        if(terminationCodes.get(transTable).contains(codon)) {
                            if(DEBUG) System.out.println("Termination codon encountered");
                            if(DEBUG) System.out.println("Codon: " +codon);
                            offsetTerm = i;
                            break;
                        }
                    }
                    if(offsetTerm == 0){
                        for (int i = 1; i <= shift; i++) {
                            if(DEBUG) System.out.println(Auxiliary.mod(stop-3-i,sequence.length()) + " "+ Auxiliary.mod(stop-i,sequence.length()));
                            String codon = extractSubsequence(sequence,Auxiliary.mod(stop-3-i,sequence.length()),Auxiliary.mod(stop-i,sequence.length()));
                            if(DEBUG) System.out.println(codon);

                            if(terminationCodes.get(transTable).contains(codon)) {
                                if(DEBUG) System.out.println("Termination codon encountered");
                                if(DEBUG) System.out.println("Codon: " +codon);
                                offsetTerm = -i;
                                break;
                            }
                        }
                    }

                }
                if(offsetTerm != 0 || terminationCodes.get(transTable).contains(stopCodon)) {
                    hasStopCodon = true;
                }

                if(DEBUG) System.out.println("New position with offsets"+  offsetInit + " " + offsetTerm);
            }

            preditions.add(
                    RowFactory.create(
                            Auxiliary.mod(start+offsetInit,sequence.length()),
                            Auxiliary.mod(stop-1+offsetTerm,sequence.length()),
                            propertyRange.getString(propertyRange.fieldIndex(ColumnIdentifier.PROPERTY)),
                            propertyRange.getString(propertyRange.fieldIndex(ColumnIdentifier.CATEGORY)),
                            propertyRange.getBoolean(propertyRange.fieldIndex(ColumnIdentifier.STRAND)),
                            transTable,
                            hasStartCodon,
                            hasStopCodon

                    )
            );



        }
        return Spark.getInstance().createDataFrame(preditions,
                    new StructType(new StructField[]{
                            new StructField(ColumnIdentifier.START, DataTypes.IntegerType,false, Metadata.empty()),
                            new StructField(ColumnIdentifier.END, DataTypes.IntegerType,false, Metadata.empty()),
                            new StructField(ColumnIdentifier.PROPERTY, DataTypes.StringType,false, Metadata.empty()),
                            new StructField(ColumnIdentifier.CATEGORY, DataTypes.StringType,false, Metadata.empty()),
                            new StructField(ColumnIdentifier.STRAND, DataTypes.BooleanType,false, Metadata.empty()),
                            new StructField(ColumnIdentifier.TRANS_TABLE, DataTypes.IntegerType,false, Metadata.empty()),
                            new StructField(ColumnIdentifier.START_CODON, DataTypes.BooleanType,false, Metadata.empty()),
                            new StructField(ColumnIdentifier.STOP_CODON, DataTypes.BooleanType,false, Metadata.empty()),
                    })
                );
    }

    private static String extractSubsequence(String sequence, int start, int end) {

        if(start > end) {
//            return sequence.substring(start) + sequence.substring(0,(end == 0) ? 1 : end);
            return sequence.substring(start) + sequence.substring(0, end);
        }
        return sequence.substring(start,end);
    }

    public static void annotateRefseq(String name, File fastaFile, int taxId, boolean circular,
                                      String dir, int weight, int minCount, int k, String refseq, double alpha, boolean keepIntermediateResults) throws Exception {
        String sequence = readSingleFastaFile(fastaFile.getAbsolutePath());
        if(sequence == null) {
            LOGGER.error("No multi fasta files are supported.");
            System.exit(1);
        }
        else if(sequence.length() < MIN_SEQUENCE_LENGTH) {
            LOGGER.error("Sequence too short must be at least " + DeGeCI.MIN_SEQUENCE_LENGTH + " nt long.");
            System.exit(1);
        }
        annotateRefseq(name,sequence,taxId,circular,dir,weight,minCount,k,refseq,alpha,keepIntermediateResults);
    }



    /***
     *
     * @param name
     * @param sequence
     * @param taxId
     * @param circular
     * @param dir
     * @param weight
     * @param minCount
     * @param k
     * @param refseq
     * @param alpha
     * @param keepIntermediateResults
     * @throws IOException
     */
    public static void annotateRefseq(String name, String sequence, int taxId, boolean circular,
                                      String dir, int weight, int minCount, int k, String refseq, double alpha, boolean keepIntermediateResults,
                                      List<Integer> includeOnlyRecords
                                      ) throws Exception {


        Dataset<Integer> recordsDS = Spark.getInstance().createDataset(includeOnlyRecords,Encoders.INT());

        annotateRefseq(name,sequence,taxId,circular,dir,weight,minCount,k,refseq,alpha,keepIntermediateResults,
                SparkComputer.readORC(ProjectDirectoryManager.getRECORD_DATA_DIR())
                .join(recordsDS,col(ColumnIdentifier.VALUE).equalTo(col(ColumnIdentifier.RECORD_ID))
                        ,SparkComputer.JOIN_TYPES.SEMI));

    }

    public static void annotateRefseq(String name, String sequence, int taxId, boolean circular,
                                      String dir, int weight, int minCount, int k, String refseq, double alpha,
                                      boolean keepIntermediateResults, Dataset<Row> recordData) throws Exception {


        String fastaFileName = dir+"/" + name+".fna";
        String bedFileName = dir+"/"+name+".bed";
        // create Fasta File
        createFastaFile(sequence,name,fastaFileName);
        Dataset<Row> sequences = SparkComputer.readORC(ProjectDirectoryManager.getSEQUENCES_DIR());

        Dataset<Row> gmV = SparkComputer.readORC(ProjectDirectoryManager.getPROPERTY_VOIDS_DIR());
//        StopWatch stopwatch = new StopWatch();
//        stopwatch.start();
        SequenceInformationDT seqInfo = new SequenceInformationDT(sequence.length(),name,
                circular, taxId, "");
        System.out.println(seqInfo);

        LOGGER.info("Creating kmer mappings");
        int rec = -1;
//        int rec = RecordDT.mapToRecordId(name);
        SparkComputer.persistDataFrameORC(createKmerDatasets(k,sequence,refseq,seqInfo,rec,recordData),dir + "/KMER_MAPPING_"+refseq);

        Dataset<Row> mappingRanges = Spark.checkPointWithRandomAccessor(createMappingRanges(SparkComputer.readORC(dir + "/KMER_MAPPING_"+refseq), k),seqInfo.getId()).persist();
        bridge(mappingRanges.withColumn(ColumnIdentifier.TOPOLOGY, lit(seqInfo.isTopology())), sequences, weight, seqInfo, sequence, dir);
        LOGGER.info("Bridging CCs");
        Dataset<Row> bridgedRanges = null;
        if (Spark.checkExisting(dir + "/BRIDGED")) {
            bridgedRanges = Spark.checkPointWithRandomAccessor(SparkComputer.readORC(dir + "/BRIDGED"),name).persist();
        }
        LOGGER.info("Annotating");
        createPredictions(bridgedRanges, mappingRanges, gmV, seqInfo, 70, k, k + 1, minCount, dir);
//        stopwatch.stop();
//        long time = stopwatch.getTime();
//        System.out.println(TimeDT.asMinutes(time));
        FileIO.writeToFile(createBEDPropertyAnnotations(alpha,seqInfo,dir+"/CLUSTER",getGeneLengths(), SparkComputer.readORC(dir+"/JOINED_GENE_RANGES"), sequence),
                bedFileName,false,""
        );
        extractFastaFromBED(fastaFileName,bedFileName,dir + "/"+ name+ ".faa");



        Spark.clearDirectory(dir+"/JOINED_GENE_RANGES");
        if(!keepIntermediateResults){
            Spark.clearDirectory(dir+"/CLUSTER");
            Spark.clearDirectory(dir+"/BRIDGED");

            Spark.clearDirectory(dir+"/JOINED_GENE_RANGES_LS");
            Spark.clearDirectory(dir+"/KMER_MAPPING_"+refseq);
        }

        Spark.clearDirectory(Spark.SPARK_CHECKPOINT_DIR+"/"+ seqInfo.getId());

        if(new File(dir+"/"+ name+".fna.fai").exists()) {
            new File(dir+"/"+ name+".fna.fai").delete();
        }
    }


    public static void annotateRefseq(String name, String sequence, int taxId, boolean circular,
                                       String dir, int weight, int minCount, int k, String refseq, double alpha, boolean keepIntermediateResults) throws Exception {

        annotateRefseq(name,sequence,taxId,circular,dir,weight,minCount,k,refseq,alpha,keepIntermediateResults,SparkComputer.readORC(ProjectDirectoryManager.getRECORD_DATA_DIR()));
    }



    public static void annotate(int recordId, String basedir, int weight, int minCount, int k, String id) throws Exception {
        Dataset<Row> sequences = SparkComputer.readORC(basedir+"/SEQUENCES");
        String sequence =sequences.filter(col(ColumnIdentifier.RECORD_ID).equalTo(recordId)).select(col(ColumnIdentifier.SEQUENCE)).as(Encoders.STRING()).first();
        Dataset<Row> recordData = SparkComputer.readORC(ProjectDirectoryManager.getGRAPH_DATA_DIR()+"/RECORD_DATA");
        Row record = recordData.filter(col(ColumnIdentifier.RECORD_ID).equalTo(recordId)).first();

        Dataset<Row> gmV = SparkComputer.readORC(ProjectDirectoryManager.getGRAPH_DATA_DIR()+"/PROPERTY_RANGES_VOIDS_PER_STRAND");

        SequenceInformationDT seqInfo = new SequenceInformationDT(sequence.length(), record.getString(record.fieldIndex(ColumnIdentifier.NAME)),
                record.getBoolean(record.fieldIndex(ColumnIdentifier.TOPOLOGY)),record.getInt(record.fieldIndex(ColumnIdentifier.TAXID)),"");
        System.out.println(seqInfo);
        String dir =basedir+ "/" + recordId;

        StopWatch stopwatch = new StopWatch();
        stopwatch.start();

//        SparkComputer.persistDataFrameORC(createMappingRanges(SparkComputer.readORC(dir+"/KMER_MAPPING"),k)
//                .withColumn(ColumnIdentifier.TOPOLOGY,lit(seqInfo.isTopology())),dir+"/MAPPING_RANGES");
//        Dataset<Row> mappingRanges = SparkComputer.readORC(dir+"/MAPPING_RANGES");
            Dataset<Row> mappingRanges = Spark.checkPointWithRandomAccessor(createMappingRanges(SparkComputer.readORC(dir+"/KMER_MAPPING"),k),id).persist();

        bridge(mappingRanges.withColumn(ColumnIdentifier.TOPOLOGY,lit(seqInfo.isTopology())),sequences,weight,seqInfo,sequence,dir);
        RunTimeDT time = new RunTimeDT(stopwatch.getTime(),recordId+"");
        LOGGER.info("bridging: " + time+"");
        stopwatch.reset();
        stopwatch.start();
        Dataset<Row> bridgedRanges = null;
        if(Spark.checkExisting(dir+"/BRIDGED")){
            bridgedRanges =       Spark.checkPointWithRandomAccessor(SparkComputer.readORC(dir+"/BRIDGED"),id).persist();
        }

        LOGGER.info("Starting to annotate");
        createPredictions(bridgedRanges,mappingRanges,gmV,seqInfo,70,k,k+1,minCount,dir);
        time = new RunTimeDT(stopwatch.getTime(),recordId+"");
        LOGGER.info("predictions: " + time+"");
//        Dataset<Row> predictions = SparkComputer.readORC(dir+"/JOINED_GENE_RANGES");
//        Dataset<Row> predictionsLS = SparkComputer.readORC(dir+"/JOINED_GENE_RANGES_LS");
    }

    public static void clear(String path, String refseq) {
        Spark.clearDirectory(path+"/BRIDGED");
//        TimeUnit.SECONDS.sleep(30);
        Spark.clearDirectory(path+"/CLUSTER");
//        TimeUnit.SECONDS.sleep(30);
        Spark.clearDirectory(path+"/JOINED_GENE_RANGES");
//        TimeUnit.SECONDS.sleep(30);
        Spark.clearDirectory(path+"/JOINED_GENE_RANGES_LS");
//        TimeUnit.SECONDS.sleep(30);
        Spark.clearDirectory(path+"/KMER_MAPPING_"+refseq);

        Spark.clearDirectory(path+"/PREDICTIONS");

    }

    public static void move(String oldPath,String newPath) throws IOException {
        new File(newPath).mkdirs();

        if(new File(oldPath + "/BRIDGED").exists()){
            Files.move(Paths.get(oldPath + "/BRIDGED"), Paths.get(newPath + "/BRIDGED"));
        }
        Files.move(Paths.get(oldPath + "/CLUSTER"), Paths.get(newPath + "/CLUSTER"));
        Files.move(Paths.get(oldPath + "/JOINED_GENE_RANGES"), Paths.get(newPath + "/JOINED_GENE_RANGES"));
        Files.move(Paths.get(oldPath + "/JOINED_GENE_RANGES_LS"), Paths.get(newPath + "/JOINED_GENE_RANGES_LS"));

        if(new File(oldPath + "/EVALUATION").exists()){
            Files.move(Paths.get(oldPath + "/EVALUATION"), Paths.get(newPath + "/EVALUATION"));
        }
    }



}


