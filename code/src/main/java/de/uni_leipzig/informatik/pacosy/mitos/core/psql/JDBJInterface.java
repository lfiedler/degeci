package de.uni_leipzig.informatik.pacosy.mitos.core.psql;

import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.DBColumnName;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.FileIO;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URLEncoder;
import java.sql.*;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Properties;
import java.util.function.Function;


public class JDBJInterface {

    protected static final Logger LOGGER = LoggerFactory.getLogger(JDBJInterface.class);

    private static Properties connectionProperties;

    private static String url;

    private static String JDBC_FORMAT = "jdbc";
    private static String QUERY_TAG = "query";
    private static String URL_TAG = "url";
    private static String DB_TABLE_TAG = "dbtable";
    private static String CONSTRAINT_CREATE_FUNCTION = "complete.create_constraint_if_not_exists";
    private static String CONSTRAINT_CHECK_FUNCTION = "complete.create_constraint_if_not_exists";
    private static Connection con;

    private static Statement stmt;

    private static JDBJInterface jdbjInterface;



    private JDBJInterface() {
        init();
    }

    public static synchronized JDBJInterface getInstance() {
        if(JDBJInterface.jdbjInterface == null) {
            LOGGER.debug("Building new JDBInterface");
            jdbjInterface = new JDBJInterface();
        }
        return JDBJInterface.jdbjInterface;
    }


    public static void closeConnection() {
        try {
            if(con != null) {
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void reset() {

        try {
            closeConnection();
            con = DriverManager.getConnection(url);
            con.setAutoCommit(false);
            stmt = con.createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void rollBack() throws  SQLException {
        con.rollback();
    }



    /***Initialization *******************************************************************/
    private static void initConnectionProperties() {
        connectionProperties = new Properties();

        try {
            connectionProperties.load(new FileInputStream(ProjectDirectoryManager.getACCESS_DIR()+"/psql.properties"));
            connectionProperties.setProperty("password",
                    FileIO.readStringFromFileSkipLines(ProjectDirectoryManager.getACCESS_DIR()+"/postgres-passwd",0).trim());

            url = getURL(connectionProperties);

        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("Could not load psql configuration file");
            System.exit(-1);
        }
    }

    private static void init() {
        initConnectionProperties();
        try {
            con = DriverManager.getConnection(url);
            con.setAutoCommit(false);
            stmt = con.createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    /***Printing *******************************************************************/
    public static void printResults(ResultSet resultSet, String... columnNames) throws SQLException {
        while (resultSet.next()) {
            String result = "";
            for(String columnName: columnNames) {
                result+= resultSet.getObject(columnName) + " ";
            }
            System.out.println(result);
        }
    }

    public static void printResults(ResultSet resultSet, DBColumnName... columnNames) throws SQLException {
        printResults(resultSet,asStringArray(columnNames));
    }

    /***Auxiliary *******************************************************************/
    private static void checkKey(Properties properties, String key) {
        if(!properties.containsKey(key)) {
            LOGGER.error(key+" entry is missing in psql configuration file");
            System.exit(-1);
        }
    }

    private static String getURL(Properties properties) {
        checkKey(properties,"host");
        checkKey(properties,"port");
        checkKey(properties,"db");
        checkKey(properties,"password");
        checkKey(properties,"user");

//        System.out.println("jdbc:postgresql://" + properties.getProperty("host") + ":" + properties.getProperty("port") + "/" +
//                properties.getProperty("db") +"?user="+ properties.getProperty("user")+
//                "&password="+URLEncoder.encode(properties.getProperty("password")));
        return "jdbc:postgresql://" + properties.getProperty("host") + ":" + properties.getProperty("port") + "/" +
                properties.getProperty("db") +"?user="+ properties.getProperty("user")+
                "&password="+URLEncoder.encode(properties.getProperty("password"));
    }

    /***Parsing *******************************************************************/

    public Array parseToSQLArray(Integer [] values) throws SQLException {
        return con.createArrayOf("integer",values);
    }

    public Array parseToSQLArray(String [] values) throws SQLException {
        return con.createArrayOf("TEXT",values);
    }

    public static String[] asStringArray(DBColumnName[] column_names) {

        String [] strings = new String[column_names.length];
        for(int i = 0; i < column_names.length; i++) {

            strings[i] = DBColumnName.rawIdentifier(column_names[i]);
        }
        return strings;
    }

    public static String parseToColumnFormat(String value) {
        return  "(" + value + ")";
    }

    public static String parseToColumnFormat(String... values) {
        String result = "(";
        for(int i = 0; i < values.length; i++) {
            result += values[i] + ",";
        }
        return result.substring(0,result.length()-1) + ")";
    }

    public static <T> String parseToColumnFormat(Collection<T> values, Class<T> tClass) {

        Function<T,String> resultParser = tClass.equals(String.class) ?
                (t -> "'" + t + "'") :
                (t -> t+"");
        String result = "(";


        Iterator<T> it = values.iterator();
        while (it.hasNext()) {
            result += resultParser.apply(it.next()) + ",";
        }
        return result.substring(0,result.length()-1) + ")";
    }

    public static <T> String parseRowListToColumnFormat(Collection<Row> values, Class<T> tClass) {


        Function<Row,String> resultParser = tClass.equals(String.class) ?
                (t) -> {
                    String result = "(";
                    for(int i = 0; i < t.length(); i++) {
                        result += "'" + t.getString(i) +"',";
                    }

                    result= result.substring(0,result.length()-1) + ")";
                    return result;
                } :
                (t) -> {
                    String result = "(";
                    for(int i = 0; i < t.length(); i++) {
                        result +=  t.get(i) +",";
                    }
                    ;
                    result= result.substring(0,result.length()-1)+ ")";
                    return result;
                } ;
        String result = "(";


        Iterator<Row> it = values.iterator();
        while (it.hasNext()) {

            result += resultParser.apply(it.next()) + ",";
        }
        return result.substring(0,result.length()-1) + ")";
    }

    public static String getColumnFormat(String rawColumnName) {
        return "\"" + rawColumnName + "\"";
    }

    /***DSL *******************************************************************/
    public void createTable(String tablename, String values, boolean checkExisting) throws  SQLException {

//        System.out.println("CREATE TABLE IF NOT EXISTS "+ tablename  +
//                "(" +
//                values +
//                ")");
        if(checkExisting) {
//            System.out.println("CREATE TABLE IF NOT EXISTS "+ tablename  +
//                    "(" +
//                    values +
//                    ")");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS "+ tablename  +
                    "(" +
                    values +
                    ")");

        } else {
            stmt.executeUpdate("CREATE TABLE "+ tablename  +
                    "(" +
                    values +
                    ")");

        }
        con.commit();

    }

    public void commit() throws  SQLException {
        con.commit();
    }

    public void execute(String updateQuery) throws  SQLException {
//        System.out.println(updateQuery);
//        try{
//            stmt.executeUpdate(updateQuery);
//            con_.commit();
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//            throw e;
//        }

        stmt.execute(updateQuery);
//        commit();

    }

    public void executeUpdate(String updateQuery) throws  SQLException {
//        System.out.println(updateQuery);
//        try{
//            stmt.executeUpdate(updateQuery);
//            con_.commit();
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//            throw e;
//        }

        stmt.executeUpdate(updateQuery);
        con.commit();
    }

    public void executeQueryUpdate(String query) throws  SQLException{
        stmt.executeQuery(query);
        con.commit();
    }

    public void insertDataIntoTable(String tablename, String values) throws  SQLException {
        if(values.contains("(")) {
            stmt.executeUpdate("INSERT INTO " +tablename+ " VALUES "+ values);
        }
        else {
            stmt.executeUpdate("INSERT INTO " +tablename+ " VALUES( "+ values+ ")");
        }

        con.commit();

    }

    public <T> void insertDataIntoTable(String tablename, Dataset<T> dataset) {
//        dataset.write().format(JDBC_FORMAT).option(URL_TAG,url).option(DB_TABLE_TAG,tablename).save();
        dataset.write().mode("append").format(JDBC_FORMAT).option(URL_TAG,url).option(DB_TABLE_TAG,tablename).save();
    }

    public <T extends Serializable> void insertDataIntoTable(String tablename, T data, Class<T> tClass) {
        Spark.getInstance().
                createDataFrame(Collections.singletonList(data),tClass).
                write().format(JDBC_FORMAT).option(URL_TAG,url).option(DB_TABLE_TAG,tablename).save();
    }


    public void insertDataIntoTableIfNotExists(String tablename, String values) throws  SQLException {
        if(values.contains("(")) {
            stmt.executeUpdate("INSERT INTO " +tablename+ " VALUES "+ values + " ON CONFLICT DO NOTHING");
        }
        else {

            stmt.executeUpdate("INSERT INTO " +tablename+ " VALUES( "+ values+ ") ON CONFLICT DO NOTHING");
        }

        con.commit();

    }

    public void removeDataFromTable(String tablename, String dropStatement) throws  SQLException {
        stmt.executeUpdate("DELETE FROM " +tablename+ " WHERE "+ dropStatement);
        con.commit();
    }

    public void dropTable(String tablename, boolean checkExisting) throws  SQLException {
        if(checkExisting) {
            stmt.executeUpdate("DROP TABLE IF EXISTS "+ tablename );
        } else {
            stmt.executeUpdate("DROP TABLE "+ tablename );
        }

        con.commit();

    }

    public void createPrimaryKey(String tableName, String constraintName, String... columnName) throws SQLException {
        executeUpdate("ALTER TABLE " + tableName +
                " ADD CONSTRAINT " + constraintName +
                " PRIMARY KEY " + parseToColumnFormat(columnName)  +"");
    }

    public void dropPrimaryKey(String tableName, String constraintName) throws SQLException {

        if(!constraintExists(constraintName)) {
            return;
        }
        executeUpdate("ALTER TABLE " + tableName +
                " DROP CONSTRAINT " + constraintName );
    }

    public void dropFunction(String functionName) throws SQLException {
        executeUpdate("DROP FUNCTION IF EXISTS " + functionName + "(property TEXT)");
    }

    private ResultSet existsQuery(String tableName) throws SQLException {

        return executeQuery("SELECT to_regclass('"+ tableName + "')");
    }

    public boolean exists(String tableName) throws SQLException {
        ResultSet resultSet = existsQuery(tableName);
        resultSet.next();
        if(resultSet.getString(1) == null){
            return false;
        }
        return true;

    }
    /***Queries *******************************************************************/
    public Dataset<Row> fetchQueryResult(String query) {
        return Spark.getInstance().read().format(JDBC_FORMAT).
                option(QUERY_TAG,query).
                option(URL_TAG,url).load();

    }

    public <T> Dataset<T> fetchQueryResult(String query, Encoder<T> encoder) {
        return fetchQueryResult(query).as(encoder);
    }

    public <T> Dataset<T> fetchQueryResult(String query, Class<T> tClass) {
//        System.out.println(query);
        return fetchQueryResult(query,Encoders.bean(tClass));
    }

    public Dataset<Row> fetchTable(String tableName) {
        return Spark.getInstance().read().format(JDBC_FORMAT).
                option(DB_TABLE_TAG,tableName).
                option(URL_TAG,url).load();
    }

    public <T> Dataset<T> fetchTable(String tableName, Encoder<T> encoder) {
        return fetchTable(tableName).as(encoder);
    }

    public <T> Dataset<T> fetchTable(String tableName, Class<T> tClass) {
        return fetchTable(tableName,Encoders.bean(tClass));
    }

    public ResultSet executeQuery(String query) throws  SQLException{
        return stmt.executeQuery(query);
    }

    /***Getter and Setter *********************************************************/


    public boolean constraintExists(String constraintName) throws SQLException {


        ResultSet resultSet = executeQuery(
                "SELECT constraint_name FROM information_schema.constraint_column_usage " +
                "intersect SELECT  '"+constraintName +"'");
        return resultSet.next();
//        if(resultSet.getString(1) == null){
//            return false;
//        }
//        return true;
    }

    private void createConstraintCreateFunction ()throws SQLException {
        executeUpdate("create or replace function "+ CONSTRAINT_CREATE_FUNCTION + "(\n" +
                "    t_name text, c_name text, constraint_sql text\n" +
                ") \n" +
                "returns void AS\n" +
                "$$\n" +
                "begin\n" +
                "    if not exists (select constraint_name \n" +
                "                   from information_schema.constraint_column_usage \n" +
                "                   where table_name = t_name  and constraint_name = c_name) then\n" +
                "        execute constraint_sql;\n" +
                "    end if;\n" +
                "end;\n" +
                "$$ language 'plpgsql'");
    }

    private void test(Dataset<Long> dataset) throws SQLException {
        execute("Create temp table t1 (id integer)");
        insertDataIntoTable("t1",dataset);
        ResultSet resultSet = JDBJInterface.getInstance().
                executeQuery("SELECT * FROM t1");
        while(resultSet.next()) {
            System.out.println(resultSet.getInt(1));
        }
//        Dataset<Row> s = Spark.getInstance().sql("SELECT * FROM complete.test ");
////        Dataset<Row> s = Spark.getInstance().sql("SELECT * FROM complete.test t2,t  WHERE t.id = t2.id ");
//        s.show();
    }

}
