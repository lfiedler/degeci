package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping;

import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.ColumnFunctions;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.ColumnIdentifier;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.EvalueParameterDT;
import org.apache.commons.math3.random.MersenneTwister;
import org.apache.spark.sql.*;
import org.apache.spark.sql.api.java.*;
import org.apache.spark.sql.types.*;
import org.biojava.nbio.alignment.Alignments;
import org.biojava.nbio.alignment.SimpleGapPenalty;
import org.biojava.nbio.alignment.template.PairwiseSequenceAligner;
import org.biojava.nbio.core.alignment.matrices.SimpleSubstitutionMatrix;
import org.biojava.nbio.core.alignment.template.AlignedSequence;
import org.biojava.nbio.core.alignment.template.SubstitutionMatrix;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.compound.AmbiguityDNACompoundSet;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.features.FeatureInterface;
import org.biojava.nbio.core.sequence.features.Qualifier;
import org.biojava.nbio.core.sequence.io.FileProxyDNASequenceCreator;
import org.biojava.nbio.core.sequence.io.GenbankReader;
import org.biojava.nbio.core.sequence.io.GenbankSequenceParser;
import org.biojava.nbio.core.sequence.io.GenericGenbankHeaderParser;
import org.biojava.nbio.core.sequence.template.AbstractSequence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

public class PairwiseAligner {



    public enum NUCLEOTIDE{

        A('A'),
        C('C'),
        T('T'),
        G('G'),
        U('U'),
        Y('Y'),
        R('R'),
        S('S'),
        W('W'),
        K('K'),
        M('M'),
        B('B'),
        V('V'),
        D('D'),
        H('H'),
        N('N')
        ;

        public final char identifier;

        public static NUCLEOTIDE get(char n) {
            for(NUCLEOTIDE nuc : NUCLEOTIDE.values()) {
                if(nuc.identifier == n) {
                    return nuc;
                }
            }
            return null;
        }

        NUCLEOTIDE(char identifier) {
            this.identifier = identifier;
        }
    }


    protected static final Logger LOGGER = LoggerFactory.getLogger(PairwiseAligner.class);
    public static int upperDiag = 40;
    public static int lowerDiag = -40;
    public static int open = -2;
    public static int extend = -2;
    public static SimpleGapPenalty gapPenalty = new SimpleGapPenalty(open,extend);
    public static short match = 1;
    public static short missmatch = -2;
    public static int openHOXID = -100;
    public static int extendHOXID = -30;
    protected static String alignmentPath = ProjectDirectoryManager.getSTATISTICS_DIR() + "/evalParameters" ;

    public static EvalueParameterDT evalue = SparkComputer.getFirstOrElseNull(Spark.getInstance().
            read().format("csv") .option("inferSchema", "true")
            .option("header", "true").load(alignmentPath).as(EvalueParameterDT.ENCODER));

    protected static SubstitutionMatrix<NucleotideCompound> substitutionMatrix = new SimpleSubstitutionMatrix(AmbiguityDNACompoundSet.getDNACompoundSet(),match, missmatch);
    public static final String ALIGN_LOCAL_UDF = "alignLocal";
    public static final String ALIGN_LOCAL_FAST_UDF = "alignLocalFast";

    public static final String ALIGN_DEGECCI = "alignDeGeccI";
    public static final String ALIGN_GLOBAL_UDF = "alignGlobal";
    public static final String ALIGN_GLOBAL_FAST_UDF = "alignGlobalFast";
    public static final String ALIGN_LOCAL_SCORE_EVALUE = "alignEvalue";
    public static final String ALIGN_LOCAL_RETAIN_QUALITY = "alignLocalRetainHighQuality";
    public static final String ALIGN_LOCAL_RETAIN_QUALITY_FAST = "alignLocalRetainHighQualityFast";

    public static final String ALIGN_LOCAL_SCORE_UDF = "alignScore";
    public static final String PERCENTAGE_IDENTITY_UDF = "percentageIdentity";
    public static File HOXD_SCORING_MATRIX_FILE = new File(ProjectDirectoryManager.getPROJECT_DIR()+"/HOXD-scoringMatrix");
    //
    public static final StructType ALIGN_STRUCT = new StructType(new StructField[]{
            new StructField(ColumnIdentifier.EVALUE, DoubleType,true, Metadata.empty()),
            new StructField(ColumnIdentifier.SCORE, DoubleType,true, Metadata.empty()),
            new StructField("pQ1", IntegerType,true, Metadata.empty()),
            new StructField("pQ2", IntegerType,true, Metadata.empty()),
            new StructField("pT1", IntegerType,true, Metadata.empty()),
            new StructField("pT2", IntegerType,true, Metadata.empty()),
            new StructField("sQ", StringType,true, Metadata.empty()),
            new StructField("sT", StringType,true, Metadata.empty()),
            new StructField(ColumnIdentifier.IDENTICAL, IntegerType,true, Metadata.empty()),
    });

    public static final StructType ALIGN_STRUCT_SHORT = new StructType(new StructField[]{
            new StructField(ColumnIdentifier.EVALUE, DoubleType,true, Metadata.empty()),
            new StructField(ColumnIdentifier.SCORE, DoubleType,true, Metadata.empty()),
            new StructField("pQ1", IntegerType,true, Metadata.empty()),
            new StructField("pQ2", IntegerType,true, Metadata.empty()),
            new StructField("pT1", IntegerType,true, Metadata.empty()),
            new StructField("pT2", IntegerType,true, Metadata.empty()),
            new StructField(ColumnIdentifier.IDENTICAL, IntegerType,true, Metadata.empty()),
    });

    public static final StructType DEGECCI_ALIGN_STRUCT = new StructType(new StructField[]{
            new StructField(ColumnIdentifier.EVALUE,DataTypes.DoubleType,true, Metadata.empty()),
//            new StructField(ColumnIdentifier.SCORE,DataTypes.DoubleType,true, Metadata.empty()),
            new StructField("aQ", ArrayType.apply(IntegerType),true, Metadata.empty()),
            new StructField("aT", ArrayType.apply(IntegerType),true, Metadata.empty()),
            new StructField("sQ", StringType,true, Metadata.empty()),
            new StructField("sT", StringType,true, Metadata.empty()),
//            new StructField(ColumnIdentifier.IDENTICAL,DataTypes.IntegerType,true, Metadata.empty()),
    });

    public static final StructType ALIGN_STRUCT2 = new StructType(new StructField[]{
            new StructField(ColumnIdentifier.EVALUE, DoubleType,true, Metadata.empty())
    });



    public static char complement(char n){
        char r = 'X';
        switch (n){
            case 'A':
                r= 'T';
                break;
            case 'C':
                r= 'G';
                break;
            case 'G':
                r= 'C';
                break;
            case 'T':
                r= 'A';
                break;

            case 'U':
                r= 'A';
                break;

            case 'Y':
                r= 'R';
                break;

            case 'R':
                r= 'Y';
                break;

            case 'S':
                r= 'S';
                break;

            case 'W':
                r= 'W';
                break;

            case 'K':
                r= 'M';
                break;

            case 'M':
                r= 'K';
                break;

            case 'B':
                r= 'V';
                break;

            case 'V':
                r= 'B';
                break;

            case 'D':
                r= 'H';
                break;

            case 'H':
                r= 'D';
                break;

            case 'N':
                r= 'N';
                break;
            default:
                System.out.println("Invalid nucleotide " + n);
                //System.exit(-1);
        }
        return r;
    }

    public static String reverseComplement(String kmer){
        String revComp = "";
        CharacterIterator it = new StringCharacterIterator(kmer);
        while (it.current() != CharacterIterator.DONE) {
            revComp += complement(it.current());
            it.next();
        }
        StringBuilder in = new StringBuilder();
        in.append(revComp);
        return in.reverse().toString();
    }

    public static int fetchTranslationTable(File gbFile) throws Exception {


        AmbiguityDNACompoundSet.getDNACompoundSet();

        GenbankReader<DNASequence, NucleotideCompound> GenbankProxyReader =
                new GenbankReader<DNASequence, NucleotideCompound>(
                        gbFile,
                        new GenericGenbankHeaderParser<DNASequence, NucleotideCompound>(),
                        new FileProxyDNASequenceCreator(
                                gbFile,
                                AmbiguityDNACompoundSet.getDNACompoundSet(),
                                new GenbankSequenceParser<AbstractSequence<NucleotideCompound>, NucleotideCompound>()
                        )
                );

        System.out.println(gbFile.getName());
//            LinkedHashMap<String, DNASequence> codingSequences = GenbankReaderHelper.readGenbankDNASequence(gbFile,true);
        LinkedHashMap<String, DNASequence> codingSequences = GenbankProxyReader.process();
        int transTable = -1;
        for (DNASequence cds : codingSequences.values()) {
            for (FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound> cdsFeature : cds.getFeatures()) {
                if (cdsFeature.getType().equals("CDS")){

                    Map<String, List<Qualifier>> qualifiers = cdsFeature.getQualifiers();
//                        Auxiliary.printMap(qualifiers);

                    List<Qualifier> geneNameList = qualifiers.get("transl_table");
                    if (geneNameList != null) {
                        for (Qualifier geneName : geneNameList){
                            transTable = Integer.parseInt(geneName.getValue());

                        }
                    }
                }
            }
        }
        if(transTable == -1) {
            throw new RuntimeException("Translation table not found");
        }
        return transTable;
    }

    public static Dataset<Row> addReverseComplementSequences(Dataset<Row> sequences) {
        return sequences.withColumn(ColumnIdentifier.RC_SEQUENCE,reverse(translate(col(ColumnIdentifier.SEQUENCE),
                "A" +
                        "C" +
                        "T" +
                        "G" +
                        "U" +
                        "Y" +
                        "R" +
                        "S" +
                        "W" +
                        "K" +
                        "M" +
                        "B" +
                        "V" +
                        "D" +
                        "H" +
                        "N",

                "T" +
                        "G" +
                        "A" +
                        "C" +
                        "A" +
                        "R" +
                        "Y" +
                        "S" +
                        "W" +
                        "M" +
                        "K" +
                        "V" +
                        "B" +
                        "H" +
                        "D" +
                        "N")));
    }

    public static double getMinScore( Alignments.PairwiseSequenceAlignerType type, int l1, int l2) {
        if(type.equals(Alignments.PairwiseSequenceAlignerType.GLOBAL)) {
            return 2*open+(l1+l2)*extend;
        }
        else {
            return 0;
        }
    }

    public static double getMaxScore( int l1, int l2) {
        return Math.max(l1,l2)*match;
    }

    public static double getSimilarity(Alignments.PairwiseSequenceAlignerType type, int l1, int l2, double score) {
        double minScore = getMinScore(type,l1,l2);
        return (score-minScore)/(getMaxScore(l1,l2)-minScore);
    }

    public static void setEvalueParameters(String path) {
        alignmentPath = path;
        evalue = SparkComputer.getFirstOrElseNull(Spark.getInstance().read().format("csv") .option("inferSchema", "true")
                .option("header", "true").load(alignmentPath).as(EvalueParameterDT.ENCODER));
    }

    public static UDF2<String,String,Double> alignScore = (s1, s2) -> {
        if(s1.length() == 0 || s2.length() == 0) return 0.0;
        DNASequence query = new DNASequence(s1, AmbiguityDNACompoundSet.getDNACompoundSet());
        query.setDNAType(DNASequence.DNAType.MITOCHONDRIAL);
        DNASequence target = new DNASequence(s2, AmbiguityDNACompoundSet.getDNACompoundSet());
        target.setDNAType(DNASequence.DNAType.MITOCHONDRIAL);

        PairwiseSequenceAligner<DNASequence, NucleotideCompound> aligner =
                Alignments.getPairwiseAligner(query, target, Alignments.PairwiseSequenceAlignerType.GLOBAL, gapPenalty, substitutionMatrix);
        return aligner.getSimilarity();
    };

    public static UDF2<String,String,Double> alignEValue = (s1, s2) -> {
        if(s1.length() == 0 || s2.length() == 0) return Double.MAX_VALUE;
        DNASequence query = new DNASequence(s1, AmbiguityDNACompoundSet.getDNACompoundSet());
        query.setDNAType(DNASequence.DNAType.MITOCHONDRIAL);
        DNASequence target = new DNASequence(s2, AmbiguityDNACompoundSet.getDNACompoundSet());
        target.setDNAType(DNASequence.DNAType.MITOCHONDRIAL);

        PairwiseSequenceAligner<DNASequence, NucleotideCompound> aligner =
                Alignments.getPairwiseAligner(query, target, Alignments.PairwiseSequenceAlignerType.GLOBAL, gapPenalty, substitutionMatrix);
        return evalue.eValue(s1.length(),s2.length(),(int)aligner.getScore());
    };

    public static PairwiseSequenceAligner<DNASequence, NucleotideCompound> align(String queryStr, String targetStr,  Alignments.PairwiseSequenceAlignerType type,
                                                                                 int open, int extend, short match, short mismatch) throws CompoundNotFoundException {
        if(queryStr.length() == 0 || targetStr.length() == 0) return null;
        DNASequence query = new DNASequence(queryStr, AmbiguityDNACompoundSet.getDNACompoundSet());
        query.setDNAType(DNASequence.DNAType.MITOCHONDRIAL);
        DNASequence target = new DNASequence(targetStr, AmbiguityDNACompoundSet.getDNACompoundSet());
        target.setDNAType(DNASequence.DNAType.MITOCHONDRIAL);

        PairwiseSequenceAligner<DNASequence, NucleotideCompound> aligner =
                Alignments.getPairwiseAligner(query, target, type, new SimpleGapPenalty(open,extend), new SimpleSubstitutionMatrix(AmbiguityDNACompoundSet.getDNACompoundSet(),match,mismatch));
        return aligner;
    }

    public static Row alignDeGeccI(String queryStr, String targetStr, double evalueCutoff) throws CompoundNotFoundException {

        if(queryStr.length() == 0 || targetStr.length() == 0) return RowFactory.create(null,null,null,null);


        DNASequence query = new DNASequence(queryStr, AmbiguityDNACompoundSet.getDNACompoundSet());
        query.setDNAType(DNASequence.DNAType.MITOCHONDRIAL);
        DNASequence target = new DNASequence(targetStr, AmbiguityDNACompoundSet.getDNACompoundSet());
        target.setDNAType(DNASequence.DNAType.MITOCHONDRIAL);

        PairwiseSequenceAligner<DNASequence, NucleotideCompound> aligner =
                Alignments.getPairwiseAligner(query, target, Alignments.PairwiseSequenceAlignerType.LOCAL, gapPenalty, substitutionMatrix);

        AlignedSequence<DNASequence, NucleotideCompound> queryAlignment = aligner.getPair().getQuery();
        AlignedSequence<DNASequence, NucleotideCompound> targetAlignment = aligner.getPair().getTarget();

        double e = evalue.eValue(queryStr.length(),targetStr.length(),(int)aligner.getScore());
        if(e > evalueCutoff){
            return RowFactory.create(null,null,null,null);
        }

        int[] queryP;
        int[] targetP;
        try {
            queryP = queryAlignment.getSequenceFromAlignment();
            targetP = targetAlignment.getSequenceFromAlignment();
        } catch (NullPointerException nullPointerException) {

            return RowFactory.create(null,null,null,null);
        }

        return RowFactory.create(e,
                queryP,targetP,
                queryAlignment.toString(),targetAlignment.toString());
    }

    /**
     * s1 is query, s2 is target
     */
    public static UDF3<String,String, Double, Row> alignDeGECCI = (s1, s2, e) -> {

        return alignDeGeccI(s1,s2,e);
    };

    public static UDF2<String,String,Double> percentageIdentity = (UDF2<String, String, Double>) (s1,s2) -> {
        DNASequence query = new DNASequence(s1, AmbiguityDNACompoundSet.getDNACompoundSet());
        query.setDNAType(DNASequence.DNAType.MITOCHONDRIAL);
        DNASequence target = new DNASequence(s2, AmbiguityDNACompoundSet.getDNACompoundSet());
        target.setDNAType(DNASequence.DNAType.MITOCHONDRIAL);

        PairwiseSequenceAligner<DNASequence, NucleotideCompound> aligner =
                Alignments.getPairwiseAligner(query, target, Alignments.PairwiseSequenceAlignerType.GLOBAL, gapPenalty, substitutionMatrix);
        return aligner.getPair().getPercentageOfIdentity(true);
    };
//    {
//        Spark.getInstance().udf().register(ALIGN_UDF,align, DataTypes.DoubleType);
//    }

    public static Dataset<Row> addSubsequencesToRight(Dataset<Row> sequences, Dataset<Row> dataset, int distance, String position, String record) {
        return addSubsequencesToRight(sequences,dataset,distance,col(position),col(record));
    }

    /**
     *
     * @param sequences
     * @param dataset
     * @param distance size of subsequence to extract
     * @param position zero based
     * @param record
     * @return
     */
    public static Dataset<Row> addSubsequencesToRight(Dataset<Row> sequences, Dataset<Row> dataset, Column distance, Column position, Column record) {
        return dataset.join(sequences.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),record.equalTo(col("r"))).
                drop("r").
                withColumn("p",position.plus(1)).
                withColumn("l",length(col(ColumnIdentifier.SEQUENCE))).
                withColumn(ColumnIdentifier.SUBSEQUENCE,
                        expr("substring("+ColumnIdentifier.SEQUENCE+","+
                                col("p") + "," + distance+")")).
                withColumn(ColumnIdentifier.SUBSEQUENCE,
                        when(col("p").plus(distance).gt(col("l")),
                                concat_ws("",col(ColumnIdentifier.SUBSEQUENCE),
                                        expr("substring("+ColumnIdentifier.SEQUENCE+","+
                                                lit(1) + "," + (col("p").plus(distance)).minus(col("l"))+")"))).
                                otherwise(col(ColumnIdentifier.SUBSEQUENCE))).
                drop(ColumnIdentifier.SEQUENCE,"l","d","p");
//               drop(ColumnIdentifier.SEQUENCE,"l","d","p");
    }

    /**
     *
     * @param sequences
     * @param dataset
     * @param distance size of subsequence to extract
     * @param position zero based
     * @param record
     * @return
     */
    public static Dataset<Row> addSubsequencesToRight(Dataset<Row> sequences, Dataset<Row> dataset, int distance, Column position, Column record) {

        return addSubsequencesToRight2(sequences,dataset,lit(distance),position,record);

//       return dataset.join(sequences.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),record.equalTo(col("r"))).
//                drop("r").
//               withColumn("p",position.plus(1)).
//               withColumn("d",lit(distance)).
//               withColumn("l",length(col(ColumnIdentifier.SEQUENCE))).
//               withColumn(ColumnIdentifier.SUBSEQUENCE,
//                               expr("substring("+ColumnIdentifier.SEQUENCE+","+
//                                       col("p") + "," + col("d")+")")).
//                withColumn(ColumnIdentifier.SUBSEQUENCE,
//                        when(col("p").plus(col("d")).gt(col("l")),
//                        concat_ws("",col(ColumnIdentifier.SUBSEQUENCE),
//                                expr("substring("+ColumnIdentifier.SEQUENCE+","+
//                                        lit(1) + "," + (col("p").plus(col("d"))).minus(col("l")).minus(1)+")"))).
//                                otherwise(col(ColumnIdentifier.SUBSEQUENCE))).
//               drop(ColumnIdentifier.SEQUENCE,"l","d","p");
//               drop(ColumnIdentifier.SEQUENCE,"l","d","p");
    }

    public static Dataset<Row> addSubsequencesToRight2(Dataset<Row> sequences, Dataset<Row> dataset, Column distance, Column position, Column record) {

        return dataset.

                join(sequences.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),record.equalTo(col("r"))).
                drop("r").

                withColumn(ColumnIdentifier.SEQUENCE,concat_ws("",col(ColumnIdentifier.SEQUENCE),col(ColumnIdentifier.SEQUENCE))).
                withColumn(ColumnIdentifier.SUBSEQUENCE,ColumnFunctions.substring(col(ColumnIdentifier.SEQUENCE),position,distance)).
                drop(ColumnIdentifier.SEQUENCE);

//       return dataset.join(sequences.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),record.equalTo(col("r"))).
//                drop("r").
//               withColumn("p",position.plus(1)).
//               withColumn("d",lit(distance)).
//               withColumn("l",length(col(ColumnIdentifier.SEQUENCE))).
//               withColumn(ColumnIdentifier.SUBSEQUENCE,
//                               expr("substring("+ColumnIdentifier.SEQUENCE+","+
//                                       col("p") + "," + col("d")+")")).
//                withColumn(ColumnIdentifier.SUBSEQUENCE,
//                        when(col("p").plus(col("d")).gt(col("l")),
//                        concat_ws("",col(ColumnIdentifier.SUBSEQUENCE),
//                                expr("substring("+ColumnIdentifier.SEQUENCE+","+
//                                        lit(1) + "," + (col("p").plus(col("d"))).minus(col("l")).minus(1)+")"))).
//                                otherwise(col(ColumnIdentifier.SUBSEQUENCE))).
//               drop(ColumnIdentifier.SEQUENCE,"l","d","p");
//               drop(ColumnIdentifier.SEQUENCE,"l","d","p");
    }

    /**
     *
     * @param sequences
     * @param dataset
     * @param distance size of subsequence to extract
     * @param position zero based
     * @param record
     * @return
     */
    public static Dataset<Row> addSubsequencesToRight(Dataset<Row> sequences, Dataset<Row> dataset, Column distance, Column position, Column record,Column strand,Column length) {

        return dataset.

                join(sequences.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),record.equalTo(col("r"))).
                drop("r").

                withColumn(ColumnIdentifier.SEQUENCE,concat_ws("",col(ColumnIdentifier.SEQUENCE),col(ColumnIdentifier.SEQUENCE))).
                withColumn(ColumnIdentifier.SUBSEQUENCE,
                        when(strand,
                                ColumnFunctions.substring(col(ColumnIdentifier.SEQUENCE),position.plus(1),distance))
                                .otherwise(ColumnFunctions.substring(col(ColumnIdentifier.SEQUENCE),length.minus(position).plus(1),distance))).
                drop(ColumnIdentifier.SEQUENCE);

//       return dataset.join(sequences.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),record.equalTo(col("r"))).
//                drop("r").
//               withColumn("p",position.plus(1)).
//               withColumn("d",lit(distance)).
//               withColumn("l",length(col(ColumnIdentifier.SEQUENCE))).
//               withColumn(ColumnIdentifier.SUBSEQUENCE,
//                               expr("substring("+ColumnIdentifier.SEQUENCE+","+
//                                       col("p") + "," + col("d")+")")).
//                withColumn(ColumnIdentifier.SUBSEQUENCE,
//                        when(col("p").plus(col("d")).gt(col("l")),
//                        concat_ws("",col(ColumnIdentifier.SUBSEQUENCE),
//                                expr("substring("+ColumnIdentifier.SEQUENCE+","+
//                                        lit(1) + "," + (col("p").plus(col("d"))).minus(col("l")).minus(1)+")"))).
//                                otherwise(col(ColumnIdentifier.SUBSEQUENCE))).
//               drop(ColumnIdentifier.SEQUENCE,"l","d","p");
//               drop(ColumnIdentifier.SEQUENCE,"l","d","p");
    }

    public static Dataset<Row> addSubsequencesToLeft(Dataset<Row> sequences, Dataset<Row> dataset, int k, int distance, String position, String record) {
        return addSubsequencesToLeft(sequences,dataset,k,distance,col(position),col(record));
    }

    public static Dataset<Row> addSubsequencesToLeft(Dataset<Row> sequences, Dataset<Row> dataset, int k, int distance, Column position, Column record) {

        return dataset.

                join(sequences.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),record.equalTo(col("r"))).
                drop("r").
                withColumn("p",position.plus(length(col(ColumnIdentifier.SEQUENCE)))).
                withColumn(ColumnIdentifier.SEQUENCE,concat_ws("",col(ColumnIdentifier.SEQUENCE),col(ColumnIdentifier.SEQUENCE))).
                withColumn(ColumnIdentifier.SUBSEQUENCE,reverse(ColumnFunctions.substring(col(ColumnIdentifier.SEQUENCE),col("p").minus(distance-1),lit(distance)))).
                drop("p",ColumnIdentifier.SEQUENCE);
//
//        return dataset.join(sequences.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),record.equalTo(col("r"))).
//                drop("r").
////                withColumn("p",position.minus(k)).
//                withColumn("p",position.plus(1)).
//                withColumn("d",lit(distance)).
//                withColumn("l",length(col(ColumnIdentifier.SEQUENCE))).
////                        withColumn("c",col("d").plus(col("p").minus(col("d")))).
////                withColumn("s",col("p").minus(col("d")) ).
//                withColumn(ColumnIdentifier.SUBSEQUENCE,
//                        expr("substring("+ColumnIdentifier.SEQUENCE+","+
//                                col("p").minus(col("d")).plus(1) + "," + col("d")+")")).
//                withColumn(ColumnIdentifier.SUBSEQUENCE,
//                        reverse(when(col("p").minus(col("d")).plus(1).lt(0),
//                                concat_ws("",
//                                        col(ColumnIdentifier.SUBSEQUENCE),
//                                        expr("substring("+ColumnIdentifier.SEQUENCE+","+
//                                        lit(1) + "," + col("d").plus(col("p").minus(col("d")).plus(1))+")"))
//                        ).
//                                otherwise(col(ColumnIdentifier.SUBSEQUENCE)))).
//
////                        drop(ColumnIdentifier.SEQUENCE),"l";
//                drop(ColumnIdentifier.SEQUENCE,"l","d","p");
    }

    public static Dataset<Row> addSubsequencesToLeft(Dataset<Row> sequences, Dataset<Row> dataset, int k, int distance, Column position, Column record, Column strand) {
        return dataset.join(sequences.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),record.equalTo(col("r"))).
                drop("r").
//                withColumn("p",position.minus(k)).
        withColumn("p",position.plus(1)).
                withColumn("d",lit(distance)).
                withColumn("l",length(col(ColumnIdentifier.SEQUENCE))).
//                        withColumn("c",col("d").plus(col("p").minus(col("d")))).
//                withColumn("s",col("p").minus(col("d")) ).
        withColumn(ColumnIdentifier.SUBSEQUENCE,
        expr("substring("+ColumnIdentifier.SEQUENCE+","+
                col("p").minus(col("d")).plus(1) + "," + col("d")+")")).
                withColumn(ColumnIdentifier.SUBSEQUENCE,
                        reverse(when(col("p").minus(col("d")).plus(1).lt(0),
                                concat_ws("",
                                        col(ColumnIdentifier.SUBSEQUENCE),
                                        expr("substring("+ColumnIdentifier.SEQUENCE+","+
                                                lit(1) + "," + col("d").plus(col("p").minus(col("d")).plus(1))+")"))
                        ).
                                otherwise(col(ColumnIdentifier.SUBSEQUENCE)))).

//                        drop(ColumnIdentifier.SEQUENCE),"l";
        drop(ColumnIdentifier.SEQUENCE,"l","d","p");
    }

    // shuffled scores based on all persisted data
    public static void getShuffledScores(final Map<NUCLEOTIDE,Long> nucleotideDistribution, int sequenceLength, String directory,
                                         Alignments.PairwiseSequenceAlignerType type,
                                         int open, int extend, short match, short mismatch) throws CompoundNotFoundException {

        long seed = System.currentTimeMillis();
        long randNum;
        long cummulativeSum = 0;
        StringBuilder s = new StringBuilder();
        String [] sequences = new String[2];
        int sequenceIndex = 0;
        Long currentCount;
        int counter = 0;
        String nucleotide;




        Map<NUCLEOTIDE,Long> nucleotideDistributionCopy =
                nucleotideDistribution.entrySet().stream().
                        collect(Collectors.toMap(n -> n.getKey(), n -> n.getValue()));

        MersenneTwister mersenneTwister = new MersenneTwister(seed);
        List<Row> scores = new ArrayList<>();
        long nucleotideCount = nucleotideDistributionCopy.values().stream().mapToLong( n -> n).sum();
        while (nucleotideCount > 0) {
            randNum = (long) (mersenneTwister.nextDouble()*nucleotideCount);
            cummulativeSum = 0;
            for(NUCLEOTIDE n: nucleotideDistributionCopy.keySet()) {
                if(randNum < cummulativeSum+nucleotideDistributionCopy.get(n)) {
                    s.append(n.identifier);
                    nucleotideCount--;
                    currentCount = nucleotideDistributionCopy.get(n);
                    if(currentCount == null) {
                        currentCount = 0L;
                    }
                    nucleotideDistributionCopy.put(n,currentCount-1);
                    break;
                }
                cummulativeSum+=nucleotideDistributionCopy.get(n);
            }
            // store sequence if length is achieved
            if(s.length() == sequenceLength) {
                // get a copy of the current sequence
                sequences[sequenceIndex] = s.toString();
                //reset stringbuilder
                s.setLength(0);
                if(sequenceIndex == 1){
                    // compute the alignment based on s1 and s2 and write the result into resultFile
                    PairwiseSequenceAligner<DNASequence, NucleotideCompound> aliger = align(sequences[0],sequences[1],type,open,extend,match,mismatch);
                    scores.add(RowFactory.create(aliger.getScore(),aliger.getPair().getQuery().getSequenceFromAlignment().length));
                    counter++;
                    sequenceIndex = 0;
                }
                else {
                    sequenceIndex++;
                }
//                System.out.println(nucleotideCount);
            }

        }

        directory = directory+ (type.equals(Alignments.PairwiseSequenceAlignerType.GLOBAL) ? "/global" : "/local");
//        Dataset<Double> sccores = Spark.getInstance().createDataset(scores,Encoders.DOUBLE());
        Dataset<Row> result = Spark.getInstance().createDataFrame(scores,new StructType(new StructField[]{
                new StructField(ColumnIdentifier.SCORE, DoubleType,false,Metadata.empty()),
                new StructField("l", IntegerType,false,Metadata.empty())}));
//        sccores.show();
        SparkComputer.persistDataFrameORCAppend(result,directory+"_SCORES_o"+open+"_e"+extend+"_match"+ match+"_missmatch"+mismatch+ "_l"+ sequenceLength);



    }

    public static void getShuffledScores(Map<NUCLEOTIDE,Long> nucleotideCounts, int sequenceLength, int runs, String directory,
                                         Alignments.PairwiseSequenceAlignerType type,
                                         int open, int extend, short match, short mismatch) throws CompoundNotFoundException {

        for(int runCount = 0; runCount < runs; runCount++) {


//                System.out.println(runCount);
            getShuffledScores(nucleotideCounts,sequenceLength,directory,type,open,extend,match,mismatch);

        }

    }

    public static void getShuffledScores(Dataset<Row> sequences, int sequenceLength, int runs, String directory,
                                         Alignments.PairwiseSequenceAlignerType type,
                                         int open, int extend, short match, short mismatch) throws CompoundNotFoundException{
        List<Row> nucleotideComposition = sequences.withColumn("s",explode(split(col(ColumnIdentifier.SEQUENCE),""))).
                filter(col("s").notEqual("")).
                groupBy("s").count().collectAsList();
        Map<NUCLEOTIDE,Long> nucleotideCounts = new HashMap<>();

        for(Row r: nucleotideComposition) {
            nucleotideCounts.put(NUCLEOTIDE.get(r.getString(r.fieldIndex("s")).charAt(0)),r.getLong(r.fieldIndex(ColumnIdentifier.COUNT)));
        }
        getShuffledScores(nucleotideCounts,sequenceLength,runs,directory,type,open,extend,match,mismatch);

    }

}
