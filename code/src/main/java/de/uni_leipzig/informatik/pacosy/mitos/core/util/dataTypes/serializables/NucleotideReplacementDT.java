package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class NucleotideReplacementDT implements Serializable {
    public static final Encoder<NucleotideReplacementDT> ENCODER = Encoders.bean(NucleotideReplacementDT.class);

    protected int recordId;
    protected int position;
    protected String nPrev;
    protected String nNew;

    public NucleotideReplacementDT() {
    }


    public NucleotideReplacementDT(int recordId, int position, String nPrev, String nNew) {
        this.recordId = recordId;
        this.position = position;
        this.nPrev = nPrev;
        this.nNew = nNew;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getnPrev() {
        return nPrev;
    }

    public void setnPrev(String nPrev) {
        this.nPrev = nPrev;
    }

    public String getnNew() {
        return nNew;
    }

    public void setnNew(String nNew) {
        this.nNew = nNew;
    }

    @Override
    public String toString() {
        return "NucleotideReplacementDT{" +
                "recordId=" + recordId +
                ", position=" + position +
                ", nPrev='" + nPrev + '\'' +
                ", nNew='" + nNew + '\'' +
                '}';
    }
}
