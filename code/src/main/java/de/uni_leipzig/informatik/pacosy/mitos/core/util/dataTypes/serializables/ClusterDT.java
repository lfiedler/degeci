package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class ClusterDT implements Serializable {

    protected int clusterID;
    protected int position;
    public static final Encoder<ClusterDT> ENCODER = Encoders.bean(ClusterDT.class);

    public ClusterDT() {
    }

    public ClusterDT(int clusterID, int position) {
        this.clusterID = clusterID;
        this.position = position;
    }

    public int getClusterID() {
        return clusterID;
    }

    public void setClusterID(int clusterID) {
        this.clusterID = clusterID;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
