package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.RecordIdInterface;
import org.apache.spark.sql.*;


import java.io.Serializable;

import static org.apache.spark.sql.functions.*;

public class TaxonomicGroupMappingDT implements Serializable, RecordIdInterface {
    private int recordId;
    private String name;
    private String taxonomicGroupName;
    private int taxId;
    public static final Encoder<TaxonomicGroupMappingDT> ENCODER = Encoders.bean(TaxonomicGroupMappingDT.class);
    public static Dataset<TaxonomicGroupMappingDT> taxonomicGroupDataset;

    public TaxonomicGroupMappingDT() {
    }

    public TaxonomicGroupMappingDT(int recordId, String name, String taxonomicGroupName, int taxId) {
        this.recordId = recordId;
        this.name = name;
        this.taxonomicGroupName = taxonomicGroupName;
        this.taxId = taxId;
    }

    public static Dataset<TaxonomicGroupMappingDT> fetchTaxonomicGroupMapping() {
        if(taxonomicGroupDataset == null) {
            taxonomicGroupDataset = SparkComputer.read(
                    ProjectDirectoryManager.getTAXONOMIC_GROUPS_MAPPING_DIR() + SparkComputer.PARQUET_PATH,
                    TaxonomicGroupMappingDT.ENCODER);
        }
        return taxonomicGroupDataset;
    }

    @Override
    public int getRecordId() {
        return recordId;
    }

    @Override
    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaxonomicGroupName() {
        return taxonomicGroupName;
    }

    public void setTaxonomicGroupName(String taxonomicGroupName) {
        this.taxonomicGroupName = taxonomicGroupName;
    }

    public int getTaxId() {
        return taxId;
    }

    public void setTaxId(int taxId) {
        this.taxId = taxId;
    }

    public static Dataset<Row> joinWithTaxonomicGroups(Dataset<?> dataset) {
        Dataset<TaxonomicGroupMappingDT> taxonomicGroupDataset = fetchTaxonomicGroupMapping();

        return dataset.join(taxonomicGroupDataset.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),
                col(ColumnIdentifier.RECORD_ID).
                        equalTo(col("r"))
        ).drop(col("r"));

    }

    public static Dataset<Row> joinWithTaxonomicGroupsKeepColumns(Dataset<?> dataset, Column... keepCol) {
        Dataset<TaxonomicGroupMappingDT> taxonomicGroupDataset = fetchTaxonomicGroupMapping();
        String[] cols = dataset.columns();
        return dataset.join(taxonomicGroupDataset.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),
                col(ColumnIdentifier.RECORD_ID).
                        equalTo(col("r"))
        ).drop(col("r")).select(SparkComputer.getJoinedColumns(SparkComputer.getColumns(cols),keepCol));

    }

    public static Dataset<Row> leftJoinWithTaxonomicGroups(Dataset<?> dataset) {
        Dataset<TaxonomicGroupMappingDT> taxonomicGroupDataset = fetchTaxonomicGroupMapping();
        return dataset.join(taxonomicGroupDataset.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),
                col(ColumnIdentifier.RECORD_ID).
                        equalTo(col("r"))
                ,SparkComputer.JOIN_TYPES.LEFT
        ).drop(col("r")).na().fill("other",new String[]{ColumnIdentifier.TAXONOMIC_GROUP_NAME});

    }

    /**
     *
     * @param dataset
     * @param recordIdIdentifier how column recordId is named in dataset
     * @param appendIdentifier what to append to taxonomicgroup dataset during join
     * @return
     */
    public static Dataset<Row> joinWithTaxonomicGroups(Dataset<?> dataset, String recordIdIdentifier, String appendIdentifier) {
        Dataset<TaxonomicGroupMappingDT> taxonomicGroupDataset = fetchTaxonomicGroupMapping();
        return dataset.join(SparkComputer.appendIdentifier(taxonomicGroupDataset,appendIdentifier).
                        withColumnRenamed(ColumnIdentifier.RECORD_ID+appendIdentifier,"r"),
                col(recordIdIdentifier).
                        equalTo(col("r"))
        ).drop(col("r"));

    }

    /**
     *
     * @param dataset
     * @param recordIdIdentifier how column recordId is named in dataset
     * @param appendIdentifier what to append to taxonomicgroup dataset during join
     * @return
     */
    public static Dataset<Row> leftJoinWithTaxonomicGroups(Dataset<?> dataset, String recordIdIdentifier, String appendIdentifier) {
        Dataset<TaxonomicGroupMappingDT> taxonomicGroupDataset = fetchTaxonomicGroupMapping();
        return dataset.join(SparkComputer.appendIdentifier(taxonomicGroupDataset,appendIdentifier).
                        withColumnRenamed(ColumnIdentifier.RECORD_ID+appendIdentifier,"r"),
                col(recordIdIdentifier).
                        equalTo(col("r"))
        ).drop(col("r"));

    }


}
