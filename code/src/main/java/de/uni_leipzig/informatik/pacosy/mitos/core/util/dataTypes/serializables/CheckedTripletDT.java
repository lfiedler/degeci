package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class CheckedTripletDT extends TripletDT implements Serializable {

    protected  int lca;
    protected int taxonomyStatus;
    public static final Encoder<CheckedTripletDT> ENCODER = Encoders.bean(CheckedTripletDT.class);

    public CheckedTripletDT() {
    }

    public CheckedTripletDT(int t1, int t2, int t3, int amount, int lca, int taxonomyStatus) {
        super(t1, t2, t3, amount);
        this.lca = lca;
        this.taxonomyStatus = taxonomyStatus;
    }

    public CheckedTripletDT(TripletDT triplet,  int lca, int taxonomyStatus) {
        this.t1 = triplet.getT1();
        this.t2 = triplet.getT2();
        this.t3 = triplet.getT3();
        this.lca = lca;
        this.amount = triplet.amount;

        this.taxonomyStatus = taxonomyStatus;
    }
    public int getT1() {
        return t1;
    }

    public void setT1(int t1) {
        this.t1 = t1;
    }

    public int getT2() {
        return t2;
    }

    public void setT2(int t2) {
        this.t2 = t2;
    }

    public int getT3() {
        return t3;
    }

    public void setT3(int t3) {
        this.t3 = t3;
    }

    public int getTaxonomyStatus() {
        return taxonomyStatus;
    }

    public void setTaxonomyStatus(int taxonomyStatus) {
        this.taxonomyStatus = taxonomyStatus;
    }

    @Override
    public String toString() {
        return "(" + t1 + "," + t2 + "|" + t3 + "): " + taxonomyStatus;
    }
}
