#!/bin/bash

# Returns the absolute path of this script regardless of symlinks
abs_path() {
    # From: http://stackoverflow.com/a/246128
    #   - To resolve finding the directory after symlinks
    SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do
        DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
    done
    echo "$( cd -P "$( dirname "$SOURCE" )" && pwd )"
}

BIN=`abs_path`
DB_COPY_FILE=
PSQL_HOST=127.0.0.1
PSQL_PORT=4242
PSQL_PW="supersecretpswd"
PSQL_ACCESSION_FILE=${BIN}/accessProperties/psql.properties
REFSEQ_89=FALSE
REFSEQ_204=FALSE
PROGRAM_DIR=${BIN}/code
USE_DOCKER=FALSE

COMMAND=
VALID_OPTS=h;
# Options requiring an argument
#VALID_LONG_OPTS+=,psql-host:;
VALID_LONG_OPTS+=,psql-port:;
VALID_LONG_OPTS+=,psql-pw:;


# No argument options
VALID_LONG_OPTS+=,help;
VALID_LONG_OPTS+=,refseq89;
VALID_LONG_OPTS+=,refseq204;
VALID_LONG_OPTS+=,build;
VALID_LONG_OPTS+=,init;
VALID_LONG_OPTS+=,no-docker;

show_psql_accession_data() {
    echo "Accession data:" $PSQL_HOST $PSQL_PORT $PSQL_PW
}

set_psql_accession_data2() {
    sed -E -i -e "s#(host=).*#\1${PSQL_HOST}#"  $PSQL_ACCESSION_FILE
    sed -E -i -e "s#(port=).*#\1${PSQL_PORT}#"  $PSQL_ACCESSION_FILE
    sed -E -i -e "s#(password=).*#\1${PSQL_PW}#"  $PSQL_ACCESSION_FILE
    
}

set_psql_accession_data() {
    #sed -E -i -e "s#(host=).*#\1${PSQL_HOST}#"  $PSQL_ACCESSION_FILE
    sed -E -i -e "s#(port=).*#\1${PSQL_PORT}#"  $PSQL_ACCESSION_FILE
    sed -E -i -e "s#([0-9]+:)(5432)#${PSQL_PORT}:\2#" ${BIN}/docker-compose.yaml

    if [ "$PSQL_PW" = "supersecretpswd" ];
    then
	echo "generating random password"
	command -v openssl >/dev/null 2>&1 || { echo >&2 " openssl not found! Please install or provide a password. Then try again! aborting"; exit 1;}
	PSQL_PW=$(openssl rand -base64 20)
	echo $PSQL_PW > ${BIN}/accessProperties/postgres-passwd
    fi



    if [ ${REFSEQ_204} != TRUE ];
    then
	#echo "commenting 204"
	sed -E -i -e "s|.*(      )(- ./db/refseq204Dump:/var/lib/postgresql/refseq204Dump)|\1#\2|" ./docker-compose.yaml
    else
	sed -E -i -e "s|(.*)#(- ./db/refseq204Dump:/var/lib/postgresql/refseq204Dump)|\1\2|" ./docker-compose.yaml
    fi

    if [ ${REFSEQ_89} != TRUE ];
    then
	#echo "commenting 89"
	sed -E -i -e "s|.*(      )(- ./db/refseq89Dump:/var/lib/postgresql/refseq89Dump)|\1#\2|" ./docker-compose.yaml
    else
	sed -E -i -e "s|(.*)#(- ./db/refseq89Dump:/var/lib/postgresql/refseq89Dump)|\1\2|" ./docker-compose.yaml
    fi    
    sed -E -i -e "s#(REFSEQ_204=)(.*)#\1${REFSEQ_204}\"#" ${BIN}/docker-compose.yaml
    sed -E -i -e "s#(REFSEQ_89=)(.*)#\1${REFSEQ_89}\"#" ${BIN}/docker-compose.yaml


    set_psql_accession_data2
    show_psql_accession_data
}


create_db_from_copy() {
    if [ "$PSQL_PW" = "supersecretpswd" ];
    then

       PSQL_PW=$(cat  ${BIN}/accessProperties/postgres-passwd)
    fi
    echo ${BIN}/createDBFromCopy.sh ${PSQL_HOST} ${PSQL_PORT} ${PSQL_PW} ${DB_COPY_FILE}
    ${BIN}/createDBFromCopy.sh ${PSQL_HOST} ${PSQL_PORT} ${PSQL_PW} ${DB_COPY_FILE}
}


download() {
    command -v unzip >/dev/null 2>&1 || { echo >&2 " unzip not found! Please install (apt install unzip) and try again! aborting"; exit 1;}
    command -v tar >/dev/null 2>&1 || { echo >&2 " tar not found! Please install  and try again! aborting"; exit 1;}
    command -v wget  >/dev/null 2>&1 || { echo >&2 " wget not found! Please install (apt install wget) and try again! aborting"; exit 1;}

    #cd ${BIN}/db
    if [ ${REFSEQ_89} == TRUE ];
    then
	    echo "fetching refseq 89"
	    wget --no-check-certificate -O refseq89.zip https://zenodo.org/records/7010767/files/refseq89.zip?download=1

	    
	    mv ${BIN}/refseq89.zip ${BIN}/db/refseq89Dump.zip
	    echo "unzipping files"
	    unzip refseq89Dump.zip
    fi

    if [ ${REFSEQ_204} == TRUE ];
    then
	echo "fetching refseq204"
	wget --no-check-certificate -O refseq204.zip https://zenodo.org/records/7010767/files/refseq204.zip?download=D1
	mv ${BIN}/refseq204.zip ${BIN}/db/refseq204Dump.zip
	echo "unzipping files"
	unzip refseq204Dump.zip
    fi

    echo "fetching cassandra/janusgraph database"

    if [ ${USE_DOCKER} == TRUE ]
    then
	echo "Using docker"
	wget --no-check-certificate -O db.tar.xz https://zenodo.org/records/10199867/files/db.tar.xz?download=1
	wget --no-check-certificate -O cassandra.yaml https://zenodo.org/records/10199867/files/cassandra.yaml?download=1
    else
	echo "Not using docker"
	wget --no-check-certificate -O db.tar.xz https://zenodo.org/records/10174026/files/db.tar.xz?download=1
	wget --no-check-certificate -O cassandra.yaml https://zenodo.org/records/10174026/files/cassandra.yaml?download=1
    fi
    mv ${BIN}/cassandra.yaml ${BIN}/janusgraph/conf/cassandra/cassandra.yaml
    mv ${BIN}/db.tar.xz ${BIN}/db/db.tar.xz
    echo "unzipping files"
    cd ${BIN}/db
    
    tar -xf db.tar.xz
}

init_dbs() {
    set_psql_accession_data
    download
}

build() {
    cd ${PROGRAM_DIR}
    mvn clean package
}

exit_abnormal() {                              # Function: Exit with error.
  usage
  exit 1
}

exit_normal() {                              # Function: Exit without error.
  usage
  exit 0
}

usage() {
    echo "Usage: $0 [options] {create-db-from-copy}" >&2
    echo " --init:  Initialize accession properties and download postgresql and cassandra dump files" >&2
    echo " --psql-port:  Specify postgresql hostport (default 4242)" >&2
    echo " --psql-pw:  Specify postgresql password (if not specified randomly generates a password)" >&2
    echo " --refseq204:  Use Refseq 204 as reference database (at least one refseq must be specified)" >&2
    echo " --refseq89:  Use Refseq 89 as reference database (at least one refseq must be specified)" >&2
    echo " --build:  Build the jar files for execution" >&2
}

# By calling getopt with '--options' we activate the extended mode which can
# also handle spaces and other special characters in the parameters
# this will extract the parameters as list in "PARSED"
PARSED=$(getopt --options=${VALID_OPTS} --longoptions=${VALID_LONG_OPTS} --name "$0" -- "$@")


# Set the positional parameters. By using eval we achieve, that the quoted
# output of getopt is interpreted another time by the shell (see man getopt)
eval set -- "$PARSED"


while true; do
    case "$1" in
     --psql-port)
	 PSQL_PORT=$2
         echo "Setting psql port to ${PSQL_PORT} "
         shift 2
         ;;
     --psql-pw)
	 PSQL_PW=$2
         echo "Setting psql pw to ${PSQL_PW} "
         shift 2
         ;;
     --no-docker)
	 USE_DOCKER=$2
	 echo "do not use docker"
	 shift 1
	 ;;
     --refseq204)
         echo "Using refseq 204 as reference databse "
	 REFSEQ_204=TRUE
	 shift 1
         ;;
     --refseq89)
         echo "Using refseq 89 as reference databse "
	 REFSEQ_89=TRUE
	 shift 1
         ;;        
     -h|--help)
         # print help message and exit
         echo "Help"
	 #usage
         exit_normal
         ;;
     --init)
	 echo "Initializing accession properties and downloading postgresql and cassandra dump files " 
	 COMMAND=init_dbs
	 shift 1
	 ;;
     --build)
	 echo "Building jars" $2
	 COMMAND=build
	 shift 1
	 ;;
    --)
        shift 1
        break
        ;;
  	*)  echo "How could that happen? " $1
        exit_abnormal;;
  esac
done


if [ -n "$COMMAND" ]; then
    $COMMAND
else
    usage
    exit 1
fi
