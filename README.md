# DeGeCI 1.1
A tool for annotation of mitochondrial genomes

## Prerequisites
- Java 8 (jre)
- Docker or postgresql server
- For web Application: npm >= 9.8.1

## Initialization and downloads

- Graph creation, i.e., population of the database with all genomes of RefSeq89 or RefSeq204 (one release is sufficient).
  1. To download and unzip the postgres dump files and cassandra tables (for taxonomic filtering) from ZENODO
  If you want to use docker run (You can also only specify one of --refseq204 or --refseq89):
  **Execute** ./init.sh --init --refseq204 --refseq89
  If you do NOT want to use docker run:
  **Execute** ./init.sh --init --refseq204 --refseq89 --no-docker

  If you want to change the default setting for the postgres port (4242) and a certain password, use --psql-port <your port> --psql-pw <your password>. A random password will be generated if nothing is specified.
  Please *BE AWARE:* Depending on your downstream rate the download will take some time (Refseq89: 2.4 GB, Refseq204: 2.7GB)!


## Database population using docker

**Execute**: docker compose up postgresrefseq
This will create the postgres database in a docker container from the dump files.
**Results:**
Database MDBG with schemas *complete* with following tables:
- kmer_plus_16_&lt;refseqversion&gt;
- kmer_minus_16_&lt;refseqversion&gt;

## Alternatively: Database population using a postgres SQL server

**Execute:**
 ./init.sh --create-db-from-copy &lt;dumpfile&gt; --psql-host &lt;host&gt; --psql-port &lt;port&gt;  --psql-pw &lt;password&gt;
 Make sure to use the same postgres options as in the initialization process.

## Taxonomic filtering

Optionally, only a subset of the species in the database fullfilling a certain taxonomic constraint can be used for the annotation. By providing the scientific names (--taxonomic-groups &lt;group1&gt; &lt;group1&gt;) or the taxid (--taxids &lt;id1&gt; &lt;id2&gt;) of one or several taxonomic groups, all database species in the phylogenetic subtree below the lowest common ancestor of the specified taxonomic groups are determined.
By using --exclude-taxids these species will NOT be used for the annotation, otherwise ONLY these species will be used for the annotation.

## Annotation

Gene annotation for a supplied input genome.
This requires that the postgresql database was created and if taxonomic filtering shall be used that the cassandra db was dowloaded (see step above)!


**Build:**
To build the java jars run the following command (This just needs to be done once):
./init.sh --build

**Execute:**
- Make sure the postgres server is up and running, otherwise start it or launch the docker container with: docker compose up postgresrefseq
- To use taxonomic filtering, start the janusgraph/cassandra server for the taxonomic filtering by running:
  docker compose up janusgraph
  OR
  ./janusgraph/bin/control-cassandra.sh --start-all
- Start annotating:
./DeGeCI [options]
To see which other parameters can be specified run:
./DeGeCI --help

**Examples**
Two examples for basic application from fasta files . The fasta files are provided in the examples directory.
1. Annotate a genome stored in fasta file examples/NC_036405/NC_036405.1.fna using refSeq version 89 and specify the translation table for improved gene ends. Note that an incorrect trans-table might impair the result quality, so only specify it, if you know the correct translation table.
./DeGeCI --fasta-file ./examples/NC_036405/NC_036405.1.fna --result-directory ./examples/NC_036405 --name NC_036405 --refseq 89 --trans-table 2

2. Annotate a genome stored in fasta file examples/NC_022828/NC_022828.1.fna using refSeq version 89, translation table 1, and only use reference genomes of taxonomic group Cnidaria
./DeGeCI --fasta-file ./examples/NC_022828/NC_022828.1.fna --result-directory ./examples/NC_022828 --name NC_022828 --refseq 89 --taxonomic-groups Cnidaria --trans-table 1

One example for basic application from a sequence string.
1. Annoatate genome from sequence string ACTGACCTAG (of course your sequence is probably much larger than this ;)) using refSeq version 204
./DeGeCI --sequence ACTGACCTAG --result-directory ./examples/annotation --name mysequence --refseq 204

**Results:**
1. A bed File containing the gene annotation for the supplied input genome. The end positions in the bed file are exclusive.
2. A text file containing the gene order
3. An faa file containing the subsequences of the detected genes
5. A parameter setting file

## Web application

To use the Web Frontend. Once the application is started, it can be accessed in the browser at: localhost:3000

**Execute:**
./DeGeCI_web

## Bugs

Should you encounter any bugs, please report to lfiedler@informatik.uni-leipzig.de
