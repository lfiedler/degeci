#!/bin/bash

# Returns the absolute path of this script regardless of symlinks
abs_path() {
    # From: http://stackoverflow.com/a/246128
    #   - To resolve finding the directory after symlinks
    SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do
        DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
    done
    echo "$( cd -P "$( dirname "$SOURCE" )" && pwd )"
}

BIN=`abs_path`

DB_NAME_NEW=$1

#echo ${DB_NAME_NEW}

DB_NAME_CURRENT=$(grep "path.data" ${BIN}/elasticsearch.yml | sed -E -e 's#(path.data:\s*)(.*)(/es/data)$#\2#')
#echo ${DB_NAME_CURRENT}

if [[ ${DB_NAME_NEW} == ${DB_NAME_CURRENT} ]]
then
    echo "Database name already matches!"
elif [[ -z ${DB_NAME_NEW} ]]
then
    echo "Current: ${DB_NAME_CURRENT}"
else
    # replace path.data:
    sed -i -E -e 's#(path.data:\s*).*(/es/data)$#\1'$1'\2#' ${BIN}/elasticsearch.yml

    # replace path.logs:
    sed -i -E -e 's#(path.logs:\s*).*(/es/log)$#\1'$1'\2#' ${BIN}/elasticsearch.yml 

   echo "Set new database name to $1"
fi


