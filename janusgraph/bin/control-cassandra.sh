#!/bin/bash



CASSANDRA_NAME=org.apache.cassandra.service.CassandraDaemon
VERBOSE=
COMMAND=
DB_DIR=
CASSANDRA_STARTUP_TIMEOUT_S=120
CASSANDRA_SHUTDOWN_TIMEOUT_S=120
ELASTICSEARCH_STARTUP_TIMEOUT_S=60
ELASTICSEARCH_SHUTDOWN_TIMEOUT_S=60
ELASTICSEARCH_IP=127.0.0.1
ELASTICSEARCH_PORT=9200
SLEEP_INTERVAL_S=2
: ${GSRV_STARTUP_TIMEOUT_S:=60}
: ${GSRV_SHUTDOWN_TIMEOUT_S:=60}
: ${GSRV_IP:=127.0.0.1}
: ${GSRV_PORT:=8182}

VALID_OPTS=h;     VALID_LONG_OPTS=help;
VALID_OPTS+=,v;
# Options requiring an argument


# No argument options
VALID_LONG_OPTS+=,set-db-directory;
VALID_LONG_OPTS+=,set-es-db-directory;
VALID_LONG_OPTS+=,start-all;
VALID_LONG_OPTS+=,stop-all;
VALID_LONG_OPTS+=,start;
VALID_LONG_OPTS+=,start-server;
VALID_LONG_OPTS+=,stop-server;
VALID_LONG_OPTS+=,stop;
VALID_LONG_OPTS+=,start-es;
VALID_LONG_OPTS+=,stop-es;
VALID_LONG_OPTS+=,status;
VALID_LONG_OPTS+=,status-es;
VALID_LONG_OPTS+=,show-es-db-directory;
VALID_LONG_OPTS+=,show-db-directory;
VALID_LONG_OPTS+=,show-current-ports;

GREMLIN_FRIENDLY_NAME='Gremlin-Server'
GREMLIN_CLASS_NAME=org.apache.tinkerpop.gremlin.server.GremlinServer
ES_FRIENDLY_NAME=Elasticsearch
ES_CLASS_NAME=org.elasticsearch.bootstrap.Elasticsearch
CASSANDRA_FRIENDLY_NAME=Cassandra
CASSANDRA_CLASS_NAME=org.apache.cassandra.service.CassandraDaemon

# Locate the jps command.  Check $PATH, then check $JAVA_HOME/bin.
# This does not need to by cygpath'd.
JPS=
for maybejps in jps "${JAVA_HOME}/bin/jps"; do
    type "$maybejps" >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        JPS="$maybejps"
        break
    fi
done

if [ -z "$JPS" ]; then
    echo "jps command not found.  Put the JDK's jps binary on the command path." >&2
    exit 1
fi


# Returns the absolute path of this script regardless of symlinks
abs_path() {
    # From: http://stackoverflow.com/a/246128
    #   - To resolve finding the directory after symlinks
    SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do
        DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
    done
    echo "$( cd -P "$( dirname "$SOURCE" )" && pwd )"
}

BIN=`abs_path`

# set cassandra variables
. ${BIN}/set-cassandra-env-variables.sh
# echo "Cassandra home: " $CASSANDRA_HOME
 echo "Cassandra conf: " $CASSANDRA_CONF
# echo "Cassandra log: " $logdir
# echo "Cassandra run dir: " $runningdir
# echo "Pid log: " $pidlog
# echo "Port log: " $portlog
#echo "ES_HOME: ${ELASTICSEARCH_HOME}"


usage() {
    echo "Usage: $0 [options] {start|stop|status}" >&2
    echo " --start-es:  Start Elasticsearch" >&2
    echo " --stop-es:  Stop Elasticsearch" >&2
    echo " --start:  Fork Cassandra" >&2
    echo " --stop:   Kill running Cassandra" >&2
    echo " --set-db-directory <path to directory>:   Set absolute path of database directory" >&2
    echo " --show-es-db-directory :  Show current es db directory setting" >&2
    echo " --show-db-directory :  Show current db directory setting" >&2
    echo " --show-current-ports :  Show current port setting" >&2
    echo " --status: Print process status" >&2
    echo " --status-es: Print es process status" >&2
    echo " -v      Enable logging to console in addition to logfiles" >&2
}

exit_abnormal() {                              # Function: Exit with error.
  usage
  exit 1
}

exit_normal() {                              # Function: Exit without error.
  usage
  exit 0
}

find_verb() {
    if [ "$1" = 'start' -o \
	 "$1" = 'stop_all' -o \
	 "$1" = 'start_all' -o \
         "$1" = 'start_es' -o \
	 "$1" = 'start_server' -o \
	 "$1" = 'stop_server' -o \
         "$1" = 'stop' -o \
         "$1" = 'stop_es' -o \
	 "$1" = 'stop_server' -o \
         "$1" = 'set_db_directory' -o \
         "$1" = 'set_es_db_directory' -o \
         "$1" = 'show_db_directory' -o \
         "$1" = 'show_es_db_directory' -o \
         "$1" = 'status_es' -o \
         "$1" = 'status' ]; then
        COMMAND="$1"
        return 0
    fi
    return 1
}

wait_for_cassandra() {
    local now_s=`date '+%s'`
    local stop_s=$(( $now_s + $CASSANDRA_STARTUP_TIMEOUT_S ))
    local status_thrift=

    echo -n 'Running `nodetool statusthrift`'
    while [ $now_s -le $stop_s ]; do
        echo -n .
        # The \r\n deletion bit is necessary for Cygwin compatibility
        status_thrift="`$BIN/nodetool statusthrift #2>/dev/null | tr -d '\n\r'`"
        if [ $? -eq 0 -a 'running' = "$status_thrift" ]; then
            echo ' OK (returned exit status 0 and printed string "running").'
            return 0
        fi
        sleep $SLEEP_INTERVAL_S
        now_s=`date '+%s'`
    done

    echo " timeout exceeded ($CASSANDRA_STARTUP_TIMEOUT_S seconds)" >&2
    return 1
}

# wait_for_startup friendly_name host port timeout_s
wait_for_startup() {
    local friendly_name="$1"
    local host="$2"
    local port="$3"
    local timeout_s="$4"

    local now_s=`date '+%s'`
    local stop_s=$(( $now_s + $timeout_s ))
    local status=

    echo -n "Connecting to $friendly_name ($host:$port)"
    while [ $now_s -le $stop_s ]; do
        echo -n .
        $BIN/checksocket.sh $host $port >/dev/null 2>&1
        if [ $? -eq 0 ]; then
            echo " OK (connected to $host:$port)."
            return 0
        fi
        sleep $SLEEP_INTERVAL_S
        now_s=`date '+%s'`
    done

    echo " timeout exceeded ($timeout_s seconds): could not connect to $host:$port" >&2
    return 1
}

# wait_for_shutdown friendly_name class_name timeout_s
wait_for_shutdown() {
    local friendly_name="$1"
    local class_name="$2"
    local timeout_s="$3"

    local now_s=`date '+%s'`
    local stop_s=$(( $now_s + $timeout_s ))

    while [ $now_s -le $stop_s ]; do
        status_class "$friendly_name" $class_name >/dev/null
        if [ $? -eq 1 ]; then
            # Class not found in the jps output.  Assume that it stopped.
            return 0
        fi
        sleep $SLEEP_INTERVAL_S
        now_s=`date '+%s'`
    done

    echo "$friendly_name shutdown timeout exceeded ($timeout_s seconds)" >&2
    return 1
}

set_db_directory()  {
    echo "Setting db directory to $DB_DIR"
    "$CASSANDRA_CONF"/setDBdirectory.sh $DB_DIR
    return 0
}

set_es_db_directory()  {
    echo "Setting es db directory to $ES_DIR"
    "$ELASTICSEARCH_CONF"/setDBdirectory.sh $ES_DIR
    return 0
}

remove_running_logs() {
  rm -f ${pidlog}
  rm -f ${portlog}
}

kill_class() {
    local p=`$JPS -l | grep "$2" | awk '{print $1}'`
    if [ -z "$p" ]; then
        echo "$1 ($2) not found in the java process table"
        return
    fi
    echo "Killing $1 (pid $p)..." >&2
    case "`uname`" in
        CYGWIN*) taskkill /F /PID "$p" ;;
        *)       kill "$p" ;;
    esac
}


stop_server()  {
    kill_class        $GREMLIN_FRIENDLY_NAME $GREMLIN_CLASS_NAME
    wait_for_shutdown $GREMLIN_FRIENDLY_NAME $GREMLIN_CLASS_NAME $GSRV_SHUTDOWN_TIMEOUT_S
}


stop()  {
    kill_class        $CASSANDRA_FRIENDLY_NAME $CASSANDRA_CLASS_NAME
    wait_for_shutdown $CASSANDRA_FRIENDLY_NAME $CASSANDRA_CLASS_NAME $CASSANDRA_SHUTDOWN_TIMEOUT_S
}

stop_es() {
    kill_class        $ES_FRIENDLY_NAME $ES_CLASS_NAME
    wait_for_shutdown $ES_FRIENDLY_NAME $ES_CLASS_NAME $ELASTICSEARCH_SHUTDOWN_TIMEOUT_S
}

stop_all()  {
    stop
    stop_es
    stop_server
}



status_class() {
    local p=`$JPS -l | grep "$2" | awk '{print $1}'`

    if [ -n "$p" ]; then
        echo "$1 ($2) is running with pid $p"
        return 0
    else
        echo "$1 ($2) does not appear in the java process table"
        return 1
    fi
}


status_server() {
    status_class $GREMLIN_FRIENDLY_NAME $GREMLIN_CLASS_NAME
}

status() {
    status_class $CASSANDRA_FRIENDLY_NAME $CASSANDRA_CLASS_NAME
}

status_es() {
    status_class $ES_FRIENDLY_NAME $ES_CLASS_NAME
}

start_server() {

    status_server && exit 1
        echo "Forking Gremlin-Server..."
    if [ -n "$VERBOSE" ]; then
        "$BIN"/gremlin-server.sh conf/gremlin-server/gremlin-server-cql-es.yaml &
	#"$BIN"/gremlin-server.sh conf/gremlin-server/gremlin-server.yaml &
    else
        "$BIN"/gremlin-server.sh conf/gremlin-server/gremlin-server-cql-es.yaml >/dev/null 2>&1 &
	#"$BIN"/gremlin-server.sh conf/gremlin-server/gremlin-server.yaml >/dev/null 2>&1 &
    fi
    wait_for_startup 'Gremlin-Server' $GSRV_IP $GSRV_PORT $GSRV_STARTUP_TIMEOUT_S || {
        echo "See $BIN/../logs/gremlin-server.log for Gremlin-Server log output."  >&2
        return 1
    }
    
}

start() {
    status && exit 1
	
    
    echo "Forking Cassandra..."
    if [ -n "$VERBOSE" ]; then
        CASSANDRA_INCLUDE="$BIN"/cassandra.in.sh "$BIN"/cassandra || exit 1
    else
        CASSANDRA_INCLUDE="$BIN"/cassandra.in.sh "$BIN"/cassandra >/dev/null 2>&1 || exit 1
    fi

    echo "done forking"
    sleep 30

    echo "Waiting for nodetool"
    wait_for_cassandra || {
        echo "See $BIN/../log/cassandra.log for Cassandra log output."    >&2
        return 1
    }

}

start_es() {
  status_es && exit 1

  echo "Forking Elasticsearch..."
  if [ -n "$VERBOSE" ]; then
      "$ELASTICSEARCH_BIN"/elasticsearch -d
  else
      "$ELASTICSEARCH_BIN"/elasticsearch -d >/dev/null 2>&1
  fi
  wait_for_startup Elasticsearch $ELASTICSEARCH_IP $ELASTICSEARCH_PORT $ELASTICSEARCH_STARTUP_TIMEOUT_S || {
      echo "See $BIN/../log/elasticsearch.log for Elasticsearch log output."  >&2
      return 1
  }
}


start_all(){
    start
    start_es
    start_server
}


# By calling getopt with '--options' we activate the extended mode which can
# also handle spaces and other special characters in the parameters
# this will extract the parameters as list in "PARSED"
PARSED=$(getopt --options=${VALID_OPTS} --longoptions=${VALID_LONG_OPTS} --name "$0" -- "$@")



# Set the positional parameters. By using eval we achieve, that the quoted
# output of getopt is interpreted another time by the shell (see man getopt)
eval set -- "$PARSED"

while true; do
  case "$1" in
    -h|--help)
        # print help message and exit
        echo "Help"
        exit_normal
        ;;
    --start-es)
        echo "Attempting to start elasticsearch"
        find_verb "start_es"
        shift 1
        ;;
     --start-server)
        echo "Attempting to start gremlin server"
        find_verb "start_server"
        shift 1
  	    ;;
     --start)
        echo "Attempting to start cassandra"
        find_verb "start"
        shift 1
  	;;
     --start-all)
        echo "Attempting to start cassandra, elasticsearch and server"
        find_verb "start_all"
        shift 1
  	;;
      --stop-all)
        echo "Attempting to start cassandra, elasticsearch and server"
        find_verb "stop_all"
        shift 1
  	;;
     --stop-server)
        echo "Attempting to stop server"
        find_verb "stop_server"
        shift 1
        ;;
      --stop)
        echo "Attempting to stop cassandra"
        find_verb "stop"
        shift 1
  	    ;;
    --stop-es)
        echo "Attempting to stop elasticsearch"
        find_verb "stop_es"
        shift 1
        ;;
  	--stop)
        echo "Attempting to stop cassandra"
        find_verb "stop"
        shift 1
  	    ;;
    --status-es)
        echo "Fetching elasticsearch status"
        find_verb "status_es"
        shift 1
        ;;
     --status)
        echo "Fetching cassandra status"
        find_verb "status"
        shift 1
        ;;
    --set-es-db-directory)
        echo "Setting es directory " 
        find_verb "set_es_db_directory"
        ES_DIR="${BIN}/../../db/cassandraDB"
        shift 1
        ;;
    --set-db-directory)
        echo "Setting db directory " 
        find_verb "set_db_directory"
        DB_DIR="${BIN}/../../db/cassandraDB"
        shift 1
        ;;
    --show-es-db-directory)
        "$ELASTICSEARCH_CONF"/setDBdirectory.sh
        exit 0
        ;;
    --show-db-directory)
        "$CASSANDRA_CONF"/setDBdirectory.sh
        exit 0
        ;;
    --show-current-ports)
        "$CASSANDRA_CONF"/setPorts.sh $1
        exit 0
        ;;
    -v)
        VERBOSE=yes
        shift 1
        ;;
    --)
        shift 1
        break
        ;;
  	*)  echo "How could that happen? " $1
        exit_abnormal;;
  esac
done

# echo "Command: " $COMMAND
# echo "DB_DIR: " $DB_DIR

# while [ 1 ]; do
#     if find_verb ${!OPTIND}; then
#         OPTIND=$(($OPTIND + 1))
#     elif getopts 'v' option; then
#         case $option in
#         v) VERBOSE=yes;;
#         *) usage; exit 1;;
#         esac
#     else
#         break
#     fi
# done

if [ -n "$COMMAND" ]; then
    $COMMAND
else
    usage
    exit 1
fi
