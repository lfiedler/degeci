graph = JanusGraphFactory.open('conf/graphAccess/janusgraph-cql-mitos.properties')
g = graph.traversal()

tinkerGraph = TinkerGraph.open()
subG = tinkerGraph.traversal()
subG.addV("prefix").property("kmer","ACTG").as("v1").addV("prefix").property("kmer","CTGG").as("v2").addV("prefix").property("kmer","TGGA").as("v3").
addV("no_match").property("kmer","GGAC").as("v4").addV("no_match").property("kmer","GACT").as("v5").
addE("suffix").from("v1").to("v2").property("pos",[1,6]).
addE("suffix").from("v2").to("v3").property("pos",[2,7]).
addE("no_suffix").from("v3").to("v4").property("pos",[3]).
addE("no_suffix").from("v4").to("v5").property("pos",[4]).
addE("no_suffix").from("v5").to("v1").property("pos",[5]).iterate()

/*tinkerGraph = TinkerGraph.open()
subG = tinkerGraph.traversal()
subG.io("../graphFiles/NC_021974/dbgGraphs/matchSubgraph_NC_021974.xml").read().iterate()*/

/*tinkerGraph = TinkerGraph.open()
subG = tinkerGraph.traversal()
//subG.io("../graphFiles/NC_024698/dbgGraphs/matchSubgraph_NC_024698.xml").read().iterate()
subG.io("../graphFiles/NC_024698/dbgGraphs/extendedMatchSubgraph_NC_024698.xml").read().iterate()*/


/*tinkerGraph = TinkerGraph.open()
subG = tinkerGraph.traversal()
subG.io("../graphFiles/NC_026039/dbgGraphs/extendedMatchSubgraph_NC_026039.xml").read().iterate()
subG.io("../graphFiles/NC_038220/dbgGraphs/extendedMatchSubgraph_NC_038220.xml").read().iterate()*/