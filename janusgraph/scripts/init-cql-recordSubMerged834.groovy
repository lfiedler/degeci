
tinkerGraph = TinkerGraph.open()
g = tinkerGraph.traversal()
g.io("../recordData/834/PLUS/merged.xml").read().iterate()


[repOrigin:[false],rrna:[false,true],protein:[cox2,cox3,cox1,nad4,lagli,nad3,nad4l,nad2,nad1,atp6,atp8,nad6,cob,nad5],trna:[A,C,D,L1,E,L2,F,G,H,I,K,M,P,R,T,V,W,S1,S2]]
