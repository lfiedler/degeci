
conf = new BaseConfiguration()
conf.setProperty("gremlin.tinkergraph.defaultVertexPropertyCardinality","list")
tinkerGraph = TinkerGraph.open(conf)
subG = tinkerGraph.traversal()
subG.io("../graphFiles/NC_023118/propertyCluster/plusClusterTree.gryo").with(IO.reader,IO.gryo).
                with(IO.registry, JanusGraphIoRegistry.class.getName()).
                read().iterate()
