
conf = new BaseConfiguration()

tinkerGraph = TinkerGraph.open(conf)
subG = tinkerGraph.traversal()
subG.io("../graphFiles/toplolgyEdgesGt4.kryo").with(IO.reader,IO.gryo).
                with(IO.registry, JanusGraphIoRegistry.class.getName()).
                read().iterate()
