// Copyright 2019 JanusGraph Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// an init script that returns a Map allows explicit setting of global bindings.
def globals = [:]

// defines a sample LifeCycleHook that prints some output to the Gremlin Server console.
// note that the name of the key in the "global" map is unimportant.
globals << [hook : [
        onStartUp: { ctx ->
            ctx.logger.info("Executed once at startup of Gremlin Server.")
        },
        onShutDown: { ctx ->
            ctx.logger.info("Executed once at shutdown of Gremlin Server.")
        }
] as LifeCycleHook]

// define the default TraversalSource to bind queries to - this one will be named "g".
globals << [gTax : graphTaxonomy.traversal(), gTest : graphTest.traversal(),
 gPlusOne : graphPlusOne.traversal(), gMinusOne : graphMinusOne.traversal(), 
 gPlusTwo : graphPlusTwo.traversal(), gMinusTwo : graphMinusTwo.traversal(), 
 gPlusThree : graphPlusThree.traversal(), gMinusThree : graphMinusThree.traversal(), 
 gPlusFour : graphPlusFour.traversal(), gMinusFour : graphMinusFour.traversal(), 
 gPlusFive : graphPlusFive.traversal(), gMinusFive : graphMinusFive.traversal(), 
 gPlusSix : graphPlusSix.traversal(), gMinusSix : graphMinusSix.traversal(), 
 gPlusSeven : graphPlusSeven.traversal(), gMinusSeven : graphMinusSeven.traversal(), 
 gPlusEight : graphPlusEight.traversal(), gMinusEight : graphMinusEight.traversal(), 
 gPlusNine : graphPlusNine.traversal(), gMinusNine : graphMinusNine.traversal(), 
 gPlusTen : graphPlusTen.traversal(), gMinusTen : graphMinusTen.traversal(), 
 gPlusEleven : graphPlusEleven.traversal(), gMinusEleven : graphMinusEleven.traversal(), 
 gPlusTwelve : graphPlusTwelve.traversal(), gMinusTwelve : graphMinusTwelve.traversal(), 
 gPlusThirteen : graphPlusThirteen.traversal(), gMinusThirteen : graphMinusThirteen.traversal(), 
 gPlusFourteen : graphPlusFourteen.traversal(), gMinusFourteen : graphMinusFourteen.traversal(), 
 gPlusFifteen : graphPlusFifteen.traversal(), gMinusFifteen : graphMinusFifteen.traversal(), 
 gPlusSixteen : graphPlusSixteen.traversal(), gMinusSixteen : graphMinusSixteen.traversal(), 
 gPlusSeventeen : graphPlusSeventeen.traversal(), gMinusSeventeen  : graphMinusSeventeen.traversal(), 
 gPlusEighteen : graphPlusEighteen.traversal(), gMinusEighteen : graphMinusEighteen.traversal(), 
 gPlusNineteen : graphPlusNineteen.traversal(), gMinusNineteen : graphMinusNineteen.traversal(), 
 gPlusTwenty : graphPlusTwenty.traversal(), gMinusTwenty : graphMinusTwenty.traversal(), 
 gPlusTwentyone : graphPlusTwentyone.traversal(), gMinusTwentyone : graphMinusTwentyone.traversal()
 ]
