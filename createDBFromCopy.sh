#!/bin/bash
 
HOST=$1
PORT=$2
PW=$3
DB=mdbg
CREATION_FILE=$4

psql --dbname=postgresql://postgres:${PW}@${HOST}:${PORT}/postgres -c "CREATE DATABASE \"${DB}\"
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'en_US.UTF-8'
       LC_CTYPE = 'en_US.UTF-8'
       CONNECTION LIMIT = -1
       TEMPLATE template0"
psql --dbname=postgresql://postgres:${PW}@${HOST}:${PORT}/${DB} -c "CREATE SCHEMA complete AUTHORIZATION postgres";

psql --dbname=postgresql://postgres:${PW}@${HOST}:${PORT}/${DB} -f ${CREATION_FILE}

